#include "stdafx.h"

/****************************************************************************************************/

void ANIMACJA::Rysuj( POS Pozycja, bool Wyswietl )
{
	StdBuf.WstawZnak( Znak, Kolor, Pozycja, false );
	if( Wyswietl ) {
		StdBuf.Rysuj();
		Sleep( Czas * 2 );
	}
}

/****************************************************************************************************/

bool WLASCIWOSC::Uzyj( POSTAC& postac )
{
	if( CzasTrwania == 0 ) return false;

	if( CzasTrwania % 10 != 0 ) { CzasTrwania--; return true; }

	if( CzasRozpoczecia > 0 ) CzasRozpoczecia--;
	else {
		switch( Typ )
		{
			case WLA_OBRAZENIA_OD_MAGII: postac.BronSie( OBRAZENIA(OBR_MAGIA, Wartosc) ); break;
			case WLA_OBRAZENIA_OD_POCISKU: postac.BronSie( OBRAZENIA(OBR_POCISKI, Wartosc) ); break;
			case WLA_OBRAZENIA_OD_BRONI: postac.BronSie( OBRAZENIA(OBR_BRON, Wartosc) ); break;
			case WLA_DO_TURY: if( postac.Typ != TPO_BOHATER ) dynamic_cast<POSTAC_NIEZALEZNA&>(postac).Tura += Wartosc * 5; break;
			case WLA_DO_ZYCIA: postac.DoZycia( Wartosc ); break;
			case WLA_DO_ENERGII: postac.DoEnergii( Wartosc ); break;
			case WLA_DO_MAX_ZYCIA: if( postac.Typ == TPO_BOHATER ) dynamic_cast<BOHATER&>(postac).MaxZycie += Wartosc; break;
			case WLA_DO_MAX_ENERGII: if( postac.Typ == TPO_BOHATER ) dynamic_cast<BOHATER&>(postac).MaxEnergia += Wartosc; break;
			case WLA_OD_GLODU: postac.DoGlodu( -Wartosc ); break;
			case WLA_OD_PRAGNIENIA: postac.DoPragnienia( -Wartosc ); break;
		}

		CzasTrwania--;
	}
	return true;
}

/****************************************************************************************************/

bool UROK::Uzyj( POSTAC& postac )
{
	for( int i = 0; i < Wlasciwosci.size(); i++ )
		if( !Wlasciwosci[i].Uzyj(postac) ) {
			Wlasciwosci.erase(i);
			i--;
		}

	if( Wlasciwosci.size() == 0 ) return false;

	if( StanGry->Sekunda % 10 == 0 && StanGry->WidocznePola[ postac.Pozycja.X ][ postac.Pozycja.Y ] )
		Animacja.Rysuj( postac.Pozycja( 1, 2 ) + SRODEK_RYSOWANIA - StanGry->Postac.Pozycja );

	return true;
}

/****************************************************************************************************/

bool WLASCIWOSC::Zaladuj( const RLGL::ELEMENT& data )
{
	if( data.GetName() == "od_magii" )           Typ = WLA_OBRAZENIA_OD_MAGII;
	else if( data.GetName() == "od_pocisku" )    Typ = WLA_OBRAZENIA_OD_POCISKU;
	else if( data.GetName() == "od_broni" )      Typ = WLA_OBRAZENIA_OD_BRONI;
	else if( data.GetName() == "do_tury" )       Typ = WLA_DO_TURY;
	else if( data.GetName() == "do_zycia" )      Typ = WLA_DO_ZYCIA;
	else if( data.GetName() == "do_energii" )    Typ = WLA_DO_ENERGII;
	else if( data.GetName() == "do_max_zycia" )  Typ = WLA_DO_MAX_ZYCIA;
	else if( data.GetName() == "do_max_energii" )Typ = WLA_DO_MAX_ENERGII;
	else if( data.GetName() == "od_glodu" )      Typ = WLA_OD_GLODU;
	else if( data.GetName() == "od_pragnienia" ) Typ = WLA_OD_PRAGNIENIA;
	else return false;

	Wartosc = data[0];
	CzasTrwania = AS_INT( data.Size() > 1 ? data[1] : 1 ) * 10;
	CzasRozpoczecia = AS_INT( data.Size() > 2 ? data[2] : 0 ) * 10;

	return true;
}

/****************************************************************************************************/

bool UROK::Zaladuj( const RLGL::ELEMENT& data )
{
	Animacja.Znak = data("ANIMATION")[0];
	Animacja.Kolor = data("ANIMATION")[1];
	Animacja.Czas = data("ANIMATION")[2];

	for( unsigned i = 0; i < data("EFFECTS").Size(); i++ ) {
		Wlasciwosci.add( WLASCIWOSC() );
		Wlasciwosci.back().Zaladuj( data("EFFECTS")(i) );
	}

	return true;
}

/****************************************************************************************************/

void WLASCIWOSC::Zapisz( PLIK& Plik )
{
	BYTE typ = Typ;
	Plik << typ
		 << Wartosc
		 << CzasRozpoczecia
		 << CzasTrwania;
}

/****************************************************************************************************/

void WLASCIWOSC::Wczytaj( PLIK& Plik )
{
	BYTE typ;
	Plik >> typ
		 >> Wartosc
		 >> CzasRozpoczecia
		 >> CzasTrwania;
	Typ = (TYP_WLASCIWOSCI) typ;
}

/****************************************************************************************************/

void UROK::Zapisz( PLIK& Plik )
{
	Plik << Wlasciwosci.size();
	for(WORD i = 0; i < Wlasciwosci.size(); i++) Wlasciwosci[i].Zapisz(Plik);
	Plik << Animacja.Znak
		 << Animacja.Kolor
		 << Animacja.Czas;
}

/****************************************************************************************************/

void UROK::Wczytaj( PLIK& Plik )
{
	unsigned ilosc;
	Plik >> ilosc;
	Wlasciwosci.resize(ilosc);
	for(WORD i = 0; i < Wlasciwosci.size(); i++) Wlasciwosci[i].Wczytaj(Plik);
	Plik >> Animacja.Znak
		 >> Animacja.Kolor
		 >> Animacja.Czas;
}

/****************************************************************************************************/