#pragma once

#include "stdafx.h"

/****************************************************************************************************/

enum TYP_KAFELKA { TK_GRUNT, TK_SCIANA, TK_BLOKADA };

/****************************************************************************************************/

struct KAFELEK : OBJEKT
{
	string      Nazwa;
	TYP_KAFELKA Typ;
	bool        Zakrwawialny;

	KAFELEK() {}
	KAFELEK( string _ID, string _Nazwa, char _Znak, WORD _Kolor, TYP_KAFELKA _Typ, bool _Zakrwawialny )
	{
		ID = _ID;
		Nazwa = _Nazwa;
		Znak = _Znak;
		Kolor = _Kolor;
		Typ = _Typ;
		Zakrwawialny = _Zakrwawialny;
	}
};

/****************************************************************************************************/

struct PRZEDMIOT_NA_MAPIE
{
	WORD NR;
	POS  Pozycja;
	WORD Ilosc;
};

/****************************************************************************************************/

struct PRZEJSCIE
{
	POS  Pozycja;
	WORD Mapa;
	POS  NowaPozycja;
	bool Automatyczne;
};

/****************************************************************************************************/

struct MAPA_GENEROWANA
{
	virtual bool WstawKafelek( BYTE kafelek, POS pozycja ) = 0;
	virtual bool DodajPrzedmiot( WORD przedmiot, WORD ilosc, POS pozycja ) = 0;
	virtual bool DodajPostac( WORD postac, POS pozycja ) = 0;
	virtual bool DodajOsobe( WORD osoba, POS pozycja ) = 0;
	
	virtual POS LosujPozycje( POS pozycja_obszaru, POS rozmiar_obszaru ) = 0;
	virtual POS PozycjaObok( POS pozycja, POS pozycja_obszaru, POS rozmiar_obszaru ) = 0;
};

/****************************************************************************************************/

struct MAPA : MAPA_GENEROWANA
{
	string                     ID;
	string                     Nazwa;
	bool                       SwiatloSloneczne;
	POS                        Rozmiar;
	BYTE**                     Kafelki;
	BYTE**                     Odkryte;

	// mapy pomocnicze, tymczasowe
	static short przedmioty[SzerokoscMapy][WysokoscMapy];
	static short postacie[SzerokoscMapy][WysokoscMapy];
	void StworzMapyPomocnicze();

	vector<PRZEDMIOT_NA_MAPIE> Przedmioty;
	vector<POSTAC_NIEZALEZNA*> Postacie;
	vector<PRZEJSCIE>          Przejscia;

	MAPA() : Kafelki(0), Odkryte(0) {}
	~MAPA()
	{
		if( Kafelki ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Kafelki[i];
		if( Odkryte ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Odkryte[i];
		for( unsigned i = 0; i < Postacie.size(); i++ ) delete Postacie[i];
	}

	void Rysuj( WORD Kolor = xcNie, bool TakzeNiewidoczne = false, BYTE kolor_ciemnosci = xcNie );

	void Odkryj( POS Pozycja );

	void UstawRozmiar( POS rozmiar );

	////
	bool WstawKafelek( BYTE kafelek, POS pozycja );
	bool DodajPrzedmiot( WORD przedmiot, WORD ilosc, POS pozycja );
	bool DodajPostac( WORD postac, POS pozycja );
	bool DodajOsobe( WORD osoba, POS pozycja );

	POS LosujPozycje( POS pozycja_obszaru, POS rozmiar_obszaru );
	POS PozycjaObok( POS pozycja, POS pozycja_obszaru = POS(0, 0), POS rozmiar_obszaru = POS(0, 0) );
	////

	bool WstawPrzejscie( POS pozycja, WORD mapa, POS nowa_pozycja, bool automatyczne = false );

	bool UsunPrzejscie( POS pozycja );
	bool UsunPostac( WORD postac );
	bool UsunPrzedmiot( WORD przedmiot, WORD ilosc );

	bool PozycjaWolna( POS pozycja );
	bool PozycjaWolnaDlaPostaci( POS pozycja );

	POS PozycjaDalejOd( POS pozycja, POS pozycja_biezaca ); // podaje pozycj�, kt�ra jest dalej od 'pozycja' ni� 'pozycja_biezaca'
		// przydatna np je�li jaka� posta� ucieka przed inn� postaci�
	short SzukajPrzejscia( POS pozycja );
	short SzukajNajblizszegoWroga( POS Pozycja ); // 'Pozycja' - pozycja dla kt�rej szuka� najbli�szego wroga
	short SzukajPostaci( POS Pozycja );
	short SzukajPrzedmiotu( POS Pozycja, WORD KtoryZKolei = 1 );
	short SzukajPrzedmiotuOPodanymNumerze( WORD NR, POS Pozycja );
	POS SzukajKafelka( BYTE index_kafelka, unsigned kolejnosc = 0 );
	POS PozycjaPrzedmiotu( WORD index_przedmiotu );

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

void RysujListeWidocznychObiektow( POS pozycja, BYTE max_obiektow = 18 );

void DodajKafelek( string ID, string Nazwa, char Znak, WORD Kolor, TYP_KAFELKA Typ, bool Zakrwawialny );

void AlokacjaKafelkow();

void StworzPunkty( array< POS >& Punkty );

short IndexKafelka( string ID );

/****************************************************************************************************/

extern array< KAFELEK > Kafelki;

/****************************************************************************************************/