#include "stdafx.h"

/****************************************************************************************************/

STAN_GRY* StanGry;

/****************************************************************************************************/

void STAN_GRY::UstawRozmiar( POS rozmiar )
{
	if(Rozmiar == rozmiar) return;

	if(WidocznePola) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] WidocznePola[i];
	if(Swiatlo) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Swiatlo[i];

	Rozmiar = rozmiar;
	WidocznePola = new BYTE*[Rozmiar.X];
	for(unsigned i = 0; i < Rozmiar.X; i++) WidocznePola[i] = new BYTE[Rozmiar.Y];
	Swiatlo = new BYTE*[Rozmiar.X];
	for(unsigned i = 0; i < Rozmiar.X; i++) Swiatlo[i] = new BYTE[Rozmiar.Y];
}

/****************************************************************************************************/

void STAN_GRY::RysujPanel()
{
	StdBuf.Czysc( POS( 0, 37 ), 240 );

	if( !StanGry->Postac.Zyje )
		StdBuf >> POS( 18, 0 ) >> xCze << "Twoja posta� nie �yje. Wci�nij " >> xZul << "[Crtl] + [Q]" >> xCze << " aby zako�czy� gr�.";
	else if( Postac.Punkty > 0 ) StdBuf >> POS( 49, 0 ) >> xBia << "Masz " << Postac.Punkty << " pkt. do przydzielenia.";

	StdBuf >> POS( 0, 38 ) >> Postac.Kolor << Postac.Nazwa;

	StdBuf >> xZul >> POS( 15, 38 ) << Postac.Poziom;
	StdBuf >> xBia << " " << Postac.Doswiadczenie << "/" << Doswiadczenie(Postac.Poziom) << "(" <<
		( Postac.Doswiadczenie - Doswiadczenie(Postac.Poziom - 1) ) * 100 / Doswiadczenie(Postac.Poziom, false) << "%)";

	StdBuf >> POS( 15, 39 ) >> xCze <<  AktualnaMapa->Nazwa.substr( 0, 34 );
	StdBuf >> POS( 0, 39 ) >> xSza << "Czas: " << Godziny()
		<< (Minuty() < 10 ? ":0" : ":") << Minuty()
		>> xcSza << (Sekundy() < 10 ? ":0" : ":") << Sekundy();

	Postac.Ekwipunek.RusujCoMaWRecach();

	char IloscKresekZycia = ceil( Postac.Zycie * 36 / Postac.MaxZycie );
	for( char i = 0; i < 35; i++ )
	{
		if( IloscKresekZycia <= i ) StdBuf.WstawZnak(179, xBia, POS( 0, 36 - i ), false);
		else StdBuf.WstawZnak(178, xCze, POS( 0, 36 - i ) );
	}
	char IloscKresekEnergii = ceil( Postac.Energia * 36 / Postac.MaxEnergia );
	for( char i = 0; i < 35; i++ )
	{
		if( IloscKresekEnergii <= i ) StdBuf.WstawZnak(179, xBia, POS( 79, 36 - i ), false);
		else StdBuf.WstawZnak(178, xNie, POS( 79, 36 - i ));
	}
	StdBuf.UstawPozycjePisania( POS( 0, 37 ) );

	if( !Postac.Zyje ) StdBuf >> xCze << "NIE �YJE";
	else
	{
		if( Postac.Niewidoczny )
			StdBuf >> xcZie << "niewidoczny ";
		if( Postac.Pragnienie >= 50 )
			StdBuf >> ( Postac.Pragnienie >= 90 ? xCze : ( Postac.Pragnienie >= 70 ? xZul : xBia ) ) << "spragniony ";
		if( Postac.Glod >= 50 )
			StdBuf >> ( Postac.Glod >= 90 ? xCze : ( Postac.Glod >= 70 ? xZul : xBia ) ) << "g�odny ";
		if( Postac.Energia <= Postac.MaxEnergia * 0.3 )
			StdBuf >> ( Postac.Energia <= Postac.MaxEnergia * 0.1 ? xCze : ( Postac.Energia <= Postac.MaxEnergia * 0.2 ? xZul : xBia ) ) << "zm�czony ";
		if( Postac.Zycie <= Postac.MaxZycie * 0.3 )
			StdBuf >> ( Postac.Zycie <= Postac.MaxZycie * 0.1 ? xCze : ( Postac.Zycie <= Postac.MaxZycie * 0.2 ? xZul : xBia ) ) << "ranny";
	}
}

/****************************************************************************************************/

void STAN_GRY::PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne, POS pozycja1, POS pozycja2 )
{
	Mapy[ mapa1 ].WstawPrzejscie( pozycja1, mapa2, pozycja2, automatyczne );
	Mapy[ mapa2 ].WstawPrzejscie( pozycja2, mapa1, pozycja1, automatyczne );
	Mapy[ mapa1 ].Kafelki[ pozycja1.X ][ pozycja1.Y ] = kafelek_przejscia1;
	Mapy[ mapa2 ].Kafelki[ pozycja2.X ][ pozycja2.Y ] = kafelek_przejscia2;
}

/****************************************************************************************************/

void STAN_GRY::PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne, POS pozycja1 )
{
	POS pos2 = Mapy[ mapa2 ].LosujPozycje( POS(1, 1), Mapy[ mapa2 ].Rozmiar(-2, -2) );
	PolaczMapy( mapa1, mapa2, kafelek_przejscia1, kafelek_przejscia2, automatyczne, pozycja1, pos2 );
}

/****************************************************************************************************/

void STAN_GRY::PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne )
{
	POS pos1 = Mapy[ mapa1 ].LosujPozycje( POS(1, 1), Mapy[ mapa1 ].Rozmiar(-2, -2) );
	POS pos2 = Mapy[ mapa2 ].LosujPozycje( POS(1, 1), Mapy[ mapa2 ].Rozmiar(-2, -2) );
	PolaczMapy( mapa1, mapa2, kafelek_przejscia1, kafelek_przejscia2, automatyczne, pos1, pos2 );
}

/****************************************************************************************************/

void STAN_GRY::Zapisz( string NazwaPliku )
{
	PLIK sg( NazwaPliku );
	sg.Otworz( ZAPIS );

	sg << Mapy.size();
	for( WORD i = 0; i < Mapy.size(); i++ )
		Mapy[ i ].Zapisz( sg );

	Postac.Zapisz( sg );

	sg << Poziom
	   << Sekunda
	   << Tura
	   << StanPostaci
	   << Highscore;

	sg.Zamknij();

	RLGL::FILE stan( NazwaPliku );

	stan.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING );
	string dane = stan.ReadAllToString();
	stan.Close();

	char* bufor_docelowy = new char[ (unsigned)( dane.size() * 1.1 + 12 ) ];
	unsigned long dlugosc_po_zpakowaniu;
	compress2( (Bytef*) bufor_docelowy, &dlugosc_po_zpakowaniu, (const Bytef*) dane.c_str(), dane.size(), Z_BEST_COMPRESSION );

	stan.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	stan.Write( bufor_docelowy, dlugosc_po_zpakowaniu );
	stan.Close();

	delete[] bufor_docelowy;

	sg.Szyfruj();
}

/****************************************************************************************************/

void STAN_GRY::Wczytaj( string NazwaPliku )
{
	PLIK sg( NazwaPliku );

	sg.Szyfruj();

	RLGL::FILE stan( NazwaPliku );

	stan.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING );
	string dane = stan.ReadAllToString();
	stan.Close();

	char* bufor_docelowy = new char[ 5242880 ];
	unsigned long dlugosc_po_rozpakowaniu;
	uncompress( (Bytef*) bufor_docelowy, &dlugosc_po_rozpakowaniu, (const Bytef*) dane.c_str(), dane.size() );
	bufor_docelowy[ dlugosc_po_rozpakowaniu ] = 0;
	dane = bufor_docelowy;

	stan.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	stan.Write( bufor_docelowy, dlugosc_po_rozpakowaniu );
	stan.Close();

	delete[] bufor_docelowy;

	if( !sg.Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) return;

	unsigned ilosc;
	sg >> ilosc;
	Mapy.resize(ilosc);
	for( WORD i = 0; i < Mapy.size(); i++ )
		Mapy[ i ].Wczytaj( sg );

	Postac.Wczytaj( sg );

	sg >> Poziom
	   >> Sekunda
	   >> Tura
	   >> StanPostaci
	   >> Highscore;

	sg.Zamknij();

	RLGL::FILE stan2( NazwaPliku );

	stan2.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING );
	dane = stan2.ReadAllToString();
	stan2.Close();

	char* bufor_docelowy2 = new char[ (unsigned)( dane.size() * 1.1 + 12 ) ];
	unsigned long dlugosc_po_zpakowaniu;
	compress2( (Bytef*) bufor_docelowy2, &dlugosc_po_zpakowaniu, (const Bytef*) dane.c_str(), dane.size(), Z_BEST_COMPRESSION );

	stan2.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	stan2.Write( bufor_docelowy2, dlugosc_po_zpakowaniu );
	stan2.Close();

	delete[] bufor_docelowy2;

	sg.Szyfruj();
}

/****************************************************************************************************/

void HIGHSCORE::REKORD::Wczytaj( PLIK& Plik )
{
	Plik >> ImiePostaci
		 >> Poziom
		 >> Doswiadczenie
		 >> NazwaMapy
		 >> Klasa
		 >> StanPostaci
		 >> IloscTur;
}

/****************************************************************************************************/

void HIGHSCORE::REKORD::Zapisz( PLIK& Plik )
{
	Plik << ImiePostaci
		 << Poziom
		 << Doswiadczenie
		 << NazwaMapy
		 << Klasa
		 << StanPostaci
		 << IloscTur;
}

/****************************************************************************************************/

bool HIGHSCORE::DodajRekord( REKORD Rekord )
{
	for( UCHAR i = 0; i < m_Rekordy.size(); i++ )
	{
		if( m_Rekordy[i].Punkty() <= Rekord.Punkty() )
		{
			m_Rekordy.insert( m_Rekordy.begin() + i, Rekord );
			if( m_Rekordy.size() > 10 ) m_Rekordy.resize( 10 );

			return true;
		}
	}

	if( m_Rekordy.size() < 10 )
	{
		m_Rekordy.add( Rekord );

		return true;
	}

	return false;
}

/****************************************************************************************************/

void HIGHSCORE::Zapisz( string NazwaPliku )
{
	PLIK hs( NazwaPliku );
	hs.Otworz( ZAPIS );
	UCHAR ilosc_rekordow = m_Rekordy.size();
	hs << ilosc_rekordow;
	for( UCHAR i = 0; i < ilosc_rekordow; i++ )
	{
		m_Rekordy[ i ].Zapisz( hs );
	}
	hs.Zamknij();
	hs.Szyfruj();
}

/****************************************************************************************************/

void HIGHSCORE::Wczytaj( string NazwaPliku )
{
	PLIK hs( NazwaPliku );
	hs.Szyfruj();
	if( !hs.Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) return;
	UCHAR ilosc_rekordow = 0;
	hs >> ilosc_rekordow;
	m_Rekordy.resize( ilosc_rekordow );
	for( UCHAR i = 0; i < ilosc_rekordow; i++ )
	{
		m_Rekordy[ i ].Wczytaj( hs );
	}
	hs.Zamknij();
	hs.Szyfruj();
}

/****************************************************************************************************/

void HIGHSCORE::Rysuj()
{
	StdBuf.Czysc();
	
	StdBuf >> POS( 0, 0 ) >> xcZul << "--------------------------------------------------------------------------------";
	StdBuf >> POS( 1, 1 ) >> xZul << "Highscore";
	StdBuf >> POS( 0, 2 ) >> xcZul << "--------------------------------------------------------------------------------";

	for( UCHAR i = 0; i < m_Rekordy.size(); i++ )
	{
		StdBuf >> POS( 2, 4 + i * 3 ) >> xZul << i + 1 << "."
			>> POS( 6, 4 + i * 3 ) >> xZie << m_Rekordy[i].Punkty() << "pkt."
			>> POS( 17, 4 + i * 3 ) >> xTur << m_Rekordy[i].ImiePostaci
			 >> xBia << " - " << m_Rekordy[ i ].Klasa << " na poziomie " << m_Rekordy[ i ].Poziom << " (" << m_Rekordy[i].Doswiadczenie << " exp).";
		if( m_Rekordy[i].StanPostaci == SP_ZWYCIEZYL )
			StdBuf >> POS( 19, 5 + i * 3 ) << "Pozostaj�c niezwyci�ony zako�czy� przygod� w " << m_Rekordy[i].IloscTur << " turze.";
		else if( m_Rekordy[i].StanPostaci == SP_PODDAL_SIE )
			StdBuf >> POS( 19, 5 + i * 3 ) << "Podda� si�: " << m_Rekordy[i].NazwaMapy << ", " << m_Rekordy[i].IloscTur << " tura.";
		else
		StdBuf >> POS( 19, 5 + i * 3 ) << "Zgin��: " << m_Rekordy[i].NazwaMapy << ", " << m_Rekordy[i].IloscTur << " tura.";
	}

	StdBuf >> xZul >> POS( 0, 37 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec";
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	
	StdBuf.Rysuj();
}

/****************************************************************************************************/

void HIGHSCORE::Pokaz()
{
	Rysuj();
	for( ; ; )
	{
		switch( PobierzKlawisz() )
		{
		case 27: case 32: return;
		
		default: continue;
		}
	}
}

/****************************************************************************************************/

int IndexMapy( string ID )
{
	for( BYTE i = 0; i < StanGry->Mapy.size(); i++ )
		if( StanGry->Mapy[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/