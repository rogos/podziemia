#pragma once

#ifndef _DEFINICJE_H_
#define _DEFINICJE_H_

#define xCza (0)
#define xcNie (1)
#define xcZie (2)
#define xcTur (3)
#define xcCze (4)
#define xcFio (5)
#define xcZul (6)
#define xSza (7)
#define xcSza (8)
#define xNie (9)
#define xZie (10)
#define xTur (11)
#define xCze (12)
#define xFio (13)
#define xZul (14)
#define xBia (15)

#define tCza (16*0)
#define tcNie (16*1)
#define tcZie (16*2)
#define tcTur (16*3)
#define tcCze (16*4)
#define tcFio (16*5)
#define tcZul (16*6)
#define tSza (16*7)
#define tcSza (16*8)
#define tNie (16*9)
#define tZie (16*10)
#define tTur (16*11)
#define tCze (16*12)
#define tFio (16*13)
#define tZul (16*14)
#define tBia (16*15)

typedef enum TYP_PANCERZA { Helm, Zbroja, Szata };
typedef enum TYP_BRONI_BIALEJ { Jednoreczny, Dworeczny };
typedef enum TYP_BRONI_DYSTANSOWEJ { Luk, Kusza };
typedef enum PLEC { Mezczyzna, Kobieta };
typedef enum KLASA { Wojownik, Zabojca, Lucznik, Mysliwy, Mag, Kaplan, Alchemik, Kowal };

#define DOPOKI_FALSE(FUNKCJA) for(;;){if(FUNKCJA)break;}

const UCHAR IloscUmiejetnosci = 15;
const string NazwyUmiejetnosci[IloscUmiejetnosci] = {
	"Walka broni� jednoreczn�",
	"Walka broni� dworeczn�",
	"Strzelanie z �uku",
	"Strzelanie z kuszy",
	"Walka dwiema broniami",
	"U�ywanie tarczy",
	"Walka wrecz",
	"Skradanie",
	"Alfabet mag�w ognia",
	"Alfabet mag�w wody",
	"Tworzenie zwoj�w",
	"Tworzenie mikstur",
	"Tworzenie broni",
	"Tworzenie pancerza",
	"Zbieranie trofe�w"
};

#endif //_DEFINICJE_H_