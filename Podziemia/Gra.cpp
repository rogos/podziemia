#include "stdafx.h"
//#include "Dzwieki.h"
#include "../RLGL/RLGLmovies.h"

/****************************************************************************************************/

MAPA* AktualnaMapa;
HIGHSCORE Highscore;
RLGL::STRUCTURE DaneGry;

/****************************************************************************************************/

bool GRA::WczytajGre()
{
	array< string > pliki;

	string saves_path = DaneGry( "SAVES_PATH" ).GetValue();
	CreateDirectoryA( saves_path.c_str(), NULL );

	WIN32_FIND_DATAA dane;
	HANDLE uchwyt = FindFirstFileA( (saves_path + "*.sav").c_str(), &dane );
	if((unsigned)(uchwyt) == 0xffffffff) return false;
	if( uchwyt && dane.cFileName != saves_path + ".." ) pliki.add( dane.cFileName );
	while( FindNextFileA( uchwyt, &dane ) )
		if( dane.cFileName != saves_path + ".." ) pliki.add( dane.cFileName );
	FindClose( uchwyt );

	if(pliki.size() == 0) return false;

	unsigned wybrany = 0;
	for(;;)
	{
		StdBuf.Czysc();
		RysujRamke( POS( 24, 8 ), POS( 32, 18 ), xcCze, false );
		StdBuf >> xBia >> POS( 26, 8 ) << " Wybierz zapisany stan gry: ";
		StdBuf.WstawZnak( 26, xBia, POS( 27, 12 ) );
		for( short i = -wybrany; i < 12; i++ )
		{
			if( i < -2 ) i = -2;
			StdBuf >> xZul >> POS( 28, 12 + i ) << pliki[ i + wybrany ];
			if( i + wybrany + 1 >= pliki.size() ) break;
		}
		StdBuf.Rysuj();

		switch( PobierzKlawisz() )
		{
			case 1072: case '8': if( wybrany > 0 ) wybrany--; continue;
			case 1080: case '2': if( wybrany < pliki.size() - 1 ) wybrany++; continue;
			case 13: break;
			case 27: case 32: return false;
			default: continue;
		}
		break;
	}

	StdBuf.Czysc();
	StdBuf >> xBia >> POS( 2, 2 ) << "Prosz� czeka�. Trwa wczytywanie stanu gry...";
	StdBuf.Rysuj();

	StanGry->Wczytaj( saves_path + pliki[ wybrany ] );
	AktualnaMapa = &StanGry->Mapy[ StanGry->Poziom ];

	Komunikat << "Stan gry wczytany pomy�lnie." >> xTur;

	return true;
}

/****************************************************************************************************/

void GRA::Start( string plik_swiata_gry )
{
	//RLGL::FILE plik( "pz.rlm" );
	//plik.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING );
	//RLGL::MOVIE intro;
	//intro.Load( plik );
	//intro.Show( RLGL::POS( 10, 5 ) );

	//plik_swiata_gry = "main.pzw";

	_TRY_BEGIN

	bool spakowany = false;
	if( plik_swiata_gry.substr( plik_swiata_gry.size() - 4 ) == ".pzw" ) spakowany = true;

	RLGL::FILE plik( plik_swiata_gry );
	if( !plik.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING ) )
	{
		throw( "Nie mo�na otworzy� g��wnego pliku gry." );
	}
	string dane;
	dane = plik.ReadAllToString();
	plik.Close();

	if( spakowany )
	{
		char* bufor_docelowy = new char[ 5242880 ];
		unsigned long dlugosc_po_rozpakowaniu;
		uncompress( (Bytef*) bufor_docelowy, &dlugosc_po_rozpakowaniu, (const Bytef*) dane.c_str(), dane.size() );
		bufor_docelowy[ dlugosc_po_rozpakowaniu ] = 0;
		dane = bufor_docelowy;
		delete[] bufor_docelowy;
	}

	int index = 0;
	RLGL::MainPath = plik_swiata_gry.substr( 0, plik_swiata_gry.find_last_of('/') ) + "/";
	DaneGry.Load( dane, index );
	
	/*
	RLGL::FILE swiat( "podziemia.pzw" );
	swiat.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	
	char* bufor_docelowy = new char[ (unsigned)( dane.size() * 1.1 + 12 ) ];
	unsigned long dlugosc_po_zpakowaniu;
	compress2( (Bytef*) bufor_docelowy, &dlugosc_po_zpakowaniu, (const Bytef*) dane.c_str(), dane.size(), Z_BEST_COMPRESSION );

	swiat.Write( bufor_docelowy, dlugosc_po_zpakowaniu );

	delete[] bufor_docelowy;
	
	swiat.Close();*/


	SetConsoleTitleA( static_cast<string>( DaneGry( "NAME" ).GetValue() ).c_str() );
	

	// INICJALIZACJE -------------------------------

	extern array< POS > Punkty;
	StworzPunkty( Punkty );

	// KAFELKI
	AlokacjaKafelkow();
	// CZARY
	AlokacjaCzarow();
	// PRZEDMIOTY
	AlokacjaPrzedmiotow();
	// WROGOWIE
	AlokacjaWrogow();
	// OSOBY
	AlokacjaOsob();
	// BOHATER
	InicjalizujBohatera();
	// MAPY Z PLIKU
	for(WORD i = 0; i < DaneGry("LEVELS").Size(); i++) if( AS_STR DaneGry("LEVELS")(i)("GENERATOR")[0] == "from_file" ) {
		DEF_MAPY* dm = new DEF_MAPY;
		PLIK plik( RLGL::MainPath + AS_STR DaneGry("LEVELS")(i)("FILE")[0] );
		if( !plik.Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) error("Nie mo�na wczyta� mapy z pliku. Prawdopodobnie podano niepoprawn� �cie�k�.");
		dm->Wczytaj( plik );
		plik.Zamknij();
		MapyZPliku.resize(MapyZPliku.size() + 1);
		MapyZPliku.back().Adres = AS_STR DaneGry("LEVELS")(i)("FILE")[0];
		MapyZPliku.back().Mapa = dm;

	}
	RLGL::MainPath = "";

	_CATCH( const char* komunikat ) error( komunikat );
	_CATCH_ALL error( "B��d przy inicjalizacji obiekt�w gry." );
	_CATCH_END

	// Highscore
	Highscore.Wczytaj( AS_STR( DaneGry( "SAVES_PATH" ).GetValue() ) + "highscore" );

}

/****************************************************************************************************/

bool GRA::Gra()
{
	for(;;)
	{
		StanGry = new STAN_GRY;
		Zakonczona = false;
		Komunikat.CzyscBufor();

		BYTE opcja = 0, wybor = -1;
		for(;;)
		{
			RysujGlowneMenu();
			StdBuf.WstawZnak( 26, xBia, POS( 3, 24 + opcja*2 ) );
			StdBuf.Rysuj();
			switch( PobierzKlawisz() )
			{
				case 1072: case '8': if( opcja > 0 ) opcja--; continue;
				case 1080: case '2': if( opcja < 4 ) opcja++; continue;
				case 13: wybor = opcja; break;
				case 'n': case 'N': wybor = 0; break;
				case 'l': case 'L': wybor = 1; break;
				case 'm': case 'M': wybor = 2; break;
				case 'h': case 'H': wybor = 3; break;
				case 'q': case 'Q': wybor = 4; break;
				case 'E': { EDYTOR edytor; edytor.Przetwarzaj(); continue; }
				default: continue;
			}
			switch( wybor ) {
				case 0: break;
				case 1: if( WczytajGre() ) break; continue;
				case 2: PokazPomoc(); continue;
				case 3: Highscore.Pokaz(); continue;
				case 4: delete StanGry; exit( 0 );
				default: continue;
			}
			break;
		}
		
		Kurtyna();

		if( StanGry->Postac.Nazwa == "" )
		{
			if( !TworzeniePostaci() )
			{
				delete StanGry;
				continue;
			}

			Kurtyna();

			StworzMapy();
			StanGry->Poziom = IndexMapy( DaneGry( "START_MAP" ).GetValue() );
			AktualnaMapa = &StanGry->Mapy[ StanGry->Poziom ];

			StanGry->Postac.Pozycja = POS( DaneGry( "START_POS" )[0], DaneGry( "START_POS" )[1] );

			Komunikat << "Witaj " << StanGry->Postac.Nazwa << "!" >> xBia;

			Kurtyna();

			StdBuf.UstawKolorPisania( DaneGry( "INTRO_COLOR" ).GetValue() );
			StdBuf.NapiszIZawijaj( DaneGry( "INTRO" ).GetValue(), 64, POS( 8, 6 ) );
			StdBuf.Rysuj();
			_getch();

			Kurtyna();
		}

		_TRY_BEGIN
				
			while( !Zakonczona ) Graj();
			Koniec();

		_CATCH( const char* komunikat )
			error( komunikat );
		_CATCH_END
	}

	return true;
}

/****************************************************************************************************/

bool GRA::Graj()
{	
	bool ruch = false;
	if(StanGry->Postac.Tura > 0)
	{
		RysujGre();

		char Przycisk = 0;
		if( !StanGry->Postac.Zyje ) {
			if( _kbhit() ) Przycisk = _getch();
		}
		else Przycisk = _getch();
		
		// wykonuje dzia�ania
		ruch = Przetwarzaj( Przycisk );

		//	Komunikat o ewentualnych przedmiotach, kt�re znajduj� si� w pobli�u postaci
		if( StanGry->Postac.Zyje ) {
			short PrzedmiotNaPozycjiPostaci = AktualnaMapa->SzukajPrzedmiotu(StanGry->Postac.Pozycja);
			if(PrzedmiotNaPozycjiPostaci >= 0)
			{
				Komunikat << "Tu jest: ";
				if(AktualnaMapa->SzukajPrzedmiotu(StanGry->Postac.Pozycja, 2) >= 0)
					Komunikat << "kilka przedmiot�w";
				else {
					if(AktualnaMapa->Przedmioty[PrzedmiotNaPozycjiPostaci].Ilosc > 1) Komunikat << AktualnaMapa->Przedmioty[PrzedmiotNaPozycjiPostaci].Ilosc << "x ";
					Komunikat << Przedmioty[AktualnaMapa->Przedmioty[AktualnaMapa->SzukajPrzedmiotu(StanGry->Postac.Pozycja)].NR]->Nazwa;
				}
				Komunikat << "." >> xBia;
			}
		}
	}

	if( ruch || StanGry->Postac.Tura <= 0 || !StanGry->Postac.Zyje ) {
		// Dodaje punkty ruchu bohaterowi
		if( StanGry->Postac.Zyje ) StanGry->Postac.Tura += 5 / (StanGry->Postac.Skradanie ? 2 : 1 );
		else StanGry->Postac.Tura = 1;

		//	Aktualizowanie zmiennych
		if(ruch) StanGry->Tura++;
		if(ruch) StanGry->Postac.Tura -= 60 - StanGry->Postac.szybkosc / 4;
		if( StanGry->Sekunda % 10 == 0 && StanGry->Postac.Zyje )
		{
			StanGry->Postac.DoZycia( 0.02 );
			StanGry->Postac.DoEnergii( ( StanGry->Postac.Skradanie ? -0.02 : -0.01 ) );
			StanGry->Postac.DoPragnienia( 0.035 );
			StanGry->Postac.DoGlodu( 0.02 );
			if( StanGry->Postac.Pragnienie >= 100 || StanGry->Postac.Glod >= 100 || StanGry->Postac.Energia <= 0 )
				StanGry->Postac.DoZycia( -0.5 );
		}
		StanGry->Sekunda += 1;

		//	Ruch postaci
		for( int i = 0; i < AktualnaMapa->Postacie.size(); i++ )
		{
			AktualnaMapa->Postacie[i]->Tura += 5;
			AktualnaMapa->Postacie[i]->WykonajRuch();

			if( AktualnaMapa->Postacie[i]->Zycie <= 0 ) AktualnaMapa->UsunPostac( i );
		}
		
		RysujGre(false);
		Sleep(16);

		// Uroki
		for( int i = 0; i <  StanGry->Postac.Uroki.size(); i++ )
		{
			StanGry->Postac.Uroki[i].Uzyj(  StanGry->Postac );
			if(  StanGry->Postac.Uroki[i].Wlasciwosci.size() == 0 ) {
				 StanGry->Postac.Uroki.erase( i );
				i--;
			}
		}
	}
	
	if( StanGry->Postac.Zyje && StanGry->Postac.Zycie <= 0 ) StanGry->StanPostaci = SP_ZGINAL;

	if( StanGry->StanPostaci == SP_ZWYCIEZYL ||
		StanGry->StanPostaci == SP_PODDAL_SIE ||
		(StanGry->StanPostaci == SP_ZGINAL && StanGry->Postac.Zyje) )
	{
			// Aktualizacja highscore
			if(StanGry->Highscore) {
				HIGHSCORE::REKORD rekord = { StanGry->Postac.Nazwa, StanGry->Postac.Poziom, StanGry->Postac.Doswiadczenie,
									 AktualnaMapa->Nazwa, StanGry->Postac.Klasa, StanGry->StanPostaci, StanGry->Tura };
				Highscore.DodajRekord( rekord );

				string saves_path = DaneGry( "SAVES_PATH" ).GetValue();
				CreateDirectoryA( saves_path.c_str(), NULL );
				Highscore.Zapisz( saves_path + "highscore" );
			}
			if(StanGry->StanPostaci != SP_ZGINAL) Zakonczona = true;
	}

	//	Ewentualna �mier� postaci
	if( StanGry->StanPostaci == SP_ZGINAL && StanGry->Postac.Zyje )
	{
		StanGry->Postac.Zyje = false;
		StanGry->Postac.Punkty = 0;
		Komunikat << StanGry->Postac.Nazwa << " nie �yje..." >> xCze;
		RysujGre();
		CzekajNaSpacje();
	}

	return true;
}

/****************************************************************************************************/

void GRA::StworzMapy()
{	
	const RLGL::ELEMENT& relacja = DaneGry( "LEVELS" );
	double procent = 0;
	double procent_na_mape = AS_DBL(100) / relacja.Size();

	StanGry->Mapy.resize( relacja.Size() );

	// Generowanie map
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		const RLGL::ELEMENT& def_mapy = DaneGry("LEVELS")(i);

		StanGry->Mapy[i].ID = def_mapy.GetName();
		StanGry->Mapy[i].Nazwa = AS_STR def_mapy("NAME").GetValue();
		StanGry->Mapy[i].SwiatloSloneczne = def_mapy.Exist("SUN_LIGHT");
		StanGry->Mapy[i].UstawRozmiar( POS(def_mapy("SIZE")[0], def_mapy("SIZE")[1]) );

		GenerujMape( def_mapy, StanGry->Mapy[i] );

		procent += procent_na_mape;
		StdBuf.Czysc();
		StdBuf >> POS( 2, 2 ) >> xBia << "Prosz� czeka� trwa tworzenie podziemi...";
		StdBuf >> POS( 2, 4 ) >> xBia << "Uko�czono: " << AS_INT procent << "%    ";
		StdBuf.Rysuj();
	}

	
	// ��czenie map
	const RLGL::ELEMENT& polaczenia = DaneGry("LEVELS_CONNECTIONS");
	for( WORD i = 0; i < polaczenia.Size(); i++ ) ZlaczMapy( polaczenia("SWAP", i) );
}

/****************************************************************************************************/

bool GRA::RysujGlowneMenu()
{
	StdBuf.Czysc();

	StdBuf.UstawKolorPisania( xCze );
	StdBuf.UstawPozycjePisania( POS( 0, 7 ) );
	StdBuf << "     :%##!:,  ;@#$& :@%&:.  @#%%&$  :%%: :%#%@@&   %.  @  .&   :%%:  .%#%.      "
			  "      #'  &@  %$  &  $` :%  `  &@    @&   $%'      $#.&*&.#$    @&   &# #&      "
			  "      %@$&$'  %  :#  #  '&    $#     &@   %@%!%   :@&#%!%#&@:   &@   %$&$%      "
			  "      #'      #@ .%  % .;#   &#. ,   @&   %&.     #: &: :& :#   @&   #& &#      "
			  "     .%.      :&%&' :$@@'   %%@#@&  :%%: :%#%@@& .%:       :%. :%%: .@: :@.     "
			  "     ..,,.,,.,,,,.,,...,,.,,,,.,..,...,.,.,,..,.....,,.,.,,,,,,.,.,..,..,.,     "
			  "                                     v.1.0                                      ";
	StdBuf.UstawPozycjePisania( POS( 0, 16 ) );
	StdBuf << "                   (C) Copyright 2006-2009 by Tomasz Rogowski                   ";

	BYTE PozycjaY = 19;
	RysujRamke( POS( 0, 5 ), POS( 80, 30 ), xcCze, false );
	StdBuf >> POS( 2, 2 + PozycjaY ) >> xBia << "Witaj w Podziemiach podr�niku. Co zamierzasz zrobi�?";
	StdBuf >> POS( 4, 5 + PozycjaY ) >> xSza << "[N]ew ........ - Stworzy� now� posta� i rozpocz�� niebezpieczn� przygod�";
	StdBuf >> POS( 4, 7 + PozycjaY ) << "[L]oad ....... - Kontynuowa� rozpocz�t� wcze�niej w�dr�wk�";
	StdBuf >> POS( 4, 9 + PozycjaY ) << "[M]anual ..... - Przeczyta� ksi�g� podr�nika";
	StdBuf >> POS( 4, 11 + PozycjaY ) << "[H]ighscore .. - Zobaczy� najwi�kszych bohater�w";
	StdBuf >> POS( 4, 13 + PozycjaY ) << "[Q]uit ....... - Wyj�� z Podziemi";
	return true;
}

/****************************************************************************************************/

void GRA::RysujGre( bool komunikaty )
{
	StdBuf.Czysc();
	AktualnaMapa->Odkryj( StanGry->Postac.Pozycja );
	AktualnaMapa->StworzMapyPomocnicze();
	AktualnaMapa->Rysuj();
	RysujListeWidocznychObiektow( POS( 2, 2 ) );
	StanGry->Postac.Rysuj();
	if(komunikaty) Komunikat();
	StanGry->RysujPanel();
	StdBuf.Rysuj();
}

/****************************************************************************************************/

void UzyjPrzejscia( PRZEJSCIE& przejscie )
{
	AktualnaMapa = &StanGry->Mapy[przejscie.Mapa];
	StanGry->Poziom = przejscie.Mapa;
	StanGry->Postac.Pozycja = przejscie.NowaPozycja;
}

/****************************************************************************************************/

bool GRA::Przetwarzaj( char Przycisk )
{
	switch( Przycisk )
	{
	case '1': case '2': case '3': case '4': case '6': case '7': case '8': case '9':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.Idz( ( DIRECTION )( Przycisk - 48 ) ) ) return true;
		break;

	case '5': case 'r': case 'R':
		if( StanGry->Postac.Czekaj() ) return true;
		break;

	case 'l': case 'L': case 'v': case 'V':
		StanGry->Postac.Patrz(); break;

	case 't': case 'T':
		if( !StanGry->Postac.Zyje ) break;
			if( StanGry->Postac.Rozmawiaj() ) return true;
		break;

	case 'd': case 'D':
		if( !StanGry->Postac.Zyje ) break;
			if( StanGry->Postac.Skocz() ) return true;
		break;

	case 'a': case 'A':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.Zaatakuj() ) return true;
		break;

	case 'f': case 'F':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.Strzelaj() ) return true;
		break;

	case 'z': case 'Z':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.RzucCzar() ) return true;
		break;

	case 'x': case 'X':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.ZamknijDrzwi() ) return true;
		break;

	case 's': case 'S':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.SkradajSie() ) return true;
		break;

	case 'g': case 'G':
		if( !StanGry->Postac.Zyje ) break;
		if( StanGry->Postac.Wez() ) return true;
		break;

	case 'i': case 'I': case 'e': case 'E':
		if( PokazEkw() ) return true;
		break;

	case '@': case 'c': case 'C':
		PokazStat(); break;

	case 'j': case 'J': case 'q': case 'Q':
		StanGry->Postac.Dziennik.Przetwarzaj(); return false;

	case 'm': case 'M': case 'w': case 'W':
		PokazMape(); break;

	case '?':
		PokazPomoc(); break;

	case 13: case 'b': case 'B':
	{
		if( !StanGry->Postac.Zyje ) break;
		if( AktualnaMapa->SzukajPrzejscia( StanGry->Postac.Pozycja ) >= 0 )
			UzyjPrzejscia( AktualnaMapa->Przejscia[ AktualnaMapa->SzukajPrzejscia( StanGry->Postac.Pozycja ) ] );
		break;
	}

	case 17: {
		if(StanGry->StanPostaci != SP_ZGINAL) {
			Komunikat << "Czy na pewno chcesz zako�czy� gr� bez zapisywania? (T/N)" >> xTur;
			RysujGre();
			for(;;) {
				char znak = getch();
				if(znak == 'T' || znak == 't') {
					StanGry->StanPostaci = SP_PODDAL_SIE;
					Zakonczona = true;
					return false;
				}
				else if(znak == 'N' || znak == 'n') return false;
			}
		}
		else {
			string saves_path = DaneGry( "SAVES_PATH" ).GetValue();
			PLIK sg( saves_path + StanGry->Postac.Nazwa + ".sav" );
			bool istnieje = sg.Otworz( ODCZYT, OTWORZ_ISTNIEJACY );
			sg.Zamknij();
			if( istnieje ) {
				Komunikat << "Czy chcesz kontynuowa� gr� od ostatnio zapisanego stanu gry? (T/N)" >> xTur;
				Komunikat << "(kolejny osi�gni�ty wynik nie b�dzie uwzgl�dniany na li�cie highscore)" >> xTur;
				RysujGre();
				for(;;) {
					char znak = getch();
					if(znak == 'T' || znak == 't') {
						Komunikat << "Prosz� czeka�. Trwa wczytywanie stanu gry..." >> xTur;
						RysujGre();
						string nazwa = StanGry->Postac.Nazwa;
						delete StanGry;
						StanGry = new STAN_GRY;
						Komunikat.CzyscBufor();
						StanGry->Wczytaj( saves_path + nazwa + ".sav" );
						AktualnaMapa = &StanGry->Mapy[ StanGry->Poziom ];
						StanGry->Highscore = false;
						StanGry->Zapisz( saves_path + StanGry->Postac.Nazwa + ".sav" );
						Kurtyna();
						Komunikat << "Stan gry wczytany pomy�lnie." >> xTur;
						return false;
					}
					else if(znak == 'N' || znak == 'n') { Zakonczona = true; break; }
				}
			}
			else Zakonczona = true;
		}
		return true;
	}
	
	case 19: {
		Zakonczona = true;
	}

	default: if( StanGry->Postac.Zyje ) Komunikat << "Wci�nij [Shift]+[?] aby wy�wietli� pomoc. (" << int(Przycisk) << ")" >> xTur;
	}
	return false;
}

/****************************************************************************************************/

bool GRA::TworzeniePostaci()
{
	BOHATER& postac = StanGry->Postac;
	WORD kolor_ramek = xcCze;

	BYTE wybrana_plec = 1;
	bool wybrana = false;

	if( DaneGry("SEX_CHOICE").GetValue() )
	{
		for(;;)
		{
			StdBuf.Czysc();
			RysujRamke( POS( 20, 17 ), POS( 40, 6 ), kolor_ramek, false );
			StdBuf >> POS( 22, 17 ) >> xBia << " Wybierz p�e� postaci: ";
			StdBuf >> POS( 23, 19 ) >> xSza << Plcie[ 1 ];
			StdBuf >> POS( 23, 20 ) << Plcie[ 2 ];
			StdBuf.WstawZnak( 26, xBia, POS( 22, 18 + wybrana_plec ) );
			StdBuf.Rysuj();
			for( ; ; ) {
				switch( PobierzKlawisz() ) {
					case 1072: case '8': if( wybrana_plec <= 1 ) continue; else wybrana_plec--; break;
					case 1080: case '2': if( wybrana_plec >= 2 ) continue; else wybrana_plec++; break;
					case 13: wybrana = true; break;
					case 27: case 32: return false;
					default: continue;
				}
				break;
			}
			if(wybrana) break;
		}
	}

	WORD wybrana_klasa = 0;
	wybrana = false;
	const RLGL::ELEMENT& klasy = DaneGry( "CLASSES" );

	if( DaneGry("CLASS_CHOICE").GetValue() )
	{
		for(;;)
		{
			StdBuf.Czysc();
			RysujRamke( POS( 0, 12 ), POS( 40, 16 ), kolor_ramek, false );
			RysujRamke( POS( 40, 12 ), POS( 40, 16 ), kolor_ramek, false );
			StdBuf >> POS( 2, 12 ) >> xBia << " Wybierz klas� postaci: ";
			StdBuf.WstawZnak( 26, xBia, POS( 2, 16 ) );
			for( short i = -wybrana_klasa; i < 10; i++ ) {
				if( i < -2 ) i = -2;
				if( i + wybrana_klasa + 1 > klasy.Size() ) break;
				
				StdBuf >> xSza >> POS( 3, 16 + i ) << AS_STR klasy(wybrana_klasa + i)("NAME").GetValue();
			}

			StdBuf.WstawZnak( '@', klasy(wybrana_klasa)("COLOR").GetValue(), POS( 42, 14 ) );
			StdBuf >> POS( 44, 14 ) >> xBia << ZWielkiejLitery( AS_STR klasy(wybrana_klasa)("NAME").GetValue() );
			StdBuf.NapiszIZawijaj( klasy(wybrana_klasa)("DESCRIPTION").GetValue(), 36, POS( 42, 16 ) );
			StdBuf.Rysuj();

			for(;;) {
				switch( PobierzKlawisz() ) {
					case 1072: case '8': if( klasy.Size() <= 1 ) continue; if( wybrana_klasa <= 0 )continue; else wybrana_klasa--; break;
					case 1080: case '2': if( klasy.Size() <= 1 ) continue; if( wybrana_klasa >= klasy.Size() - 1 ) continue; else wybrana_klasa++; break;
					case 13: wybrana = true; break;
					case 27: case 32: return false;
					default: continue;
				}
				break;
			}
			if(wybrana) break;
		}
	}

	for(;;)
	{
		StdBuf.Czysc();
		RysujRamke( POS( 20, 17 ), POS( 40, 5 ), kolor_ramek, false );
		StdBuf >> POS( 22, 17 ) >> xBia << " Podaj imi� postaci: ";
		StdBuf.Rysuj();
		StanGry->Postac.Nazwa = Pobierz( "a�bc�de�fghijkl�mn�o�qprs�tuvwyxzz��A�BCDE�FGHIJKL�MN�O�QPRS�TUVWYXZ��1234567890-_ ", 10, POS( 22, 19 ) , xBia );
		if( StanGry->Postac.Nazwa.length() > 2 )
		{
			string saves_path = DaneGry( "SAVES_PATH" ).GetValue();
			PLIK sg( saves_path + StanGry->Postac.Nazwa + ".sav" );
			if( sg.Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) {
				StdBuf >> xCze >> POS( 22, 24 ) << "Taka posta� ju� istnieje.";
				StdBuf >> xBia >> POS( 22, 25 ) << "Wci�nij dowolny klawisz...";
				StdBuf.Rysuj();
				PobierzKlawisz();
				continue;
			}
			break;
		}
		StdBuf >> xCze >> POS( 22, 24 ) << "Za kr�tka nazwa. (min. 3 znaki)";
		StdBuf >> xBia >> POS( 22, 25 ) << "Wci�nij dowolny klawisz...";
		StdBuf.Rysuj();
		PobierzKlawisz();
	}
	if( StanGry->Postac.Nazwa == "_~FALSE_" ) return false;

	/********************************************************************************/

	StanGry->Postac.Plec = static_cast<PLEC>(wybrana_plec);
	switch( StanGry->Postac.Plec ) {
		case Mezczyzna: 
			postac.Energia += 2;
			postac.MaxEnergia += 2;
			break;
		case Kobieta: 
			postac.Zycie += 2;
			postac.MaxZycie += 2;
	}

	const RLGL::ELEMENT& klasa = klasy(wybrana_klasa);
	const RLGL::ELEMENT& atrybuty = klasa("ATTRIBUTES");
	const RLGL::ELEMENT& umiejetnosci = klasa("SKILLS");

	postac.UstawStatystyki( atrybuty("STRENGTH")[0], atrybuty("DEXTERITY")[0], atrybuty("SPEED")[0], atrybuty("HP")[0], atrybuty("EP")[0] );
	for( WORD i = 0; i < Umiejetnosci.size(); i++ )
		postac.Umiejetnosci[ Umiejetnosci[i].ID ] = umiejetnosci( Umiejetnosci[i].ID ).GetValue();
	postac.Klasa = AS_STR klasa("NAME")[0];

	for( WORD i = 0; klasa.Exist( "ITEMS", i ); i++ ) {
		const RLGL::ELEMENT& przedmioty = klasa( "ITEMS", i );
		postac.Ekwipunek.Plecak.DodajPrzedmiot( IndexPrzedmiotu( przedmioty[0] ), przedmioty[1] );
	}
	postac.Waga = Losuj( 62, 88 );
	StanGry->Postac.Wzrost = Losuj( 162, 198 );

	postac.Kolor = klasa("COLOR")[0];

	return true;
}

/****************************************************************************************************/

void GRA::Koniec()
{
	string saves_path = DaneGry( "SAVES_PATH" ).GetValue();

	if( StanGry->StanPostaci == SP_ZWYCIEZYL ) {
		Kurtyna();
		StdBuf.UstawKolorPisania( DaneGry( "EXTRO_COLOR" ).GetValue() );
		StdBuf.NapiszIZawijaj( DaneGry( "EXTRO" ).GetValue(), 64, POS( 8, 6 ) );
		StdBuf.Rysuj();
		_getch();
		Kurtyna();
	}
	else if( StanGry->StanPostaci == SP_ZYJE ) {
		StdBuf.Czysc();
		StdBuf >> POS( 2, 2 ) >> xBia << "Prosz� czeka�. Trwa zapisywanie stanu gry...";
		StdBuf.Rysuj();
		CreateDirectoryA( saves_path.c_str(), NULL );
		StanGry->Zapisz( saves_path + StanGry->Postac.Nazwa + ".sav" );
	}
	if( StanGry->StanPostaci == SP_ZWYCIEZYL ||
		StanGry->StanPostaci == SP_PODDAL_SIE ||
		StanGry->StanPostaci == SP_ZGINAL ) UsunPlik( saves_path + StanGry->Postac.Nazwa + ".sav" );
	delete StanGry;
}

/****************************************************************************************************/