#include "stdafx.h"
#include "GeneratoryMap.h"

/****************************************************************************************************/

void POSTAC::BronSie( OBRAZENIA obrazenia )
{
	short _ochrona = Ochrona.Wartosci[obrazenia.Typ];
	WORD ochrona = Losuj( _ochrona / 2, _ochrona );
	if( ochrona < obrazenia.Wartosc ) obrazenia.Wartosc -= ochrona;
	else {
		obrazenia.Wartosc = 0;
		if( Typ != TPO_BOHATER ) Komunikat << ZWielkiejLitery( Nazwa ) << " unika obra�e�." >> xBia;
		else Komunikat << "Unikasz obra�e�." >> xBia;
	}
	DoZycia( -obrazenia.Wartosc );

	if( Typ != TPO_BOHATER ) {
		dynamic_cast< POSTAC_NIEZALEZNA* >(this)->WrogoNastawiony = true;
		dynamic_cast< POSTAC_NIEZALEZNA* >(this)->PozycjaAtaku = StanGry->Postac.Pozycja;
	}

	if( obrazenia.Wartosc > 0 ) {
		if( Typ != TPO_BOHATER )
		{
			switch( obrazenia.Typ ) {
				case OBR_BRON: Komunikat << ZWielkiejLitery( Nazwa ) << " traci " << obrazenia.Wartosc << " HP." >> xcZie; break;
				case OBR_POCISKI: Komunikat << ZWielkiejLitery( Nazwa ) << " traci " << obrazenia.Wartosc << " HP od pocisku." >> xcZie; break;
				case OBR_MAGIA: Komunikat << ZWielkiejLitery( Nazwa ) << " traci " << obrazenia.Wartosc << " HP od magii." >> xcZie; break;
			}
		}
		else
		{
			switch( obrazenia.Typ ) {
				case OBR_BRON: Komunikat << "Tracisz " << obrazenia.Wartosc << " HP." >> xZul; break;
				case OBR_POCISKI: Komunikat << "Tracisz " << obrazenia.Wartosc << " HP od pocisku." >> xZul; break;
				case OBR_MAGIA: Komunikat << "Tracisz " << obrazenia.Wartosc << " HP od magii." >> xZul; break;
			}
		}
	}
}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::Czaruj( WORD czar )
{
	CZAR_POCISK* _czar = static_cast< CZAR_POCISK* >( Czary[ czar ] );

	PUNKTY linia;
	linia.Stworz( Pozycja, StanGry->Postac.Pozycja );

	for( UCHAR i = 1; i < linia.IloscPunktow; i++ ) {
		if( i > _czar->Zasieg ) break;

		StdBuf.Czysc();
		AktualnaMapa->StworzMapyPomocnicze();
		AktualnaMapa->Rysuj();
		StanGry->Postac.Rysuj();
		_czar->Urok.Animacja.Rysuj( linia.Punkty[ i ]( 1 + SRODEK_RYSOWANIA.X - StanGry->Postac.Pozycja.X, 2 + SRODEK_RYSOWANIA.Y - StanGry->Postac.Pozycja.Y ) );

		if( StanGry->Postac.Pozycja == linia.Punkty[ i ] ) StanGry->Postac.Uroki.add( _czar->Urok );
	}
}

/****************************************************************************************************/

void POSTAC::DoZycia( double Ile )
{
	Zycie += Ile;
	if( Zycie > PobMaxZycie() ) Zycie = PobMaxZycie();
}

/****************************************************************************************************/

void POSTAC::ZapiszPostac( PLIK& Plik )
{
	BYTE typ = Typ;
	Plik << typ
		 << ID
		 << Nazwa
		 << Znak
		 << Kolor
		 << Zycie
		 << Tura;
	Plik.Zapisz( &Ochrona, sizeof( OCHRONA ) );
	Plik << (WORD)( Uroki.size() );
	for(unsigned i = 0; i < Uroki.size(); i++) Uroki[ i ].Zapisz( Plik );
	Plik.Zapisz( &Pozycja, sizeof( POS ) );
}

/****************************************************************************************************/

void POSTAC::WczytajPostac( PLIK& Plik )
{
	BYTE typ;
	Plik >> typ
		 >> ID
		 >> Nazwa
		 >> Znak
		 >> Kolor
		 >> Zycie
		 >> Tura;
	Typ = (TYP_POSTACI) Typ;
	Plik.Wczytaj( &Ochrona, sizeof( OCHRONA ) );
	WORD ilosc;
	Plik >> ilosc;
	Uroki.resize(ilosc);
	for(unsigned i = 0; i < Uroki.size(); i++) Uroki[ i ].Wczytaj( Plik );
	Plik.Wczytaj( &Pozycja, sizeof( POS ) );
}

/****************************************************************************************************/

bool POSTAC_NIEZALEZNA::Idz( POS pozycja )
{
	POS p = pozycja;

	if( Losuj( 1, 100 ) <= PobDzikosc() ) p = Pozycja + VALUES_OF_DIRECTIONS[ Losuj( 0, 8 ) ];

	if( p == StanGry->Postac.Pozycja && WrogoNastawiony && StanGry->Postac.Zyje )
	{
		Atakuj();
		return false;
	}
	else
	{
		if( !AktualnaMapa->PozycjaWolnaDlaPostaci( p ) ||
			p == StanGry->Postac.Pozycja ) return false;
		Pozycja = p;
	}

	if( !( p == pozycja ) ) return false;
	return true;
}

/****************************************************************************************************/

bool POSTAC_NIEZALEZNA::Atakuj()
{
	// Losuje obra�enia
	short obr = Losuj( 0, PobObrazenia() );

	// Je�eli wylosuje 0 to znaczy �e nie trafi� i wy�wietla odpowiedni komunikat
	if( obr <= 0 ) {
		Komunikat << ZWielkiejLitery( Nazwa ) << " nie trafia." >> xBia;
		return false;
	}

	// Sprawdza obron� przy pomocy tarczy
	TARCZA* Tarcza = 0;
	if( StanGry->Postac.Ekwipunek.LewaDlon >= 0 ) Tarcza = dynamic_cast< TARCZA* >( Przedmioty[ StanGry->Postac.Ekwipunek.LewaDlon ] );
	if( Tarcza && Losuj( 1, 100 ) <= StanGry->Postac.Umiejetnosci["tarcza"] ) {
		obr -= Tarcza->Obrona.Rand();
		if( obr <= 0 ) {
			Komunikat << "Os�aniasz si� tarcz�." >> xBia;
			return false;
		}
	}

	// Zadaje obra�enia
	StanGry->Postac.BronSie( OBRAZENIA( OBR_BRON, obr ) );
	
	return true;
}

/****************************************************************************************************/
//
//void POSTAC_NIEZALEZNA::WykonajRuch()
//{
//	/* Przetwarza skutki urok�w */
//	for( int i = 0; i < Uroki.size(); i++ )
//	{
//		Uroki[i].Uzyj( *this );
//		if( Uroki[i].Wlasciwosci.size() == 0 ) {
//			Uroki.erase( i );
//			i--;
//		}
//	}
//	/* Je�li HP jest poni�ej 0 to posta� umiera */
//	if( Zycie <= 0 ) {
//		Smierc();
//		return;
//	}
//
//	/* Je�li bohater wszed� na chronione pole zaczyna relacj� */
//	if(	Typ == TPO_OSOBA && !WrogoNastawiony )
//	{
//		OSOBA* osoba = dynamic_cast< OSOBA* >( this );
//		if( abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= osoba->ZasiegChronionegoPola &&
//			abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= osoba->ZasiegChronionegoPola )
//		{
//			RELACJONER relacjoner;
//			RELACJA def_relacji;
//			def_relacji.Laduj( DaneGry( "DIALOGS" )( osoba->Rozmowa ) );
//			relacjoner.Laduj( &def_relacji, &StanGry->Postac, osoba, "field" );
//			for(;;)
//			{
//				if( relacjoner.End ) break;
//				relacjoner.Rysuj();
//				relacjoner.Przetwarzaj( _getch() );
//			}
//		}
//	}
//
//	PUNKTY droga;
//	PATH droga_rzeczywista( *AktualnaMapa, Pozycja - POS(49, 49) );
//	WORD krok = 0;
//	for(;;)
//	{
//		if( Tura <= 0 ) return;
//
//		/* Je�li posta� stoi i nie jest wrogo nastawiona to nie wykonuje ruchu */
//		if( ZasiegRuchu == 0 && !WrogoNastawiony )
//		{
//			Tura = 0;
//			return;
//		}
//
//		if( krok == 0 )
//		{
//
//			bool widoczny = false;
//
//			if( WrogoNastawiony && StanGry->Postac.Zyje )
//			{
//				if( abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= ZasiegAtaku && abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= ZasiegAtaku )
//				{
//					widoczny = true;
//
//					droga.Stworz( StanGry->Postac.Pozycja, Pozycja );
//
//					/* Nie widzi wroga je�li co� go zas�ania */
//					for( short i = droga.IloscPunktow - 1; i >= 0; i-- )
//						if( Kafelki[ AktualnaMapa->Kafelki[ droga.Punkty[ i ].X ][ droga.Punkty[ i ].Y ] ].Typ == TK_SCIANA ) {
//							widoczny = false;
//							break;
//						}
//
//					/* Wr�g staje si� widoczny je�li zostanie zauwa�ony podczas skradania */
//					if( widoczny ) {
//						if( StanGry->Postac.Skradanie && Losuj( 1, 100 ) > droga.IloscPunktow / 3 * StanGry->Postac.Umiejetnosci["skradanie"] )
//							StanGry->Postac.Niewidoczny = false;
//					}
//						
//					if( abs(StanGry->Postac.Pozycja.X - PozycjaAtaku.X) <= 5 && abs(StanGry->Postac.Pozycja.Y - PozycjaAtaku.Y) <= 5)
//						PozycjaAtaku = StanGry->Postac.Pozycja;
//
//					if( widoczny ) {
//						PozycjaAtaku = StanGry->Postac.Pozycja;
//
//						// Rzuca czar je�li posiada
//						if( Czar >= 0 && Losuj(1, 3) == 1 && PozycjaAtaku == StanGry->Postac.Pozycja && krok == 0 && droga.IloscPunktow > 3 &&
//							static_cast< CZAR_POCISK* >( Czary[ Czar ] )->Zasieg >= droga.IloscPunktow - 1 )
//						{
//							Czaruj( Czar );
//							Tura -= PobCzas1Ruchu() * 5;
//							return;
//						}
//					}
//				}
//			}
//
//			// Je�eli nie widzi wroga, wykonuje ruch w dowolnym kierunku
//			if( !widoczny && abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= 18 && abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= 18 )
//			{
//				if( ( PozycjaAtaku == Pozycja || !StanGry->Postac.Zyje ) && Losuj( 1, 10 ) == 1 )
//					PozycjaAtaku = POS( Losuj( StalaPozycja.X - ZasiegRuchu, StalaPozycja.X + ZasiegRuchu ),
//										Losuj( StalaPozycja.Y - ZasiegRuchu, StalaPozycja.Y + ZasiegRuchu ) );
//			}
//
//			droga_rzeczywista.Create( Pozycja, PozycjaAtaku, true );
//		}
//
//		
//		// Przesuwa si� o jedno pole
//		if( droga.IloscPunktow - krok >= 2 && Idz( droga.Punkty[ droga.IloscPunktow - 2 - krok ] ) ) krok++;
//		else {
//			krok = 0;
//			PozycjaAtaku = Pozycja;
//		}
//
//		Tura -= PobCzas1Ruchu() * 5;
//	}
//}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::WykonajRuch()
{
	/* Przetwarza skutki urok�w */
	for( int i = 0; i < Uroki.size(); i++ )
	{
		Uroki[i].Uzyj( *this );
		if( Uroki[i].Wlasciwosci.size() == 0 ) {
			Uroki.erase( i );
			i--;
		}
	}
	/* Je�li HP jest poni�ej 0 to posta� umiera */
	if( Zycie <= 0 ) {
		Smierc();
		return;
	}

	/* Je�li bohater wszed� na chronione pole zaczyna relacj� */
	if(	Typ == TPO_OSOBA && !WrogoNastawiony )
	{
		OSOBA* osoba = dynamic_cast< OSOBA* >( this );
		if( abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= osoba->ZasiegChronionegoPola &&
			abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= osoba->ZasiegChronionegoPola )
		{
			RELACJONER relacjoner;
			RELACJA def_relacji;
			def_relacji.Laduj( DaneGry( "DIALOGS" )( osoba->Rozmowa ) );
			relacjoner.Laduj( &def_relacji, &StanGry->Postac, osoba, "field" );
			for(;;)
			{
				if( relacjoner.End ) break;
				relacjoner.Rysuj();
				relacjoner.Przetwarzaj( _getch() );
			}
		}
	}

	PUNKTY droga;
	WORD krok = 0;
	for(;;)
	{
		if( Tura <= 0 ) return;

		/* Je�li posta� stoi i nie jest wrogo nastawiona to nie wykonuje ruchu */
		if( ZasiegRuchu == 0 && !WrogoNastawiony )
		{
			Tura = 0;
			return;
		}

		if( krok == 0 )
		{

			bool widoczny = false;

			if( WrogoNastawiony && StanGry->Postac.Zyje )
			{
				if( abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= ZasiegAtaku && abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= ZasiegAtaku )
				{
					widoczny = true;

					droga.Stworz( StanGry->Postac.Pozycja, Pozycja );

					/* Nie widzi wroga je�li co� go zas�ania */
					for( short i = droga.IloscPunktow - 1; i >= 0; i-- )
						if( Kafelki[ AktualnaMapa->Kafelki[ droga.Punkty[ i ].X ][ droga.Punkty[ i ].Y ] ].Typ == TK_SCIANA ) {
							widoczny = false;
							break;
						}

					/* Wr�g staje si� widoczny je�li zostanie zauwa�ony podczas skradania */
					if( widoczny ) {
						if( StanGry->Postac.Skradanie && Losuj( 1, 100 ) > droga.IloscPunktow / 3 * StanGry->Postac.Umiejetnosci["skradanie"] )
							StanGry->Postac.Niewidoczny = false;
					}
					if(StanGry->Postac.Niewidoczny) widoczny = false;

					if( widoczny ) {
						PozycjaAtaku = StanGry->Postac.Pozycja;

						// Rzuca czar je�li posiada
						if( Czar >= 0 && Losuj(1, 3) == 1 && PozycjaAtaku == StanGry->Postac.Pozycja && krok == 0 && droga.IloscPunktow > 3 &&
							static_cast< CZAR_POCISK* >( Czary[ Czar ] )->Zasieg >= droga.IloscPunktow - 1 )
						{
							Czaruj( Czar );
							Tura -= PobCzas1Ruchu() * 2;
							return;
						}
					}
				}
			}

			// Je�eli nie widzi wroga, wykonuje ruch w dowolnym kierunku
			if( !widoczny && abs( StanGry->Postac.Pozycja.X - Pozycja.X ) <= 18 && abs( StanGry->Postac.Pozycja.Y - Pozycja.Y ) <= 18 )
			{
				if( ( PozycjaAtaku == Pozycja || !StanGry->Postac.Zyje ) && Losuj( 1, 10 ) == 1 )
					PozycjaAtaku = POS( Losuj( StalaPozycja.X - ZasiegRuchu, StalaPozycja.X + ZasiegRuchu ),
										Losuj( StalaPozycja.Y - ZasiegRuchu, StalaPozycja.Y + ZasiegRuchu ) );
				droga.Stworz( PozycjaAtaku, Pozycja );
			}

		}

		
		// Przesuwa si� o jedno pole
		if( droga.IloscPunktow - krok >= 2 && Idz( droga.Punkty[ droga.IloscPunktow - 2 - krok ] ) ) krok++;
		else {
			krok = 0;
			PozycjaAtaku = Pozycja;
		}

		Tura -= PobCzas1Ruchu() * 5;
	}
}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::Smierc()
{
	//KREW
	for( char i = 0; i < 5; i++ )
	{
		POS p = Pozycja + VALUES_OF_DIRECTIONS[ Losuj( 0, 8 ) ];

		if( Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].Zakrwawialny )
		{
			if( Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].Typ == TK_SCIANA ) AktualnaMapa->Kafelki[ p.X ][ p.Y ] = LosujKafelek( DaneGry( "STYLES" )( "blooded" )( "WALLS" ) );
			else AktualnaMapa->Kafelki[ p.X ][ p.Y ] = LosujKafelek( DaneGry( "STYLES" )( "blooded" )( "GROUNDS" ) );
		}
	}

	PozostawTrofea();

	Komunikat << ZWielkiejLitery( Nazwa ) << " nie �yje." >> xcZie;

	DoDoswiadczenia( ( PobMaxZycie() - ( PobCzas1Ruchu() + 1 ) / 2 + PobObrazenia() ) / 3 );

	StanGry->Postac.Dziennik.DodajWpis( "killed", ID );
	StanGry->Postac.Dziennik.WartoscWpisu( "killed", ID )++;
}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::RysujPasekZycia( POS pozycja )
{
	char ilosc_kratek = ceil( ( float )( Zycie * 10 / PobMaxZycie() ) );
	BYTE kolor = xCze;
	if( ilosc_kratek >= 7 ) kolor = xZie;
	else if( ilosc_kratek >= 4 ) kolor = xZul;

	for( char i = 0; i < 10; i++ ) {
		if( ilosc_kratek <= i ) StdBuf >> pozycja( i , 0 ) >> xBia << "-";
		else StdBuf >> pozycja( i , 0 ) >> kolor << "*";
	}
}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::Zapisz( PLIK& Plik )
{
	ZapiszPostac( Plik );

	Plik << WrogoNastawiony
		 << Czar
		 << Dzikosc
		 << ZasiegRuchu
		 << ZasiegAtaku;
	Plik.Zapisz( &PozycjaAtaku, sizeof( POS ) );
	Plik.Zapisz( &StalaPozycja, sizeof( POS ) );
}

/****************************************************************************************************/

void POSTAC_NIEZALEZNA::Wczytaj( PLIK& Plik )
{
	WczytajPostac( Plik );

	Plik >> WrogoNastawiony
		 >> Czar
		 >> Dzikosc
		 >> ZasiegRuchu
		 >> ZasiegAtaku;
	Plik.Wczytaj( &PozycjaAtaku, sizeof( POS ) );
	Plik.Wczytaj( &StalaPozycja, sizeof( POS ) );
}

