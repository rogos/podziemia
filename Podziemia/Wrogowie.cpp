#include "stdafx.h"

void AlokacjaWrogow()
{
	const RLGL::ELEMENT& relacja = DaneGry( "MONSTERS" );
	RLGL::STRUCTURE s;
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		const RLGL::ELEMENT& wrog = relacja( i );
		DodajWroga( wrog.GetName(), wrog("NAME")[0], wrog("SYMBOL")[0], wrog("SYMBOL")[1], OCHRONA( wrog("PROTECTION")[0], wrog("PROTECTION")[1], wrog("PROTECTION")[2] ),
		          ( wrog.Exist("HOSTILE") ? wrog("HOSTILE")[0] : 1 ), wrog("DAMAGE")[0], wrog("WILD")[0], wrog("MAX_HP")[0], wrog("MOVE_TIME")[0], wrog("MOVE_RANGE")[0], wrog("ATTACK_RANGE")[0], wrog("OCCURRENCE")[0],
		          ( wrog.Exist("TROPHIES") ? dynamic_cast<const RLGL::STRUCTURE&>( wrog("TROPHIES") ) : s ), ( wrog.Exist("SPELL") ? IndexCzaru( wrog("SPELL")[0] ) : -1 ) );
	}
}