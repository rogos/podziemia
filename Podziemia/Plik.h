#pragma once

#include "stdafx.h"

/****************************************************************************************************/

enum CEL_OTWARCIA{ ZAPIS, ODCZYT, ZAPIS_I_ODCZYT, NIE_OTWARTY };
enum SPOSOB_OTWARCIA{ STWORZ_ZAWSZE = 2, STWORZ = 1, OTWORZ_ZAWSZE = 4, OTWORZ_ISTNIEJACY = 3 };

/****************************************************************************************************/

class PLIK
{
private:
	string m_Adres;
	HANDLE m_Plik;
	CEL_OTWARCIA m_CelOtwarcia;
public:
	PLIK( string Adres ) : m_Adres( Adres ), m_Plik( 0 ), m_CelOtwarcia( NIE_OTWARTY ) {}
	~PLIK()
	{
		if( m_Plik ) CloseHandle( m_Plik );
	}

	bool Otworz( CEL_OTWARCIA CelOtwarcia, SPOSOB_OTWARCIA SposobOtwarcia = STWORZ_ZAWSZE );
	bool Zamknij();

	bool Zapisz( LPCVOID Zmienna, DWORD Ilosc );
	bool Wczytaj( LPVOID Zmienna, DWORD Ilosc );
	string WczytajDoStringa();

	unsigned Rozmiar();

	PLIK& operator << ( string Zmienna );
	PLIK& operator << ( int Zmienna );
	PLIK& operator << ( unsigned Zmienna );
	PLIK& operator << ( float Zmienna );
	PLIK& operator << ( double Zmienna );
	PLIK& operator << ( short Zmienna );
	PLIK& operator << ( WORD Zmienna );
	PLIK& operator << ( char Zmienna );
	PLIK& operator << ( UCHAR Zmienna );
	PLIK& operator << ( bool Zmienna );

	PLIK& operator >> ( string& Zmienna );
	PLIK& operator >> ( int& Zmienna );
	PLIK& operator >> ( unsigned& Zmienna );
	PLIK& operator >> ( float& Zmienna );
	PLIK& operator >> ( double& Zmienna );
	PLIK& operator >> ( short& Zmienna );
	PLIK& operator >> ( WORD& Zmienna );
	PLIK& operator >> ( char& Zmienna );
	PLIK& operator >> ( UCHAR& Zmienna );
	PLIK& operator >> ( bool& Zmienna );

	bool Szyfruj();
};

/****************************************************************************************************/

bool UsunPlik( string Adres );

/****************************************************************************************************/
