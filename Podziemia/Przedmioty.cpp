
#include "stdafx.h"

/****************************************************************************************************/

array< PRZEDMIOT* > Przedmioty;

/****************************************************************************************************/

void POJEMNIK::DodajPrzedmiot( WORD id, unsigned ilosc )
{
	int poz = -1;
	for( unsigned i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].Typ == id )
		{
			poz = i;
			break;
		}
		
	Waga += ::Przedmioty[ id ]->Waga * ilosc;

	if( poz < 0 )
	{
		ZBIOR z = { id, ilosc };
		Przedmioty.add( z );
	}
	else Przedmioty[ poz ].Ilosc += ilosc;
}

/****************************************************************************************************/

unsigned POJEMNIK::UsunPrzedmiot( WORD id, unsigned ilosc )
{
	unsigned ilosc_usunietych = 0;
	
	for( unsigned i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].Typ == id )
		{
			unsigned _ilosc = Przedmioty[ i ].Ilosc;
			if( ilosc_usunietych + _ilosc > ilosc ) {
				_ilosc = ilosc - ilosc_usunietych;
				Przedmioty[ i ].Ilosc -= _ilosc;
			}
			else {
				Przedmioty.erase( Przedmioty.begin() + i );
				i--;
			}
			Waga -= ::Przedmioty[ id ]->Waga * _ilosc;
			ilosc_usunietych += _ilosc;
		}

	return ilosc_usunietych;
}

/****************************************************************************************************/

unsigned POJEMNIK::UsunPrzedmiotNa( unsigned index, unsigned ilosc )
{
	Waga -= ::Przedmioty[ Przedmioty[ index ].Typ ]->Waga * ilosc;
	if( ilosc >= Przedmioty[ index ].Ilosc ) Przedmioty.erase( Przedmioty.begin() + index );
	else Przedmioty[ index ].Ilosc -= ilosc;
	return ilosc;
}

/****************************************************************************************************/

unsigned POJEMNIK::IloscPrzedmiotow( WORD id ) const
{
	unsigned ilosc = 0;
	for( unsigned i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].Typ == id ) 
			ilosc += Przedmioty[ i ].Ilosc;
	return ilosc;
}

/****************************************************************************************************/

void POJEMNIK::Zapisz( PLIK& Plik )
{
	Plik << Waga;
	Plik << Przedmioty.size();
	for( WORD i = 0; i < Przedmioty.size(); i++ ) Plik.Zapisz( &Przedmioty[ i ], sizeof( ZBIOR ) );
}

/****************************************************************************************************/

void POJEMNIK::Wczytaj( PLIK& Plik )
{
	Plik >> Waga;
	unsigned ilosc;
	Plik >> ilosc; 
	Przedmioty.resize( ilosc );
	for( WORD i = 0; i < Przedmioty.size(); i++ ) Plik.Wczytaj( &Przedmioty[ i ], sizeof( ZBIOR ) );
}

/****************************************************************************************************/

TYP_PRZEDMIOTU PRZEDMIOT::PodajTyp() {
	return TypPrzedmiotu;
}

/****************************************************************************************************/

void PRZEDMIOT::RysujOpis( POS pozycja )
{
	for( BYTE i = 1; i < 9; i++ ) StdBuf.Czysc( pozycja( 1, i ), 38 );
	RysujRamke( pozycja, POS( 40, 10 ), xcZul, false );
	StdBuf >> xBia >> pozycja( 2, 0 ) << " Opis przedmiotu: ";
	RysujRamke( pozycja( 2, 2 ), POS( 5, 5 ), xcZul, false );
	StdBuf.WstawZnak( Znak, Kolor, pozycja( 4, 4 ), false );
	StdBuf >> xSza >> pozycja( 8, 2 ) << "Nazwa: " >> xBia << ZWielkiejLitery( Nazwa );
	StdBuf >> xSza >> pozycja( 8, 3 ) << "Waga: " >> xBia << Waga << "dag";
	StdBuf >> xSza >> pozycja( 8, 4 ) << "Warto��: " >> xBia << Wartosc << "zm";
}

/****************************************************************************************************/

bool ZWYKLY::Uzyj()
{
	Komunikat << "Nie mo�esz tego u�y�!" >> xZul;
	return false;
}

/****************************************************************************************************/

void ZWYKLY::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
}

/****************************************************************************************************/

bool JEDNORAZOWY::Uzyj()
{
	StanGry->Postac.Uroki.add( Wlasciwosci );
	Komunikat << TekstPrzyUzyciu >> xBia;
	return true;
}

/****************************************************************************************************/

void JEDNORAZOWY::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
}

/****************************************************************************************************/

bool RELACYJNY::Uzyj()
{
	RELACJONER relacjoner;
	RELACJA def_relacji;
	array< array<bool> > relacja;

	if( !DaneGry( "DIALOGS" ).Exist( DefRelacji ) ) return false;

	def_relacji.Laduj( DaneGry( "DIALOGS" )( DefRelacji ) );
	relacjoner.Laduj( &def_relacji, &StanGry->Postac, &relacja );

	for(;;) {
		if( relacjoner.End ) break;
		relacjoner.Rysuj( xZul );
		relacjoner.Przetwarzaj( PobierzKlawisz() );
	}
	return false;
}

/****************************************************************************************************/

void RELACYJNY::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
}

/****************************************************************************************************/

bool ZWOJ::Uzyj()
{
	if( !StanGry->Postac.RzucCzar( Czar ) ) return false;
	return true;
}

/****************************************************************************************************/

void ZWOJ::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Wymagana energia: " >> xBia << Czary[Czar]->KosztRzucenia;
}

/****************************************************************************************************/

bool RUNA::Uzyj()
{
	for( WORD i = 0; i < StanGry->Postac.Czary.size(); i++ )
		if( StanGry->Postac.Czary[ i ] == Czar )
		{
			Komunikat << "Znasz ju� czar, kt�ry uczy ta runa." >> xZul;
			return false;
		}

	if( Czary[ Czar ]->StopienTrudnosci > StanGry->Postac.Umiejetnosci["magia"] ) {
		Komunikat << "Zbyt s�abo znasz si� na magii aby nauczy� si� tego czaru." >> xZul;
		return false;
	}

	StanGry->Postac.Czary.add( Czar );
	Komunikat << "Pozna�e� nowy czar - " << Czary[ Czar ]->Nazwa << "." >> xZie;
	return true;
}

/****************************************************************************************************/

void RUNA::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Wymagana znajomo�� magii: " >> xBia << Czary[Czar]->StopienTrudnosci;
	StdBuf >> xSza >> pozycja( 8, 6 ) << "Wymagana energia: " >> xBia << Czary[Czar]->KosztRzucenia;
}

/****************************************************************************************************/

bool KSIEGA::Uzyj()
{
	if( StanGry->Postac.Umiejetnosci[ Umiejetnosc ] >= 100 )
	{
		Komunikat << "Ta umiej�tno�� nie mo�e ju� by� rozwijana!" >> xZul;
		return false;
	}
	StanGry->Postac.Umiejetnosci[ Umiejetnosc ]++;
	Komunikat << "Rozwijasz umiej�tno�� \"" << ::Umiejetnosci[ Umiejetnosc ].Nazwa << "\"." >> xBia;
	return true;
}

/****************************************************************************************************/

void KSIEGA::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
}

/****************************************************************************************************/

bool BRON_BIALA::Uzyj()
{
	if( Typ == Jednoreczny && !StanGry->Postac.Umiejetnosci["bron_jednoreczna"] ) {
		Komunikat << "Nie potrafisz walczy� broni� jednor�czn�!" >> xZul;
		return false;
	}
	if( Typ == Dworeczny && !StanGry->Postac.Umiejetnosci["bron_dwureczna"] ) {
		Komunikat << "Nie potrafisz walczy� broni� dw�r�czn�!" >> xZul;
		return false;
	}
	if( WymaganaSila > StanGry->Postac.sila ) {
		Komunikat << "Masz zbyt ma�� si��!" >> xZul;
		return false;
	}
	if( Typ == Nareczna )
	{
		if( StanGry->Postac.Ekwipunek.PrawaDlon >= 0 )
		{
			if( StanGry->Postac.Ekwipunek.LewaDlon == -1 )
			{
				StanGry->Postac.Ekwipunek.LewaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;	
				Komunikat << "Za�o�y�e� na lew� d�o�: " << Nazwa << "." >> xBia;
				return true;
			}
			Komunikat << "Masz ju� co� w prawej d�oni!" >> xZul;
			return false;
		}
		StanGry->Postac.Ekwipunek.PrawaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
		Komunikat << "Za�o�y�e� na praw� d�o�: " << Nazwa << "." >> xBia;
	}
	else if( Typ == Jednoreczny )
	{
		if( StanGry->Postac.Ekwipunek.PrawaDlon >= 0 )
		{
			if( StanGry->Postac.Umiejetnosci["dwie_bronie"] > 0 && StanGry->Postac.Ekwipunek.LewaDlon == -1 )
			{
				StanGry->Postac.Ekwipunek.LewaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;	
				Komunikat << "Trzymasz teraz w lewej d�oni: " << Nazwa << "." >> xBia;
				return true;
			}
			Komunikat << "Masz ju� co� w prawej d�oni!" >> xZul;
			return false;
		}
		StanGry->Postac.Ekwipunek.PrawaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
		Komunikat << "Trzymasz teraz w prawej d�oni: " << Nazwa << "." >> xBia;
	}
	else
	{
		if( StanGry->Postac.Ekwipunek.LewaDlon >= 0 || StanGry->Postac.Ekwipunek.PrawaDlon >= 0 )
		{
			Komunikat << "Trzymasz ju� co� w d�oniach!" >> xZul;
			return false;
		}
		StanGry->Postac.Ekwipunek.PrawaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
		StanGry->Postac.Ekwipunek.LewaDlon = -2;
		Komunikat << "Trzymasz teraz w obydw�ch d�oniach: " << Nazwa << "." >> xBia;
	}
	return true;
}

/****************************************************************************************************/

void BRON_BIALA::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Typ: " >> xBia << (Typ == Nareczna ? "bro� nar�czna" : (Typ == Jednoreczny ? "bro� jednor�czna" : "bro� dwur�czna"));
	StdBuf >> xSza >> pozycja( 8, 6 ) << "Obra�enia: " >> xBia << Obrazenia.Begin << "-" << Obrazenia.End;
	StdBuf >> xSza >> pozycja( 8, 7 ) << "Wymagana si�a: " >> xBia << WymaganaSila;
}

/****************************************************************************************************/

bool BRON_DYSTANSOWA::Uzyj()
{
	if( Typ == Luk && !StanGry->Postac.Umiejetnosci["luk"] ) {
		Komunikat << "Nie potrafisz strzela� z �uku!" >> xZul;
		return false;
	}
	if( Typ == Kusza && !StanGry->Postac.Umiejetnosci["kusza"] ) {
		Komunikat << "Nie potrafisz strzela� z kuszy!" >> xZul;
		return false;
	}
	if( WymaganaZrecznosc > StanGry->Postac.zrecznosc ) {
		Komunikat << "Masz zbyt ma�� zr�czno��!" >> xZul;
		return false;
	}
	if( StanGry->Postac.Ekwipunek.PrawaDlon >= 0 ) {
		Komunikat << "Masz ju� co� w prawej d�oni!" >> xZul;
		return false;
	}
	StanGry->Postac.Ekwipunek.PrawaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
	Komunikat << "Trzymasz teraz w prawej d�oni: " << Nazwa << "." >> xBia;
	return true;
}

/****************************************************************************************************/

void BRON_DYSTANSOWA::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Typ: " >> xBia << (Typ == Luk ? "�uk" : "kusza");
	StdBuf >> xSza >> pozycja( 8, 6 ) << "Zasi�g: " >> xBia << Zasieg << " p�l";
	StdBuf >> xSza >> pozycja( 8, 7 ) << "Wymagana zr�czno��: " >> xBia << WymaganaZrecznosc;
}

/****************************************************************************************************/

bool POCISK::Uzyj()
{
	if( Typ == Luk && !StanGry->Postac.Umiejetnosci["luk"] ) {
		Komunikat << "Najpierw musisz nauczy� si� strzela� z �uku!" >> xZul;
		return false;
	}
	if( Typ == Kusza && !StanGry->Postac.Umiejetnosci["kusza"] ) {
		Komunikat << "Najpierw musisz nauczy� si� strzela� z kuszy!" >> xZul;
		return false;
	}
	if( StanGry->Postac.Ekwipunek.LewaDlon == -2 || StanGry->Postac.Ekwipunek.LewaDlon >= 0 ) {
		Komunikat << "Masz ju� co� w lewej d�oni!" >> xZul;
		return false;
	}
	StanGry->Postac.Ekwipunek.LewaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;

	Komunikat << "Trzymasz teraz w r�ce: " << Nazwa >> xBia;

	return true;
}

/****************************************************************************************************/

void POCISK::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Typ: " >> xBia << (Typ == Luk ? "strza�a" : "be�t");
	StdBuf >> xSza >> pozycja( 8, 6 ) << "Obra�enia: " >> xBia << Obrazenia.Begin << "-" << Obrazenia.End;
}

/****************************************************************************************************/

bool PANCERZ::Uzyj()
{
	short* miejsce;
	switch( MiejsceZalozenia ) {
		case 0: miejsce = &StanGry->Postac.Ekwipunek.Helm; break;
		case 1: miejsce = &StanGry->Postac.Ekwipunek.Zbroja; break;
		case 2: miejsce = &StanGry->Postac.Ekwipunek.Szata; break;
	}
	if( *miejsce >= 0 ) {
		switch( MiejsceZalozenia ) {
			case 0: Komunikat << "Nosisz ju� co� na g�owie." >> xZul; break;
			case 1: Komunikat << "Nosisz ju� jaki� pancerz." >> xZul; break;
			case 2: Komunikat << "Nosisz ju� jak�� szat�." >> xZul; break;
		}
		return false;
	}
	*miejsce = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
	StanGry->Postac.Ochrona += Ochrona;
	Komunikat << "Zak�adasz: " << Nazwa << "." >> xBia;
	return true;
}

/****************************************************************************************************/

void PANCERZ::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Typ: " >> xBia << (MiejsceZalozenia == Helm ? "he�m" : (MiejsceZalozenia == Zbroja ? "zbroja" : "szata"));
	StdBuf >> xSza >> pozycja( 8, 6 ) << "Ochrona: " >> xBia << "[" << Ochrona.Bron << ", " << Ochrona.Pociski << ", " << Ochrona.Magia << "]";
}

/****************************************************************************************************/

bool TARCZA::Uzyj()
{
	if( !StanGry->Postac.Umiejetnosci["tarcza"] )
	{
		Komunikat << "Nie potrafisz u�ywa� tarczy!" >> xZul;
		return false;
	}
	if( StanGry->Postac.Ekwipunek.LewaDlon == -2 || StanGry->Postac.Ekwipunek.LewaDlon >= 0 )
	{
		Komunikat << "Masz ju� co� w lewej d�oni!" >> xZul;
		return false;
	}
	StanGry->Postac.Ekwipunek.LewaDlon = StanGry->Postac.Ekwipunek.Plecak[ StanGry->Postac.Ekwipunek.WybranyPrzedmiot ].Typ;
	Komunikat << "Trzymasz teraz w lewej d�oni: " << Nazwa << "." >> xBia;
	return true;
}

/****************************************************************************************************/

void TARCZA::RysujOpis( POS pozycja )
{
	PRZEDMIOT::RysujOpis( pozycja );
	StdBuf >> xSza >> pozycja( 8, 5 ) << "Obrona: " >> xBia << Obrona.Begin << "-" << Obrona.End;
}

/****************************************************************************************************/

void DodajPrzedmiot( PRZEDMIOT* Przedmiot )
{
	Przedmioty.add( Przedmiot );
}

/****************************************************************************************************/

void AlokacjaPrzedmiotow()
{
	const RLGL::ELEMENT& relacja = DaneGry( "ITEMS" );
	PRZEDMIOT* p;
	PRZEDMIOT _p;
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		const RLGL::ELEMENT& przedmiot = relacja( i );

		_p.ID = przedmiot.GetName();
		_p.Nazwa = AS_STR przedmiot("NAME").GetValue();
		_p.Znak = przedmiot("SYMBOL")[0];
		_p.Kolor = przedmiot("SYMBOL")[1];
		_p.Waga = przedmiot("WEIGHT").GetValue();
		_p.Wartosc = przedmiot("VALUE").GetValue();

		string typ_przedmiotu = przedmiot("TYPE").GetValue();

		if( typ_przedmiotu == "relational" ) {
			RELACYJNY* p2 = new RELACYJNY;
			p2->DefRelacji = AS_STR przedmiot("RELATION").GetValue();
			p2->Sciana = przedmiot("CONST").GetValue();
			p = p2;
		}
		else if( typ_przedmiotu == "common" ) {
			ZWYKLY* p2 = new ZWYKLY;
			p = p2;
		}
		else if( typ_przedmiotu == "unarmed_weapon" || typ_przedmiotu == "one_handed_weapon" || typ_przedmiotu == "two_handed_weapon" ) {
			BRON_BIALA* p2 = new BRON_BIALA;
			p2->Obrazenia = SECTOR( przedmiot("DAMAGE")[0], przedmiot("DAMAGE")[1] );
			p2->WymaganaSila = przedmiot("REQUIRED_STRENGTH").GetValue();
			if( typ_przedmiotu == "unarmed_weapon" ) p2->Typ = Nareczna;
			else if( typ_przedmiotu == "one_handed_weapon" ) p2->Typ = Jednoreczny;
			else p2->Typ = Dworeczny;
			p = p2;
		}
		else if( typ_przedmiotu == "bow" || typ_przedmiotu == "crossbow" ) {
			BRON_DYSTANSOWA* p2 = new BRON_DYSTANSOWA;
			p2->Zasieg = przedmiot("RANGE").GetValue();
			p2->WymaganaZrecznosc = przedmiot("REQUIRED_DEXTERITY").GetValue();
			if( typ_przedmiotu == "bow" ) p2->Typ = Luk;
			else p2->Typ = Kusza;
			p = p2;
		}
		else if( typ_przedmiotu == "helmet" || typ_przedmiotu == "armor" || typ_przedmiotu == "robe" ) {
			PANCERZ* p2 = new PANCERZ;
			p2->Ochrona = OCHRONA( przedmiot("PROTECTION")[0], przedmiot("PROTECTION")[1], przedmiot("PROTECTION")[2] );
			if( typ_przedmiotu == "helmet" ) p2->MiejsceZalozenia = Helm;
			else if( typ_przedmiotu == "armor" ) p2->MiejsceZalozenia = Zbroja;
			else p2->MiejsceZalozenia = Szata;
			p = p2;
		}
		else if( typ_przedmiotu == "disposable" ) {
			JEDNORAZOWY* p2 = new JEDNORAZOWY;
			p2->TekstPrzyUzyciu = AS_STR przedmiot("REPORT").GetValue();
			for( unsigned i = 0; i < przedmiot("EFFECTS").Size(); i++ ) {
				p2->Wlasciwosci.Wlasciwosci.add( WLASCIWOSC() );
				p2->Wlasciwosci.Wlasciwosci.back().Zaladuj( przedmiot("EFFECTS")(i) );
			}
			p = p2;
		}
		else if( typ_przedmiotu == "rune" ) {
			RUNA* p2 = new RUNA;
			p2->Czar = przedmiot("SPELL").GetValue();
			p = p2;
		}
		else if( typ_przedmiotu == "scroll" ) {
			ZWOJ* p2 = new ZWOJ;
			p2->Czar = przedmiot("SPELL").GetValue();
			p = p2;
		}
		else if( typ_przedmiotu == "book" ) {
			KSIEGA* p2 = new KSIEGA;
			p2->Umiejetnosc = przedmiot("SKILL").GetValue();
			p = p2;
		}
		else if( typ_przedmiotu == "arrow" || typ_przedmiotu == "bolt" ) {
			POCISK* p2 = new POCISK;
			p2->Obrazenia = SECTOR( przedmiot("DAMAGE")[0], przedmiot("DAMAGE")[1] );
			if( typ_przedmiotu == "arrow" ) p2->Typ = Luk;
			else p2->Typ = Kusza;
			p = p2;
		}
		else if( typ_przedmiotu == "shield" ) {
			TARCZA* p2 = new TARCZA;
			p2->Obrona = SECTOR( przedmiot("PROTECTION")[0], przedmiot("PROTECTION")[1] );
			p = p2;	
		}

		TYP_PRZEDMIOTU t = p->TypPrzedmiotu;
		*p = _p;
		p->TypPrzedmiotu = t;
		DodajPrzedmiot( p );
	}

	for( WORD i = 0; i < Czary.size(); i++ ) 
	{
		Przedmioty.add( new ZWOJ( "scroll_" + Czary[i]->ID, "zw�j - " + Czary[i]->Nazwa, '~', Czary[i]->Urok.Animacja.Kolor, 1, Czary[i]->StopienTrudnosci / 2, i ) );
		Przedmioty.add( new RUNA( "rune_" + Czary[i]->ID, "runa - " + Czary[i]->Nazwa, 246, Czary[i]->Urok.Animacja.Kolor, 20, Czary[i]->StopienTrudnosci * 10, i ) );
	}

	if( IndexPrzedmiotu( "gold_coin" ) < 0 ) {
		DodajPrzedmiot( new ZWYKLY( "z�ota moneta", '.', xZul, 1, 1 ) );
		Przedmioty.back()->ID = "gold_coin";
	}
}

/****************************************************************************************************/

short SzukajPrzedmiotu( string Nazwa )
{
	for( WORD i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ]->Nazwa == Nazwa ) return i;

	return -1;
}

/****************************************************************************************************/

int IndexPrzedmiotu( string ID )
{
	for( BYTE i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[i]->ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/