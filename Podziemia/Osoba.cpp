
#include "stdafx.h"

/****************************************************************************************************/

array< OSOBA > Osoby;

/****************************************************************************************************/

void AlokacjaOsob()
{
	const RLGL::ELEMENT& relacja = DaneGry( "NPC" );
	RLGL::STRUCTURE s;
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		const RLGL::ELEMENT& osoba = relacja( i );
		POJEMNIK poj;
		for( WORD i = 0; osoba.Exist( "ITEMS", i ); i++ ) {
			const RLGL::ELEMENT& przedmioty = osoba( "ITEMS", i );
			short ilosc = 1;
			if( przedmioty.Size() > 1 ) {
				ilosc = ( przedmioty.Size() > 2 ? Losuj( przedmioty[1], przedmioty[2] ) : przedmioty[1] );
				if( ilosc < 0 ) ilosc = 0;
			}
			poj.DodajPrzedmiot( IndexPrzedmiotu( przedmioty[0] ), ilosc );
		}
		Osoby.add( OSOBA( osoba.GetName(), osoba("NAME")[0], osoba("SYMBOL")[0], osoba("SYMBOL")[1],
			OCHRONA( osoba("PROTECTION")[0], osoba("PROTECTION")[1], osoba("PROTECTION")[2] ),
			( osoba.Exist("HOSTILE") ? osoba("HOSTILE")[0] : 0 ), osoba("DAMAGE")[0], ( osoba.Exist("WILD") ? osoba("WILD")[0] : 5 ),
			( osoba.Exist("HP") ? osoba("HP")[0] : osoba("MAX_HP")[0] ), osoba("MAX_HP")[0], osoba("MOVE_TIME")[0], osoba("MOVE_RANGE")[0], osoba("ATTACK_RANGE")[0],
			AS_STR osoba("DIALOG")[0], poj, ( osoba.Exist("AUTO_DIALOG") ? osoba("AUTO_DIALOG")[0] : 0 ),
			( osoba.Exist("SPELL") ? IndexCzaru( osoba("SPELL")[0] ) : -1 ) ) );
	}
}

/****************************************************************************************************/

void OSOBA::PozostawTrofea()
{
	for( unsigned i = 0; i < Ekwipunek.Ilosc(); i++ )
		AktualnaMapa->DodajPrzedmiot( Ekwipunek[i].Typ, Ekwipunek[i].Ilosc, Pozycja );
}

/****************************************************************************************************/

void OSOBA::Zapisz( PLIK& Plik )
{
	POSTAC_NIEZALEZNA::Zapisz( Plik );

	Plik << Obrazenia
		 << MaxZycie
		 << Czas1Ruchu
		 << Rozmowa
		 << TekstStartowy
		 << ZasiegChronionegoPola;

	Ekwipunek.Zapisz( Plik );

	Plik << Relacja.size();
	for( WORD i = 0; i < Relacja.size(); i++ ) {
		Plik << Relacja[i].size();
		for( WORD j = 0; j < Relacja[i].size(); j++ ) Plik << Relacja[i][j];
	}
}

/****************************************************************************************************/

void OSOBA::Wczytaj( PLIK& Plik )
{
	POSTAC_NIEZALEZNA::Wczytaj( Plik );

	Plik >> Obrazenia
		 >> MaxZycie
		 >> Czas1Ruchu
		 >> Rozmowa
		 >> TekstStartowy
		 >> ZasiegChronionegoPola;

	Ekwipunek.Wczytaj( Plik );

	unsigned rozmiar;
	Plik >> rozmiar;
	Relacja.resize(rozmiar);
	for( WORD i = 0; i < Relacja.size(); i++ ) {
		Plik >> rozmiar;
		Relacja[i].resize(rozmiar);
		for( WORD j = 0; j < Relacja[i].size(); j++ ) Plik >> Relacja[i][j];
	}
}

/****************************************************************************************************/

int IndexOsoby( string ID )
{
	for( BYTE i = 0; i < Osoby.size(); i++ )
		if( Osoby[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/