#include "stdafx.h"

/****************************************************************************************************/

array< CZAR* > Czary;

/****************************************************************************************************/

void CZAR::RysujPasek( POS Pozycja, UCHAR Szerokosc )
{
	if( Szerokosc < 24 ) Szerokosc = 24;

	StdBuf.UstawPozycjePisania( Pozycja( 0, 0 ) );
	StdBuf.Napisz( ZWielkiejLitery( Nazwa ), 0, Szerokosc - 24 );
	StdBuf >> Pozycja( Szerokosc - 6, 0 ) << KosztRzucenia;
}

/****************************************************************************************************/

int IndexCzaru( string ID )
{
	for( BYTE i = 0; i < Czary.size(); i++ )
		if( Czary[i]->ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/