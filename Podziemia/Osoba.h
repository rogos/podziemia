#pragma once

/****************************************************************************************************/

#include "stdafx.h"
#include "Przedmioty.h"

/****************************************************************************************************/

struct OSOBA : POSTAC_NIEZALEZNA
{
	WORD MaxZycie;
	UCHAR Czas1Ruchu;
	WORD Obrazenia;

	POJEMNIK Ekwipunek;

	string Rozmowa;
	array< array<bool> > Relacja;
	UCHAR TekstStartowy;

	BYTE ZasiegChronionegoPola; // np. stra�nik mo�e pilnowa� wej�cia do magazynu

	OSOBA()
	{
		Czar = -1;
		WrogoNastawiony = false;
		Typ = TPO_OSOBA;
		Obrazenia = MaxZycie = Czas1Ruchu = 0;
		Rozmowa = "";
		TekstStartowy = 0;
		ZasiegChronionegoPola = 0;
	}

	OSOBA( string _ID, string _Nazwa, char _Znak, WORD _Kolor, OCHRONA& _Ochrona, bool _WrogoNastawiony,
		   WORD _Obrazenia, BYTE _Dzikosc, WORD _Zycie, WORD _MaxZycie, BYTE _Czas1Ruchu, BYTE _ZasiegRuchu, BYTE _ZasiegAtaku,
		   string _Rozmowa, const POJEMNIK& _Ekwipunek, BYTE _ZasiegChronionegoPola = 0, short _Czar = -1 )
		{
			ID = _ID;
			Nazwa = _Nazwa;
			Znak = _Znak;
			Kolor = _Kolor;
			Ochrona = _Ochrona;
			Tura = 0;
			Zycie = _Zycie;
			MaxZycie = _MaxZycie;
			Czas1Ruchu = _Czas1Ruchu;
			ZasiegRuchu = _ZasiegRuchu;
			ZasiegAtaku = _ZasiegAtaku;
			Obrazenia = _Obrazenia;
			Dzikosc = _Dzikosc;
			TekstStartowy = 0;
			WrogoNastawiony = _WrogoNastawiony;
			Typ = TPO_OSOBA;
			Rozmowa = _Rozmowa;
			ZasiegChronionegoPola = _ZasiegChronionegoPola;
			Ekwipunek = _Ekwipunek;
			Czar = _Czar;
		}

	WORD PobMaxZycie() { return MaxZycie; }
	UCHAR PobCzas1Ruchu() { return Czas1Ruchu; }
	WORD PobObrazenia() { return Obrazenia; }

	void PozostawTrofea();

	void DoEnergii( double Ile ) {}
	void DoPragnienia( double Ile ) {}
	void DoGlodu( double Ile ) {}

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

extern array< OSOBA > Osoby;

int IndexOsoby( string ID );

void AlokacjaOsob();

/****************************************************************************************************/