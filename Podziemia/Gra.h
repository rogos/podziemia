#pragma once

#include "stdafx.h"

/****************************************************************************************************/

class GRA
{
private:
	bool Zakonczona;
	bool WczytajGre();
	bool Graj();
	void StworzMapy();
	bool RysujGlowneMenu();
	void RysujGre( bool komunikaty = true );
	bool Przetwarzaj( char Przycisk );
	bool TworzeniePostaci();
	void Koniec();

public:
	void Start( string plik_swiata_gry );
	bool Gra();

	~GRA() {
		for( unsigned i = 0; i < Przedmioty.size(); i++ ) delete Przedmioty[i];
	}
};

/****************************************************************************************************/

void UzyjPrzejscia( PRZEJSCIE& przejscie );

/****************************************************************************************************/

extern MAPA* AktualnaMapa;

/****************************************************************************************************/