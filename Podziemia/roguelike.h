#pragma once

#include <wincon.h>
#include <windef.h>
#include <ctime>

#ifdef __cplusplus
extern "C++" {
#endif
using namespace std;

inline int Random(int Min, int Max)
{
	static bool First = true;
	if(First)
	{
		srand(static_cast<unsigned>(time(NULL)));
		First = false;
	}
	if(Min == Max) return Min;
	return rand() % (Max - Min + 1) + Min;
}

#pragma warning(disable: 4996)

inline string _itostr(int Number)
{
	char* _Text=new char[65];
	string Text;
	_itoa(Number,_Text,10);
	Text=_Text;
	delete[] _Text;
	return Text;
}

typedef struct _POS
{
	short X;
	short Y;

	_POS()
	{
		X = 0;
		Y = 0;
	}

	_POS( WORD _X, WORD _Y )
	{
		X = _X;
		Y = _Y;
	}

	_POS Set( short NewX, short NewY )
	{
		_POS old( X, Y );
		X = NewX;
		Y = NewY;
		return old;
	}

	_POS operator () ( short AddToX, short AddToY )
	{
		return _POS( X + AddToX, Y + AddToY );	
	}

	_POS operator + ( const _POS& Pos )
	{
		return _POS( X + Pos.X, Y + Pos.Y );	
	}
	
	_POS operator - ( const _POS& Pos )
	{
		return _POS( X - Pos.X, Y - Pos.Y );	
	}

	void operator += ( const _POS& Pos )
	{
		X += Pos.X;
		Y += Pos.Y;
	}

	void operator -= ( const _POS& Pos )
	{
		X -= Pos.X;
		Y -= Pos.Y;
	}

	bool operator == ( const _POS& Pos )
	{
		if( X == Pos.X && Y == Pos.Y ) return true;
		return false;
	}

	operator COORD()
	{
		COORD c = { X, Y };
		return c;
	}
}POS,*PPOS;

const POS POS_NOT_FOUND = POS( -32768, -32768 );

enum DIRECTION
{
	NW = 7,  N  = 8,  NE = 9,
	W  = 4,  C  = 5,  E  = 6,
	SW = 1,  S  = 2,  SE = 3
};

const POS VALUES_OF_DIRECTIONS[ 9 ] =
{
	POS( -1, +1 ),
	POS( +0, +1 ),
	POS( +1, +1 ),
	POS( -1, +0 ),
	POS( +0, +0 ),
	POS( +1, +0 ),
	POS( -1, -1 ),
	POS( +0, -1 ),
	POS( +1, -1 )
};

template< typename TYPE = WORD > struct _SECTOR
{
	TYPE Begin;
	TYPE End;
	_SECTOR()
	{
		Begin = 0;
		End = 0;
	}
	_SECTOR(TYPE _Begin, TYPE _End)
	{
		Begin = _Begin;
		End = _End;
	}
	_SECTOR Set(TYPE NewBegin, TYPE NewEnd)
	{
		_SECTOR< TYPE > Old(Begin, End);
		Begin = NewBegin;
		End = NewEnd;
		return Old;
	}
	_SECTOR operator() (TYPE AddToBegin, TYPE AddToEnd)
	{
		_SECTOR Returning(Begin + AddToBegin, End + AddToEnd);
		return Returning;	
	}
	WORD Rand()
	{
		return Random( Begin, End );
	}
};
typedef _SECTOR<> SECTOR;
typedef _SECTOR< double > FSECTOR;

#ifdef __cplusplus
}
#endif