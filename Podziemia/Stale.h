#pragma once

#include "stdafx.h"

const string Tytul = "...::: Podziemia :::... v.1.0";
const string Strona = "http://www.podziemia.go.pl";

const WORD IloscPoziomow = 50;

const UCHAR SzerokoscMapy = 78;
const UCHAR WysokoscMapy = 35;
const WORD RozmiarMapy = SzerokoscMapy * WysokoscMapy;

const POS SRODEK_RYSOWANIA = POS( 55, 17 );

enum KIERUNEK { Poziomo, Pionowo };

#define xCza (0)
#define xcNie (1)
#define xcZie (2)
#define xcTur (3)
#define xcCze (4)
#define xcFio (5)
#define xcZul (6)
#define xSza (7)
#define xcSza (8)
#define xNie (9)
#define xZie (10)
#define xTur (11)
#define xCze (12)
#define xFio (13)
#define xZul (14)
#define xBia (15)

#define tCza (16*0)
#define tcNie (16*1)
#define tcZie (16*2)
#define tcTur (16*3)
#define tcCze (16*4)
#define tcFio (16*5)
#define tcZul (16*6)
#define tSza (16*7)
#define tcSza (16*8)
#define tNie (16*9)
#define tZie (16*10)
#define tTur (16*11)
#define tCze (16*12)
#define tFio (16*13)
#define tZul (16*14)
#define tBia (16*15)

typedef enum TYP_PANCERZA { Helm, Zbroja, Szata };
typedef enum TYP_BRONI_BIALEJ { Jednoreczny, Dworeczny, Nareczna };
typedef enum TYP_BRONI_DYSTANSOWEJ { Luk, Kusza };
typedef enum PLEC { BrakPlci, Mezczyzna, Kobieta };

const string Plcie[3] = {
	"",
	"M�czyzna",
	"Kobieta"
};