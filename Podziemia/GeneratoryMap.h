#pragma once

#include "stdafx.h"

/****************************************************************************************************/

struct DEF_KLOCKA
{
	string Mapka;
	bool N;
	bool E;
	bool S;
	bool W;
	BYTE SumaPrzejsc();
	void Obroc();
};

extern const BYTE ILOSC_KLOCKOW;
extern DEF_KLOCKA KLOCKI[];

/****************************************************************************************************/

struct KLOCEK {
	bool Zakryte;
	bool WZasiegu;
	union {
		struct {
			bool N;
			bool E;
			bool S;
			bool W;
		};
		bool Wyjscia[4];
	};
	KLOCEK() : Zakryte(false), WZasiegu(false), W(false), E(false), N(false), S(false) {}
};

/****************************************************************************************************/

struct POKOJ
{
	bool DzielonyPoziomo;
	POS Pozycja;
	POS Rozmiar;
};

/****************************************************************************************************/

bool GenerujMape( const RLGL::ELEMENT& def_mapy, MAPA_GENEROWANA& mapa, POS pozycja = POS(0, 0) );

BYTE LosujKafelek( const RLGL::ELEMENT& def_stylu );

bool PodzielPokoj( bool poziomo, SECTOR pozycja_sciany, array<POKOJ>& pokoje, BYTE indeks_pokoju );
bool GanerujMapePokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, BYTE ilosc_pokoi, const RLGL::ELEMENT& def_stylu );
bool GanerujMapePojedynczychPokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, BYTE ilosc_pokoi, const RLGL::ELEMENT& def_stylu, POS min_czesc_pokoju = POS(100, 100) );
bool GanerujMapeJaskini( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu );
bool GanerujMapeLabiryntu( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu );
bool GanerujMapeRegularnychPokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu );
bool GanerujMapeZPliku( MAPA_GENEROWANA& mapa, POS pozycja, const string adres_pliku );
bool GanerujMapeZeStringa( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& symbole_kafelkow, const RLGL::ELEMENT& symbole_objektow, string kafelki, string objekty, const RLGL::ELEMENT& def_stylu = RLGL::STRUCTURE() );

void WstawPrzedmioty( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD przedmiot, short ilosc );
void WstawPostacie( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD postac, short ilosc );
void WstawOsobe( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD osoba );

bool ZlaczMapy( const RLGL::ELEMENT& def_polaczenia );

/****************************************************************************************************/