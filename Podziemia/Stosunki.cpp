#include "stdafx.h"

/****************************************************************************************************/

bool RELACJA::Laduj( const RLGL::ELEMENT& relacja )
{
	for( WORD i = 0; relacja.Exist( "RELATION", i ); i++ )
	{
		const RLGL::ELEMENT& d = relacja( "RELATION", i );
		DZIAL dzial;

		for( WORD j = 0; d.Exist( "TEXT", j ); j++ )
			dzial.Teksty.add( d( "TEXT", j ).GetValue() );

		for( WORD j = 0; d.Exist( "OPTION", j ); j++ )
		{
			OPCJA opcja = {
				( d( "OPTION", j ).Exist( "IF" ) ? *dynamic_cast< const RLGL::STRUCTURE* >( &d( "OPTION", j )( "IF" ) ) : RLGL::STRUCTURE() ),
				*dynamic_cast< const RLGL::STRUCTURE* >( &d( "OPTION", j )( "EFFECTS" ) ),
				d( "OPTION", j )( "CONTENT" ).GetValue(),
				d( "OPTION", j ).Exist( "UNAVAILABLE" ),
				d( "OPTION", j ).Exist( "INVISIBLE" )
			};
			dzial.Opcje.add( opcja );
			
		}

		Dzialy.add( dzial );
	}
	return true;
}

/****************************************************************************************************/
/****************************************************************************************************/

void RELACJONER::Laduj( const RELACJA* def_relacji, BOHATER* bohater, OSOBA* osoba, string nazwa_opcji )
{
	pDefRelacji = def_relacji;
	pOsoba = osoba;
	pRelacja = &pOsoba->Relacja;
	pBohater = bohater;

	if( pRelacja->size() < pDefRelacji->Dzialy.size() )
		*pRelacja = array< array<bool> >( 0, pDefRelacji->Dzialy.size() );

	for( unsigned i = 0; i < pOsoba->Relacja.size(); i++ )
		if( (*pRelacja)[i].size() < pDefRelacji->Dzialy[i].Opcje.size() )
		{
			(*pRelacja)[i] = array<bool>( 0, pDefRelacji->Dzialy[i].Opcje.size() );
			for( unsigned j = 0; j < pDefRelacji->Dzialy[i].Opcje.size(); j++ )
				(*pRelacja)[i][j] = ( pDefRelacji->Dzialy[i].Opcje[j].Niedostepna - 1 ) * -1;
		}

	PrzetwarzajOpcje( nazwa_opcji );
}

/****************************************************************************************************/

void RELACJONER::Laduj( const RELACJA* def_relacji, BOHATER* bohater, array< array<bool> >* relacja, string nazwa_opcji )
{
	pDefRelacji = def_relacji;
	pRelacja = relacja;
	pBohater = bohater;

	if( pRelacja->size() < pDefRelacji->Dzialy.size() )
		*pRelacja = array< array<bool> >( 0, pDefRelacji->Dzialy.size() );

	for( unsigned i = 0; i < pRelacja->size(); i++ )
		if( (*pRelacja)[i].size() < pDefRelacji->Dzialy[i].Opcje.size() )
		{
			(*pRelacja)[i] = array<bool>( 0, pDefRelacji->Dzialy[i].Opcje.size() );
			for( unsigned j = 0; j < pDefRelacji->Dzialy[i].Opcje.size(); j++ )
				(*pRelacja)[i][j] = ( pDefRelacji->Dzialy[i].Opcje[j].Niedostepna - 1 ) * -1;
		}

	PrzetwarzajOpcje( nazwa_opcji );
}

/****************************************************************************************************/

void RELACJONER::Rysuj( WORD kolor_ramki )
{
	StdBuf.Czysc();
	AktualnaMapa->Rysuj();
	StanGry->Postac.Rysuj();
	Komunikat();
	StanGry->RysujPanel();
	for( WORD i = 0; i < 28; i++ ) StdBuf.Czysc( POS( 6, i + 6 ), 68 );
	RysujRamke( POS( 6, 6 ), POS( 68, 20 ), kolor_ramki, true );
	RysujRamke( POS( 6, 26 ), POS( 68, 8 ), kolor_ramki, false );
	StdBuf.UstawKolorPisania( xBia );
	StdBuf.NapiszIZawijaj( pDefRelacji->Dzialy[ Dzial ].Teksty[ Tekst ], 64, POS( 8, 8 ) );

	StdBuf.WstawZnak( 26, xBia, POS( 7, 29 ) );
	int _opcja = -Opcja;
	for( short i = -Opcja; _opcja < 3; i++ )
	{
		if( Opcja + i >= pDefRelacji->Dzialy[ Dzial ].Opcje.size() ) break;
		if( !(*pRelacja)[ Dzial ][ Opcja + i ] || pDefRelacji->Dzialy[ Dzial ].Opcje[ Opcja + i ].Niewidoczna ||
			!SprawdzWarunki( Opcja + i ) ) continue;

		if( _opcja >= -1 ) StdBuf >> xBia >> POS( 8, 29 + _opcja ) << pDefRelacji->Dzialy[ Dzial ].Opcje[ Opcja + i ].Tresc;
		_opcja++;
	}

	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool RELACJONER::Przetwarzaj( WORD znak )
{
	switch( znak )
	{
		case 1072: case '8': {
			if( Opcja > 0 ) Opcja--;
			break;
		}
		case 1080: case '2': {
			WORD ilosc = 0;
			for( unsigned i = 0; i < pDefRelacji->Dzialy[ Dzial ].Opcje.size(); i++ )
				if( (*pRelacja)[ Dzial ][ i ] && !pDefRelacji->Dzialy[ Dzial ].Opcje[ i ].Niewidoczna && SprawdzWarunki( i ) ) ilosc++; 
			if( Opcja + 1 < ilosc ) Opcja++;
			break;
		}
		case 13: {
			WORD opcja = 0;
			for( int i = -1; i < Opcja; opcja++ )
				if( (*pRelacja)[ Dzial ][ opcja ] && SprawdzWarunki( opcja ) && !pDefRelacji->Dzialy[ Dzial ].Opcje[ opcja ].Niewidoczna ) i++;
			opcja--;
			if( PrzetwarzajOpcje( opcja ) ) return true;
		}
	}
	return false;
}

/****************************************************************************************************/

bool RELACJONER::SprawdzWarunki( WORD opcja )
{
	if( !(*pRelacja)[ Dzial ][ opcja ] ) return false;
	const RLGL::ELEMENT& warunki = pDefRelacji->Dzialy[ Dzial ].Opcje[ opcja ].Warunki;

	for( unsigned i = 0; i < warunki.Size(); i++ )
	{
		const RLGL::ELEMENT& warunek = warunki( i );

		if( warunek.GetName() == "JOURNAL" ) {
			if( warunek.Size() > 2 )
			{
				if( AS_STR warunek[2] == "not_exist" || AS_STR warunek[2] == "exist" ) {
					if( pBohater->Dziennik.IstniejeWpis( warunek[0], warunek[1] ) == ( AS_STR( warunek[2] ) == "not_exist" ) ) return false;
				}
				else {
					if( !pBohater->Dziennik.IstniejeWpis( warunek[0], warunek[1] ) ||
						pBohater->Dziennik.WartoscWpisu( warunek[0], warunek[1] ) != AS_INT warunek[2] ) return false;
				}
			}
			else if( !pBohater->Dziennik.IstniejeWpis( warunek[0], warunek[1] ) ) return false;
		}

		else if( warunek.GetName() == "ITEM" ) {
			if( pBohater->Ekwipunek.Plecak.IloscPrzedmiotow( IndexPrzedmiotu( warunek[0] ) ) < AS_INT warunek[1] ) return false;
		}

		else if( warunek.GetName() == "POINTS" ) {
			if( pBohater->Punkty < AS_INT warunek[0] ) return false;
		}

		else if( warunek.GetName() == "SKILL" ) {
			if( pBohater->Umiejetnosci[ AS_STR warunek[0] ] < AS_INT warunek[1] ) return false;
		}
	}
	return true;
}

/****************************************************************************************************/

bool RELACJONER::PrzetwarzajOpcje( WORD opcja )
{
	const RLGL::ELEMENT& skutki = pDefRelacji->Dzialy[ Dzial ].Opcje[ opcja ].Skutki;

	for( unsigned i = 0; i < skutki.Size(); i++ )
	{
		const RLGL::ELEMENT& funkcja = skutki( i );
		
		if( funkcja.GetName() == "END" ) {
			End = true;
			return true;
		}

		else if( funkcja.GetName() == "GO" ) {
			IdzDo( funkcja[ 0 ], funkcja[ 1 ] );
		}

		else if( funkcja.GetName() == "ADD_OPTION" ) {
			(*pRelacja)[ Dzial ][ funkcja[ 0 ] ] = true;
		}

		else if( funkcja.GetName() == "DELETE_OPTION" ) {
			if( AS_STR funkcja[ 0 ] == "this" ) (*pRelacja)[ Dzial ][ opcja ] = false;
			else (*pRelacja)[ Dzial ][ AS_INT funkcja[ 0 ] ] = false;
		}

		else if( funkcja.GetName() == "SELECT" ) {
			IdzDo( funkcja[ 0 ], 0 );
			PrzetwarzajOpcje( AS_STR funkcja[ 1 ] );
		}

		/************************************************/

		else if( funkcja.GetName() == "DELETE_ITEM" ) {
			pBohater->Ekwipunek.Plecak.UsunPrzedmiot( IndexPrzedmiotu( funkcja[ 0 ] ), ( funkcja.Size() < 2 ? 1 : funkcja[ 1 ] ) );
		}

		else if( funkcja.GetName() == "ADD_ITEM" ) {
			pBohater->Ekwipunek.Plecak.DodajPrzedmiot( IndexPrzedmiotu( funkcja[ 0 ] ), ( funkcja.Size() < 2 ? 1 : funkcja[ 1 ] ) );
		}

		else if( funkcja.GetName() == "ADD_POINTS" ) {
			pBohater->Punkty += AS_INT funkcja[ 0 ];
		}

		else if( funkcja.GetName() == "TAKE_POINTS" ) {
			pBohater->Punkty -= AS_INT funkcja[ 0 ];
		}

		else if( funkcja.GetName() == "EXP" ) {
			DoDoswiadczenia( funkcja[ 0 ] );
		}

		else if( funkcja.GetName() == "ADD_TO_SKILL" ) {
			pBohater->Umiejetnosci[ AS_STR funkcja[0] ] += AS_INT funkcja[1];
		}

		else if( funkcja.GetName() == "ITEM_EFFECTS" ) {
			UROK u;
			for( unsigned i = 0; i < funkcja.Size(); i++ ) {
				u.Wlasciwosci.add( WLASCIWOSC() );
				u.Wlasciwosci.back().Zaladuj( funkcja(i) );
			}
			pBohater->Uroki.add( u );
		}

		else if( funkcja.GetName() == "WIN" ) {
			StanGry->StanPostaci = SP_ZWYCIEZYL;
			End = true;
			return true;
		}

		else if( funkcja.GetName() == "JOURNAL" ) {
			pBohater->Dziennik.DodajWpis( funkcja[0], funkcja[1] );
			if(funkcja.Size() > 2 && AS_STR funkcja[2] == "DONE") pBohater->Dziennik.UstawJakoWykonane( funkcja[0] );
		}

		else if( funkcja.GetName() == "TELEPORT" ) {
			PRZEJSCIE p = { POS(), IndexMapy(funkcja[0]),
				StanGry->Mapy[ IndexMapy(funkcja[0]) ].PozycjaObok( StanGry->Mapy[ IndexMapy(funkcja[0]) ].PozycjaPrzedmiotu( IndexPrzedmiotu(funkcja[1]) ) ), 0 };
			UzyjPrzejscia( p );
		}

		else if( funkcja.GetName() == "MOVE_BACK" && pOsoba ) {
			POS p = AktualnaMapa->PozycjaDalejOd( pOsoba->Pozycja, pBohater->Pozycja );
			if( !(p == POS_NOT_FOUND) ) pBohater->Pozycja = p;
		}

		else if( funkcja.GetName() == "REPORT" ) {
			Komunikat << pOsoba->Nazwa << ": \"" << AS_STR funkcja[ 0 ] << "\"" >> xFio;
		}

		else if( funkcja.GetName() == "REPORT2" ) {
			Komunikat << AS_STR funkcja[ 0 ] >> xBia;
		}

		else if( funkcja.GetName() == "TRADE" && pOsoba ) {
			HANDEL handel;
			handel.Laduj( &pBohater->Ekwipunek.Plecak, &pOsoba->Ekwipunek );
			for(;;) {
				handel.Rysuj();
				if( handel.Przetwarzaj(PobierzKlawisz()) ) break; 
			}
		}

		else if( funkcja.GetName() == "FIGHT" && pOsoba ) {
			pOsoba->WrogoNastawiony = true;
			End = true;
			return true;
		}
	}
	Opcja = 0;
	return true;
}

/****************************************************************************************************/

bool RELACJONER::PrzetwarzajOpcje( string opcja )
{
	for( unsigned i = 0; i < pDefRelacji->Dzialy[Dzial].Opcje.size(); i++ )
	{
		if( pDefRelacji->Dzialy[Dzial].Opcje[i].Tresc == opcja && SprawdzWarunki( i ) )
		{
			if( PrzetwarzajOpcje( i ) ) return true;
			return false;
		}
	}
	return false;
}

/****************************************************************************************************/

bool RELACJONER::IdzDo( WORD dzial, WORD tekst )
{
	if( dzial >= pDefRelacji->Dzialy.size() || tekst >= pDefRelacji->Dzialy[ dzial ].Teksty.size() ) return false;
	Dzial = dzial;
	Tekst = tekst;
	return true;
}

/****************************************************************************************************/
/****************************************************************************************************/

void HANDEL::Laduj( POJEMNIK* przedmioty_kupca, POJEMNIK* przedmioty_sprzedawcy )
{
	PrzedmiotyKupca = przedmioty_kupca;
	PrzedmiotySprzedawcy = przedmioty_sprzedawcy;
}

/****************************************************************************************************/

bool HANDEL::Zakoncz()
{
	return true;
}

/****************************************************************************************************/

void HANDEL::Rysuj()
{
	StdBuf.Czysc();
	AktualnaMapa->Rysuj();
	StanGry->Postac.Rysuj();
	Komunikat();
	StanGry->RysujPanel();
	for( WORD i = 0; i < 28; i++ ) StdBuf.Czysc( POS( 6, i + 6 ), 68 );
	RysujRamke( POS( 6, 6 ), POS( 68, 14 ), ( Lista == L_SPRZEDAWCY ? xZie : xcZie ), false );
	RysujRamke( POS( 6, 20 ), POS( 68, 14 ), ( Lista == L_KUPCA ? xZie : xcZie ), false );
	StdBuf >> xBia >> POS( 10, 8 ) << "Przedmiot:                                        Warto��:";
	for( short i = -PozycjaKupna; i < 7; i++ )
	{
		if( i < -2 ) i = -2;
		if( i + PozycjaKupna + 1 > PrzedmiotySprzedawcy->Ilosc() ) break;
		StdBuf.WstawZnak( Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Typ ]->Znak, Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Typ ]->Kolor, POS( 8, 11 + i ), false );
		StdBuf >> xSza >> POS( 10, 11 + i );
		if( (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Ilosc > 1 ) StdBuf << (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Ilosc << "x ";
		StdBuf << Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Typ ]->Nazwa;
		StdBuf >> xZul >> POS( 60, 11 + i ) << Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna + i ].Typ ]->Wartosc;
	}
	StdBuf >> xBia >> POS( 10, 22 ) << "Przedmiot:                                        Warto��:";
	for( short i = -PozycjaSprzedazy; i < 7; i++ )
	{
		if( i < -2 ) i = -2;
		if( i + PozycjaSprzedazy + 1 > PrzedmiotyKupca->Ilosc() ) break;
		StdBuf.WstawZnak( Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Typ ]->Znak, Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Typ ]->Kolor, POS( 8, 25 + i ), false );
		StdBuf >> xSza >> POS( 10, 25 + i );
		if( (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Ilosc > 1 ) StdBuf << (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Ilosc << "x ";
		StdBuf << Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Typ ]->Nazwa;
		StdBuf >> xZul >> POS( 60, 25 + i ) << ( Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy + i ].Typ ]->Wartosc + 1 ) / 2;
	}
	StdBuf.WstawZnak( 26, xBia, (Lista == L_SPRZEDAWCY ? POS( 7, 11 ) : POS( 7, 25 )) );
	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool HANDEL::Przetwarzaj( WORD znak )
{
	switch( znak )
	{
		case 'd': case 'D': {
			if( Lista == L_KUPCA ) { if( PrzedmiotyKupca->Ilosc() > 0 ) Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy ].Typ ]->RysujOpis( POS( 20, 12 ) ); }
			else if( PrzedmiotySprzedawcy->Ilosc() > 0 ) Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna ].Typ ]->RysujOpis( POS( 20, 12 ) );
			else break;
			StdBuf.Rysuj();
			for(;;) {
				switch( _getch() ) {
					case 'd': case 'D': case 27: case 32: break;
					default: continue;
				}
				break;
			}
			break;
		}
		case 1072: case '8': {
			WORD& pozycja = Lista == L_KUPCA ? PozycjaSprzedazy : PozycjaKupna;
			if( pozycja > 0 ) pozycja--;
			break;
		}
		case 1080: case '2': {
			unsigned ilosc = Lista == L_KUPCA ? PrzedmiotyKupca->Ilosc() : PrzedmiotySprzedawcy->Ilosc();
			WORD& pozycja = Lista == L_KUPCA ? PozycjaSprzedazy : PozycjaKupna;
			if( pozycja < signed( ilosc - 1 ) ) pozycja++;
			break;
		}
		case 1075: case '4': if( Lista == L_KUPCA ) Lista = L_SPRZEDAWCY; break;
		case 1077: case '6': if( Lista == L_SPRZEDAWCY ) Lista = L_KUPCA; break;
		case 13:
		{
			if( Lista == L_KUPCA )
			{
				if( PrzedmiotyKupca->Ilosc() == 0 ) break;
				WORD koszt = ( Przedmioty[ (*PrzedmiotyKupca)[ PozycjaSprzedazy ].Typ ]->Wartosc + 1 ) / 2;
				if( PrzedmiotySprzedawcy->IloscPrzedmiotow( IndexPrzedmiotu("gold_coin") ) >= koszt )
				{
					PrzedmiotyKupca->DodajPrzedmiot( IndexPrzedmiotu("gold_coin"), koszt );
					PrzedmiotySprzedawcy->UsunPrzedmiot( IndexPrzedmiotu("gold_coin"), koszt );
					PrzedmiotySprzedawcy->DodajPrzedmiot( (*PrzedmiotyKupca)[ PozycjaSprzedazy ].Typ, 1 );
					PrzedmiotyKupca->UsunPrzedmiotNa( PozycjaSprzedazy, 1 );
					if( PozycjaSprzedazy >= PrzedmiotyKupca->Ilosc() && PozycjaSprzedazy > 0 ) PozycjaSprzedazy--;
				}
			}
			else
			{
				if( PrzedmiotySprzedawcy->Ilosc() == 0 ) break;
				WORD koszt = Przedmioty[ (*PrzedmiotySprzedawcy)[ PozycjaKupna ].Typ ]->Wartosc;
				if( PrzedmiotyKupca->IloscPrzedmiotow( IndexPrzedmiotu("gold_coin") ) >= koszt )
				{
					PrzedmiotySprzedawcy->DodajPrzedmiot( IndexPrzedmiotu("gold_coin"), koszt );
					PrzedmiotyKupca->UsunPrzedmiot( IndexPrzedmiotu("gold_coin"), koszt );
					PrzedmiotyKupca->DodajPrzedmiot( (*PrzedmiotySprzedawcy)[ PozycjaKupna ].Typ, 1 );
					PrzedmiotySprzedawcy->UsunPrzedmiotNa( PozycjaKupna, 1 );
					if( PozycjaKupna >= PrzedmiotySprzedawcy->Ilosc() && PozycjaKupna > 0 ) PozycjaKupna--;
				}
			}
			break;
		}
		case 32: case 27: if( Zakoncz() ) return true; break;
	}
	return false;
}

/****************************************************************************************************/