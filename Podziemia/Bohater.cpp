#include "stdafx.h"

/****************************************************************************************************/

array<UMIEJETNOSC> Umiejetnosci;
vector<DEF_TEMATU> TematyDziennika;

/****************************************************************************************************/

short IndexUmiejetnosci( string ID )
{
	for( BYTE i = 0; i < Umiejetnosci.size(); i++ )
		if( Umiejetnosci[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/

void InicjalizujBohatera()
{
	const RLGL::ELEMENT& relacja = DaneGry("SKILLS");
	for( WORD i = 0; i < relacja.Size(); i++ )
		Umiejetnosci.add( UMIEJETNOSC( relacja( i ).GetName(), AS_STR relacja( i ).GetValue() ) );

	if( IndexUmiejetnosci( "bron_jednoreczna" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "bron_jednoreczna", "bro� jednor�czna" ) );
	if( IndexUmiejetnosci( "bron_dwureczna" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "bron_dwureczna", "bro� dwur�czna" ) );
	if( IndexUmiejetnosci( "luk" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "luk", "�uki" ) );
	if( IndexUmiejetnosci( "kusza" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "kusza", "kusze" ) );
	if( IndexUmiejetnosci( "dwie_bronie" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "dwie_bronie", "dwie bronie" ) );
	if( IndexUmiejetnosci( "tarcza" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "tarcza", "tarcza" ) );
	if( IndexUmiejetnosci( "walka_wrecz" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "walka_wrecz", "walka wr�cz" ) );
	if( IndexUmiejetnosci( "skradanie" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "skradanie", "skradanie" ) );
	if( IndexUmiejetnosci( "magia" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "magia", "magia" ) );
	if( IndexUmiejetnosci( "trofea" ) < 0 ) Umiejetnosci.add( UMIEJETNOSC( "trofea", "trofea" ) );

	const RLGL::ELEMENT& tematy = DaneGry("JOURNAL");
	TematyDziennika.resize( tematy.Size() );
	for( WORD i = 0; i < tematy.Size(); i++ ) {
		TematyDziennika[i].ID = tematy(i).GetName();
		TematyDziennika[i].Tytul = AS_STR tematy(i)("TITLE").GetValue();
		const RLGL::ELEMENT& wpisy = tematy(i)("ENTRIES");
		TematyDziennika[i].Wpisy.resize( wpisy.Size() );
		for( WORD j = 0; j < wpisy.Size(); j++ ) {
			TematyDziennika[i].Wpisy[j].ID = wpisy(j).GetName();
			TematyDziennika[i].Wpisy[j].Tresc = AS_STR wpisy(j).GetValue();
		}
	}
}

/****************************************************************************************************/

BYTE& UMIEJETNOSCI::operator [] ( string id )
{
	return Wartosci[ IndexUmiejetnosci( id ) ];
}

BYTE& UMIEJETNOSCI::operator [] ( WORD index )
{
	return Wartosci[ index ];
}

/****************************************************************************************************/

short IndexTematu( string ID )
{
	for( WORD i = 0; i < TematyDziennika.size(); i++ )
		if( TematyDziennika[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/

DZIENNIK::TEMAT& DZIENNIK::operator [] ( unsigned temat )
{
	return Tematy[ temat ];
}

/*..................................................................................................*/

bool DZIENNIK::UstawJakoWykonane( string temat )
{
	for( WORD i = 0; i < Tematy.size(); i++ )
		if( Tematy[i].ID == temat ) { Tematy[i].Wykonane = true; return true; }

	return false;
}

/*..................................................................................................*/

bool DZIENNIK::IstniejeWpis( string temat, string wpis )
{
	for( unsigned i = 0; i < Tematy.size(); i++ )
	{
		if( Tematy[i].ID == temat )
		{
			for( unsigned j = 0; j < Tematy[i].Wpisy.size(); j++ )
				if( Tematy[i].Wpisy[j].ID == wpis ) return true;
			break;
		}
	}

	return false;
}

/*..................................................................................................*/

bool DZIENNIK::DodajWpis( string temat, string wpis, int wartosc )
{
	for( unsigned i = 0; i < Tematy.size(); i++ )
		if( Tematy[i].ID == temat )
		{
			// je�eli wpis ju� istnieje przerywa funkcj�
			for( unsigned j = 0; j < Tematy[i].Wpisy.size(); j++ )
				if( Tematy[i].Wpisy[j].ID == wpis ) return false;
			
			// dodaje wpis
			Tematy[i].Wpisy.add( WPIS(wpis, wartosc) );
			return true;
		}

	// je�eli temat nie istnieje dodaje go razem z wpisem
	Tematy.add( TEMAT() );
	Tematy.back().ID = temat;
	Tematy.back().Wpisy.add( WPIS(wpis, wartosc) );

	return true;
}

/*..................................................................................................*/

int& DZIENNIK::WartoscWpisu( string temat, string wpis )
{
	for( unsigned i = 0; i < Tematy.size(); i++ )
		if( Tematy[i].ID == temat )
		{
			for( unsigned j = 0; j < Tematy[i].Wpisy.size(); j++ )
				if( Tematy[i].Wpisy[j].ID == wpis ) return Tematy[i].Wpisy[j].Wartosc;
			break;
		}
}

/*..................................................................................................*/

void DZIENNIK::Rysuj()
{
	StdBuf.Czysc();

	StdBuf >> xcZul >> POS( 0, 0 ) << "--------------------------------------------------------------------------------";
	StdBuf >> xZul >> POS( 1, 1 ) << "Dziennik";
	StdBuf >> xcZul >> POS( 0, 2 ) << "--------------------------------------------------------------------------------";
	StdBuf.WstawZnak( 26, xBia, POS( 1, 5 ) );
	int temat = -WybranyTemat;
	for( short i = -WybranyTemat; temat < 30; i++ )
	{
		if( WybranyTemat + i >= Tematy.size() ) break;
		short index = IndexTematu(Tematy[WybranyTemat + i].ID);
		if( index < 0 ) continue;
		if( temat >= -1 ) StdBuf >> (Tematy[WybranyTemat + i].Wykonane ? xZie : xBia) >> POS( 2, 5 + temat ) << TematyDziennika[index].Tytul;
		temat++;
	}
	StdBuf >> xZul >> POS( 0, 37 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec + [8]-Do g�ry + [2]-Na d� + [Enter]-Poka� temat";
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";

	StdBuf.Rysuj();
}

/*..................................................................................................*/

bool DZIENNIK::RysujTemat()
{
	short temat = -1;
	WORD _temat;
	WORD ilosc = 0;
	for( unsigned i = 0; i < Tematy.size(); i++ ) if( IndexTematu(Tematy[i].ID) >= 0 ) {
		if( ilosc == WybranyTemat ) { _temat = i; temat = IndexTematu(Tematy[i].ID); break; }
		ilosc++;
	}
	if(temat < 0) return false;

	StdBuf.Czysc();

	StdBuf >> xcZul >> POS(0, 0) << "--------------------------------------------------------------------------------";
	StdBuf >> xZul >> POS(1, 1) << "Dziennik - " << TematyDziennika[temat].Tytul;
	StdBuf >> xcZul >> POS(0, 2) << "--------------------------------------------------------------------------------";
	StdBuf.UstawKolorPisania( xBia );
	string TrescTematu;
	for( short i = 0; i < Tematy[_temat].Wpisy.size(); i++ ) {
		short wpis = -1;
		for( WORD j = 0; j < TematyDziennika[temat].Wpisy.size(); j++ )
			if( TematyDziennika[temat].Wpisy[j].ID == Tematy[_temat].Wpisy[i].ID ) { wpis = j; break; }

		if(wpis >= 0) TrescTematu += TematyDziennika[temat].Wpisy[wpis].Tresc + "\\n\\n";
	}
	StdBuf.NapiszIZawijaj( TrescTematu, 78, POS(1, 4) );
	StdBuf >> xZul >> POS(0, 37) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS(1, 38) << "[Space]-Koniec";
	StdBuf >> xZul >> POS(0, 39) << "+------------------------------------------------------------------------------+";

	StdBuf.Rysuj();

	return true;
}

/*..................................................................................................*/

void DZIENNIK::Przetwarzaj()
{
	for(;;)
	{
		Rysuj();

		for(;;)
		{
			switch( PobierzKlawisz() )
			{
				case 13: {
					if( !RysujTemat() ) continue;
					char c;
					while(c = _getch(), c != 27 && c != 32);
					break;
				}

				case 1072: case '8':  {
					if( WybranyTemat > 0 ) WybranyTemat--;
					else continue;
					break;
				}
				case 1080: case '2':  {
					WORD ilosc = 0;
					for( unsigned i = 0; i < Tematy.size(); i++ ) if( IndexTematu(Tematy[i].ID) >= 0 ) ilosc++; 
					if( WybranyTemat + 1 < ilosc ) WybranyTemat++;
					else continue;
					break;
				}
				
				case 27: case 32: return;

				default: continue;
			}
			break;
		}
	}
}

/****************************************************************************************************/

void BOHATER::RysujStatystyki()
{
	StdBuf.Czysc();
	StdBuf >> xZul >> POS( 0, 0 ) << "+------------------------------------------------------------------------------+";
	RysujOkienko();
	StdBuf >> xSza >> POS( 12, 3 ) << "Imi�: " >> xBia << Nazwa;
	if( DaneGry("SEX_CHOICE").GetValue() ) StdBuf >> xSza >> POS( 12, 4 ) << "P�e�: " >> xBia << Plcie[ Plec ];
	if( DaneGry("CLASS_CHOICE").GetValue() ) StdBuf >> xSza >> POS( 12, 5 ) << "Klasa: " >> xBia << Klasa;
	StdBuf >> xSza >> POS( 12, 6 ) << "Waga: " >> xBia << Waga << "kg";
	StdBuf >> xSza >> POS( 12, 7 ) << "Wzrost: " >> xBia << Wzrost << "cm";
	StdBuf >> xCze >> POS( 44, 3 ) << "�ycie: " << ceil( Zycie ) << "/" << MaxZycie;
	StdBuf >> xNie >> POS( 44, 4 ) << "Energia: " << ceil( Energia ) << "/" << MaxEnergia;
	StdBuf >> xSza >> POS( 44, 5 ) << "Si�a: " >> xBia << sila;
	if( Punkty > 0 ) StdBuf >> xSza << " (" << IloscZabieranychPunktow( sila ) << "pkt.)";
	StdBuf >> xSza >> POS( 44, 6 ) << "Zr�czno��: " >> xBia << zrecznosc;
	if( Punkty > 0 ) StdBuf >> xSza << " (" << IloscZabieranychPunktow( zrecznosc ) << "pkt.)";
	StdBuf >> xSza >> POS( 44, 7 ) << "Szybko��: " >> xBia << szybkosc;
	if( Punkty > 0 ) StdBuf >> xSza << " (" << IloscZabieranychPunktow( szybkosc ) << "pkt.)";
	StdBuf >> xZul >> POS( 0, 10 ) << "+------------------------------------------------------------------------------+";
	RysujRamke( POS( 0, 11 ), POS( 80, 7 ), xcZul, false );
	StdBuf >> xBia >> POS( 4, 11 ) << " Do�wiadczenie postaci: ";
	StdBuf >> xSza >> POS( 2, 12 ) << "Poziom: " >> xBia << Poziom;
	StdBuf >> xSza >> POS( 2, 13 ) << "Do�wiadczenie: " >> xBia << Doswiadczenie;
	StdBuf >> xSza >> POS( 2, 14 ) << "Nast�pny poziom: " >> xBia << ::Doswiadczenie(Poziom);
	if( Punkty > 0 ) StdBuf >> xZul >> POS( 2, 15 ) << "Niewykorzystane punkty: " >> xBia << Punkty;
	StdBuf >> xBia >> POS( 2, 16 ) << "[";
	StdBuf >> xBia >> POS( 77, 16 ) << "]";
	char ilosc_kresek = ceil( ( double ) ( Doswiadczenie * 74 / ::Doswiadczenie(Poziom) ) );
	char ilosc_starych_kresek = ceil( ( double ) ( ::Doswiadczenie(Poziom - 1) * 74 / ::Doswiadczenie(Poziom) ) );
	for( char i = 0; i < 74; i++ )
	{
		if( ilosc_kresek <= i ) StdBuf.WstawZnak( '-', xBia, POS( 3 + i, 16 ), false );
		else StdBuf.WstawZnak( '=', ( i < ilosc_starych_kresek ? xCze : xZul ), POS( 3 + i, 16 ) );
	}
	StdBuf >> xZul >> POS( 0, 18 ) << "+------------------------------------------------------------------------------+";
	if(ListaParametrow) StdBuf.WstawZnak( 26, xBia, POS( 43, 3 + Parametr ) );
	else StdBuf.WstawZnak( 26, xBia, POS( 1, 21 ) );
	for( short i = -Umiejetnosci.Wybrana; i < 15; i++ )
	{
		if( i < -2 ) i = -2;
		StdBuf >> xSza >> POS( 2, 21 + i ) << ::Umiejetnosci[ i + Umiejetnosci.Wybrana ].Nazwa;
		StdBuf >> POS( 64, 21 + i ) << Umiejetnosci[ i + Umiejetnosci.Wybrana ] << "%";
		if( Punkty > 0 ) StdBuf >> POS( 70, 21 + i ) << "(" << IloscZabieranychPunktow( Umiejetnosci[ i + Umiejetnosci.Wybrana ] ) << "pkt.)";
		if(i + Umiejetnosci.Wybrana + 1 >= ::Umiejetnosci.size() ) break;
	}
	StdBuf >> xZul >> POS( 0, 36 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 37 ) << "[8]-Do g�ry + [2]-Na d� + [4]-Umiej�tno�ci + [6]-Parametry + [Space]-Koniec";
	if( Punkty > 0 ) StdBuf >> POS( 1, 38 ) << "[Enter]-Dodaj do umiej�tno�ci/parametru";
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool BOHATER::PokazStatystyki()
{
	for(;;)
	{
		RysujStatystyki();
		for(;;)
		{
			switch( PobierzKlawisz() )
			{
			case 13: {
				if(ListaParametrow) {
					if(Parametr == 0) { if( Punkty <= 0 || MaxZycie >= 1000 ) continue;	Punkty--; MaxZycie++; Zycie++; break; }
					else if(Parametr == 1) { if( Punkty <= 0 || MaxEnergia >= 1000 ) continue;	Punkty--; MaxEnergia++; Energia++; break; }
					else if(Parametr == 2) { if( PrzydzielPunkty( sila ) ) break; continue; }
					else if(Parametr == 3) { if( PrzydzielPunkty( zrecznosc ) ) break; continue; }
					else if(Parametr == 4) { if( PrzydzielPunkty( szybkosc ) ) break; continue; }
				}
				else { if( PrzydzielPunkty( Umiejetnosci[ Umiejetnosci.Wybrana ] ) ) break; continue; }
			}

			case 1075: case '4': ListaParametrow = false; break;
			case 1077: case '6': ListaParametrow = true; break;

			case 1072: case '8': {
				if(ListaParametrow) { if( Parametr <= 0 ) continue; Parametr--; break; }
				else { if( Umiejetnosci.Wybrana <= 0 ) continue; Umiejetnosci.Wybrana--; break; }
			}
			case 1080: case '2': {
				if(ListaParametrow) { if( Parametr >= 4 ) continue; Parametr++; break; }
				else { if( Umiejetnosci.Wybrana >= ::Umiejetnosci.size() - 1 ) continue; Umiejetnosci.Wybrana++; break; }
			}
			
			case 27: case 32: return true;

			default: continue;
			}
			break;
		}
	}
	return true;
}

/****************************************************************************************************/

void BOHATER::Rysuj()
{
	if( StanGry->Postac.Zycie <= 0 )
	{
		if( AktualnaMapa->SzukajPostaci( Pozycja ) >= 0 ) return;
		else StdBuf.WstawZnak( Znak, Kolor + tcCze, POS( 1 + SRODEK_RYSOWANIA.X, 2 + SRODEK_RYSOWANIA.Y ) );
	}
	else
		StdBuf.WstawZnak( ( Zycie * 100 / MaxZycie <= 20 ? 49 + Zycie * 100 / MaxZycie / 5 : Znak ),
						  ( Skradanie ? Kolor + tcSza : Kolor ), POS( 1 + SRODEK_RYSOWANIA.X, 2 + SRODEK_RYSOWANIA.Y ) );
}

/****************************************************************************************************/

bool BOHATER::Idz( DIRECTION Kierunek )
{
	POS p = Pozycja + VALUES_OF_DIRECTIONS[ Kierunek - 1 ];

	if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ) return false;

	if( Ekwipunek.Plecak.Ilosc() > 100 || Ekwipunek.Plecak.PobWaga() > sila * 200 )
	{
		Komunikat << "Nosisz zbyt du�o przedmiot�w!" >> xZul;
		return false;
	}

	int postac = AktualnaMapa->SzukajPostaci( p );
	if( postac >= 0 ) {
		if( AktualnaMapa->Postacie[ postac ]->WrogoNastawiony ) Uderz( postac );
		return true;
	}

	if( ::Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].ID.substr( 0, 6 ) == "doorc_" )
	{
		short index_kafelka = IndexKafelka( AS_STR "dooro_" + ::Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].ID.substr( 6 ) );
		if( index_kafelka < 0 ) Komunikat << "Tych drzwi nie da si� otworzy�." >> xZul;
		else {
			AktualnaMapa->Kafelki[ p.X ][ p.Y ] = index_kafelka;
			Komunikat << "Otworzy�e� drzwi." >> xBia;
		}
		return true;
	}

	if( AktualnaMapa->SzukajPrzejscia( p ) >= 0 )
	{
		PRZEJSCIE pr = AktualnaMapa->Przejscia[ AktualnaMapa->SzukajPrzejscia( p ) ];
		if( pr.Automatyczne ) {
			UzyjPrzejscia( pr );
			return true;
		}
	}

	short przedmiot = AktualnaMapa->SzukajPrzedmiotu( p );
	if( przedmiot >= 0 && Przedmioty[ AktualnaMapa->Przedmioty[ przedmiot ].NR ]->PodajTyp() == Relacyjny &&
		dynamic_cast< RELACYJNY* >( Przedmioty[ AktualnaMapa->Przedmioty[ przedmiot ].NR ] )->Sciana )
	{
		Przedmioty[ AktualnaMapa->Przedmioty[ przedmiot ].NR ]->Uzyj();
		return true;
	}

	if( Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].Typ != TK_GRUNT ) return false;

	Pozycja = p;

	return true;
}

/****************************************************************************************************/

bool BOHATER::Patrz()
{
	POS pozycja = Pozycja;
	for(;;)
	{
		StdBuf.Czysc();

		RysujPanelPatrzenia( pozycja );
		StdBuf >> xZul >> POS( 3, 0 ) << "< [PATRZ] - Przegl�danie... >";

		AktualnaMapa->Rysuj();
		Rysuj();
		StanGry->RysujPanel();
		StdBuf.PokolorujZnak( tcZul + xCza, pozycja( 1 + SRODEK_RYSOWANIA.X - Pozycja.X, 2 + SRODEK_RYSOWANIA.Y - Pozycja.Y ) );
		
		StdBuf.Rysuj();

		for(;;)
		{
			char ch = _getch();
			if( ch == 27 || ch == 32 ) return false;
			short k = ch - 48;
			if( k < 1 || k > 9 || k == 5 ) continue;
			POS p = pozycja + VALUES_OF_DIRECTIONS[ k - 1 ];

			if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ||
				p.X - Pozycja.X + SRODEK_RYSOWANIA.X < 0 || p.X - Pozycja.X + SRODEK_RYSOWANIA.X >= SzerokoscMapy ||
				p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y < 0 || p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y >= WysokoscMapy ||
				!StanGry->WidocznePola[p.X][p.Y] ) continue;

			pozycja = p;

			break;
		}
	}
}

/****************************************************************************************************/

bool BOHATER::Rozmawiaj()
{
	POS pozycja = Pozycja;
	for(;;)
	{
		StdBuf.Czysc();

		RysujPanelPatrzenia( pozycja );
		StdBuf >> xZul >> POS( 3, 0 ) << "< [ROZMOWA] - Wybierz osob�... >";

		AktualnaMapa->Rysuj();
		Rysuj();
		StanGry->RysujPanel();
		StdBuf.PokolorujZnak( tcZie + xCza, pozycja( 1 + SRODEK_RYSOWANIA.X - Pozycja.X, 2 + SRODEK_RYSOWANIA.Y - Pozycja.Y ) );
		
		StdBuf.Rysuj();

		for(;;)
		{
			char ch = _getch();
			switch( ch )
			{
				case '1': case '2': case '3': case '4': case '6': case '7': case '8': case '9': break;
				case 27: case 32: return false;
				case 't': case 'T': case '5':
				{
					PUNKTY droga;
					droga.Stworz( Pozycja, pozycja );

					bool cel_bez_zasiegu = false;
					for( UCHAR i = 1; i < droga.IloscPunktow; i++ )
					{
						if( Kafelki[ AktualnaMapa->Kafelki[ droga.Punkty[ i ].X ][ droga.Punkty[ i ].Y ] ].Typ == TK_SCIANA )
							cel_bez_zasiegu = true;
					}

					short postac = AktualnaMapa->SzukajPostaci( pozycja );
					if( postac < 0 || cel_bez_zasiegu ) continue;
					if( droga.IloscPunktow > 6 )
					{
						Komunikat << "Za daleko!" >> xZul;
						return false;
					}
					/************************************************************/
					if( AktualnaMapa->Postacie[ postac ]->WrogoNastawiony ||
						AktualnaMapa->Postacie[ postac ]->Typ == TPO_WROG )
					{
						Komunikat << "Ta posta� nie chce z tob� rozmawia�!" >> xZul;
						return false;
					}

					OSOBA* osoba = dynamic_cast< OSOBA* >( AktualnaMapa->Postacie[ postac ] );

					RELACJONER relacjoner;
					RELACJA def_relacji;
					def_relacji.Laduj( DaneGry( "DIALOGS" )( osoba->Rozmowa ) );
					relacjoner.Laduj( &def_relacji, this, osoba );
					for(;;)
					{
						if( relacjoner.End ) break;
						relacjoner.Rysuj();
						relacjoner.Przetwarzaj( PobierzKlawisz() );
					}
					/************************************************************/
					return true;
				}
				default: continue;
			}
			short k = ch - 48;
			POS p = pozycja + VALUES_OF_DIRECTIONS[ k - 1 ];

			if(	p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ||
				p.X - Pozycja.X + SRODEK_RYSOWANIA.X < 0 || p.X - Pozycja.X + SRODEK_RYSOWANIA.X >= SzerokoscMapy ||
				p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y < 0 || p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y >= WysokoscMapy ||
				!StanGry->WidocznePola[p.X][p.Y] ) continue;

			pozycja = p;

			break;
		}
	}
}

/****************************************************************************************************/

void BOHATER::Handluj( OSOBA* osoba )
{
	HANDEL handel;
	handel.Laduj( &Ekwipunek.Plecak, &osoba->Ekwipunek );
	for(;;)
	{
		handel.Rysuj();
		if( handel.Przetwarzaj( _getch() ) ) break; 
	}
}

/****************************************************************************************************/

bool BOHATER::Atakuj( WORD Postac, OBRAZENIA Obrazenia )
{
	if( Niewidoczny && Obrazenia.Typ != OBR_MAGIA ) Obrazenia.Wartosc *= 2;

	AktualnaMapa->Postacie[ Postac ]->BronSie( Obrazenia );

	return false;
}



/****************************************************************************************************/

bool BOHATER::Zaatakuj()
{
	Komunikat << "Podaj kierunek..." >> xBia;
	Komunikat();
	StdBuf.Rysuj();

	short k = _getch() - 48;
	if( k < 1 || k > 9 || k == 5 ) return false;
	POS p = Pozycja + VALUES_OF_DIRECTIONS[ k - 1 ];

	if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ) return false;

	int postac = AktualnaMapa->SzukajPostaci( p );
	if( postac >= 0 ) {
		Uderz( postac );
		return true;
	}
	else Komunikat << "Tu nikogo nie ma." >> xZul;
	return false;
}

/****************************************************************************************************/

bool BOHATER::Uderz( WORD Postac )
{
	BRON_BIALA* bron = 0;
	BRON_BIALA* bron2 = 0;
	if( Ekwipunek.PrawaDlon >= 0 && Przedmioty[ Ekwipunek.PrawaDlon ]->PodajTyp() == BronBiala )
		bron = dynamic_cast< BRON_BIALA* >( Przedmioty[Ekwipunek.PrawaDlon] );

	bool dwa_ciosy = false;
	if( !bron || (bron->Typ == Nareczna && Ekwipunek.LewaDlon < 0) ) dwa_ciosy = true;
	else if((bron->Typ == Nareczna || bron->Typ == Jednoreczny) &&
			Ekwipunek.LewaDlon >= 0 && Przedmioty[Ekwipunek.LewaDlon]->PodajTyp() == BronBiala ) {
		bron2 = dynamic_cast< BRON_BIALA* >( Przedmioty[Ekwipunek.LewaDlon] );
		if(bron2->Typ == bron->Typ) dwa_ciosy = true;
	}
	
	Komunikat << ( dwa_ciosy ? "Zadajesz dwa ciosy, jeden po drugim." : "Wymierzasz cios." ) >> xFio;

	for( BYTE i = 0; i < dwa_ciosy + 1; i++ )
	{
		bool trafil = false;
		if( i >= 1 ) bron = bron2;

		if(!bron || bron->Typ == Nareczna) {
			if( Losuj( 1, 100 ) <= Umiejetnosci["walka_wrecz"] ) {
				if( Atakuj( Postac, OBRAZENIA(OBR_BRON, Losuj( sila / 30 + (bron ? bron->Obrazenia.Begin : 0),
															   sila / 15 + (bron ? bron->Obrazenia.End : 0) )) ) ) break;
			} else Komunikat << "Nie trafiasz wroga." >> xBia;
		}
		else {
			if(bron->Typ == Jednoreczny) {
				if(dwa_ciosy) {
					if( Losuj( 1, 100 ) <= Umiejetnosci["bron_jednoreczna"] * (float(Umiejetnosci["dwie_bronie"] / 2 + 50) / 100) ) trafil = true;
				}
				else if( Losuj( 1, 100 ) <= Umiejetnosci["bron_jednoreczna"] ) trafil = true;
			}
			else if( Losuj( 1, 100 ) <= Umiejetnosci["bron_dwureczna"] ) trafil = true;

			if(trafil) {
				if( Atakuj(Postac, OBRAZENIA(OBR_BRON, Losuj(bron->Obrazenia.Begin, bron->Obrazenia.End))) ) break;
			}
			else Komunikat << "Tw�j cios nie trafia wroga." >> xBia;
		}
	}
	return true;
}

/****************************************************************************************************/

bool BOHATER::Skocz()
{
	if( Energia < 1 ) {
		Komunikat << "Masz zbyt ma�o energii." >> xZul;
		return false;
	}
	Komunikat << "Podaj kierunek..." >> xBia;
	Komunikat();
	StdBuf.Rysuj();

	short k = _getch() - 48;
	if( k < 1 || k > 9 ) return false;
	POS p1 = Pozycja + VALUES_OF_DIRECTIONS[k - 1];
	POS p = Pozycja + VALUES_OF_DIRECTIONS[k - 1] + VALUES_OF_DIRECTIONS[k - 1];

	if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ||
		Kafelki[ AktualnaMapa->Kafelki[p1.X][p1.Y] ].Typ != TK_GRUNT || !AktualnaMapa->PozycjaWolnaDlaPostaci(p) ) {
		Komunikat << "Nie mo�esz tu skoczy�." >> xZul;
		return false;
	}

	Pozycja = p1;
	StdBuf.Czysc();
	AktualnaMapa->Odkryj( Pozycja );
	AktualnaMapa->StworzMapyPomocnicze();
	AktualnaMapa->Rysuj();
	Rysuj();
	StdBuf.Rysuj();
	Sleep(100);

	Pozycja = p;
	DoEnergii( -1 );

	return true;
}

/****************************************************************************************************/

POS BOHATER::Celuj( string TekstNaglowka, char ZnakPotwierdzenia, UCHAR Zasieg )
{
	POS pozycja_celu = Pozycja;
	PUNKTY linia;

	for(;;)
	{
		linia.Stworz( Pozycja, pozycja_celu );
		StdBuf.Czysc();

		RysujPanelPatrzenia( pozycja_celu );
		StdBuf >> xZul >> POS( 3, 0 ) << TekstNaglowka;

		AktualnaMapa->Rysuj();
		Rysuj();
		
		RysujPunktWyboruPozycji( linia, Zasieg );
		
		StdBuf.Rysuj();

		for(;;)
		{
			char przycisk = _getch();

			short k = przycisk - 48;

			POS p;
			if( k >= 1 && k <= 9 )
			{
				p = pozycja_celu + VALUES_OF_DIRECTIONS[ k - 1 ];
				if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ||
					p.X - Pozycja.X + SRODEK_RYSOWANIA.X < 0 || p.X - Pozycja.X + SRODEK_RYSOWANIA.X >= SzerokoscMapy ||
					p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y < 0 || p.Y - Pozycja.Y + SRODEK_RYSOWANIA.Y >= WysokoscMapy ) continue;
			}

			switch( przycisk )
			{
			case '1': case '2': case '3': case '4': case '6': case '7': case '8': case '9':
				pozycja_celu = p; break;

			case '5': return pozycja_celu;
			
			case 27: case 32: return POS( -1, 0 );

			default: if( toupper( przycisk ) == toupper( ZnakPotwierdzenia ) ) return pozycja_celu; continue;
			}

			break;
		}
	}
}

/****************************************************************************************************/

bool BOHATER::Strzelaj()
{
	if( Ekwipunek.PrawaDlon < 0 || Przedmioty[ Ekwipunek.PrawaDlon ]->PodajTyp() != BronDystansowa ) {
		Komunikat << "Nie masz czym strzela�!" >> xZul;
		return false;
	}
	if( Ekwipunek.LewaDlon < 0 || Przedmioty[ Ekwipunek.LewaDlon ]->PodajTyp() != Pocisk ) {
		Komunikat << "Nie masz pocisk�w!" >> xZul;
		return false;
	}

	BRON_DYSTANSOWA* Bron = dynamic_cast< BRON_DYSTANSOWA* >( Przedmioty[ Ekwipunek.PrawaDlon ] );
	POCISK* Pociski = dynamic_cast< POCISK* >( Przedmioty[ Ekwipunek.LewaDlon ] );

	if( Bron->Typ != Pociski->Typ ) {
		Komunikat << "Masz z�e pociski!" >> xZul;
		return false;
	}

	POS pozycja_celu = Celuj( "< [STRZA�] - Wybierz cel... >", 'f', Bron->Zasieg );

	if( pozycja_celu.X == -1 ) return false;

	if( pozycja_celu == Pozycja ) {
		Komunikat << "Chyba nie chcesz tego zrobi�!?" >> xZul;
		return false;
	}

	Komunikat << "Wystrzeliwujesz pocisk." >> xFio;

	WORD obrazenia = 0;
	
	PUNKTY linia;
	linia.Stworz( Pozycja, pozycja_celu );

	for( UCHAR i = 1; i < linia.IloscPunktow; i++ )
	{
		if( Kafelki[ AktualnaMapa->Kafelki[ linia.Punkty[ i ].X ][ linia.Punkty[ i ].Y ] ].Typ == TK_SCIANA || i > Bron->Zasieg ) {
			AktualnaMapa->DodajPrzedmiot( Ekwipunek.LewaDlon, 1, linia.Punkty[ i - 1 ] );
			break;
		}

		StdBuf.Czysc();
		AktualnaMapa->Rysuj();
		Rysuj();
		StdBuf.WstawZnak('+', Pociski->Kolor, linia.Punkty[ i ]( 1 + SRODEK_RYSOWANIA.X - Pozycja.X, 2 + SRODEK_RYSOWANIA.Y -Pozycja.Y ));
		StdBuf.Rysuj();
		Sleep( 20 );

		if( AktualnaMapa->SzukajPostaci( linia.Punkty[ i ] ) >= 0 )
		{
			bool trafil = false;
			switch ( Bron->Typ )
			{
			case Luk : if( Losuj( 1, 100 ) <= Umiejetnosci["luk"] / ( (float)( i + 2 ) / 7 ) ) trafil = true; break;
			case Kusza : if( Losuj( 1, 100 ) <= Umiejetnosci["kusza"] / ( (float)( i + 2 ) / 7 ) ) trafil = true; break;
			}
			if( trafil ) obrazenia = Losuj( Pociski->Obrazenia.Begin, Pociski->Obrazenia.End );
			else {
				Komunikat << ( Losuj(0,1) ? "Strza�a mija wroga." : "Strza�a przelatuje tu� obok wroga." ) >> xBia;
				continue;
			}
			Atakuj( AktualnaMapa->SzukajPostaci( linia.Punkty[ i ] ) , OBRAZENIA(OBR_POCISKI, obrazenia) );
			if( obrazenia == 0 ) AktualnaMapa->DodajPrzedmiot( Ekwipunek.LewaDlon, 1, linia.Punkty[ i ] );
			break;
		}
		if( i >= linia.IloscPunktow - 1 )
			AktualnaMapa->DodajPrzedmiot( Ekwipunek.LewaDlon, 1, linia.Punkty[ i ] );
	}
	if( Ekwipunek.Plecak.IloscPrzedmiotow( Ekwipunek.LewaDlon ) > 0 )
		Ekwipunek.Plecak.UsunPrzedmiot( Ekwipunek.LewaDlon, 1 );
	else Ekwipunek.LewaDlon = -1;

	return true;
}

/****************************************************************************************************/

void BOHATER::RysujListeCzarow()
{
	StdBuf.Czysc();
	StdBuf.UstawPozycjeKursora( POS( 79, 39 ) );
	StdBuf >> xZul >> POS( 0, 0 ) << "--------------------------------------------------------------------------------";
	StdBuf >> xBia >> POS( 1, 1 ) << "Czary";
	StdBuf >> xZul >> POS( 0, 2 ) << "--------------------------------------------------------------------------------";
	RysujRamke( POS( 0, 3 ), POS( 80, 3 ), xcZul, false );
	StdBuf >> xBia >> POS( 4, 4 ) << "Nazwa:                                                               Koszt:";

	StdBuf.WstawZnak( 26, xBia, POS( 1, 8 ) );

	WORD ilosc_czarow = Czary.size();

	for( short i = -WybranyCzar; i <= ilosc_czarow - WybranyCzar - 1; i++ )
	{
		if( i < -2 ) i = -2;

		::Czary[ Czary[ WybranyCzar + i ] ]->RysujPasek( POS( 2, 8 + i ), 77 );

		if( i >= 30 ) break;
	}
	StdBuf >> xZul >> POS( 0, 37 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec + [8]-Do g�ry + [2]-Na d� + [Z],[Enter]-Wybierz";
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool BOHATER::WybierzCzar()
{
	WORD stary_wybrany_czar = WybranyCzar;
	for( ; ; )
	{
		RysujListeCzarow();
		for( ; ; )
		{
			switch( PobierzKlawisz() )
			{
			case 1072: case '8': if( WybranyCzar <= 0 ) continue; else WybranyCzar--; break;
			case 1080: case '2': if( WybranyCzar >= Czary.size() - 1 ) continue; else WybranyCzar++; break;

			case 13: case 'z': case 'Z': return true;

			case 27: case 32: WybranyCzar = stary_wybrany_czar; return false;

			default: continue;
			}
			break;
		}
	}
}

/****************************************************************************************************/

bool BOHATER::RzucCzar( short Czar )
{
	CZAR* czar;

	if( Czar == -1 ) {
		if( Czary.size() == 0 ) {
			Komunikat << "Nie znasz �adnego czaru!" >> xZul;
			return false;
		}
		if( !WybierzCzar() ) return false;
		czar = ::Czary[ Czary[ WybranyCzar ] ];
	}
	else czar = ::Czary[ Czar ];

	if( czar->KosztRzucenia > Energia ) {
		Komunikat << "Masz zbyt ma�o energii aby rzuci� ten czar!" >> xZul;
		return false;
	}

	/**************************************************/

	if( czar->Typ == TC_POCISK )
	{
		CZAR_POCISK* czar_o = static_cast< CZAR_POCISK* >( czar );
		POS pozycja_celu = Pozycja;

		if( czar_o->Zasieg > 0 )
		{
			pozycja_celu = Celuj( "< [CZAR] - Wybierz cel... >", 'z', czar_o->Zasieg );
			if( pozycja_celu.X == -1 ) return false;

			/*if( pozycja_celu == Pozycja ){
				Komunikat << "Nie mo�esz rzuca� tego czaru na siebie!" >> xZul;
				return false;
			}*/
		}

		WORD obrazenia = 0;

		Komunikat << "Rzucasz czar." >> xFio;
		PUNKTY linia;
		linia.Stworz( Pozycja, pozycja_celu );

		POS centrum = Pozycja;
		array< POS > obszar;

		for( UCHAR i = 1; i < linia.IloscPunktow; i++ )
		{
			if( i > czar_o->Zasieg || Kafelki[ AktualnaMapa->Kafelki[ linia.Punkty[i].X ][ linia.Punkty[i].Y ] ].Typ == TK_SCIANA ) {
				centrum = linia.Punkty[ i - 1 ];
				break;
			}

			StdBuf.Czysc();
			AktualnaMapa->Rysuj();
			StanGry->Postac.Rysuj();
			czar->Urok.Animacja.Rysuj( linia.Punkty[ i ]( 1 + SRODEK_RYSOWANIA.X - Pozycja.X, 2 + SRODEK_RYSOWANIA.Y - Pozycja.Y ) );
			
			short w = AktualnaMapa->SzukajPostaci( linia.Punkty[ i ] );
			if( w >= 0 )
			{
				if( !czar_o->PrzechodziPrzezWrogow ) {
					centrum = linia.Punkty[ i ];
					break;
				}
				else obszar.add( linia.Punkty[ i ] );
			}

			if( i == linia.IloscPunktow - 1 ) centrum = linia.Punkty[ i ];
		}
		obszar.add( centrum );
		array< POS > punkty;
		punkty.add( centrum );

		// Wyszukuje punkty w zasi�gu fali uderzeniowej
		for( unsigned i = 1; i <= czar_o->ZasiegFaliUderzeniowej; i++ )
		{
			// Je�eli nie ma �adnych punkt�w do sprawdzenia (ca�e pomieszczenie jest w zasi�gu uderzenia)
			if( punkty.size() == 0 ) break;

			array< POS > _punkty = punkty;
			punkty.clear();
			
			// Sprawdza punkty w danej warstwie uderzenia
			for( unsigned j = 0; j < _punkty.size(); j++ )
			{
				// Sprawdza miejsca dooko�a danego punktu
				for( BYTE k = 0; k < 8; k++ )
				{
					POS pozycja = _punkty[ j ] + WEKTORY[ k ];
					if( ::Kafelki[ AktualnaMapa->Kafelki[ pozycja.X ][ pozycja.Y ] ].Typ != TK_SCIANA )
					{
						// Je�eli punkt nie nale�y jeszcze do obszaru dodaje go
						for( int l = 0; l < obszar.size(); l++ ) {
							if( obszar[l] == pozycja ) break;
							if( l == obszar.size() - 1 ) {
								obszar.add( pozycja );
								punkty.add( pozycja );
							}
						}
					}
				}
			}
		}

		StdBuf.Czysc();
		AktualnaMapa->Rysuj();
		StanGry->Postac.Rysuj();

		// Zadaje cios postaciom znajduj�cym si� w zasi�gu fali uderzeniowej
		for( unsigned i = 0; i < obszar.size(); i++ )
		{
			if( StanGry->WidocznePola[obszar[i].X][obszar[i].Y] && Losuj( 1, 4 ) == 1 )
				czar->Urok.Animacja.Rysuj(  obszar[i] + SRODEK_RYSOWANIA - Pozycja + POS( 1, 2 ), false );
			short w = AktualnaMapa->SzukajPostaci( obszar[i] );
			if( w >= 0 ) {
				AktualnaMapa->Postacie[w]->WrogoNastawiony = true;
				AktualnaMapa->Postacie[w]->Uroki.add( czar->Urok );
			}
		}

		StdBuf.Rysuj();
		Sleep( czar->Urok.Animacja.Czas * 2 );
	}

	/**************************************************/

	if( czar->Typ == TC_NA_SIEBIE )
	{
		Komunikat << "Rzucasz czar." >> xFio;

		StdBuf.Czysc();
		AktualnaMapa->Rysuj();

		Uroki.add( czar->Urok );
	}

	Energia -= czar->KosztRzucenia;

	return true;
}

/****************************************************************************************************/

bool BOHATER::Czekaj()
{
	DoEnergii( 0.02 * (60 - StanGry->Postac.szybkosc / 4) / 10 );
	return true;
}

/****************************************************************************************************/

bool BOHATER::SkradajSie()
{
	if( Umiejetnosci["skradanie"] == 0 )
	{
		Komunikat << "Nie umiesz si� skrada�!" >> xZul;
		return false;
	}

	if( Skradanie )
	{
		Skradanie = false;
		Niewidoczny = false;
		Komunikat << "Przestajesz si� skrada�." >> xBia;
	}
	else
	{
		Skradanie = true;
		Niewidoczny = true;
		Komunikat << "Zaczynasz si� skrada�." >> xBia;
	}

	return true;
}

/****************************************************************************************************/

bool BOHATER::ZamknijDrzwi()
{
	Komunikat << "Podaj kierunek..." >> xBia;
	Komunikat();
	StdBuf.Rysuj();

	short k = _getch() - 48;
	if( k < 1 || k > 9 || k == 5 ) return false;
	POS p = Pozycja + VALUES_OF_DIRECTIONS[ k - 1 ];

	if( p.X < 0 || p.X >= AktualnaMapa->Rozmiar.X || p.Y < 0 || p.Y >= AktualnaMapa->Rozmiar.Y ) return false;

	if( ::Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].ID.substr( 0, 6 ) == "dooro_" )
	{
		if( AktualnaMapa->SzukajPostaci( p ) >= 0 || AktualnaMapa->SzukajPrzedmiotu( p ) >= 0 ) {
			Komunikat << "Nie mo�esz teraz zamkn�� tych drzwi." >> xZul;
			return false;
		}
		short index_kafelka = IndexKafelka( AS_STR "doorc_" + ::Kafelki[ AktualnaMapa->Kafelki[ p.X ][ p.Y ] ].ID.substr( 6 ) );
		if( index_kafelka < 0 ) Komunikat << "Tych drzwi nie da si� zamkn��." >> xZul;
		else {
			AktualnaMapa->Kafelki[ p.X ][ p.Y ] = index_kafelka;
			Komunikat << "Zamkn��e� drzwi." >> xBia;
		}
		return true;
	}
	else Komunikat << "Nie ma tu �adnych drzwi do zamkni�cia." >> xZul;
	return false;
}

/****************************************************************************************************/

void BOHATER::PodniesPrzedmiot( WORD KtoryZKoleiPrzedmiotNaMapie, unsigned Ilosc )
{
	short Przedmiot = AktualnaMapa->SzukajPrzedmiotu( Pozycja, KtoryZKoleiPrzedmiotNaMapie );

	if( Ilosc == 0 ) Ilosc = AktualnaMapa->Przedmioty[Przedmiot].Ilosc;
	Ekwipunek.Dodaj( AktualnaMapa->Przedmioty[Przedmiot].NR, Ilosc );

	Komunikat << "Podnosisz: ";
	if( Ilosc > 1 ) Komunikat << Ilosc << "x ";
	Komunikat << ::Przedmioty[ AktualnaMapa->Przedmioty[Przedmiot].NR ]->Nazwa << "." >> xBia;

	AktualnaMapa->UsunPrzedmiot( Przedmiot, Ilosc );
}

/****************************************************************************************************/

void BOHATER::RysujWyborPrzedmiotow(WORD PozycjaStrzalki)
{
	StdBuf.Czysc();
	StdBuf >> xcZul >> POS( 0, 0 ) << "--------------------------------------------------------------------------------";
	StdBuf >> xZul >> POS( 1, 1 ) << "Wybierz przedmioty kt�re chcesz podnie��";
	StdBuf >> xcZul >> POS( 0, 2 ) << "--------------------------------------------------------------------------------";
	RysujRamke( POS( 0, 3 ), POS( 80, 3 ), xcZul, false );
	StdBuf >> xBia >> POS( 4, 4 ) << "Przedmiot:                                                      Waga:";
	StdBuf.WstawZnak( 26, xBia, POS( 1, 8 ) );
	for( short i = -PozycjaStrzalki; i < 31; i++ )
	{
		if( i < -2 ) i = -2;
		WORD Przedmiot = AktualnaMapa->SzukajPrzedmiotu( Pozycja, PozycjaStrzalki + i + 1 );
		StdBuf.WstawZnak( Przedmioty[ AktualnaMapa->Przedmioty[ Przedmiot ].NR ]->Znak, Przedmioty[ AktualnaMapa->Przedmioty[ Przedmiot ].NR ]->Kolor, POS( 2, 8 + i ), false );
		StdBuf >> xSza >> POS( 4, 8 + i );
		if(AktualnaMapa->Przedmioty[ Przedmiot ].Ilosc > 1 ) StdBuf << AktualnaMapa->Przedmioty[ Przedmiot ].Ilosc << "x ";
		StdBuf << Przedmioty[AktualnaMapa->Przedmioty[Przedmiot].NR]->Nazwa;
		StdBuf >> POS( 68, 8 + i ) << Przedmioty[AktualnaMapa->Przedmioty[ Przedmiot ].NR]->Waga * AktualnaMapa->Przedmioty[Przedmiot].Ilosc << " dag";
		if(AktualnaMapa->SzukajPrzedmiotu(Pozycja, PozycjaStrzalki + i + 2) < 0) break;
	}
	StdBuf >> xZul >> POS( 0, 37 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec + [8]-Do g�ry + [2]-Na d� + [G],[Enter]-We�";
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool BOHATER::Wez()
{
	short Przedmiot = AktualnaMapa->SzukajPrzedmiotu(Pozycja);
	if(Przedmiot >= 0)
	{
		unsigned Ilosc = 1;
		if(AktualnaMapa->SzukajPrzedmiotu(Pozycja,2) >= 0)
		{
			WORD WybranyPrzedmiot = 0;
			for(;;)
			{
				RysujWyborPrzedmiotow(WybranyPrzedmiot);
				for(;;)
				{
					switch( PobierzKlawisz() )
					{
					case 72: case '8': if(WybranyPrzedmiot <= 0)continue; else WybranyPrzedmiot--; break;
					case 80: case '2': if(AktualnaMapa->SzukajPrzedmiotu(Pozycja, WybranyPrzedmiot + 2) < 0)continue; else WybranyPrzedmiot++; break;
					 
					case 13: case 'g': case 'G': PodniesPrzedmiot(WybranyPrzedmiot + 1); return true;

					case 27: case 32: return false;

					default: continue;
					}
					if(AktualnaMapa->SzukajPrzedmiotu(Pozycja, WybranyPrzedmiot + 1) < 0)WybranyPrzedmiot--;
					break;
				}
			}
		}
		else PodniesPrzedmiot(1);

		return true;
	}
	Komunikat << "Nie ma tu nic co m�g�by� podnie��." >> xZul;
	return false;
}

/****************************************************************************************************/

void BOHATER::UstawStatystyki(UCHAR _Sila, UCHAR _Zrecznosc, UCHAR _Szybkosc, WORD _Zycie, WORD _Energia)
{
	sila = _Sila;
	zrecznosc = _Zrecznosc;
	szybkosc = _Szybkosc;
	Zycie = _Zycie;
	MaxZycie = _Zycie;
	Energia = _Energia;
	MaxEnergia = _Energia;
}

/****************************************************************************************************/

void BOHATER::DoEnergii( double Ile )
{
	Energia += Ile;
	if( Energia > MaxEnergia ) Energia = MaxEnergia;
}

/****************************************************************************************************/

void BOHATER::DoPragnienia( double Ile )
{
	Pragnienie += Ile;
	if( Pragnienie < 0 ) Pragnienie = 0;
}

/****************************************************************************************************/

void BOHATER::DoGlodu( double Ile )
{
	Glod += Ile;
	if( Glod < 0 ) Glod = 0;
}

/****************************************************************************************************/

void BOHATER::Zapisz( PLIK& Plik )
{
	ZapiszPostac( Plik );
	BYTE plec = Plec;
	Plik << plec
	     << Klasa
	     << Waga
		 << Wzrost
		 << Energia
		 << MaxEnergia
		 << MaxZycie
		 << Glod
		 << Pragnienie
		 << sila
		 << zrecznosc
		 << szybkosc;
	Plik << Umiejetnosci.Wybrana;
	for( unsigned i = 0; i < Umiejetnosci.Wartosci.size(); i++) Plik << Umiejetnosci.Wartosci[i];
	Plik << Czary.size();
	for( unsigned i = 0; i < Czary.size(); i++ ) Plik << Czary[ i ];

	Plik << Dziennik.Tematy.size();
	for( unsigned i = 0; i < Dziennik.Tematy.size(); i++) {
		Plik << Dziennik.Tematy[i].ID;
		Plik << Dziennik.Tematy[i].Wykonane;
		Plik << Dziennik.Tematy[i].Wpisy.size();
		for( unsigned j = 0; j < Dziennik.Tematy[i].Wpisy.size(); j++) {
			Plik << Dziennik.Tematy[i].Wpisy[j].ID;
			Plik << Dziennik.Tematy[i].Wpisy[j].Wartosc;
		}
	}

	Plik << Zyje
		 << Skradanie
		 << Niewidoczny
		 << WybranyCzar;
	Ekwipunek.Zapisz( Plik );
	Plik << Poziom
		 << Doswiadczenie
		 << NowyPoziom
		 << DoNastepnegoPoziomu
		 << Punkty;
}

/****************************************************************************************************/

void BOHATER::Wczytaj( PLIK& Plik )
{
	WczytajPostac( Plik );
	BYTE wartosc;
	Plik.Wczytaj( &wartosc, sizeof( BYTE ) );
	Plec = (PLEC)wartosc;
	Plik >> Klasa;
	Plik >> Waga
		 >> Wzrost
		 >> Energia
		 >> MaxEnergia
		 >> MaxZycie
		 >> Glod
		 >> Pragnienie
		 >> sila
		 >> zrecznosc
		 >> szybkosc;
	Plik >> Umiejetnosci.Wybrana;
	for( unsigned i = 0; i < Umiejetnosci.Wartosci.size(); i++) Plik >> Umiejetnosci.Wartosci[i];
	unsigned ilosc;
	Plik >> ilosc;
	Czary.resize(ilosc);
	for( unsigned i = 0; i < Czary.size(); i++ ) Plik >> Czary[ i ];

	Plik >> ilosc;
	Dziennik.Tematy.resize(ilosc);
	for( unsigned i = 0; i < Dziennik.Tematy.size(); i++) {
		Plik >> Dziennik.Tematy[i].ID;
		Plik >> Dziennik.Tematy[i].Wykonane;
		Plik >> ilosc;
		Dziennik.Tematy[i].Wpisy.resize(ilosc);
		for( unsigned j = 0; j < Dziennik.Tematy[i].Wpisy.size(); j++) {
			Plik >> Dziennik.Tematy[i].Wpisy[j].ID;
			Plik >> Dziennik.Tematy[i].Wpisy[j].Wartosc;
		}
	}

	Plik >> Zyje
		 >> Skradanie
		 >> Niewidoczny
		 >> WybranyCzar;
	Ekwipunek.Wczytaj( Plik );
	Plik >> Poziom
		 >> Doswiadczenie
		 >> NowyPoziom
		 >> DoNastepnegoPoziomu
		 >> Punkty;
}

/****************************************************************************************************/