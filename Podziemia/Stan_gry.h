#pragma once

#include "stdafx.h"

/****************************************************************************************************/

enum STAN_POSTACI {
	SP_ZYJE,
	SP_PODDAL_SIE,
	SP_ZGINAL,
	SP_ZWYCIEZYL
};

/****************************************************************************************************/

struct STAN_GRY
{
	array<MAPA> Mapy;
	BOHATER Postac;
	WORD Poziom;
	unsigned Sekunda;
	unsigned Tura;
	BYTE StanPostaci;
	bool Highscore;

	BYTE Sekundy() { return (Sekunda + 30000) % 86400 % 60; }
	BYTE Minuty() { return (Sekunda + 30000) % 86400 / 60 % 60; }
	BYTE Godziny() { return (Sekunda + 30000) % 86400 / 3600 % 24; }
	WORD Dni() { return (Sekunda + 30000) / 86400; }
	
	POS Rozmiar;
	BYTE** WidocznePola;
	BYTE** Swiatlo;
	void UstawRozmiar( POS rozmiar );

	void PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne, POS pozycja1, POS pozycja2 );
	void PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne, POS pozycja1 );
	void PolaczMapy( WORD mapa1, WORD mapa2, WORD kafelek_przejscia1, WORD kafelek_przejscia2, bool automatyczne );
	void RysujPanel();
	void Zapisz( string NazwaPliku );
	void Wczytaj( string NazwaPliku );

	STAN_GRY() : Postac(0), Sekunda(0), Tura(0), StanPostaci(SP_ZYJE), Highscore(true), Poziom(0), WidocznePola(0), Swiatlo(0) {}
};

/****************************************************************************************************/

class HIGHSCORE
{
public:
	struct REKORD
	{
		string ImiePostaci;
		BYTE Poziom;
		unsigned Doswiadczenie;
		string NazwaMapy;
		string Klasa;
		BYTE StanPostaci;
		unsigned IloscTur;
		void Wczytaj( PLIK& Plik );
		void Zapisz( PLIK& Plik );
		unsigned Punkty() { return Doswiadczenie * 10 / ( (float)(IloscTur) / 1000 + 1 ) + (StanPostaci == SP_ZWYCIEZYL ? 5000 : 0); }
	};
private:
	array< REKORD > m_Rekordy;
public:
	bool DodajRekord( REKORD Rekord );
	void Zapisz( string NazwaPliku );
	void Wczytaj( string NazwaPliku );
	void Rysuj();
	void Pokaz();
};

/****************************************************************************************************/

extern STAN_GRY* StanGry;

/****************************************************************************************************/

int IndexMapy( string ID );

/****************************************************************************************************/