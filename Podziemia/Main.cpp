#include "stdafx.h"
//#include "Dzwieki.h"
#include "../RLGL/RLGLio.h"
//#include <SDL.h>

//SDL_Surface* LoadBmp( const char* path )
//{
//	SDL_Surface* tmp,* image;
// 
//	tmp = SDL_LoadBMP( path );
//	if( !tmp ) return NULL;
//
//	image = SDL_DisplayFormat( tmp );
//	SDL_FreeSurface( tmp );
//
//	return image;
//}

//#undef main
int main( unsigned argc, char* argv[] )
{
	//argc = ilo�� argument�w
	//argv[] = tablica argument�w typu char*

	/*atexit(SDL_Quit);
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "Unable to init SDL: %s\n", SDL_GetError());
		_getch();
		exit(1);
	}
	
	int done = false;
	SDL_Event _event;

	SDL_Surface* screen = SDL_SetVideoMode( 640, 480, 0, SDL_ANYFORMAT );
	SDL_Surface* image = LoadBmp( "1.bmp" );
	if( image == 0 )
	{
		printf( "Unable to load image: %s\n", SDL_GetError());
		_getch();
		exit(1);
	}

	Uint32 color = SDL_MapRGB( image->format, 0xff, 0xff, 0xff );
	SDL_SetColorKey( image, SDL_SRCCOLORKEY | SDL_RLEACCEL, color );

	SDL_Rect dest = { 0, 0, image->w, image->h };
	SDL_SetAlpha( image, SDL_SRCALPHA, 255 );
	SDL_BlitSurface( image, NULL, screen, &dest );

	while( !done )
    {
		while( SDL_PollEvent( &_event ) )
		{
            if( _event.type == SDL_KEYDOWN && _event.key.keysym.sym == SDLK_ESCAPE
				|| _event.type == SDL_QUIT ) done = true;
		}
        SDL_Flip( screen );
    }

	exit( 0 );

	_getch();

	Dzwieki::Inicjalizuj();
	Dzwieki::Graj( Dzwieki::Muzyka );*/

	StdBuf.SchowajKursor();

	COORD rozmiar_buforu = { 80, 40 };
	SMALL_RECT okno = { 0, 0, 79, 39 };

	SetConsoleScreenBufferSize( GetStdHandle( STD_OUTPUT_HANDLE ), rozmiar_buforu );
	SetConsoleWindowInfo( GetStdHandle( STD_OUTPUT_HANDLE ), TRUE, &okno );

	GRA gra;
	if( argc < 2 ) //error( "Brakuje argumentu z adresem do pliku gry." );
	{
		StdBuf >> POS(1, 1) >> tBia << "�ADOWANIE DANYCH GRY";
		StdBuf >> POS(1, 3) >> xBia << "adres do g��wnego pliku:";
		string adres = Pobierz("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 _-/\\.", 78, POS(1, 4), xBia);
		if(adres == NIE_WPROWADZONO || adres.size() < 1 ) return false;
		gra.Start( adres );
	}
	else gra.Start( argv[1] );
	if( argc > 2 && AS_STR(argv[2]) == "editor" ) {
		EDYTOR edytor;
		edytor.Przetwarzaj();
	}
	gra.Gra();

    return 0;
}
