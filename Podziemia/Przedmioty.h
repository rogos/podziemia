#pragma once

#include "stdafx.h"

/****************************************************************************************************/

enum TYP_PRZEDMIOTU
{
	Nieokreslony,
	Zwykly,
	Jednorazowy,
	Zwoj,
	Runa,
	Ksiega,
	BronBiala,
	BronDystansowa,
	Pocisk,
	Pancerz,
	Tarcza,
	Relacyjny
};

/****************************************************************************************************/

struct ZBIOR
{
	WORD Typ;
	unsigned Ilosc;
};

/****************************************************************************************************/

class POJEMNIK
{
private:
	array<ZBIOR> Przedmioty;
	unsigned     Waga;

public:
	POJEMNIK() : Waga( 0 ) {}

	unsigned PobWaga() const { return Waga; }
	const ZBIOR operator [] ( unsigned id ) const { return Przedmioty[ id ]; }
	const unsigned Ilosc() const { return Przedmioty.size(); } 

	void DodajPrzedmiot( WORD id, unsigned ilosc );
	unsigned UsunPrzedmiot( WORD id, unsigned ilosc );
	unsigned UsunPrzedmiotNa( unsigned index, unsigned ilosc );
	unsigned IloscPrzedmiotow( WORD id ) const;

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

struct PRZEDMIOT : OBJEKT
{
	string Nazwa;
	WORD Waga;
	WORD Wartosc;
	TYP_PRZEDMIOTU TypPrzedmiotu;
	TYP_PRZEDMIOTU PodajTyp();
	virtual bool Uzyj() { return 0; }
	virtual void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct ZWYKLY : PRZEDMIOT
{
	ZWYKLY() { TypPrzedmiotu = Zwykly; }
	ZWYKLY( string _Nazwa, char _Znak, WORD _Kolor, WORD _Waga, WORD _Wartosc )
	{
		TypPrzedmiotu = Zwykly;
		Nazwa = _Nazwa;
		Znak = _Znak;
		Kolor = _Kolor;
		Waga = _Waga;
		Wartosc = _Wartosc;
	}
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct JEDNORAZOWY : PRZEDMIOT
{
	UROK Wlasciwosci;
	string TekstPrzyUzyciu;
	JEDNORAZOWY() { TypPrzedmiotu = Jednorazowy; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct RELACYJNY : PRZEDMIOT
{
	string DefRelacji;
	bool Sciana;
	RELACYJNY() { TypPrzedmiotu = Relacyjny; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct ZWOJ : PRZEDMIOT
{
	WORD Czar;
	ZWOJ(){ TypPrzedmiotu = Zwoj; }
	ZWOJ( string id, string nazwa, char znak, WORD kolor, WORD waga, WORD wartosc, WORD czar )
	{
		TypPrzedmiotu = Zwoj;
		ID = id;
		Nazwa = nazwa;
		Znak = znak;
		Kolor = kolor;
		Waga = waga;
		Wartosc = wartosc;
		Czar = czar;
	}
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct RUNA : PRZEDMIOT
{
	WORD Czar;
	RUNA(){ TypPrzedmiotu = Runa; }
	RUNA( string id, string nazwa, char znak, WORD kolor, WORD waga, WORD wartosc, WORD czar )
	{
		TypPrzedmiotu = Runa;
		ID = id;
		Nazwa = nazwa;
		Znak = znak;
		Kolor = kolor;
		Waga = waga;
		Wartosc = wartosc;
		Czar = czar;
	}
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct KSIEGA : PRZEDMIOT // mo�na si� z nich uczy� umiej�tno�ci
{
	WORD Umiejetnosc;
	KSIEGA() { TypPrzedmiotu = Ksiega; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct BRON_BIALA : PRZEDMIOT
{
	SECTOR Obrazenia;
	UCHAR WymaganaSila;
	//UCHAR WymaganyPoziom;
	TYP_BRONI_BIALEJ Typ;
	BRON_BIALA() { TypPrzedmiotu = BronBiala; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct BRON_DYSTANSOWA : PRZEDMIOT
{
	//UCHAR DoUmiejetnosci;
	UCHAR Zasieg;
	UCHAR WymaganaZrecznosc;
	TYP_BRONI_DYSTANSOWEJ Typ;
	BRON_DYSTANSOWA() { TypPrzedmiotu = BronDystansowa; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct POCISK : PRZEDMIOT
{
	SECTOR Obrazenia;
	TYP_BRONI_DYSTANSOWEJ Typ;
	POCISK() { TypPrzedmiotu = Pocisk; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct PANCERZ : PRZEDMIOT
{
	OCHRONA Ochrona;
	TYP_PANCERZA MiejsceZalozenia;
	PANCERZ() { TypPrzedmiotu = Pancerz; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

struct TARCZA : PRZEDMIOT
{
	SECTOR Obrona;
	TARCZA() { TypPrzedmiotu = Tarcza; }
	bool Uzyj();
	void RysujOpis( POS pozycja );
};

/****************************************************************************************************/

void DodajPrzedmiot( PRZEDMIOT* Przedmiot );

void AlokacjaPrzedmiotow();

short SzukajPrzedmiotu( string Nazwa );
int IndexPrzedmiotu( string ID );

/****************************************************************************************************/

extern array< PRZEDMIOT* > Przedmioty;

/****************************************************************************************************/