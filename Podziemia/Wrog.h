#pragma once

#include "stdafx.h"

/****************************************************************************************************/

struct WROG : OBJEKT
{
	string Nazwa;
	OCHRONA Ochrona;
	WORD Obrazenia;
	short Czar;
	BYTE Dzikosc;
	WORD MaxZycie;
	BYTE Czas1Ruchu; // 1pkt. = 0,1s.
	BYTE ZasiegRuchu;
	BYTE ZasiegAtaku;
	BYTE MaxWGrupie;
	RLGL::STRUCTURE Trofea;
	bool WrogoNastawiony;
};

/****************************************************************************************************/

struct WROG_NA_MAPIE : POSTAC_NIEZALEZNA
{
	WORD NR;

	WROG_NA_MAPIE()
	{
		Czar = -1;
		Typ = TPO_WROG;
	}

	WORD PobMaxZycie();
	UCHAR PobCzas1Ruchu();
	WORD PobObrazenia();
	UCHAR PobDzikosc();

	void PozostawTrofea();

	void DoEnergii( double Ile ) {}
	void DoPragnienia( double Ile ) {}
	void DoGlodu( double Ile ) {}

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

void DodajWroga( string ID, string Nazwa, char Znak, WORD Kolor, OCHRONA Ochrona, bool WrogoNastawiony,
				 WORD Obrazenia, BYTE Dzikosc, WORD MaxZycie, BYTE Czas1Ruchu, BYTE ZasiegRuchu, BYTE ZasiegAtaku,
				 BYTE MaxWGrupie, const RLGL::STRUCTURE& Trofea, short Czar = -1 );

void AlokacjaWrogow();

short SzukajWroga( string Nazwa );

int IndexWroga( string ID );

/****************************************************************************************************/

extern array< WROG > Wrogowie;

/****************************************************************************************************/