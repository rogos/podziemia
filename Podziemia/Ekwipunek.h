#pragma once

#include "stdafx.h"

/****************************************************************************************************/

struct EKWIPUNEK
{
	POJEMNIK Plecak;
	UCHAR& SilaPostaci;
	unsigned MaxObciazenie;
	union {
		short NoszonePrzedmioty[5];
		struct {
			short PrawaDlon;
			short LewaDlon;
			short Helm;
			short Zbroja;
			short Szata;
		};
	};
	WORD WybranyPrzedmiot;
	enum PROCES{ Przegladanie, PodawanieIlosci } Proces;
	bool ListaNoszonychPrzedmiotow;
	BYTE NoszonyPrzedmiot;
	EKWIPUNEK( UCHAR& _SilaPostaci ) : SilaPostaci( _SilaPostaci )
	{
		LewaDlon = PrawaDlon = Helm = Zbroja = Szata = -1;
		WybranyPrzedmiot = 0;
		Proces = Przegladanie;
		ListaNoszonychPrzedmiotow = false;
		NoszonyPrzedmiot = 0;
	}
	void Rysuj();
	bool Pokaz( POS PozycjaPostaci );
	void RusujCoMaWRecach();

	unsigned Dodaj( string NazwaPrzedmiotu, WORD Ilosc = 1 );
	unsigned Dodaj( WORD NRPrzedmiotu, WORD Ilosc = 1 );
	void Wyrzuc( POS Pozycja, unsigned Ilosc = 1 );
	bool Zdejmij( char Miejsce );
	bool UzyjPrzedmiotu( CHAR Adres );

	void Zapisz( PLIK& plik );
	void Wczytaj( PLIK& plik );
};

/****************************************************************************************************/