#include "stdafx.h"

/****************************************************************************************************/

STD_BUFOR StdBuf;

/****************************************************************************************************/

bool BUFOR::operator () ()
{
	for( WORD i = 0; i < 80 * 40; i++ )
	{
		if( StareZnaki[ i ].Char.AsciiChar != Znaki[ i ].Char.AsciiChar || StareZnaki[ i ].Attributes != Znaki[ i ].Attributes ) break;
		if( i == 80 * 40 - 1 && StaraPozycjaKursora == PozycjaKursora ) return false;
	}

	COORD Pozycja = { 0, 0 };
	COORD Rozmiar = { 80, 40 };
	SMALL_RECT Prostokat = { 0, 0, 79, 39 };

	WriteConsoleOutputA( GetStdHandle(STD_OUTPUT_HANDLE), Znaki, Rozmiar, Pozycja, &Prostokat );
	SetConsoleCursorPosition( GetStdHandle(STD_OUTPUT_HANDLE), PozycjaKursora );

	for( WORD i = 0; i < 80 * 40; i++ ) StareZnaki[ i ] = Znaki[ i ];
	StaraPozycjaKursora = PozycjaKursora;

	return true;
}

/****************************************************************************************************/

void STD_BUFOR::WstawZnak( char Znak, WORD Kolor, POS Pozycja, bool PoprawNaPolskieLitery )
{
	if( PoprawNaPolskieLitery ) PoprawZPolskimiLiterami( Znak );

	StdBufor->Znaki[ Pozycja.Y * 80 + Pozycja.X ].Char.AsciiChar = Znak;
	StdBufor->Znaki[ Pozycja.Y * 80 + Pozycja.X ].Attributes = Kolor;
}

/****************************************************************************************************/

void STD_BUFOR::PokolorujZnak( WORD Kolor, POS Pozycja )
{
	StdBufor->Znaki[ Pozycja.Y * 80 + Pozycja.X ].Attributes = Kolor;
}

/****************************************************************************************************/

void STD_BUFOR::Napisz( string Tekst, unsigned Od, unsigned Do )
{
	short pozycja_znaku = 0;

	for( int i = Od; i < ( Do == 0 || Do > Tekst.length() ? Tekst.length() : Do ); i++ )
	{
		if( Do != 0 && i >= Do ) break;

		WstawZnak( Tekst[i], KolorPisania, PozycjaPisania( pozycja_znaku, 0 ) );
		pozycja_znaku++;
	}

	PozycjaPisania.X += Tekst.length();
	PozycjaPisania.Y += PozycjaPisania.X / 80;
	PozycjaPisania.X = PozycjaPisania.X % 80;
}

/****************************************************************************************************/

void STD_BUFOR::NapiszIZawijaj( string Tekst, WORD Szerokosc, POS Pozycja )
{
	WORD znak_linii = 0;
	WORD linia = 0;
	unsigned index = 0, index2;
	while( index = Tekst.find( "\\n", index ), index != string::npos ) {
		Tekst.replace( index, 2, " \\n " );
		index += 4;
	}
	index = 0;
	for(;;)
	{
		UstawPozycjePisania( Pozycja( znak_linii, linia ) );

		index2 = Tekst.find_first_of( ' ', index );
		
		string slowo = Tekst.substr( index, ( index2 == string::npos ? Tekst.length() : index2 ) - index );
		if( slowo == "\\n" )
		{
			znak_linii = 0;
			linia++;
			UstawPozycjePisania( Pozycja( 0, linia ) );
			index = index2 + 1;
			continue;
		}

		znak_linii += slowo.length() + 1;
		if( znak_linii - 1 > Szerokosc )
		{
			znak_linii = slowo.length() + 1;
			linia++;
			UstawPozycjePisania( Pozycja( 0, linia ) );
		}
		if( index2 == string::npos )
		{
			Napisz( slowo );
			break;
		}
		else Napisz( slowo + " " );

		index = index2 + 1;
	}
}

/****************************************************************************************************/

STD_BUFOR& STD_BUFOR::operator << ( string Tekst )
{
	Napisz( Tekst );
	return *this;
}

/****************************************************************************************************/

STD_BUFOR& STD_BUFOR::operator << ( int Tekst )
{
	Napisz( _itostr( Tekst ) );
	return *this;
}

/****************************************************************************************************/

STD_BUFOR& STD_BUFOR::operator >> ( WORD Kolor )
{
	KolorPisania = Kolor;
	return *this;
}

/****************************************************************************************************/

STD_BUFOR& STD_BUFOR::operator >> ( POS Pozycja )
{
	PozycjaPisania = Pozycja;
	return *this;
}

/****************************************************************************************************/

void STD_BUFOR::Wypelnij( WORD Kolor, char Znak )
{
	for( int i = 0; i < 80 * 40; i++ )
	{
		if( Znak != 0 ) StdBufor->Znaki[ i ].Char.AsciiChar = Znak;
		StdBufor->Znaki[ i ].Attributes = Kolor;
	}
}

/****************************************************************************************************/

void STD_BUFOR::Czysc()
{
	Wypelnij( 0, 0 );
}

/****************************************************************************************************/

void STD_BUFOR::Czysc( POS Pozycja, WORD Dlugosc )
{
	for( int i = 0; i < Dlugosc; i++ )
	{
		StdBufor->Znaki[ i + Pozycja.Y * 80 + Pozycja.X ].Char.AsciiChar = ' ';
		StdBufor->Znaki[ i + Pozycja.Y * 80 + Pozycja.X ].Attributes = xCza + tCza;
	}
}

/****************************************************************************************************/

void STD_BUFOR::Ustaw( BUFOR& Bufor )
{
	StdBufor = &Bufor;
}

/****************************************************************************************************/

void STD_BUFOR::UstawDomyslny()
{
	StdBufor = DomyslnyBufor;
}

/****************************************************************************************************/

void STD_BUFOR::UstawPozycjePisania( POS NowaPozycja )
{
	PozycjaPisania = NowaPozycja;
}

/****************************************************************************************************/

void STD_BUFOR::UstawKolorPisania( WORD NowyKolor )
{
	KolorPisania = NowyKolor;
}

/****************************************************************************************************/

void STD_BUFOR::UstawPozycjeKursora( POS NowaPozycja )
{
	StdBufor->PozycjaKursora = NowaPozycja;
}

/****************************************************************************************************/

void STD_BUFOR::SchowajKursor()
{
	CONSOLE_CURSOR_INFO kursor = { 100, false };
	SetConsoleCursorInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &kursor );
}

/****************************************************************************************************/

void STD_BUFOR::PokazKursor( DWORD Rozmiar )
{
	CONSOLE_CURSOR_INFO kursor = { Rozmiar, true };
	SetConsoleCursorInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &kursor );
}

/****************************************************************************************************/

void STD_BUFOR::Rysuj()
{
	( *StdBufor )();
}

/****************************************************************************************************/

char PoprawZPolskimiLiterami( char& Znak )
{
	switch( Znak )
	{
	case '�': Znak = 165; break;
	case '�': Znak = 164; break;
	case '�': Znak = 134; break;
	case '�': Znak = 143; break;
	case '�': Znak = 169; break;
	case '�': Znak = 169; break;
	case '�': Znak = 136; break;
	case '�': Znak = 157; break;
	case '�': Znak = 228; break;
	case '�': Znak = 227; break;
	case '�': Znak = 162; break;
	case '�': Znak = 224; break;
	case '�': Znak = 152; break;
	case '�': Znak = 151; break;
	case '�': Znak = 190; break;
	case '�': Znak = 189; break;
	case '�': Znak = 171; break;
	case '�': Znak = 141; break;
	}
	return Znak;
}

/****************************************************************************************************/