#pragma once

/****************************************************************************************************/

#include "stdafx.h"

/****************************************************************************************************/

enum TYP_POSTACI{ TPO_BOHATER, TPO_WROG, TPO_OSOBA };

/****************************************************************************************************/

struct POSTAC : OBJEKT
{
	string Nazwa;

	TYP_POSTACI Typ;

	double Zycie;
	virtual WORD PobMaxZycie() = 0;

	short Tura;

	OCHRONA Ochrona;
	
	array< UROK > Uroki;

	POS Pozycja;

	void BronSie( OBRAZENIA obrazenia );

	void DoZycia( double Ile );
	virtual void DoEnergii( double Ile ) = 0;
	virtual void DoPragnienia( double Ile ) = 0;
	virtual void DoGlodu( double Ile ) = 0;

	void ZapiszPostac( PLIK& Plik );
	void WczytajPostac( PLIK& Plik );
};

enum STATUS_POSTACI { SP_CHODZI, SP_STOI, SP_ATAKUJE, SP_SPI };

/****************************************************************************************************/

struct POSTAC_NIEZALEZNA : POSTAC
{
	bool WrogoNastawiony;

	short Czar;
	BYTE Dzikosc;

	POS PozycjaAtaku;
	POS StalaPozycja;
	BYTE ZasiegRuchu;
	BYTE ZasiegAtaku;

	bool Idz( POS pozycja );
	bool Atakuj();
	void WykonajRuch();

	virtual UCHAR PobCzas1Ruchu() = 0;
	virtual WORD PobObrazenia() = 0;
	virtual WORD PobMaxZycie() = 0;
	virtual UCHAR PobDzikosc() { return Dzikosc; }

	virtual void PozostawTrofea() = 0;

	void Czaruj( WORD czar );
	void Smierc();

	void RysujPasekZycia( POS pozycja = POS( 66, 1 ) );

	virtual void Zapisz( PLIK& Plik );
	virtual void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

bool StworzSciezke( array< POS >& Path, POS Begin, POS End, bool Diagonal = true );

/****************************************************************************************************/