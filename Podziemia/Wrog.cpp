#include "stdafx.h"

/****************************************************************************************************/

array< WROG > Wrogowie;

/****************************************************************************************************/

WORD WROG_NA_MAPIE::PobMaxZycie() { return Wrogowie[ NR ].MaxZycie; }
UCHAR WROG_NA_MAPIE::PobCzas1Ruchu() { return Wrogowie[ NR ].Czas1Ruchu; }
WORD WROG_NA_MAPIE::PobObrazenia() { return Wrogowie[ NR ].Obrazenia; }
UCHAR WROG_NA_MAPIE::PobDzikosc() { return Wrogowie[ NR ].Dzikosc; }

/****************************************************************************************************/

void WROG_NA_MAPIE::PozostawTrofea()
{
	RLGL::ELEMENT& trofea = Wrogowie[ NR ].Trofea;
	for( WORD i = 0; i < trofea.Size(); i++ ) {
		if( Losuj( 1, 100 ) <= StanGry->Postac.Umiejetnosci["trofea"] )
			AktualnaMapa->DodajPrzedmiot( IndexPrzedmiotu( trofea( i ).GetName() ), trofea( i ).GetValue(), Pozycja );
	}
}

/****************************************************************************************************/

void WROG_NA_MAPIE::Zapisz( PLIK& Plik )
{
	POSTAC_NIEZALEZNA::Zapisz( Plik );
	Plik << NR;
}

/****************************************************************************************************/

void WROG_NA_MAPIE::Wczytaj( PLIK& Plik )
{
	POSTAC_NIEZALEZNA::Wczytaj( Plik );
	Plik >> NR;
}

/****************************************************************************************************/
/****************************************************************************************************/

void DodajWroga( string ID, string Nazwa, char Znak, WORD Kolor, OCHRONA Ochrona, bool WrogoNastawiony,
				 WORD Obrazenia, BYTE Dzikosc, WORD MaxZycie, BYTE Czas1Ruchu, BYTE ZasiegRuchu, BYTE ZasiegAtaku, BYTE MaxWGrupie, const RLGL::STRUCTURE& Trofea, short Czar )
{
	WROG w;
	w.ID = ID;
	w.Nazwa = Nazwa;
	w.Znak = Znak;
	w.Kolor = Kolor;
	w.Ochrona = Ochrona;
	w.WrogoNastawiony = WrogoNastawiony;
	w.Obrazenia = Obrazenia;
	w.MaxZycie = MaxZycie;
	w.Dzikosc = Dzikosc;
	w.Czas1Ruchu = Czas1Ruchu;
	w.ZasiegRuchu = ZasiegRuchu;
	w.ZasiegAtaku = ZasiegAtaku;
	w.MaxWGrupie = MaxWGrupie;
	w.Trofea = Trofea;
	w.Czar = Czar;
	Wrogowie.add( w );
}

/****************************************************************************************************/

short SzukajWroga( string Nazwa )
{
	for( WORD i = 0; i < Wrogowie.size(); i++ )
		if( Wrogowie[ i ].Nazwa == Nazwa )
		{
			return i;
		}

	return -1;
}

/****************************************************************************************************/

int IndexWroga( string ID )
{
	for( BYTE i = 0; i < Wrogowie.size(); i++ )
		if( Wrogowie[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/