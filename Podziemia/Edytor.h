#pragma once

#include "stdafx.h"

/****************************************************************************************************/

struct POSTAC_NA_MAPIE
{
	WORD NR;
	POS  Pozycja;
};

struct WPIS_LEGENDY
{
	WORD NR;
	string ID;
};

/****************************************************************************************************/

struct DEF_MAPY : MAPA_GENEROWANA
{
	POS    Rozmiar;
	BYTE** Kafelki;

	array<PRZEDMIOT_NA_MAPIE> Przedmioty;
	array<POSTAC_NA_MAPIE> Postacie;
	array<POSTAC_NA_MAPIE> Osoby;

	DEF_MAPY() : Kafelki(0) {}
	~DEF_MAPY() {
		if( Kafelki ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Kafelki[i];
	}

	void UstawRozmiar( POS rozmiar );

	void Rysuj( POS pozycja, POS rozmiar, POS pozycja_mapy );

	bool PozycjaNaMapie( POS pozycja );

	short SzukajPrzedmiotu(POS pozycja);
	short SzukajPostaci(POS pozycja);
	short SzukajOsoby(POS pozycja);

	bool PozycjaWolna( POS pozycja );

	////
	bool WstawKafelek( BYTE kafelek, POS pozycja );
	bool DodajPrzedmiot( WORD przedmiot, WORD ilosc, POS pozycja );
	bool DodajPostac( WORD postac, POS pozycja );
	bool DodajOsobe( WORD osoba, POS pozycja );
	
	POS LosujPozycje( POS pozycja_obszaru, POS rozmiar_obszaru );
	POS PozycjaObok( POS pozycja, POS pozycja_obszaru, POS rozmiar_obszaru );
	////

	bool UsunPrzedmiot(POS pozycja);
	bool UsunPostac(POS pozycja);
	bool UsunOsobe(POS pozycja);

	void Zapisz( PLIK& plik );
	void Wczytaj( PLIK& plik );
};

/*..................................................................................................*/

struct MAPA_Z_PLIKU {
	string Adres;
	DEF_MAPY* Mapa;
	MAPA_Z_PLIKU() : Mapa(0) {}
	//MAPA_Z_PLIKU( string adres, DEF_MAPY* mapa ) : Adres(adres), Mapa(mapa) {}
	//~MAPA_Z_PLIKU() { if(Mapa) delete Mapa; }
};

extern vector<MAPA_Z_PLIKU> MapyZPliku;

struct DESTRUKTOR {
	~DESTRUKTOR() {
		for( WORD i = 0; i < MapyZPliku.size(); i++ )
			if(MapyZPliku[i].Mapa) delete MapyZPliku[i].Mapa;
	}
};

/****************************************************************************************************/

struct EDYTOR
{
	DEF_MAPY Mapa;
	POS      PozycjaMapy;
	POS      PozycjaKursora;
	WORD     WybranyObjekt[4];
	BYTE     TypObjektu;
	BYTE     RozmiarPedzla;
	bool     PokazujKursor;

	EDYTOR() : PozycjaMapy(0, -3), PozycjaKursora(1, 1), TypObjektu(0), RozmiarPedzla(0), PokazujKursor(true)
	{ memset(WybranyObjekt, 0, 8); }

	void Rysuj();
	void Przetwarzaj();

	bool StworzNowaMape();
	bool ZapiszMape();
	bool WczytajMape();

	bool WstawMape();

	POS AktualnaPozycja() { return PozycjaMapy + PozycjaKursora; };
};

/****************************************************************************************************/