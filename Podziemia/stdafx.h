// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <windows.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <ctime>
#include <math.h>
#include <stdarg.h>
#include <vector>
#include <list>
#include "zlib.h"
#include "zconf.h"

#include "roguelike.h"
#include "../RLGL/RLGL.h"
#include "../RLGL/RLGLscript.h"
#include "../RLGL/RLGLfiles.h"
#include "../RLGL/RLGLarray.h"
#include "../RLGL/RLGLio.h"

using namespace std;

#include "Stale.h"
#include "Plik.h"
#include "Ekran.h"
#include "Funkcje.h"
#include "Objekt.h"
#include "Wlasciwosci.h"
#include "Postac.h"
#include "Magia.h"
#include "Przedmioty.h"
#include "Ekwipunek.h"
#include "Wrog.h"
#include "Osoba.h"
#include "Mapa.h"
#include "GeneratoryMap.h"
#include "Bohater.h"
#include "Stosunki.h"
#include "Stan_gry.h"
#include "Edytor.h"
#include "Gra.h"