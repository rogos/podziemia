#pragma once

/****************************************************************************************************/

#include "stdafx.h"

/****************************************************************************************************/

struct OBJEKT
{
	string ID;
	char Znak;
	WORD Kolor;
	OBJEKT() : Znak( 0 ), Kolor( 0 ) {}
	OBJEKT( char _Znak, WORD _Kolor ) : Znak( _Znak ), Kolor( _Kolor ) {}
};

/****************************************************************************************************/

struct OBJEKT_NA_MAPE
{
	WORD Typ;
	SECTOR Ilosc;
};

/****************************************************************************************************/

struct ILOSC
{
	WORD Od, Do; // poziom
	FSECTOR IloscPoczatkowa, IloscKoncowa; // np. 0.6 = 60%

	ILOSC() : Od( 0 ), Do( 0 ) {}

	ILOSC( WORD _Od, WORD _Do, FSECTOR _IloscPoczatkowa, FSECTOR _IloscKoncowa )
		: Od( _Od ), Do( _Do ), IloscPoczatkowa( _IloscPoczatkowa ), IloscKoncowa( _IloscKoncowa ) {}

	ILOSC( WORD _Od, WORD _Do, SECTOR _IloscPoczatkowa, SECTOR _IloscKoncowa )
		: Od( _Od ), Do( _Do )
	{
		IloscPoczatkowa.Begin = _IloscPoczatkowa.Begin;
		IloscPoczatkowa.End = _IloscPoczatkowa.End;
		IloscKoncowa.Begin = _IloscKoncowa.Begin;
		IloscKoncowa.End = _IloscKoncowa.End;
	}
};

/****************************************************************************************************/

struct ANIMACJA : OBJEKT
{
	WORD Czas; // Czas animacji na jednym kafelku w ms.

	ANIMACJA() : OBJEKT(), Czas( 0 ) {}
	ANIMACJA( char _Znak, WORD _Kolor, WORD _Czas ) : OBJEKT( _Znak, _Kolor ), Czas( _Czas ) {}

	void Rysuj( POS Pozycja, bool Wyswietl = true );
};

/****************************************************************************************************/

extern RLGL::STRUCTURE DaneGry;