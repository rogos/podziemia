#pragma once

/****************************************************************************************************/

#include "stdafx.h"

/****************************************************************************************************/

struct UMIEJETNOSC {
	string ID;
	string Nazwa;
	UMIEJETNOSC(){}
	UMIEJETNOSC( string id, string nazwa ) : ID(id), Nazwa(nazwa) {}
};

extern array<UMIEJETNOSC> Umiejetnosci;

short IndexUmiejetnosci( string ID );

/****************************************************************************************************/

void InicjalizujBohatera();

/****************************************************************************************************/

struct UMIEJETNOSCI
{
	array<BYTE> Wartosci;
	WORD Wybrana;
	BYTE& operator [] ( string id );
	BYTE& operator [] ( WORD index );
	UMIEJETNOSCI() : Wartosci( Umiejetnosci.size() ), Wybrana(0) {}
};

/****************************************************************************************************/

struct DEF_WPISU {
	string ID;
	string Tresc;
	//bool KonczyTemat;
};
struct DEF_TEMATU {
	string ID;
	string Tytul;
	vector<DEF_WPISU> Wpisy;
};

extern vector<DEF_TEMATU> TematyDziennika;

short IndexTematu( string ID );

/****************************************************************************************************/

struct DZIENNIK
{
	struct WPIS {
		string ID;
		int Wartosc;
		WPIS() : Wartosc(0) {}
		WPIS( string id, int wartosc = 0 ) : ID(id), Wartosc(wartosc) {}
	};
	struct TEMAT {
		string ID;
		array<WPIS> Wpisy;
		bool Wykonane;
		TEMAT() : Wykonane(false) {}
	};
	array<TEMAT> Tematy;
	WORD WybranyTemat;

	DZIENNIK() : WybranyTemat(0) {}

	TEMAT& operator [] ( unsigned temat );
	bool UstawJakoWykonane( string temat );
	bool IstniejeWpis( string temat, string wpis );
	bool DodajWpis( string temat, string wpis, int wartosc = 0 );
	int& WartoscWpisu( string temat, string wpis );

	void Rysuj();
	bool RysujTemat();
	void Przetwarzaj();
};
/****************************************************************************************************/

struct BOHATER : POSTAC
{
	PLEC Plec;
	string Klasa;
	UCHAR Waga, Wzrost;

	double Energia;
	WORD MaxZycie, MaxEnergia;
	
	double Glod, Pragnienie;

	BYTE sila, zrecznosc, szybkosc;
	UMIEJETNOSCI Umiejetnosci;

	array<WORD> Czary;

	DZIENNIK Dziennik;

	bool Zyje;

	bool Skradanie;
	bool Niewidoczny;

	WORD WybranyCzar;

	EKWIPUNEK Ekwipunek;

	BYTE Poziom;
	unsigned Doswiadczenie, NowyPoziom, DoNastepnegoPoziomu;
	WORD Punkty;

	bool ListaParametrow;
	BYTE Parametr;

	BOHATER( WORD _Kolor = xBia, char _Znak = '@', string _Nazwa = "" ) :
		Ekwipunek( sila )
	{
		Kolor = _Kolor;
		Znak =  _Znak;
		Typ = TPO_BOHATER;
		Punkty = 0;
		WybranyCzar = 0;
		Tura = 1;
		Zyje = true;
		Skradanie = false;
		Niewidoczny = false;
		Umiejetnosci.Wybrana = 0;
		Doswiadczenie = 0;
		NowyPoziom = DoNastepnegoPoziomu = 50;
		Poziom = 1;
		Pragnienie = Glod = 0;
		Energia = MaxEnergia = 20;
		Zycie = MaxZycie = 20;
		ListaParametrow = false;
		Parametr = 0;
	}

	WORD PobMaxZycie() { return MaxZycie; }

	void DoEnergii( double Ile );
	void DoPragnienia( double Ile );
	void DoGlodu( double Ile );

	void RysujStatystyki();
	bool PokazStatystyki();
	void Rysuj();
	bool PrzejdzNaMapeObok( POS pozycja );
	bool Idz( DIRECTION Kierunek );
	bool Patrz();
	bool Rozmawiaj();
	void Handluj( OSOBA* osoba );
	bool Atakuj( WORD Postac, OBRAZENIA Obrazenia );
	bool Uderz( WORD Postac );
	bool Zaatakuj();
	bool Skocz();
	POS Celuj( string TekstNaglowka, char ZnakPotwierdzenia, UCHAR Zasieg );
	bool Strzelaj();
	void RysujListeCzarow();
	bool WybierzCzar();
	bool RzucCzar( short Czar = -1 );
	bool Czekaj();
	bool SkradajSie(); //creep
	bool ZamknijDrzwi();
	void PodniesPrzedmiot( WORD KtoryZKoleiPrzedmiotNaMapie, unsigned Ilosc = 0 );
	void RysujWyborPrzedmiotow( WORD PozycjaStrzalki );
	bool Wez();
	void UstawStatystyki( UCHAR _Sila, UCHAR _Zrecznosc, UCHAR _Szybkosc, WORD _Zycie, WORD _Energia );

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/