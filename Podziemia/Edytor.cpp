#include "stdafx.h"

/****************************************************************************************************/

vector<MAPA_Z_PLIKU> MapyZPliku;
DESTRUKTOR Destruktor;

/****************************************************************************************************/

void DEF_MAPY::Rysuj( POS pozycja, POS rozmiar, POS pozycja_mapy )
{
	for( int y = 0; y < rozmiar.Y; y++ )
		for( int x = 0; x < rozmiar.X; x++ )
			if( pozycja_mapy.X + pozycja.X + x >= 0 && pozycja_mapy.X + pozycja.X + x < Rozmiar.X &&
				pozycja_mapy.Y + pozycja.Y + y >= 0 && pozycja_mapy.Y + pozycja.Y + y < Rozmiar.Y )
					StdBuf.WstawZnak( ::Kafelki[ Kafelki[pozycja_mapy.X + pozycja.X + x][pozycja_mapy.Y + pozycja.Y + y] ].Znak,
					                  ::Kafelki[ Kafelki[pozycja_mapy.X + pozycja.X + x][pozycja_mapy.Y + pozycja.Y + y] ].Kolor,
									  POS( pozycja.X + x, pozycja.Y + y ), false );

	for( WORD i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[i].Pozycja.X - pozycja_mapy.X - pozycja.X >= 0 && Przedmioty[i].Pozycja.X - pozycja_mapy.X - pozycja.X < rozmiar.X &&
			Przedmioty[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y >= 0 && Przedmioty[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y < rozmiar.Y )
				StdBuf.WstawZnak( ::Przedmioty[ Przedmioty[i].NR ]->Znak, ::Przedmioty[ Przedmioty[i].NR ]->Kolor,
								  POS( Przedmioty[i].Pozycja.X - pozycja_mapy.X, Przedmioty[i].Pozycja.Y - pozycja_mapy.Y ), false );

	for( WORD i = 0; i < Postacie.size(); i++ )
		if( Postacie[i].Pozycja.X - pozycja_mapy.X - pozycja.X >= 0 && Postacie[i].Pozycja.X - pozycja_mapy.X - pozycja.X < rozmiar.X &&
			Postacie[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y >= 0 && Postacie[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y < rozmiar.Y )
				StdBuf.WstawZnak( ::Wrogowie[ Postacie[i].NR ].Znak, ::Wrogowie[ Postacie[i].NR ].Kolor,
								  POS( Postacie[i].Pozycja.X - pozycja_mapy.X, Postacie[i].Pozycja.Y - pozycja_mapy.Y ), false );

	for( WORD i = 0; i < Osoby.size(); i++ )
		if( Osoby[i].Pozycja.X - pozycja_mapy.X - pozycja.X >= 0 && Osoby[i].Pozycja.X - pozycja_mapy.X - pozycja.X < rozmiar.X &&
			Osoby[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y >= 0 && Osoby[i].Pozycja.Y - pozycja_mapy.Y - pozycja.Y < rozmiar.Y )
				StdBuf.WstawZnak( ::Osoby[ Osoby[i].NR ].Znak, ::Osoby[ Osoby[i].NR ].Kolor,
								  POS( Osoby[i].Pozycja.X - pozycja_mapy.X, Osoby[i].Pozycja.Y - pozycja_mapy.Y ), false );
}

/****************************************************************************************************/

void DEF_MAPY::UstawRozmiar( POS rozmiar )
{
	if( Kafelki ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Kafelki[i];

	Rozmiar = rozmiar;

	Kafelki = new BYTE*[Rozmiar.X];
	for(unsigned i = 0; i < Rozmiar.X; i++) {
		Kafelki[i] = new BYTE[Rozmiar.Y];
		memset( Kafelki[i], 0, Rozmiar.Y );
	}
}

/****************************************************************************************************/

bool DEF_MAPY::PozycjaNaMapie( POS pozycja )
{
	if( pozycja.X >= 0 && pozycja.X < Rozmiar.X &&
		pozycja.Y >= 0 && pozycja.Y < Rozmiar.Y ) return true;
	return false;
}

/****************************************************************************************************/

short DEF_MAPY::SzukajPrzedmiotu(POS pozycja)
{
	for(int i = Przedmioty.size() - 1; i >= 0; i--)
		if(Przedmioty[i].Pozycja == pozycja) return i;

	return -1;
}

/*..................................................................................................*/

short DEF_MAPY::SzukajPostaci(POS pozycja)
{
	for(int i = Postacie.size() - 1; i >= 0; i--)
		if(Postacie[i].Pozycja == pozycja) return i;

	return -1;
}

/*..................................................................................................*/

short DEF_MAPY::SzukajOsoby(POS pozycja)
{
	for(int i = Osoby.size() - 1; i >= 0; i--)
		if(Osoby[i].Pozycja == pozycja) return i;

	return -1;
}

/*..................................................................................................*/

bool DEF_MAPY::PozycjaWolna( POS pozycja )
{
	if( PozycjaNaMapie(pozycja) && ::Kafelki[ Kafelki[pozycja.X][pozycja.Y] ].Typ == TK_GRUNT &&
		SzukajPostaci(pozycja) < 0 && SzukajOsoby(pozycja) < 0 && SzukajPrzedmiotu(pozycja) < 0 )
		return true;
	return false;
}

/****************************************************************************************************/

bool DEF_MAPY::WstawKafelek(BYTE kafelek, POS pozycja)
{
	if(!PozycjaNaMapie(pozycja)) return false;
	Kafelki[pozycja.X][pozycja.Y] = kafelek;
	return true;
}

/*..................................................................................................*/

bool DEF_MAPY::DodajPrzedmiot(WORD przedmiot, WORD ilosc, POS pozycja)
{
	if( !PozycjaNaMapie(pozycja) ) return false;

	short p = SzukajPrzedmiotu(pozycja);

	if( p >= 0 && Przedmioty[p].NR == przedmiot ) Przedmioty[p].Ilosc += ilosc;
	else {
		Przedmioty.resize(Przedmioty.size() + 1);
		Przedmioty.back().Ilosc = ilosc;
		Przedmioty.back().NR = przedmiot;
		Przedmioty.back().Pozycja = pozycja;
	}

	return true;
}

/*..................................................................................................*/

bool DEF_MAPY::DodajPostac(WORD postac, POS pozycja)
{
	if( !PozycjaNaMapie(pozycja) ) return false;

	short p = SzukajPostaci(pozycja);
	short o = SzukajOsoby(pozycja);

	if( p >= 0 || o >= 0 ) return false;
	else {
		Postacie.resize(Postacie.size() + 1);
		Postacie.back().NR = postac;
		Postacie.back().Pozycja = pozycja;
	}

	return true;
}

/*..................................................................................................*/

bool DEF_MAPY::DodajOsobe(WORD osoba, POS pozycja)
{
	if( !PozycjaNaMapie(pozycja) ) return false;

	short p = SzukajPostaci(pozycja);
	short o = SzukajOsoby(pozycja);

	if( p >= 0 || o >= 0 ) return false;
	else {
		Osoby.resize(Osoby.size() + 1);
		Osoby.back().NR = osoba;
		Osoby.back().Pozycja = pozycja;
	}

	return true;
}

/*..................................................................................................*/

POS DEF_MAPY::LosujPozycje( POS pozycja_obszaru, POS rozmiar_obszaru )
{
	for(;;)
	{
		POS pozycja = POS( Losuj( pozycja_obszaru.X, pozycja_obszaru.X + rozmiar_obszaru.X - 1 ),
		                   Losuj( pozycja_obszaru.Y, pozycja_obszaru.Y + rozmiar_obszaru.Y - 1 ) );
		if( !PozycjaNaMapie(pozycja) || PozycjaWolna(pozycja) ) return pozycja;
	}
}

/*..................................................................................................*/

POS DEF_MAPY::PozycjaObok( POS pozycja, POS pozycja_obszaru, POS rozmiar_obszaru )
{
	array< POS > punkty;
	punkty.add( pozycja );

	for( unsigned i = 1; i < 100; i++ )
	{
		if( punkty.size() == 0 ) return POS_NOT_FOUND;

		array< POS > _punkty = punkty;
		punkty.clear();
		
		bool end = false;
		
		for( unsigned j = 0; j < _punkty.size(); j++ )
		{	
			for( BYTE k = 0; k < 8; k++ )
			{
				pozycja = _punkty[j] + WEKTORY[k];

				// Je�eli punkt wykracza poza obszar pomija go
				if( pozycja.X < pozycja_obszaru.X || pozycja.X >= pozycja_obszaru.X + rozmiar_obszaru.X ||
					pozycja.Y < pozycja_obszaru.Y || pozycja.Y >= pozycja_obszaru.Y + rozmiar_obszaru.Y ) continue;
				
				if( PozycjaNaMapie(pozycja) && !PozycjaWolna(pozycja) ) {
					if( ::Kafelki[ Kafelki[pozycja.X][pozycja.Y] ].Typ == TK_GRUNT ) punkty.add(pozycja);
				}
				else return pozycja;
			}
		}
	}
}

/****************************************************************************************************/

bool DEF_MAPY::UsunPrzedmiot(POS pozycja)
{
	short p = SzukajPrzedmiotu(pozycja);
	if( p >= 0 ) { Przedmioty.erase(p); return true; }
	return false;
}

/*..................................................................................................*/

bool DEF_MAPY::UsunPostac(POS pozycja)
{
	short p = SzukajPostaci(pozycja);
	if( p >= 0 ) { Postacie.erase(p); return true; }
	return false;
}

/*..................................................................................................*/

bool DEF_MAPY::UsunOsobe(POS pozycja)
{
	short o = SzukajOsoby(pozycja);
	if( o >= 0 ) { Osoby.erase(o); return true; }
	return false;
}

/****************************************************************************************************/

void DEF_MAPY::Zapisz( PLIK& plik )
{
	plik.Zapisz( &Rozmiar, sizeof(POS) );
	for(unsigned i = 0; i < Rozmiar.X; i++) plik.Zapisz( Kafelki[i], Rozmiar.Y );

	plik << (WORD)( Przedmioty.size() );
	plik << (WORD)( Postacie.size() );
	plik << (WORD)( Osoby.size() );
	for(unsigned i = 0; i < Przedmioty.size(); i++) plik.Zapisz( &Przedmioty[i], sizeof(PRZEDMIOT_NA_MAPIE) );
	for(unsigned i = 0; i < Postacie.size(); i++) plik.Zapisz( &Postacie[i], sizeof(POSTAC_NA_MAPIE) );
	for(unsigned i = 0; i < Osoby.size(); i++) plik.Zapisz( &Osoby[i], sizeof(POSTAC_NA_MAPIE) );

	// zapisuje id objekt�w aby zmiany w skryptach nie wp�yn�y na wygl�d mapy
	plik << (BYTE)( ::Kafelki.size() );
	for(BYTE i = 0; i < ::Kafelki.size(); i++) plik << ::Kafelki[i].ID;
	plik << (WORD)( ::Przedmioty.size() );
	for(WORD i = 0; i < ::Przedmioty.size(); i++) plik << ::Przedmioty[i]->ID;
	plik << (WORD)( ::Wrogowie.size() );
	for(WORD i = 0; i < ::Wrogowie.size(); i++) plik << ::Wrogowie[i].ID;
	plik << (WORD)( ::Osoby.size() );
	for(WORD i = 0; i < ::Osoby.size(); i++) plik << ::Osoby[i].ID;
}

/****************************************************************************************************/

void DEF_MAPY::Wczytaj( PLIK& plik )
{
	plik.Wczytaj( &Rozmiar, sizeof(POS) );
	UstawRozmiar(Rozmiar);
	for(unsigned i = 0; i < Rozmiar.X; i++) plik.Wczytaj( Kafelki[i], Rozmiar.Y );

	WORD ilosc;
	plik >> ilosc;
	Przedmioty.resize(ilosc);
	plik >> ilosc;
	Postacie.resize(ilosc);
	plik >> ilosc;
	Osoby.resize(ilosc);
	for(unsigned i = 0; i < Przedmioty.size(); i++) plik.Wczytaj( &Przedmioty[i], sizeof(PRZEDMIOT_NA_MAPIE) );
	for(unsigned i = 0; i < Postacie.size(); i++) plik.Wczytaj( &Postacie[i], sizeof(POSTAC_NA_MAPIE) );
	for(unsigned i = 0; i < Osoby.size(); i++) plik.Wczytaj( &Osoby[i], sizeof(POSTAC_NA_MAPIE) );

	// poprawia indeksy objekt�w
	short index;
	vector<string> id;
	// kafelki //
	plik >> (BYTE&)(ilosc);
	id.resize(ilosc);
	for(BYTE i = 0; i < ilosc; i++) {
		plik >> id[i];
		if(IndexKafelka(id[i]) < 0) warning(AS_STR "Nie mo�na znale�� kafelka o id '" + id[i] + "'." );
	}
	for(WORD x = 0; x < Rozmiar.X; x++) for(WORD y = 0; y < Rozmiar.Y; y++) {
		index = IndexKafelka( id[Kafelki[x][y]] );
		Kafelki[x][y] = (index >= 0 ? index : 0);
	}
	// przedmioty //
	plik >> ilosc;
	id.resize(ilosc);
	for(WORD i = 0; i < ilosc; i++) {
		plik >> id[i];
		if(IndexPrzedmiotu(id[i]) < 0) warning(AS_STR "Nie mo�na znale�� przedmiotu o id '" + id[i] + "'." );
	}
	for(WORD i = 0; i < Przedmioty.size(); i++) {
		index = IndexPrzedmiotu( id[Przedmioty[i].NR] );
		Przedmioty[i].NR = (index >= 0 ? index : 0);
	}
	// postacie //
	plik >> ilosc;
	id.resize(ilosc);
	for(WORD i = 0; i < ilosc; i++) {
		plik >> id[i];
		if(IndexWroga(id[i]) < 0) warning(AS_STR "Nie mo�na znale�� postaci o id '" + id[i] + "'." );
	}
	for(WORD i = 0; i < Postacie.size(); i++) {
		index = IndexWroga( id[Postacie[i].NR] );
		Postacie[i].NR = (index >= 0 ? index : 0);
	}
	// osoby //
	plik >> ilosc;
	id.resize(ilosc);
	for(WORD i = 0; i < ilosc; i++) {
		plik >> id[i];
		if(IndexOsoby(id[i]) < 0) warning(AS_STR "Nie mo�na znale�� osoby o id '" + id[i] + "'." );
	}
	for(WORD i = 0; i < Osoby.size(); i++) {
		index = IndexOsoby( id[Osoby[i].NR] );
		Osoby[i].NR = (index >= 0 ? index : 0);
	}
}

/****************************************************************************************************/

void EDYTOR::Rysuj()
{
	StdBuf.Czysc();

	RysujRamke( POS(0, 0), POS(80, 3), xcZul, false );
	StdBuf >> xZul >> POS( 1, 1 ) << "Tu jest: ";
	if( AktualnaPozycja().X >= 0 && AktualnaPozycja().X < Mapa.Rozmiar.X &&
		AktualnaPozycja().Y >= 0 && AktualnaPozycja().Y < Mapa.Rozmiar.Y )
	{
		// kafelek
		StdBuf << Kafelki[ Mapa.Kafelki[AktualnaPozycja().X][AktualnaPozycja().Y] ].Nazwa;
		// przedmiot
		if( Mapa.SzukajPrzedmiotu(AktualnaPozycja()) >= 0 )
			StdBuf << ", " << Mapa.Przedmioty[Mapa.SzukajPrzedmiotu(AktualnaPozycja())].Ilosc << "x "
			<< Przedmioty[ Mapa.Przedmioty[Mapa.SzukajPrzedmiotu(AktualnaPozycja())].NR ]->Nazwa;
		// posta�
		if( Mapa.SzukajPostaci(AktualnaPozycja()) >= 0 )
			StdBuf << ", " << Wrogowie[ Mapa.Postacie[Mapa.SzukajPostaci(AktualnaPozycja())].NR ].Nazwa;
		// osoba
		if( Mapa.SzukajOsoby(AktualnaPozycja()) >= 0 )
			StdBuf << ", " << Osoby[ Mapa.Osoby[Mapa.SzukajOsoby(AktualnaPozycja())].NR ].Nazwa;
	}
	else StdBuf << "-";

	Mapa.Rysuj( POS(0, 3), POS(80, 34), PozycjaMapy );

	RysujRamke( POS(0, 37), POS(80, 3), xcSza, false );
	StdBuf >> POS(1, 38) >> xBia << AktualnaPozycja().X << ", " << AktualnaPozycja().Y;
	switch( TypObjektu ){
		case 0: if( Kafelki.size() > 0 ) StdBuf << ", KAFELKI, " << Kafelki[WybranyObjekt[0]].Nazwa << ", [" << RozmiarPedzla + 1 << "]"; break;
		case 1: if( Przedmioty.size() > 0 ) StdBuf << ", PRZEDMIOTY, " << Przedmioty[WybranyObjekt[1]]->Nazwa; break;
		case 2: if( Wrogowie.size() > 0 ) StdBuf << ", POSTACIE, " << Wrogowie[WybranyObjekt[2]].Nazwa; break;
		case 3: if( Osoby.size() > 0 ) StdBuf << ", OSOBY, " << Osoby[WybranyObjekt[3]].Nazwa; break;
	}

	if(PokazujKursor) StdBuf.PokolorujZnak( tBia, POS( PozycjaKursora.X, PozycjaKursora.Y ) );

	StdBuf.Rysuj();
}

/****************************************************************************************************/

void EDYTOR::Przetwarzaj()
{
	Mapa.UstawRozmiar( POS(60, 30) );
	int horizontal_cursor_shift = 0;

	Rysuj();

	for(;;)
	{
		RLGL::EVENT e = RLGL::GetEvent();

		if( e.Exist )
		{
			/* ko�czy prac� edytora */
			if( e.KeyCode == VK_ESCAPE ) return;

			/* pokazuje/chowie siatk� */
			//if( e.KeyCode == VK_F3 ) ShowGrid = !ShowGrid;

			/* pokazuje/chowie kursor */
			if( e.KeyCode == VK_F2 ) PokazujKursor = !PokazujKursor;

			/* tworzy now� map� */
			if( e.KeyCode == VK_F8 ) StworzNowaMape();

			/* wstawia map� ze stringa */
			if( e.KeyCode == VK_F4 ) WstawMape();

			/* zapisuje/wczytuje map� */
			if( e.KeyCode == VK_F5 ) ZapiszMape();
			if( e.KeyCode == VK_F6 ) WczytajMape();

			if( e.MouseCode == LEFT_BUTTON_PRESSED )
			{
				switch(TypObjektu)
				{
					case 0: { /* wstawianie lub pobieranie id kafelka z danej pozycji */
						if( Kafelki.size() == 0 ) break;
						if( e.ControlKeyState & LEFT_ALT_PRESSED ) {
							if( Mapa.PozycjaNaMapie(AktualnaPozycja()) )
								WybranyObjekt[0] = Mapa.Kafelki[AktualnaPozycja().X][AktualnaPozycja().Y];
						}
						else {
							for(short x = AktualnaPozycja().X - RozmiarPedzla; x <= AktualnaPozycja().X + RozmiarPedzla; x++)
								for(short y = AktualnaPozycja().Y - RozmiarPedzla; y <= AktualnaPozycja().Y + RozmiarPedzla; y++)
									Mapa.WstawKafelek(WybranyObjekt[0], POS(x, y));
						}
						break;
					}
					case 1: { /* wstawianie, usuwanie lub pobieranie id przedmiotu z danej pozycji */
						if( Przedmioty.size() == 0 || e.MouseMoved ) break;
						if( e.ControlKeyState & LEFT_ALT_PRESSED ) {
							short przedmiot = Mapa.SzukajPrzedmiotu(AktualnaPozycja());
							if(przedmiot >= 0) WybranyObjekt[1] = Mapa.Przedmioty[przedmiot].NR;
						}
						else if( e.ControlKeyState & LEFT_CTRL_PRESSED ) Mapa.UsunPrzedmiot(AktualnaPozycja());
						else Mapa.DodajPrzedmiot(WybranyObjekt[1], 1, AktualnaPozycja());
						break;
					}
					case 2: { /* wstawianie lub usuwanie lub pobieranie id postaci z danej pozycji */
						if( Wrogowie.size() == 0 || e.MouseMoved ) break;
						if( e.ControlKeyState & LEFT_ALT_PRESSED ) {
							short postac = Mapa.SzukajPostaci(AktualnaPozycja());
							if(postac >= 0) WybranyObjekt[2] = Mapa.Postacie[postac].NR;
						}
						else if( e.ControlKeyState & LEFT_CTRL_PRESSED ) Mapa.UsunPostac(AktualnaPozycja());
						else Mapa.DodajPostac(WybranyObjekt[2], AktualnaPozycja());
						break;
					}
					case 3: { /* wstawianie lub usuwanie lub pobieranie id osoby z danej pozycji */
						if( Osoby.size() == 0 || e.MouseMoved ) break;
						if( e.ControlKeyState & LEFT_ALT_PRESSED ) {
							short osoba = Mapa.SzukajOsoby(AktualnaPozycja());
							if(osoba >= 0) WybranyObjekt[3] = Mapa.Osoby[osoba].NR;
						}
						else if( e.ControlKeyState & LEFT_CTRL_PRESSED ) Mapa.UsunOsobe(AktualnaPozycja());
						else Mapa.DodajOsobe(WybranyObjekt[3], AktualnaPozycja());
						break;
					}
				}
			}

			if( e.MouseCode == RIGHT_BUTTON_PRESSED && e.ControlKeyState & LEFT_CTRL_PRESSED || e.MouseCode == FROM_LEFT_2ND_BUTTON_PRESSED )
			{
				/* zmienia rozmiar p�dzla */
				RozmiarPedzla = ( RozmiarPedzla + 10 + (e.MousePos.Y - PozycjaKursora.Y) ) % 10;
			}

			if( e.MouseCode == RIGHT_BUTTON_PRESSED )
			{
				/* zmienia aktualny objekt */
				switch( TypObjektu ) {
					case 0: if( Kafelki.size() > 0 ) WybranyObjekt[0] = ( WybranyObjekt[0] + Kafelki.size() + (e.MousePos.Y - PozycjaKursora.Y) ) % Kafelki.size(); break;
					case 1: if( Przedmioty.size() > 0 ) WybranyObjekt[1] = ( WybranyObjekt[1] + Przedmioty.size() + (e.MousePos.Y - PozycjaKursora.Y) ) % Przedmioty.size(); break;
					case 2: if( Wrogowie.size() > 0 ) WybranyObjekt[2] = ( WybranyObjekt[2] + Wrogowie.size() + (e.MousePos.Y - PozycjaKursora.Y) ) % Wrogowie.size(); break;
					case 3: if( Osoby.size() > 0 ) WybranyObjekt[3] = ( WybranyObjekt[3] + Osoby.size() + (e.MousePos.Y - PozycjaKursora.Y) ) % Osoby.size(); break;
				}
				
				/* zmienia typ objektu */
				horizontal_cursor_shift += e.MousePos.X - PozycjaKursora.X;
				if(horizontal_cursor_shift % 4 == 0 && horizontal_cursor_shift != 0) TypObjektu = ( TypObjektu + 4 + (e.MousePos.X - PozycjaKursora.X) ) % 4;
			}
			else horizontal_cursor_shift = 0;

			/* zmienia pozycj� kursora */
			if( e.MouseMoved ) PozycjaKursora = POS(e.MousePos.X, e.MousePos.Y);
		}
		else Sleep(1);

		BYTE przesuniecie = (e.ControlKeyState & LEFT_CTRL_PRESSED ? 5 : 1);
		POS stara_pozycja_mapy = PozycjaMapy;
		if(PozycjaKursora.X == 0) PozycjaMapy.X -= przesuniecie;
		if(PozycjaKursora.Y == 0) PozycjaMapy.Y -= przesuniecie;
		if(PozycjaKursora.X == 79) PozycjaMapy.X += przesuniecie;
		if(PozycjaKursora.Y == 39) PozycjaMapy.Y += przesuniecie;

		if( !(stara_pozycja_mapy == PozycjaMapy) || e.Exist ) Rysuj();

		if( !(stara_pozycja_mapy == PozycjaMapy) ) Sleep(50);
	}
}

/****************************************************************************************************/

bool EDYTOR::StworzNowaMape()
{
	POS rozmiar;

	StdBuf.Czysc();
	StdBuf >> POS(1, 1) >> tBia << "NOWA MAPA";
	StdBuf >> POS(1, 3) >> xBia << "szeroko��:";
	rozmiar.X = PobierzLiczbe(3, POS(1, 4), xBia);
	if(rozmiar.X <= 0) return false;
	StdBuf >> POS(1, 6) >> xBia << "wysoko��:";
	rozmiar.Y = PobierzLiczbe(3, POS(1, 7), xBia);
	if(rozmiar.Y <= 0) return false;

	Mapa.UstawRozmiar(rozmiar);		
	Mapa.Przedmioty.clear();
	Mapa.Postacie.clear();
	Mapa.Osoby.clear();
	PozycjaMapy = POS(0, -3);

	return true;
}

/****************************************************************************************************/

bool EDYTOR::WstawMape()
{
	string nazwa;
	POS pozycja;

	StdBuf.Czysc();
	StdBuf >> POS(1, 1) >> tBia << "WSTAWIANIE MAPY";
	StdBuf >> POS(1, 3) >> xBia << "id mapy:";
	nazwa = Pobierz("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_", 78, POS(1, 4), xBia);
	if(nazwa == NIE_WPROWADZONO || nazwa.size() < 1) return false;
	if( !DaneGry("LEVELS").Exist(nazwa) ) {
		warning("Nie mo�na wczyta� mapy.");
		return false;
	}
	StdBuf >> POS(1, 6) >> xBia << "pozycja mapy (X):";
	pozycja.X = PobierzLiczbe(3, POS(1, 7), xBia);
	if(pozycja.X < 0) return false;
	StdBuf >> POS(1, 9) >> xBia << "pozycja mapy (Y):";
	pozycja.Y = PobierzLiczbe(3, POS(1, 10), xBia);
	if(pozycja.Y < 0) return false;

	if( GenerujMape(DaneGry("LEVELS")(nazwa), Mapa, pozycja) ) return true;
	return false;
}

/****************************************************************************************************/

bool EDYTOR::ZapiszMape()
{
	string nazwa;

	StdBuf.Czysc();
	StdBuf >> POS(1, 1) >> tBia << "ZAPISZ MAPE";
	StdBuf >> POS(1, 3) >> xBia << "nazwa pliku:";
	nazwa = Pobierz("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 _-", 20, POS(1, 4), xBia);
	if(nazwa == NIE_WPROWADZONO || nazwa.size() < 1 ) return false;

	PLIK mapa( nazwa + ".map" );
	if( !mapa.Otworz( ZAPIS ) ) { warning("Nie mo�na otworzy� pliku do zapisu."); return false; }
	Mapa.Zapisz( mapa );
	mapa.Zamknij();

	//// pakowanie
	//RLGL::FILE m( nazwa + ".map" );
	//m.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING );
	//string dane = m.ReadAllToString();
	//m.Close();
	//char* bufor_docelowy = new char[ (unsigned)( dane.size() * 1.1 + 12 ) ];
	//unsigned long dlugosc_po_zpakowaniu;
	//compress2( (Bytef*) bufor_docelowy, &dlugosc_po_zpakowaniu, (const Bytef*) dane.c_str(), dane.size(), Z_BEST_COMPRESSION );
	//m.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	//m.Write( bufor_docelowy, dlugosc_po_zpakowaniu );
	//m.Close();

	return true;
}

/****************************************************************************************************/

bool EDYTOR::WczytajMape()
{
	string nazwa;

	StdBuf.Czysc();
	StdBuf >> POS(1, 1) >> tBia << "WCZYTAJ MAPE";
	StdBuf >> POS(1, 3) >> xBia << "nazwa pliku:";
	nazwa = Pobierz("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 _-", 20, POS(1, 4), xBia);
	if(nazwa == NIE_WPROWADZONO || nazwa.size() < 1) return false;

	//// rozpakowywanie
	//RLGL::FILE m( nazwa + ".map" );
	//if( !m.Open( RLGL::OD_READ, RLGL::OC_OPEN_EXISTING ) ) { warning("Nie mo�na otworzy� pliku do odczytu."); return false; }
	//string dane = m.ReadAllToString();
	//m.Close();
	//char* bufor_docelowy = new char[ 5242880 ];
	//unsigned long dlugosc_po_rozpakowaniu;
	//uncompress( (Bytef*) bufor_docelowy, &dlugosc_po_rozpakowaniu, (const Bytef*) dane.c_str(), dane.size() );
	//bufor_docelowy[ dlugosc_po_rozpakowaniu ] = 0;
	//dane = bufor_docelowy;
	//m.Open( RLGL::OD_WRITE, RLGL::OC_CREATE_ALWAYS );
	//m.Write( bufor_docelowy, dlugosc_po_rozpakowaniu );
	//m.Close();

	PLIK mapa( nazwa + ".map" );
	if( !mapa.Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) { warning("Nie mo�na otworzy� pliku do odczytu."); return false; }
	Mapa.Wczytaj( mapa );
	mapa.Zamknij();


	PozycjaMapy = POS(0, -3);

	return true;
}

/****************************************************************************************************/