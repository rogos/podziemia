#pragma once

#include "stdafx.h"

/****************************************************************************************************/

enum TYP_CZARU{ TC_POCISK, TC_NA_SIEBIE };

/****************************************************************************************************/

struct CZAR
{
	string ID;
	string Nazwa;
	TYP_CZARU Typ;
	UROK Urok;
	BYTE KosztRzucenia; // Ilo�� punkt�w energii
	BYTE StopienTrudnosci;

	void RysujPasek( POS Pozycja, BYTE Szerokosc = 80 );
};

/****************************************************************************************************/

struct CZAR_POCISK : CZAR
{
	BYTE Zasieg;
	BYTE ZasiegFaliUderzeniowej;
	bool PrzechodziPrzezWrogow;
};

/****************************************************************************************************/

void AlokacjaCzarow();

/****************************************************************************************************/

extern array< CZAR* > Czary;

int IndexCzaru( string ID );

/****************************************************************************************************/