#include "stdafx.h"

/****************************************************************************************************/

void EKWIPUNEK::Rysuj()
{
	StdBuf.Czysc();
	StdBuf >> xZul >> POS( 0, 0 ) << "+------------------------------------------------------------------------------+";
	RysujOkienko();
	StdBuf >> xSza >> POS( 14, 3 ) << "Prawa d�o� : ";
	if( PrawaDlon >= 0 ) StdBuf >> Przedmioty[PrawaDlon]->Kolor << Przedmioty[PrawaDlon]->Nazwa; else StdBuf << "-";
	StdBuf >> xSza >> POS( 14, 4 ) << "Lewa d�o�  : ";
	if( LewaDlon >= 0 ) StdBuf >> Przedmioty[LewaDlon]->Kolor << Przedmioty[LewaDlon]->Nazwa;
	else if( LewaDlon == -2 ) StdBuf << "zaj�ta"; else StdBuf >> xSza << "-";
	StdBuf >> xSza >> POS( 14, 5 ) << "He�m       : ";
	if( Helm >= 0 ) StdBuf >> Przedmioty[Helm]->Kolor << Przedmioty[Helm]->Nazwa; else StdBuf << "-";
	StdBuf >> xSza >> POS( 14, 6 ) << "Zbroja     : ";
	if( Zbroja >= 0 ) StdBuf >> Przedmioty[Zbroja]->Kolor << Przedmioty[Zbroja]->Nazwa; else StdBuf << "-";
	StdBuf >> xSza >> POS( 14, 7 ) << "Szata      : ";
	if( Szata >= 0 ) StdBuf >> Przedmioty[Szata]->Kolor << Przedmioty[Szata]->Nazwa; else StdBuf << "-";
	StdBuf >> xZul >> POS( 0, 10 ) << "+------------------------------------------------------------------------------+";
	RysujRamke( POS( 0, 11 ), POS( 80, 4 ), xcZul, false );
	StdBuf >> xBia >> POS( 4, 11 ) << " Parametry zwi�zane z walk�: ";

	//////////////////////////////////////////////////
	StdBuf >> xSza >> POS( 2, 12 ) << "Obra�enia: ";
	if( PrawaDlon >= 0 && Przedmioty[ PrawaDlon ]->PodajTyp() == BronBiala )
	{
		BRON_BIALA* Bron = dynamic_cast< BRON_BIALA* >( Przedmioty[ PrawaDlon ] );
		StdBuf.UstawKolorPisania( xBia );
		StdBuf << Bron->Obrazenia.Begin + (Bron->Typ == Nareczna ? StanGry->Postac.sila / 30 : 0) << "-" << Bron->Obrazenia.End + (Bron->Typ == Nareczna ? StanGry->Postac.sila / 15 : 0);
		StdBuf.UstawKolorPisania( xSza );
		if( Bron->Typ == Nareczna ) StdBuf >> xSza << " [" << ::Umiejetnosci[IndexUmiejetnosci("walka_wrecz")].Nazwa << "]";
		else if( Bron->Typ == Jednoreczny ) StdBuf << " [" << ::Umiejetnosci[IndexUmiejetnosci("bron_jednoreczna")].Nazwa << "]";
		else StdBuf << " [" << ::Umiejetnosci[IndexUmiejetnosci("bron_dwureczna")].Nazwa << "]";
	}
	else if( LewaDlon >= 0 && Przedmioty[ LewaDlon ]->PodajTyp() == Pocisk && PrawaDlon >= 0 && Przedmioty[ PrawaDlon ]->PodajTyp() == BronDystansowa )
	{
		POCISK* Pocisk = dynamic_cast< POCISK* >( Przedmioty[ LewaDlon ] );
		StdBuf.UstawKolorPisania( xBia );
		StdBuf << Pocisk->Obrazenia.Begin << "-" << Pocisk->Obrazenia.End;
		StdBuf.UstawKolorPisania( xSza );
		if( Pocisk->Typ == Luk ) StdBuf << " [" << ::Umiejetnosci[IndexUmiejetnosci("luk")].Nazwa << "]";
		else StdBuf << " [" << ::Umiejetnosci[IndexUmiejetnosci("kusza")].Nazwa << "]";
	}
	else if( StanGry->Postac.Umiejetnosci["walka_wrecz"] > 0 )
	{
		StdBuf.UstawKolorPisania( xBia );
		StdBuf << StanGry->Postac.sila / 30 << "-" << StanGry->Postac.sila / 15;
		StdBuf >> xSza << " [" << ::Umiejetnosci[IndexUmiejetnosci("walka_wrecz")].Nazwa << "]";
	}
	else StdBuf << "-";

	StdBuf >> xSza >> POS( 2, 13 ) << "Ochrona: " >> xBia << "[" <<
		StanGry->Postac.Ochrona.Bron << ", " << StanGry->Postac.Ochrona.Pociski << ", " << StanGry->Postac.Ochrona.Magia << "]";
	if( LewaDlon >= 0 && Przedmioty[ LewaDlon ]->PodajTyp() == Tarcza )
	{
		TARCZA* Tarcza = dynamic_cast< TARCZA* >( Przedmioty[ LewaDlon ] );
		StdBuf << " + " << Tarcza->Obrona.Begin << "-" << Tarcza->Obrona.End;
		StdBuf >> xSza << " [" << ::Umiejetnosci[IndexUmiejetnosci("tarcza")].Nazwa << "]";
	}
	//////////////////////////////////////////////////

	RysujRamke( POS( 0, 15 ), POS( 80, 3 ), xcZul, false );
	StdBuf >> xBia >> POS( 4, 15 ) << " Obci��enie: ";
	StdBuf >> xSza >> POS( 2, 16 ) << "Ca�kowita waga: " << Plecak.PobWaga() << " dag / " << SilaPostaci * 200 << " dag";

	StdBuf >> xZul >> POS( 0, 18 ) << "+------------------------------------------------------------------------------+";
	if(ListaNoszonychPrzedmiotow) StdBuf.WstawZnak( 26, xBia, POS( 13, 3 + NoszonyPrzedmiot ) );
	else if(Plecak.Ilosc() > 0) StdBuf.WstawZnak( 26, xBia, POS( 1, 21 ) );
	for( short i = -WybranyPrzedmiot; i < 15; i++ )
	{
		if( i < -2 ) i = -2;
		if( i + WybranyPrzedmiot + 1 > Plecak.Ilosc() ) break;
		StdBuf.WstawZnak( Przedmioty[ Plecak[ WybranyPrzedmiot + i ].Typ ]->Znak, Przedmioty[ Plecak[ WybranyPrzedmiot + i ].Typ ]->Kolor, POS( 2, 21 + i ), false );
		StdBuf >> xSza >> POS( 4, 21 + i );
		if( Plecak[ WybranyPrzedmiot + i ].Ilosc > 1 ) StdBuf << Plecak[ WybranyPrzedmiot + i ].Ilosc << "x ";
		StdBuf << Przedmioty[ Plecak[ WybranyPrzedmiot + i ].Typ ]->Nazwa;
		StdBuf >> POS( 68, 21 + i ) << Przedmioty[ Plecak[ WybranyPrzedmiot + i ].Typ ]->Waga * Plecak[ WybranyPrzedmiot + i ].Ilosc << " dag";
	}
	StdBuf >> xZul >> POS( 0, 36 ) << "+------------------------------------------------------------------------------+";
	if( Proces == PodawanieIlosci ) StdBuf >> POS( 1, 37 ) >> xcZul << "Podaj ilo�� ([Enter]-Wyrzu�, puste pole = wszystko):";
	else
	{
		StdBuf >> xcZul >> POS( 1, 37 ) << "[8]-Do g�ry + [2]-Na d� + [4]-Przedmioty w plecaku + [6]-Noszone przedmioty";
		StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec + [D]-Opis + [Enter]-U�yj/Zdejmij + [Backspace]-Wyrzu�";
	}
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	StdBuf.Rysuj();
}

/****************************************************************************************************/

bool EKWIPUNEK::Pokaz( POS PozycjaPostaci )
{
	unsigned Ilosc;
	for(;;)
	{
		Proces = Przegladanie;
		Rysuj();
		for(;;)
		{
			switch( PobierzKlawisz() )
			{
			case 'd': case 'D':
				if(ListaNoszonychPrzedmiotow) {
					if(NoszonePrzedmioty[NoszonyPrzedmiot] >= 0) Przedmioty[ NoszonePrzedmioty[NoszonyPrzedmiot] ]->RysujOpis( POS( 20, 10 ) );
					else break;
				}
				else {
					if( Plecak.Ilosc() == 0 ) break;
					Przedmioty[ Plecak[ WybranyPrzedmiot ].Typ ]->RysujOpis( POS( 20, 10 ) );
				}
				StdBuf.Rysuj();
				for(;;) {
					switch( _getch() ) {
						case 'd': case 'D': case 27: case 32: break;
						default: continue;
					}
					break;
				}
				break;

			case 1075: case '4': ListaNoszonychPrzedmiotow = false; break;
			case 1077: case '6': ListaNoszonychPrzedmiotow = true; break;

			case 1072: case '8': {
				if(ListaNoszonychPrzedmiotow) { if( NoszonyPrzedmiot <= 0 ) continue; NoszonyPrzedmiot--; break; }
				else { if( Plecak.Ilosc() <= 1 || WybranyPrzedmiot <= 0 ) continue; WybranyPrzedmiot--; break; }
			}
			case 1080: case '2': {
				if(ListaNoszonychPrzedmiotow) { if( NoszonyPrzedmiot >= 4 ) continue; NoszonyPrzedmiot++; break; }
				else { if( Plecak.Ilosc() <= 1 || WybranyPrzedmiot >= Plecak.Ilosc() - 1 ) continue; WybranyPrzedmiot++; break; }
			}
			case 13:
				if( !StanGry->Postac.Zyje ) continue;
				if(ListaNoszonychPrzedmiotow) { if(!Zdejmij(NoszonyPrzedmiot)) continue; break; }
				else { if( Plecak.Ilosc() == 0 ) continue; if( UzyjPrzedmiotu(WybranyPrzedmiot) ) return true;  return false; }

			case 8:
				if( !StanGry->Postac.Zyje ) continue;
				if( ListaNoszonychPrzedmiotow || Plecak.Ilosc() == 0 ) continue;
				if( Plecak[ WybranyPrzedmiot ].Ilosc > 1 )
				{
					Proces = PodawanieIlosci;
					Rysuj();
					Ilosc = PobierzLiczbe( 10, POS( 54, 37 ), xZul );
					if(Ilosc == -1 || Ilosc == 0) break;
					if(Ilosc == -2 || Ilosc > Plecak[ WybranyPrzedmiot ].Ilosc )
						Wyrzuc( PozycjaPostaci, Plecak[ WybranyPrzedmiot ].Ilosc );
					else Wyrzuc( PozycjaPostaci, Ilosc );
				}
				else Wyrzuc( PozycjaPostaci );
				return true;

			case 27: case 32: return false;

			default: continue;
			}
			break;
		}
	}
	return false;
}

/****************************************************************************************************/

void EKWIPUNEK::RusujCoMaWRecach()
{
	StdBuf >> POS( 50, 38 ) >> xcSza << "P: ";
	if( PrawaDlon < 0 ) StdBuf >> xSza << "-";
	else StdBuf >> Przedmioty[ PrawaDlon ]->Kolor << Przedmioty[ PrawaDlon ]->Nazwa.substr( 0, 26 );

	StdBuf >> POS( 50, 39 ) >> xcSza << "L: ";
	if( LewaDlon == -2 ) StdBuf >> xSza << "zaj�ta";
	else
	{
		if( LewaDlon < 0 ) StdBuf >> xSza << "-";
		else StdBuf >> Przedmioty[ LewaDlon ]->Kolor << Przedmioty[ LewaDlon ]->Nazwa.substr( 0, 26 );
	}
}

/****************************************************************************************************/

unsigned EKWIPUNEK::Dodaj( string NazwaPrzedmiotu, WORD Ilosc )
{
	short i = IndexPrzedmiotu( NazwaPrzedmiotu );
	if( i < 0 ) return false;
	return Dodaj( i, Ilosc );
}

/****************************************************************************************************/

unsigned EKWIPUNEK::Dodaj( WORD NRPrzedmiotu, WORD Ilosc )
{
	Plecak.DodajPrzedmiot( NRPrzedmiotu, Ilosc );

	return Ilosc;
}

/****************************************************************************************************/

void EKWIPUNEK::Wyrzuc( POS Pozycja, unsigned Ilosc )
{
	AktualnaMapa->DodajPrzedmiot( Plecak[ WybranyPrzedmiot ].Typ, Ilosc, Pozycja );

	Komunikat << "Wyrzucasz: ";
	if( Ilosc > 1 ) Komunikat << Ilosc << "x ";
	Komunikat << Przedmioty[ Plecak[ WybranyPrzedmiot ].Typ ]->Nazwa << "." >> xBia;

	Plecak.UsunPrzedmiotNa( WybranyPrzedmiot, Ilosc );
}

/****************************************************************************************************/

bool EKWIPUNEK::Zdejmij( char Miejsce )
{
	if( !StanGry->Postac.Zyje ) return false;

	short* miejsce;
	switch( Miejsce )
	{
		case 0: miejsce = &PrawaDlon; break;
		case 1: miejsce = &LewaDlon; break;
		case 2: miejsce = &Helm; break;
		case 3: miejsce = &Zbroja; break;
		case 4: miejsce = &Szata; break;
	}
	if( *miejsce < 0 ) return false;

	CHAR indeks = -1;
	for( CHAR i = 0; i < Plecak.Ilosc(); i++ )
		if( Plecak[ i ].Typ == *miejsce )
		{
			indeks = i;
			break;
		}

	TYP_PRZEDMIOTU typ_przedmiotu = Przedmioty[ *miejsce ]->PodajTyp();

	Plecak.DodajPrzedmiot( *miejsce, 1 );

	if( typ_przedmiotu == BronBiala && dynamic_cast< BRON_BIALA* >( Przedmioty[ *miejsce ] )->Typ == Dworeczny )
		LewaDlon = -1;

	if( typ_przedmiotu == Pancerz ) StanGry->Postac.Ochrona -= dynamic_cast< PANCERZ* >( Przedmioty[ *miejsce ] )->Ochrona;

	if( Miejsce == 0 && LewaDlon >= 0 && Przedmioty[ LewaDlon ]->PodajTyp() == BronBiala )
	{
		*miejsce = LewaDlon;
		LewaDlon = -1;
	}
	else *miejsce = -1;

	return true;
}

/****************************************************************************************************/

bool EKWIPUNEK::UzyjPrzedmiotu( CHAR Adres )
{
	if( Adres + 1 > Plecak.Ilosc() ) return false;
	if( !Przedmioty[ Plecak[ Adres ].Typ ]->Uzyj() ) return false;

	Plecak.UsunPrzedmiotNa( Adres, 1 );
	if( Plecak.Ilosc() >= 1 && WybranyPrzedmiot >= Plecak.Ilosc() ) WybranyPrzedmiot--;
	
	return true;
}

/****************************************************************************************************/

void EKWIPUNEK::Zapisz( PLIK& plik )
{
	Plecak.Zapisz( plik );

	plik << MaxObciazenie
	     << LewaDlon
	     << PrawaDlon
	     << Helm
	     << Zbroja
	     << Szata
	     << WybranyPrzedmiot
	     << (BYTE)(Proces);
}

/****************************************************************************************************/

void EKWIPUNEK::Wczytaj( PLIK& plik )
{
	Plecak.Wczytaj( plik );

	plik >> MaxObciazenie
	     >> LewaDlon
	     >> PrawaDlon
	     >> Helm
	     >> Zbroja
	     >> Szata
	     >> WybranyPrzedmiot
	     >> (BYTE&)(Proces);
}

/****************************************************************************************************/