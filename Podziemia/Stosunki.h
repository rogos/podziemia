#pragma once

#include "stdafx.h"

/****************************************************************************************************/

struct OPCJA
{
	RLGL::STRUCTURE Warunki;
	RLGL::STRUCTURE Skutki;
	string Tresc;
	bool Niedostepna;
	bool Niewidoczna;
};

/****************************************************************************************************/

struct DZIAL
{
	array<string> Teksty;
	array<OPCJA> Opcje;
};

/****************************************************************************************************/

struct RELACJA
{
	array<DZIAL> Dzialy;
	bool Laduj( const RLGL::ELEMENT& relacja );
};

/****************************************************************************************************/

//class POSTAC
//{
//	array< array<bool> > Relacja;
//};

/****************************************************************************************************/

class RELACJONER
{
private:
	WORD Dzial;
	WORD Tekst;
	WORD Opcja;
	array<WORD> Opcje;
	
	const RELACJA* pDefRelacji;
	array< array<bool> >* pRelacja;

	BOHATER* pBohater;
	OSOBA* pOsoba;

public:
	bool End;

	RELACJONER() : Dzial( 0 ), Tekst( 0 ), Opcja( 0 ), End( 0 ), pDefRelacji( 0 ), pRelacja( 0 ), pBohater( 0 ), pOsoba( 0 ) {}
	
	void Laduj( const RELACJA* def_relacji, BOHATER* bohater, OSOBA* osoba, string nazwa_opcji = "start" );
	void Laduj( const RELACJA* def_relacji, BOHATER* bohater, array< array<bool> >* relacja, string nazwa_opcji = "start" );
	void Rysuj( WORD kolor_ramki = xcZie );
	bool Przetwarzaj( WORD znak );
	bool SprawdzWarunki( WORD opcja );
	bool PrzetwarzajOpcje( WORD opcja );
	bool PrzetwarzajOpcje( string opcja );
	
	bool IdzDo( WORD dzial, WORD tekst );
};

/****************************************************************************************************/

class HANDEL
{
	enum LISTA{ L_SPRZEDAWCY, L_KUPCA };

private:
	POJEMNIK* PrzedmiotyKupca;
	POJEMNIK* PrzedmiotySprzedawcy;
	LISTA Lista;
	WORD PozycjaKupna;
	WORD PozycjaSprzedazy;

public:
	HANDEL() : Lista( L_SPRZEDAWCY ), PozycjaKupna( 0 ), PozycjaSprzedazy( 0 ) {}

	void Laduj( POJEMNIK* przedmioty_kupca, POJEMNIK* przedmioty_sprzedawcy );
	bool Zakoncz();

	void Rysuj();
	bool Przetwarzaj( WORD znak );
};

/****************************************************************************************************/