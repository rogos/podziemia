#include "stdafx.h"
//#include "Dzwieki.h"

/****************************************************************************************************/

KOMUNIKAT Komunikat;

/****************************************************************************************************/

string Pobierz(string DozwoloneZnaki,WORD MaxDlugosc,POS Pozycja,WORD Kolor)
{
	for( WORD i = 0; i < DozwoloneZnaki.length(); i++ ) PoprawZPolskimiLiterami( DozwoloneZnaki[ i ] );
	StdBuf.UstawKolorPisania( Kolor );
	char Znak;
	string Tekst;
	StdBuf.PokazKursor();
	StdBuf.UstawPozycjeKursora( Pozycja );
	StdBuf.Rysuj();
	do
	{
		Znak = _getch();
		if(Znak == 13) break;
		if(Znak == 8)
		{
			if( Tekst.length() > 0 )
			{
				Tekst.resize( Tekst.length() - 1 );
				StdBuf.Czysc( Pozycja( Tekst.length(), 0 ), 1 );
				StdBuf.UstawPozycjeKursora( Pozycja( Tekst.length(), 0 ) );
				StdBuf.Rysuj();
			}
			continue;
		}
		int Indeks = DozwoloneZnaki == "wszystkie" ? 0 : DozwoloneZnaki.find( Znak );
		if( Indeks >= 0 )
		{
			if( Tekst.length() < MaxDlugosc )
			{
				Tekst += Znak;
				StdBuf.UstawPozycjePisania( Pozycja );
				StdBuf << Tekst;
				StdBuf.UstawPozycjeKursora( Pozycja( Tekst.length(), 0 ) );
				StdBuf.Rysuj();
			}
			else continue;
		}
		else continue;
	}while( Znak != 27 && Znak != 13 );
	StdBuf.SchowajKursor();
	if(Znak == 13) return Tekst;
	else return "_~FALSE_";
}

/****************************************************************************************************/

int PobierzLiczbe(WORD MaxDlugosc, POS Pozycja, WORD Kolor)
{
	string Ilosc = Pobierz("1234567890", MaxDlugosc, Pozycja, Kolor);
	if(Ilosc == "_~FALSE_") return -1;
	if(Ilosc.length() == 0) return -2;
	return atoi(Ilosc.c_str());
}

/****************************************************************************************************/

wchar_t PobierzKlawisz()
{
	wchar_t znak = _getch();
	if ( znak == 224 ) znak = _getch() + 1000;
	if ( znak == 0 ) znak = _getch() + 2000;
	return znak;
}

/****************************************************************************************************/

int Losuj( int Min, int Max )
{
	static bool Pierwszy = true;
	if( Pierwszy )
	{
		srand( static_cast< unsigned >( time( NULL ) ) );
		Pierwszy = false;
	}
	if( Min >= Max ) return Min;
	return rand() % ( Max - Min + 1 ) + Min;
}

/****************************************************************************************************/

void Kurtyna()
{
	StdBuf.Czysc();
	StdBuf.UstawPozycjeKursora( POS( 79, 39 ) );
	for(int i=0; i < 20; i++)
	{
		if( _kbhit() )
		{
			_getch();
			break;
		}
		for( int j = 0; j < 200; j++ )
			StdBuf.WstawZnak( Losuj( 40, 255 ), Losuj( 1, 15 ), POS(Losuj( 0, 79 ), Losuj( 0, 39 )) );
		Sleep( 10 );
		StdBuf.Rysuj();
	}
	StdBuf.Wypelnij( xCza + tBia );
	StdBuf.Rysuj();
	Sleep( 100 );
	StdBuf.Czysc();
	StdBuf.Rysuj();
}

/****************************************************************************************************/

void CzekajNaSpacje()
{
	RysujRamke( POS( 0, 0 ), POS( 9, 2 ), xNie, false );
	StdBuf >> xZul + tcNie >> POS( 1, 0 ) << "Wci�nij";
	StdBuf >> POS( 1, 1 ) << "[Space]";
	StdBuf.Rysuj();
	while( _getch() != 32 );
}

/****************************************************************************************************/

unsigned Doswiadczenie( BYTE poziom, bool suma )
{
	const WORD a1 = 40;
	const float q = 1.15f;

	if(suma) return a1 * (1 - pow(q, poziom)) / (1 - q);
	return a1 * pow(q, poziom - 1);
}

/****************************************************************************************************/

void DoDoswiadczenia( WORD Dodaj )
{
	StanGry->Postac.Doswiadczenie += Dodaj;
	for(;;)
	{
		if( StanGry->Postac.Poziom >= 100 ) return;
		if( StanGry->Postac.Doswiadczenie >= Doswiadczenie(StanGry->Postac.Poziom) )
		{
			StanGry->Postac.Poziom++;
			StanGry->Postac.Punkty += 5;
			StanGry->Postac.Zycie = StanGry->Postac.MaxZycie;
			StanGry->Postac.Energia = StanGry->Postac.MaxEnergia;
			Komunikat << "Gratulacje! Zdobywasz nowy poziom!" >> xZie;
			StdBuf.Wypelnij( tBia + xBia );
			StdBuf.Rysuj();
			Sleep( 100 );
		}
		else break;
	}
}

/****************************************************************************************************/

UCHAR IloscZabieranychPunktow( UCHAR Wartosc )
{
	if( Wartosc >= 90 ) return 3;
	if( Wartosc >= 70 ) return 2;
	return 1;
}

/****************************************************************************************************/

bool PrzydzielPunkty( UCHAR& Wartosc )
{
	if( Wartosc >= 100 || StanGry->Postac.Punkty < IloscZabieranychPunktow( Wartosc ) ) return false;

	StanGry->Postac.Punkty -= IloscZabieranychPunktow( Wartosc );

	Wartosc++;

	return true;
}

/****************************************************************************************************/

void RysujOkienko()
{
	BUFOR Mapa;
	StdBuf.Ustaw( Mapa );
	AktualnaMapa->Rysuj( xcNie );
	StanGry->Postac.Rysuj();
	StdBuf.UstawDomyslny();
	for( CHAR y = 0; y < 7; y++ ) {
		for( CHAR x = 0; x < 7; x++ ) {
			StdBuf.WstawZnak( Mapa.Znaki[ 80 * ( SRODEK_RYSOWANIA.Y - 3 + y ) + ( SRODEK_RYSOWANIA.X + 1 - 3 + x ) + 160 ].Char.AsciiChar,
							  Mapa.Znaki[ 80 * ( SRODEK_RYSOWANIA.Y - 3 + y ) + ( SRODEK_RYSOWANIA.X + 1 - 3 + x ) + 160 ].Attributes,
							  POS( x + 2, y + 2 ),
							  false );
		}
	}
	StdBuf >> xcZul >> POS( 1, 1 ) << "+-------+";
	for( CHAR i = 2; i < 10; i++ ) {
		StdBuf >> POS( 1, i ) << "|";
		StdBuf >> POS( 9, i ) << "|";
	}
	StdBuf >> POS( 1, 9 ) << "+-------+";
}

/****************************************************************************************************/

void RysujPanelPatrzenia( POS PozycjaCelu )
{
	StdBuf >> xcZul >> POS( 0, 0 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xZul >> POS( 1, 1 ) << "Widzisz: ";
	if( StanGry->WidocznePola[PozycjaCelu.X][PozycjaCelu.Y] )
	{
		StdBuf << ZWielkiejLitery( Kafelki[ AktualnaMapa->Kafelki[ PozycjaCelu.X ][ PozycjaCelu.Y ] ].Nazwa ); // Widzisz: [Nazwa kafelka]
		if( AktualnaMapa->SzukajPrzedmiotu( PozycjaCelu ) >= 0 )
		{
			StdBuf << ", ";
			if( AktualnaMapa->SzukajPrzedmiotu( PozycjaCelu, 2 ) >= 0 ) StdBuf << "kilka przedmiot�w"; // Widzisz: kilka przedmiot�w
			else StdBuf << ZWielkiejLitery( Przedmioty[ AktualnaMapa->Przedmioty[ AktualnaMapa->SzukajPrzedmiotu( PozycjaCelu ) ].NR ]->Nazwa ); // Widzisz: [Nazwa przedmiotu]
		}
		short p = AktualnaMapa->SzukajPostaci( PozycjaCelu );
		if( p >= 0 )
				StdBuf << ", " << ZWielkiejLitery( AktualnaMapa->Postacie[ p ]->Nazwa ); // Widzisz: [Nazwa wroga]
			
		if( PozycjaCelu == StanGry->Postac.Pozycja ) StdBuf << ", " << StanGry->Postac.Nazwa; // Widzisz: [Imi� postaci]

		if( p >= 0 ) AktualnaMapa->Postacie[ p ]->RysujPasekZycia();
	}
	else StdBuf << "-"; // Widzisz: -
}

/****************************************************************************************************/

void RysujPunktWyboruPozycji( PUNKTY& Linia, UCHAR Zasieg )
{
	bool cel_bez_zasiegu = false;

	for( UCHAR i = 1; i < Linia.IloscPunktow; i++ )
	{
		if( Kafelki[ AktualnaMapa->Kafelki[ Linia.Punkty[ i ].X ][ Linia.Punkty[ i ].Y ] ].Typ == TK_SCIANA || i > Zasieg )
			cel_bez_zasiegu = true;
	}
	if( Linia.IloscPunktow > 0 )
		StdBuf.WstawZnak( 'X',
						  ( cel_bez_zasiegu ? xCze : xZul ),
						  POS( Linia.Punkty[ Linia.IloscPunktow - 1 ].X + 1 + SRODEK_RYSOWANIA.X - StanGry->Postac.Pozycja.X,
								Linia.Punkty[ Linia.IloscPunktow - 1 ].Y + 2 + SRODEK_RYSOWANIA.Y - StanGry->Postac.Pozycja.Y ),
						  false );

}

/****************************************************************************************************/

void RysujRamke(POS Pozycja, POS Rozmiar, WORD Kolor, bool Podwojna)
{
	StdBuf.WstawZnak(Podwojna ? 201 : 218, Kolor, Pozycja, false);
	StdBuf.WstawZnak(Podwojna ? 187 : 191, Kolor, Pozycja(Rozmiar.X - 1, 0), false);
	StdBuf.WstawZnak(Podwojna ? 200 : 192, Kolor, Pozycja(0, Rozmiar.Y - 1), false);
	StdBuf.WstawZnak(Podwojna ? 188 : 217, Kolor, Pozycja(Rozmiar.X - 1, Rozmiar.Y - 1), false);
	for(UCHAR i=1; i < Rozmiar.X - 1; i++)
	{
		StdBuf.WstawZnak(Podwojna ? 205 : 196, Kolor, Pozycja(i, 0), false);
		StdBuf.WstawZnak(Podwojna ? 205 : 196, Kolor, Pozycja(i, Rozmiar.Y - 1), false);
	}
	for(UCHAR i=1; i < Rozmiar.Y - 1; i++)
	{
		StdBuf.WstawZnak(Podwojna ? 186 : 179, Kolor, Pozycja(0, i), false);
		StdBuf.WstawZnak(Podwojna ? 186 : 179, Kolor, Pozycja(Rozmiar.X - 1, i), false);
	}
}

/****************************************************************************************************/

bool PokazEkw()
{
	if( StanGry->Postac.Ekwipunek.Pokaz( StanGry->Postac.Pozycja ) ) return true;
	return false;
}

/****************************************************************************************************/

void PokazStat()
{
	StanGry->Postac.PokazStatystyki();
}

/****************************************************************************************************/

bool PokazMape()
{
	StdBuf.Czysc();
	AktualnaMapa->Rysuj( xcSza, true, xcSza );
	StdBuf >> xBia >> POS( 0, 0 ) << "                          Podgl�d odkrytych teren�w";
	RysujRamke( POS( 0, 1 ), POS( 80, 37 ), xcCze, false );
	StanGry->Postac.Rysuj();
	StdBuf.Rysuj();
	for(;;)
	{
		char Znak = _getch();
		if(Znak == 32 || Znak == 27 || Znak == 'm' || Znak == 'M') break;
	}
	return true;
}

/****************************************************************************************************/

bool PokazPomoc()
{
	unsigned PozycjaTekstu = 0;
	StdBuf.Czysc();
	StdBuf.UstawPozycjeKursora( POS( 79, 39 ) );
	StdBuf >> POS( 0, 0 ) >> xcZul << "--------------------------------------------------------------------------------";
	StdBuf >> POS( 1, 1 ) >> xZul << "Pomoc";
	StdBuf >> POS( 0, 2 ) >> xcZul << "--------------------------------------------------------------------------------";
	unsigned IloscLiniiTekstu;
	string* Tekst = WczytajPlikTxt( IloscLiniiTekstu, L"MANUAL.txt" );
	StdBuf >> xZul >> POS( 0, 37 ) << "+------------------------------------------------------------------------------+";
	StdBuf >> xcZul >> POS( 1, 38 ) << "[Space]-Koniec";
	if( IloscLiniiTekstu > 34 )
	{
		StdBuf << " + [8]-Do g�ry + [2]-Na d�";
	}
	StdBuf >> xZul >> POS( 0, 39 ) << "+------------------------------------------------------------------------------+";
	for(;;)
	{
		StdBuf.Czysc( POS(0,3), 34 * 80 );
		if( !IloscLiniiTekstu ) StdBuf >> xCze >> POS( 0, 3 ) << "ERROR: Nie mo�na wczyta� pliku pomocy.";
		for( UCHAR i = 0; i < 34; i++ )
		{
			if(i >= IloscLiniiTekstu) break;
			int Indeks = Tekst[ PozycjaTekstu + i ].find( "::" );
			StdBuf >> ( Indeks >= 0 ? xZul : xBia ) >> POS( 0, 3 + i ) << Tekst[PozycjaTekstu + i];
		}
		StdBuf.Rysuj();

		for(;;)
		{
			switch( PobierzKlawisz() )
			{
			case 27: case 32: delete[] Tekst; return true;

			case 1072: case '8': if(PozycjaTekstu == 0)continue; else PozycjaTekstu--; break;
			case 1080: case '2': if(PozycjaTekstu + 34 >= IloscLiniiTekstu)continue; else PozycjaTekstu++; break;
			
			default: continue;
			}
			break;
		}
	}
}

/****************************************************************************************************/

string ZWielkiejLitery( string Tekst )
{
	Tekst[ 0 ] = toupper( Tekst[ 0 ] );

	switch( Tekst[ 0 ] )
	{
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	case '�': Tekst[ 0 ] = '�'; break;
	}

	return Tekst;
}

/****************************************************************************************************/

string* WczytajPlikTxt(unsigned& IloscLiniiTekstu, wstring AdresPliku)
{
	DWORD IloscWczytanychBajtow;
	HANDLE Plik;
	IloscLiniiTekstu = 0;
	Plik = CreateFile(AdresPliku.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (Plik == INVALID_HANDLE_VALUE) return false;
	char* Tekst;
	unsigned DlugoscTekstu = GetFileSize(Plik, NULL);
	Tekst = new char[DlugoscTekstu];
	if(ReadFile(Plik, Tekst, DlugoscTekstu, &IloscWczytanychBajtow, NULL) == 0 || IloscWczytanychBajtow != DlugoscTekstu) return false;
	CloseHandle(Plik);
	for(unsigned i=0; i < DlugoscTekstu; i++)
	{
		if(Tekst[i] == 13 && i + 1 <= DlugoscTekstu && Tekst[i+1] == 10) IloscLiniiTekstu++;
	}
	string* LinieTekstu = new string[++IloscLiniiTekstu];
	unsigned KopiowanaLinia = 0;
	unsigned OdKadKopiowac = 0;
	for(unsigned i=0; i <= DlugoscTekstu; i++)
	{
		if((Tekst[i] == 13 && i + 1 <= DlugoscTekstu && Tekst[i+1] == 10) || i == DlugoscTekstu)
		{
			LinieTekstu[KopiowanaLinia].resize(i - OdKadKopiowac);
			for(unsigned j=0; j < LinieTekstu[KopiowanaLinia].length(); j++)
				LinieTekstu[KopiowanaLinia][j] = Tekst[j + OdKadKopiowac];
			i++;
			OdKadKopiowac = i + 1;
			KopiowanaLinia++;
		}
	}
	return LinieTekstu;
}

/****************************************************************************************************/

KOMUNIKAT& KOMUNIKAT::operator >> ( WORD Kolor )
{
	if( Rysowane ) CzyscBufor();
	IloscTekstow++;
	Teksty.add( NowyTekst );
	Kolory.add( Kolor );
	NowyTekst.clear();
	return *this;
}

/****************************************************************************************************/

KOMUNIKAT& KOMUNIKAT::operator << ( string Tekst )
{
	NowyTekst += Tekst;
	return *this;
}

/****************************************************************************************************/

KOMUNIKAT& KOMUNIKAT::operator << ( int Tekst )
{
	string _Tekst = _itostr( Tekst );
	NowyTekst += _Tekst;
	return *this;
}

/****************************************************************************************************/

void KOMUNIKAT::CzyscBufor()
{
	Teksty.clear();
	Kolory.clear();
	IloscTekstow = 0;
	Rysowane = false;
}

/****************************************************************************************************/

bool KOMUNIKAT::operator() ()
{
	if( Rysowane ) return false;

	UCHAR linia_komunikatu = 0;

	SECTOR pozycja_komunikatow;

	pozycja_komunikatow.Set( SRODEK_RYSOWANIA.Y + 2, WysokoscMapy - 1 );

	for( short i = 0; i < IloscTekstow; i++ )
	{
		if( linia_komunikatu > pozycja_komunikatow.End - pozycja_komunikatow.Begin )
		{
			linia_komunikatu = 0;
			CzekajNaSpacje();
			StdBuf.Czysc();
			AktualnaMapa->StworzMapyPomocnicze();
			AktualnaMapa->Rysuj();
			StanGry->Postac.Rysuj();
			RysujListeWidocznychObiektow( POS( 2, 2 ) );
			StanGry->RysujPanel();
			StdBuf.Rysuj();
		}
		POS pozycja_tekstu;
		pozycja_tekstu.X = ( 80 - Teksty[ i ].length() ) / 2;

		pozycja_tekstu.Y = IloscTekstow - i + linia_komunikatu > pozycja_komunikatow.End - pozycja_komunikatow.Begin ?
			pozycja_komunikatow.Begin + linia_komunikatu + 2 :
			pozycja_komunikatow.End - IloscTekstow % ( pozycja_komunikatow.End - pozycja_komunikatow.Begin + 1 ) + 1 + linia_komunikatu + 2;

		StdBuf.Czysc( POS( 1, pozycja_tekstu.Y ), 78 );

		StdBuf >> Kolory[ i ] >> pozycja_tekstu << Teksty[ i ];

		linia_komunikatu++;
	}

	Rysowane = true;
	return true;
}

/****************************************************************************************************/

PUNKTY& PUNKTY::Stworz( POS Od, POS Do )
{
	if( Punkty )
	{
		delete[] Punkty;
		Punkty = 0;
		IloscPunktow = 0;
	}

	if( Od == Do ) return *this;

	int xadd1, xadd2, yadd1, yadd2, den, num, numadd, numpixels;
	int dx = abs( Do.X - Od.X );
	int dy = abs( Do.Y - Od.Y );
	int x = Od.X;
	int y = Od.Y;

	if( Do.X >= Od.X ) {
		xadd1 = 1;
		xadd2 = 1;
	} else {
		xadd1 = -1;
		xadd2 = -1;
	}

	if( Do.Y >= Od.Y ) {
		yadd1 = 1;
		yadd2 = 1;
	} else {
		yadd1 = -1;
		yadd2 = -1;
	}

	if( dx >= dy ) {
		xadd1 = 0;
		yadd2 = 0;
		den = dx;
		num = dx / 2;
		numadd = dy;
		numpixels = dx;
	} else {
		xadd2 = 0;
		yadd1 = 0;
		den = dy;
		num = dy / 2;
		numadd = dx;
		numpixels = dy;
	}

	IloscPunktow = numpixels + 1;
	Punkty = new POS[ IloscPunktow ];

	for( int i = 0; i <= numpixels; i++ )
	{
		Punkty[i] = POS( x, y );
		num += numadd;
		if ( num >= den ) {
			num -= den;
			x += xadd1;
			y += yadd1;
		}
		x += xadd2;
		y += yadd2;
	}

	return *this;
}

/****************************************************************************************************/

void PATH::SetMap( MAPA& map, POS find_region_pos )
{
	FindRegionPos = find_region_pos;
	for( unsigned x = 0; x < 100; x++ )
		for( unsigned y = 0; y < 100; y++ )
		{
			POS p = find_region_pos + POS(x, y);
			if( p.X < 0 || p.Y < 0 || p.X >= map.Rozmiar.X || p.Y >= map.Rozmiar.Y ) Map[x][y] = -2;
			else Map[x][y] = Kafelki[ AktualnaMapa->Kafelki[find_region_pos.X + x][find_region_pos.Y + y] ].Typ == TK_GRUNT ? -1 : -2;
		}
}

/*..................................................................................................*/

bool PATH::Create( POS Begin, POS End, bool Diagonal, unsigned MaxLength )
{
	Begin -= FindRegionPos;
	End -= FindRegionPos;
	if( Begin.X < 0 || End.X < 0 || Begin.Y < 0 || End.Y < 0 ||
		Begin.X >= 100 || End.X >= 100 || Begin.Y >= 100 || End.Y >= 100 ) return false;
	if(MaxLength > 49) MaxLength = 49;

	Map[ Begin.X ][ Begin.Y ] = 0;
	
	POS pos = End;
	list< POS > for_fill;
	for_fill.push_back( Begin );
	list< POS >::iterator j;
	
	Size = 0;

	for( unsigned i = 1; ; i++ )
	{
		j = for_fill.begin();

		if( *j == End ) {
			Size = i;
			break;
		}

		short x = (*j).X;
		short y = (*j).Y;

		if( Diagonal )
		{
			if( Map[x - 1][y - 1] == -1 ) {
				Map[x - 1][y - 1] = i;
				for_fill.push_back( POS(x - 1, y - 1) );
			}
			if( Map[x + 1][y - 1] == -1 ) {
				Map[x + 1][y - 1] = i;
				for_fill.push_back( POS(x + 1, y - 1) );
			}
			if( Map[x - 1][y + 1] == -1 ) {
				Map[x - 1][y + 1] = i;
				for_fill.push_back( POS(x - 1, y + 1) );
			}
			if( Map[x + 1][y + 1] == -1 ) {
				Map[x + 1][y + 1] = i;
				for_fill.push_back( POS(x + 1, y + 1) );
			}
		}

		if( Map[x][y - 1] == -1 ) {
			Map[x][y - 1] = i;
			for_fill.push_back( POS(x, y - 1) );
		}
		if( Map[x - 1][y] == -1 ) {
			Map[x - 1][y] = i;
			for_fill.push_back( POS(x - 1, y) );
		}
		if( Map[x + 1][y] == -1 ) {
			Map[x + 1][y] = i;
			for_fill.push_back( POS(x + 1, y) );
		}
		if( Map[x][y + 1] == -1 ) {
			Map[x][y + 1] = i;
			for_fill.push_back( POS(x, y + 1) );
		}

		for_fill.erase(j);

		if( (for_fill.size() == 0) || (MaxLength > 0 && i >= MaxLength) ) {
			Size = 0;
			break;
		}
	}

	if( Size == 0 ) return false;
	
	pos = Begin;

	if( Path ) delete[] Path;
	Path = new POS[ Size + 1 ];

	const POS VECTORS[ 8 ] =
	{
		POS( +0, +1 ),
		POS( +0, -1 ),
		POS( -1, +0 ),
		POS( +1, +0 ),
		POS( +1, +1 ),
		POS( -1, -1 ),
		POS( -1, +1 ),
		POS( +1, -1 )
	};

	for( int i = Size; i >= 0; i-- )
	{
		for( UCHAR j = 0; j < 8; j++ )
		{
			if( Map[ pos.X + VECTORS[ j ].X ][ pos.Y + VECTORS[ j ].Y ] == i )
			{
				pos = pos + VECTORS[ j ];
				Path[ Size - i - 1 ] = FindRegionPos + pos;
				break;
			}
		}
	}
	
	return true;
}

/****************************************************************************************************/

void _error( string komunikat, string plik, unsigned linia )
{
	RysujRamke( POS( 1, 1 ), POS( 78, 4 ), xCze, false );
	StdBuf.Czysc( POS( 2, 2 ), 76 );
	StdBuf.Czysc( POS( 2, 3 ), 76 );
	StdBuf >> xBia >> POS( 3, 1 ) << " ERROR: ";
	StdBuf >> xBia >> POS( 2, 2 ) << ( AS_INT(plik.size()) - 70 < 0 ? plik : plik.substr( plik.size() - 70, 70 ) ) << ": " << linia;
	StdBuf >> xBia >> POS( 2, 3 ) << komunikat;
	StdBuf.Rysuj();
	_getch();
	exit(0);
}

void _warning( string komunikat, string plik, unsigned linia )
{
	RysujRamke( POS( 1, 1 ), POS( 78, 4 ), xZul, false );
	StdBuf.Czysc( POS( 2, 2 ), 76 );
	StdBuf.Czysc( POS( 2, 3 ), 76 );
	StdBuf >> xBia >> POS( 3, 1 ) << " WARNING: ";
	StdBuf >> xBia >> POS( 2, 2 ) << ( AS_INT(plik.size()) - 70 < 0 ? plik : plik.substr( plik.size() - 70, 70 ) ) << ": " << linia;
	StdBuf >> xBia >> POS( 2, 3 ) << komunikat;
	StdBuf.Rysuj();
	_getch();
}

/****************************************************************************************************/