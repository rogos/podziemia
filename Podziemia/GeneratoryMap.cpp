#include "stdafx.h"
#include "GeneratoryMap.h"

/****************************************************************************************************/

BYTE DEF_KLOCKA::SumaPrzejsc() { return N + E + S + W; }

void DEF_KLOCKA::Obroc()
{
	bool oldN = N;
	N = W;
	W = S;
	S = E;
	E = oldN;

	string OldMapka = Mapka;
	for(WORD i = 0; i < Mapka.size(); i++) 
		Mapka[ 7 * (i%7) + abs(i/7-6) ] = OldMapka[i];
}

/****************************************************************************************************/

bool GenerujMape( const RLGL::ELEMENT& def_mapy, MAPA_GENEROWANA& mapa, POS pozycja )
{
	string g = def_mapy("GENERATOR").GetValue();
	POS rozmiar = POS(def_mapy("SIZE")[0], def_mapy("SIZE")[1]);

	if( g == "rooms" )
		GanerujMapePokoi( mapa, pozycja, rozmiar, def_mapy( "NUM_ROOMS" ).GetValue(), DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ) );

	else if( g == "rooms2" )
		GanerujMapePojedynczychPokoi( mapa, pozycja, rozmiar, def_mapy( "NUM_ROOMS" ).GetValue(), DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ),
		                              ( def_mapy.Exist( "MIN_ROOM_SIZE" ) ? POS(def_mapy( "MIN_ROOM_SIZE" )[0], def_mapy( "MIN_ROOM_SIZE" )[1]) : POS(0,0) ) );

	else if( g == "regular_rooms" )
		GanerujMapeRegularnychPokoi( mapa, pozycja, rozmiar, DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ) );

	else if( g == "cave" )
		GanerujMapeJaskini( mapa, pozycja, rozmiar, DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ) );

	else if( g == "maze" )
		GanerujMapeLabiryntu( mapa, pozycja, rozmiar, DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ) );

	else if( g == "from_file" )
		GanerujMapeZPliku( mapa, pozycja, def_mapy( "FILE" ).GetValue() );

	else if( g == "from_string" )
		GanerujMapeZeStringa( mapa, pozycja, rozmiar, DaneGry( "SYMBOLS" )( AS_STR def_mapy( "TILES_SYMBOLS" ).GetValue() ),
		                      ( def_mapy.Exist( "OBJECTS_SYMBOLS" ) ? DaneGry( "SYMBOLS" )( AS_STR def_mapy( "OBJECTS_SYMBOLS" ).GetValue() ) : RLGL::STRUCTURE() ),
		                      def_mapy( "TILES" ).GetValue(), ( def_mapy.Exist( "OBJECTS" ) ? def_mapy( "OBJECTS" ).GetValue() : "" ),
		                      ( def_mapy.Exist( "STYLE" ) ? DaneGry( "STYLES" )( AS_STR def_mapy( "STYLE" ).GetValue() ) : RLGL::STRUCTURE() ) );
	
	else return false;


	// Wstawianie postaci/wrog�w
	for( WORD i = 0; def_mapy.Exist( "MONSTERS", i ); i++ ) {
		const RLGL::ELEMENT& wrogowie = def_mapy( "MONSTERS", i );
		WstawPostacie( mapa, pozycja, rozmiar, IndexWroga(wrogowie[0]), (wrogowie.Size() > 2 ? Losuj(wrogowie[1], wrogowie[2]) : wrogowie[1]) );
	}

	// Wstawianie os�b
	for( WORD i = 0; def_mapy.Exist( "NPC", i ); i++ ) {
		const RLGL::ELEMENT& osoba = def_mapy( "NPC", i );
		WstawOsobe( mapa, pozycja, rozmiar, IndexOsoby(osoba[0]) );
	}

	// Wstawianie przedmiot�w
	for( WORD i = 0; def_mapy.Exist( "ITEMS", i ); i++ ) {
		const RLGL::ELEMENT& przedmioty = def_mapy( "ITEMS", i );
		WstawPrzedmioty( mapa, pozycja, rozmiar, IndexPrzedmiotu(przedmioty[0]), (przedmioty.Size() > 2 ? Losuj(przedmioty[1], przedmioty[2]) : przedmioty[1]) );
	}

	return true;
}

/****************************************************************************************************/

void WstawPrzedmioty( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD przedmiot, short ilosc )
{
	while( ilosc > 0 )
	{
		//	wylosowanie ilo�ci przedmiot�w w danej grupie
		BYTE ilosc_w_grupie = Losuj( 1, ( ilosc > 10 ? 10 : ilosc ) );//::Przedmioty[index_przedmiotu].MaxWGrupie );

		//	dodawanie przedmiotu do mapy na wylosowanej pozycji
		mapa.DodajPrzedmiot( przedmiot, ilosc_w_grupie, mapa.LosujPozycje(pozycja_obszaru, rozmiar_obszaru) );
		ilosc -= ilosc_w_grupie;
	}
}

/*..................................................................................................*/

void WstawPostacie( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD postac, short ilosc )
{
	while( ilosc > 0 )
	{
		//	wylosowanie ilo�ci wrog�w w danej grupie
		BYTE ilosc_w_grupie = Losuj( 1, ::Wrogowie[postac].MaxWGrupie );

		//	wylosowanie pozycji w pobli�u kt�rej znajd� sie wrogowie
		POS pozycja = mapa.LosujPozycje(pozycja_obszaru, rozmiar_obszaru);

		for( int i = 0; i < ilosc_w_grupie && ilosc > 0; i++ )
		{
			//	dodawanie wroga do mapy na wylosowanej pozycji
			mapa.DodajPostac( postac, mapa.PozycjaObok(pozycja, pozycja_obszaru, rozmiar_obszaru) );
			ilosc--;
		}
	}
}

/*..................................................................................................*/

void WstawOsobe( MAPA_GENEROWANA& mapa, POS pozycja_obszaru, POS rozmiar_obszaru, WORD osoba )
{
	mapa.DodajOsobe( osoba, mapa.LosujPozycje(pozycja_obszaru, rozmiar_obszaru) );
}

/****************************************************************************************************/

BYTE LosujKafelek( const RLGL::ELEMENT& def_stylu )
{
	WORD suma_stosunkow = 0;
	for( BYTE i = 0; i < def_stylu.Size(); i++ ) suma_stosunkow += AS_INT def_stylu( i ).GetValue();

	WORD losowa = Losuj( 1, suma_stosunkow );
	WORD j = 0;
	
	for( BYTE i = 0; i < def_stylu.Size(); i++ ) {
		j += AS_INT def_stylu( i ).GetValue();
		if( losowa <= j ) return IndexKafelka( def_stylu( i ).GetName() );
	}
	
	return 0;
}

/****************************************************************************************************/

bool PodzielPokoj( bool poziomo, SECTOR pozycja_sciany, array<POKOJ>& pokoje, BYTE indeks_pokoju )
{
	if( pozycja_sciany.End < pozycja_sciany.Begin ) {
		warning("Nie mo�na stworzy� kolejnego pokoju poniewa� mapa jest zbyt ma�a.");
		return false;
	}

	pokoje.add( POKOJ() ); // Dodanie nowego pokoju
	WORD poz_sciany = pozycja_sciany.Rand(); // Wylosowanie pozycji �ciany

	if( poziomo )
	{
		// Ustawienie pozycji nowego pokoju
		pokoje.back().Pozycja.Set( pokoje[ indeks_pokoju ].Pozycja.X,
		                         pokoje[ indeks_pokoju ].Pozycja.Y + poz_sciany + 1 );

		// Ustawienie rozmiaru nowego pokoju
		pokoje.back().Rozmiar.Set( pokoje[ indeks_pokoju ].Rozmiar.X,
		                         pokoje[ indeks_pokoju ].Rozmiar.Y - poz_sciany - 1 );

		// Zmniejszenie rozmiaru starego pokoju
		pokoje[ indeks_pokoju ].Rozmiar.Y = poz_sciany;

		// Podanie informacji jak dzielony by� pok�j
		pokoje.back().DzielonyPoziomo = true;
	}
	else
	{
		// Ustawienie pozycji nowego pokoju
		pokoje.back().Pozycja.Set( pokoje[ indeks_pokoju ].Pozycja.X + poz_sciany + 1,
		                         pokoje[ indeks_pokoju ].Pozycja.Y );

		// Ustawienie rozmiaru nowego pokoju
		pokoje.back().Rozmiar.Set( pokoje[ indeks_pokoju ].Rozmiar.X - poz_sciany - 1,
		                         pokoje[ indeks_pokoju ].Rozmiar.Y );

		// Zmniejszenie rozmiaru starego pokoju
		pokoje[ indeks_pokoju ].Rozmiar.X = poz_sciany;

		// Podanie informacji jak dzielony by� pok�j
		pokoje.back().DzielonyPoziomo = false;
	}

	return true;
}

/****************************************************************************************************/

bool GanerujMapePokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, BYTE ilosc_pokoi, const RLGL::ELEMENT& def_stylu )
{
	array< array<BYTE> > sciany;
	sciany.resize(rozmiar.X);
	for(WORD x = 0; x < rozmiar.X; x++) sciany[x].resize(rozmiar.Y);

	// Wype�nienie ca�ej mapy �cianami
	for( int x = 0; x < rozmiar.X; x++ )
		for( int y = 0; y < rozmiar.Y; y++ ) {
			mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + POS(x, y) );
			sciany[x][y] = true;
		}

	array<POKOJ> pokoje;

	// Stworzenie pierwszego pokoju obejmuj�cego ca�� map�
	pokoje.add( POKOJ() );
	pokoje.back().Pozycja.Set( 1, 1 );
	pokoje.back().Rozmiar.Set( rozmiar.X - 2, rozmiar.Y - 2 );

	// Dzielenie kolejnych pokoi
	for( BYTE i = 1; i < ilosc_pokoi; i++ )
	{
		BYTE index = 0;
		WORD rozmiar = 0;
		// Szukanie najwi�kszego pokoju kt�ry zostanie podzielony
		for( BYTE i = 0; i < pokoje.size(); i++ )
			if( pokoje[ i ].Rozmiar.X * pokoje[ i ].Rozmiar.Y > rozmiar ) {
				rozmiar = pokoje[ i ].Rozmiar.X * pokoje[ i ].Rozmiar.Y;
				index = i;
			}

		// Je�eli szeroko�� pokoju jest wi�ksza od wysoko�ci dzieli pionowo...
		if( pokoje[index].Rozmiar.X > pokoje[ index ].Rozmiar.Y * 1.4 )
			PodzielPokoj( false, SECTOR( 1, pokoje[ index ].Rozmiar.X - 2 ), pokoje, index );
		// ...w przeciwnym razie dzieli poziomo
		else PodzielPokoj( true, SECTOR( 1, pokoje[ index ].Rozmiar.Y - 2 ), pokoje, index );
	}

	// Stworzenie mapy na podstawie stworzonych pokoi
	for( int i = 0; i < pokoje.size(); i++ )
		for( int y = 0; y < pokoje[ i ].Rozmiar.Y; y++ )
			for( int x = 0; x < pokoje[ i ].Rozmiar.X; x++ ) {
				mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + pokoje[i].Pozycja + POS(x, y) );
				sciany[pokoje[i].Pozycja.X + x][pokoje[i].Pozycja.Y + y] = false;
			}

	//---------------------------------------------------------------------------------------------//

	// Wstawia drzwi
	for( BYTE i = 1; i < pokoje.size(); i++ )
	{
		BYTE ilosc_drzwi = pokoje[i].DzielonyPoziomo == Poziomo ?
			Losuj( 1, pokoje[i].Rozmiar.X / 7 + 1 ) : Losuj( 1, pokoje[i].Rozmiar.Y / 7 + 1 );

		bool poziomo = pokoje[i].DzielonyPoziomo;

		for( BYTE j = 1; j < ilosc_drzwi + 1; j++ )
		{
			POS poz_drzwi( ( poziomo ? Losuj( pokoje[i].Pozycja.X, pokoje[i].Pozycja.X + pokoje[i].Rozmiar.X - 1 ) : pokoje[i].Pozycja.X - 1 ),
			               ( poziomo ? pokoje[i].Pozycja.Y - 1 : Losuj( pokoje[i].Pozycja.Y, pokoje[i].Pozycja.Y + pokoje[i].Rozmiar.Y - 1 ) ) );

			// je�eli obok s� ju� drzwi lub �ciana zastawia przej�cie szuka innego miejsca
			if( !sciany[ poz_drzwi.X - (poziomo ? 1 : 0) ][ poz_drzwi.Y - (poziomo ? 0 : 1) ] ||
				!sciany[ poz_drzwi.X + (poziomo ? 1 : 0) ][ poz_drzwi.Y + (poziomo ? 0 : 1) ] ||
				sciany[ poz_drzwi.X - (poziomo ? 0 : 1) ][ poz_drzwi.Y - (poziomo ? 1 : 0) ] ||
				sciany[ poz_drzwi.X + (poziomo ? 0 : 1) ][ poz_drzwi.Y + (poziomo ? 1 : 0) ] ) {
				j--;
				continue;
			}
			
			mapa.WstawKafelek( LosujKafelek(def_stylu("DOORS")), pozycja + poz_drzwi );
			sciany[poz_drzwi.X][poz_drzwi.Y] = false;
		}
	}

	return true;
}

/****************************************************************************************************/

bool GanerujMapePojedynczychPokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, BYTE ilosc_pokoi, const RLGL::ELEMENT& def_stylu, POS min_czesc_pokoju )
{
	if(min_czesc_pokoju.X > 100) min_czesc_pokoju.X = 100;
	if(min_czesc_pokoju.Y > 100) min_czesc_pokoju.Y = 100;

	// Wype�nienie ca�ej mapy �cianami
	for( int x = 0; x < rozmiar.X; x++ )
		for( int y = 0; y < rozmiar.Y; y++ ) mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + POS(x, y) );

	array<POKOJ> pokoje;

	// Stworzenie g��wnego pokoju obejmuj�cego ca�� map�
	pokoje.add( POKOJ() );
	pokoje.back().Pozycja.Set( 2, 2 );
	pokoje.back().Rozmiar.Set( rozmiar.X - 4, rozmiar.Y - 4 );

	// Rysowanie g��wnego pokoju
	for( int y = 0; y < rozmiar.Y - 2; y++ )
		for( int x = 0; x < rozmiar.X - 2; x++ )
			mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + POS(x + 1, y + 1) );

	// Dzielenie kolejnych pokoi
	for( BYTE i = 0; i < ilosc_pokoi - 1; i++ )
	{
		BYTE index = 0;
		WORD roz = 0;
		// Szukanie najwi�kszego pokoju kt�ry zostanie podzielony
		for( BYTE i = 0; i < pokoje.size(); i++ )
			if( pokoje[ i ].Rozmiar.X * pokoje[ i ].Rozmiar.Y > roz ) {
				roz = pokoje[ i ].Rozmiar.X * pokoje[ i ].Rozmiar.Y;
				index = i;
			}

		// Je�eli szeroko�� pokoju jest wi�ksza od wysoko�ci dzieli pionowo...
		if( pokoje[index].Rozmiar.X > pokoje[ index ].Rozmiar.Y * 1.4 )
			PodzielPokoj( false, SECTOR( 3, pokoje[ index ].Rozmiar.X - 4 ), pokoje, index );
		// ...w przeciwnym razie dzieli poziomo
		else PodzielPokoj( true, SECTOR( 3, pokoje[ index ].Rozmiar.Y - 4 ), pokoje, index );
	}

	// Stworzenie mapy na podstawie stworzonych pokoi
	for( int i = 0; i < pokoje.size(); i++ )
	{
		POS poz = pokoje[i].Pozycja;
		POS roz = pokoje[i].Rozmiar;
		poz = POS( Losuj( poz.X, poz.X + roz.X - max(min_czesc_pokoju.X * roz.X / 100, 3) ),
		               Losuj( poz.Y, poz.Y + roz.Y - max(min_czesc_pokoju.Y * roz.Y / 100, 3) ) );
		roz = POS( Losuj( max(min_czesc_pokoju.X * roz.X / 100, 3), roz.X - (poz.X - pokoje[i].Pozycja.X) ),
		               Losuj( max(min_czesc_pokoju.Y * roz.Y / 100, 3), roz.Y - (poz.Y - pokoje[i].Pozycja.Y) ) );
		pokoje[i].Pozycja = poz;
		pokoje[i].Rozmiar = roz;

		for( int y = 0; y < roz.Y; y++ )
			for( int x = 0; x < roz.X; x++ )
			{
				if( y == 0 || y == roz.Y - 1 || x == 0 || x == roz.X - 1 )
					mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + poz + POS(x, y) );
			}
	}

//----------------------------------------------------------------------------------------------------//

	// Wstawia drzwi
	for( BYTE i = 0; i < pokoje.size(); i++ )
	{
		POS pozycja_drzwi;
		switch(Losuj(1, 4)) {
			case 1: pozycja_drzwi = POS( 0, Losuj( 1, pokoje[i].Rozmiar.Y - 2 ) ); break;
			case 2: pozycja_drzwi = POS( pokoje[i].Rozmiar.X - 1, Losuj( 1, pokoje[i].Rozmiar.Y - 2 ) ); break;
			case 3: pozycja_drzwi = POS( Losuj( 1, pokoje[i].Rozmiar.X - 2 ), 0 ); break;
			case 4: pozycja_drzwi = POS( Losuj( 1, pokoje[i].Rozmiar.X - 2 ), pokoje[i].Rozmiar.Y - 1 ); break;
		}
		mapa.WstawKafelek( LosujKafelek(def_stylu("DOORS")), pozycja + pokoje[i].Pozycja + pozycja_drzwi );
	}

	return true;
}

/****************************************************************************************************/

bool GanerujMapeJaskini( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu )
{
	array< array<BYTE> > zamalowane;
	zamalowane.resize(rozmiar.X);
	for(unsigned i = 0; i < rozmiar.X; i++) zamalowane[i].resize(rozmiar.Y);

	for( WORD i = 0; i < rozmiar.X * rozmiar.Y; i++ ) zamalowane[i % rozmiar.X][i / rozmiar.X] = false;

	// Wype�nienie ca�ej mapy �cianami
	for( int x = 0; x < rozmiar.X; x++ )
		for( int y = 0; y < rozmiar.Y; y++ ) mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + POS(x, y) );

	POS c, pos;
	/* Tworzenie jaskini */
	for( WORD k = 0; k < rozmiar.X * rozmiar.Y / 7; k++ )
	{
		c.X = Losuj(3, rozmiar.X - 5);
		c.Y = Losuj(3, rozmiar.Y - 5);

		for( WORD angle = 0; angle < 360; angle += 3 )
		{
			for( BYTE w = 0; w < 3; w++ )
			{
				pos.X = c.X + AS_INT( w * cos(angle * PI / 180.0) );
				pos.Y = c.Y + AS_INT( w * sin(angle * PI / 180.0) ); 

				if( !zamalowane[pos.X][pos.Y] && pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 ) 
				{
					mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + pos );
					zamalowane[pos.X][pos.Y] = true;
				}
			}
		}
	}

	return true;
}

/****************************************************************************************************/

bool GanerujMapeLabiryntu( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu )
{
	array< array<BYTE> > zamalowane;
	zamalowane.resize(rozmiar.X);
	for(unsigned i = 0; i < rozmiar.X; i++) zamalowane[i].resize(rozmiar.Y);

	for( WORD i = 0; i < rozmiar.X * rozmiar.Y; i++ ) zamalowane[i % rozmiar.X][i / rozmiar.X] = false;

	// Wype�nienie ca�ej mapy �cianami
	for( int x = 0; x < rozmiar.X; x++ )
		for( int y = 0; y < rozmiar.Y; y++ ) mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + POS(x, y) );

	vector<POS> punkty;
	punkty.push_back( POS(rozmiar.X / 2, rozmiar.Y / 2) );
	zamalowane[punkty[0].X][punkty[0].Y] = true;
	mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + punkty[0] );

	// Tworzenie labiryntu
	for(;;)
	{
		int punkt = -1;
		if(punkt == -1) punkt = Losuj(0, punkty.size() - 1);
		POS pos, pos2;
		switch(Losuj(1, 4)) {
			case 1: pos = punkty[punkt](0, 2); pos2 = punkty[punkt](0, 1); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			case 2: pos = punkty[punkt](2, 0); pos2 = punkty[punkt](1, 0); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			case 3: pos = punkty[punkt](0, -2); pos2 = punkty[punkt](0, -1); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			case 4: pos = punkty[punkt](-2, 0); pos2 = punkty[punkt](-1, 0); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			
			case 5: pos = punkty[punkt](0, 2); pos2 = punkty[punkt](0, 1); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			case 6: pos = punkty[punkt](2, 0); pos2 = punkty[punkt](1, 0); if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) break;
			case 7: pos = punkty[punkt](0, -2); pos2 = punkty[punkt](0, -1); break;
		}
		
		if( pos.X > 0 && pos.Y > 0 && pos.X < rozmiar.X - 1 && pos.Y < rozmiar.Y - 1 && !zamalowane[pos.X][pos.Y] ) 
		{
			punkty.push_back( pos );
			punkt = punkty.size() - 1;
			zamalowane[pos.X][pos.Y] = true;
			mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + pos );
			mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + pos2 );
		}
		else punkty.erase(punkty.begin() + punkt);
		
		if(punkty.size() == 0) break;
	}
	
	return true;
}

/****************************************************************************************************/

bool GanerujMapeRegularnychPokoi( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& def_stylu )
{
	const POS rozmiar_planszy = POS( rozmiar.X / 7, rozmiar.Y / 7 );

	vector< vector<KLOCEK> > klocki;
	klocki.resize(rozmiar_planszy.X);
	for(unsigned i = 0; i < rozmiar_planszy.X; i++) klocki[i].resize(rozmiar_planszy.Y);

	vector<POS> pola_w_zasiegu;
	pola_w_zasiegu.push_back( POS(0, 0) );

	/* tworzy po��czenia mi�dzy klockami */
	while( pola_w_zasiegu.size() > 0 )
	{
		unsigned wylosowane = Losuj(0, pola_w_zasiegu.size() - 1);
		POS poz = pola_w_zasiegu[wylosowane];
		array<BYTE> zlacza;
		klocki[poz.X][poz.Y].Zakryte = true;
		pola_w_zasiegu.erase(pola_w_zasiegu.begin() + wylosowane);

		for( BYTE k = 0; k < 4; k++ )
		{
			POS p = poz + WEKTORY[k];
			
			if( p.X < 0 || p.Y < 0 || p.X >= rozmiar_planszy.X || p.Y >= rozmiar_planszy.Y ) continue;

			if( klocki[p.X][p.Y].Zakryte ) zlacza.add( k );
			else if( !klocki[p.X][p.Y].WZasiegu ) {
				pola_w_zasiegu.push_back( p );
				klocki[p.X][p.Y].WZasiegu = true;
			}
		}
		if( zlacza.size() == 0 ) continue;
		BYTE zlacze = zlacza[ Losuj(0, zlacza.size() - 1) ];
		klocki[poz.X][poz.Y].Wyjscia[zlacze] = true;
		klocki[poz.X + WEKTORY[zlacze].X][poz.Y + WEKTORY[zlacze].Y].Wyjscia[ (zlacze + 2) % 4 ] = true;
	}

	/* tworzy map� z losowanych klock�w */
	for(WORD x = 0; x < rozmiar_planszy.X; x++)
		for(WORD y = 0; y < rozmiar_planszy.Y; y++)
		{
			// Wyszukuje wszystkie pasuj�ce klocki
			vector<BYTE> pasujace_klocki;
			for(BYTE i = 0; i < ILOSC_KLOCKOW; i++) {
				for(BYTE j = 0; j < 4; j++) {
					// Sprawdza czy dany klocek pasuje
					if( KLOCKI[i].N == klocki[x][y].N &&
						KLOCKI[i].S == klocki[x][y].S &&
						KLOCKI[i].W == klocki[x][y].W &&
						KLOCKI[i].E == klocki[x][y].E ) {
							pasujace_klocki.push_back(i);
							break;
					}
					// Je�li nie pasuje obraca go i sprawdza jeszcze raz
					KLOCKI[i].Obroc();
				}
			}
			
			// Losuje jeden z pasuj�cych klock�w
			BYTE klocek = pasujace_klocki[ Losuj( 0, pasujace_klocki.size() - 1 ) ];

			for(BYTE j = 0; j < 4; j++) {
				// Sprawdza czy dany klocek pasuje
				if( KLOCKI[klocek].N == klocki[x][y].N &&
					KLOCKI[klocek].S == klocki[x][y].S &&
					KLOCKI[klocek].W == klocki[x][y].W &&
					KLOCKI[klocek].E == klocki[x][y].E ) break;
				// Je�li nie pasuje obraca go i sprawdza jeszcze raz
				KLOCKI[klocek].Obroc();
			}

			for(WORD x1 = 0; x1 < 7; x1++)
				for(WORD y1 = 0; y1 < 7; y1++)
				{
					POS poz = POS( x * 7 + x1, y * 7 + y1 );
					char kafelek = KLOCKI[klocek].Mapka[7 * (y1 % 7) + (x1 % 7)];
					if( kafelek == '#' ) mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + poz );
					else if( kafelek == '+' ) mapa.WstawKafelek( LosujKafelek(def_stylu("DOORS")), pozycja + poz );
					else mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + poz );
				}
		}

	return true;
}

/****************************************************************************************************/

bool GanerujMapeZPliku( MAPA_GENEROWANA& mapa, POS pozycja, const string adres_pliku )
{
	DEF_MAPY* def_mapy;
	for( WORD i = 0; i < MapyZPliku.size(); i++ ) if( MapyZPliku[i].Adres == adres_pliku ) {
		def_mapy = MapyZPliku[i].Mapa;
		break;
	}
	
	// Wstawianie kafelk�w
	for( WORD x = 0; x < def_mapy->Rozmiar.X; x++ )
		for( WORD y = 0; y < def_mapy->Rozmiar.Y; y++ )
			mapa.WstawKafelek( def_mapy->Kafelki[x][y], pozycja + POS(x, y) );

	// Wstawianie objekt�w
	for( unsigned i = 0; i < def_mapy->Przedmioty.size(); i++ )
		mapa.DodajPrzedmiot( def_mapy->Przedmioty[i].NR, def_mapy->Przedmioty[i].Ilosc, pozycja + def_mapy->Przedmioty[i].Pozycja );

	for( unsigned i = 0; i < def_mapy->Postacie.size(); i++ )
		mapa.DodajPostac( def_mapy->Postacie[i].NR, pozycja + def_mapy->Postacie[i].Pozycja );
	
	for( unsigned i = 0; i < def_mapy->Osoby.size(); i++ )
		mapa.DodajOsobe( def_mapy->Osoby[i].NR, pozycja + def_mapy->Osoby[i].Pozycja );

	return true;
}

/****************************************************************************************************/

bool GanerujMapeZeStringa( MAPA_GENEROWANA& mapa, POS pozycja, POS rozmiar, const RLGL::ELEMENT& symbole_kafelkow, const RLGL::ELEMENT& symbole_objektow, string kafelki, string objekty, const RLGL::ELEMENT& def_stylu )
{
	if( kafelki.size() != rozmiar.X * rozmiar.Y ) return false;
	
	// Wstawianie kafelk�w
	for( unsigned i = 0; i < rozmiar.X * rozmiar.Y; i++ )
	{
		POS p = POS(i % rozmiar.X, i / rozmiar.X);
		for( unsigned j = 0; j < symbole_kafelkow.Size(); j++ )
		{
			if( kafelki[i] == AS_CHAR symbole_kafelkow(j).GetValue() )
			{
				if( symbole_kafelkow(j).GetName() == "RANDOM_WALL" ) mapa.WstawKafelek( LosujKafelek(def_stylu("WALLS")), pozycja + p );
				else if( symbole_kafelkow(j).GetName() == "RANDOM_GROUND" ) mapa.WstawKafelek( LosujKafelek(def_stylu("GROUNDS")), pozycja + p );
				else if( symbole_kafelkow(j).GetName() == "RANDOM_DOOR" ) mapa.WstawKafelek( LosujKafelek(def_stylu("DOORS")), pozycja + p );
				else mapa.WstawKafelek( IndexKafelka( symbole_kafelkow(j).GetName() ), pozycja + p );

				break;
			}
			if( j == symbole_kafelkow.Size() - 1 ) { // je�eli nie znajdzie symbolu wstawia kafelek o indeksie 0 i wy�wietla ostrze�enie
				mapa.WstawKafelek( 0, pozycja + p );
				warning("Nie znaleziono symbolu kafelka wstawionego na mapie.");
				break;
			}
		}
	}

	if( objekty.size() != rozmiar.X * rozmiar.Y ) return true;

	// Wstawianie objekt�w
	for( unsigned i = 0; i < rozmiar.X * rozmiar.Y; i++ )
	{
		POS p = POS(i % rozmiar.X, i / rozmiar.X);
		for( unsigned j = 0; j < symbole_objektow.Size(); j++ )
		{
			if( objekty[i] == AS_CHAR symbole_objektow(j).GetValue() )
			{
				int index = IndexWroga( symbole_objektow(j).GetName() );
				if( index >= 0 ) mapa.DodajPostac( index, pozycja + p );
				else {
					index = IndexPrzedmiotu( symbole_objektow(j).GetName() );
					if( index >= 0 ) mapa.DodajPrzedmiot( index, 1, pozycja + p );
					else {
						index = IndexOsoby( symbole_objektow(j).GetName() );
						if( index >= 0 ) mapa.DodajOsobe( index, pozycja + p );
					}
				}
				break;
			}
		}
	}

	return true;
}

/****************************************************************************************************/

bool ZlaczMapy( const RLGL::ELEMENT& def_polaczenia )
{
	int index_mapy1 = IndexMapy( def_polaczenia[0] );
	int index_mapy2 = IndexMapy( def_polaczenia[1] );
	if( index_mapy1 == -1 || index_mapy2 == -1 ) return false;
	MAPA& mapa1 = StanGry->Mapy[index_mapy1];
	MAPA& mapa2 = StanGry->Mapy[index_mapy2];
	BYTE kafelek1 = IndexKafelka( def_polaczenia[2] );
	BYTE kafelek2 = IndexKafelka( def_polaczenia[3] );

	POS pozycja1, pozycja2;
	for( unsigned j = 0; ; j++ ) {
		pozycja1 = mapa1.SzukajKafelka( kafelek1, j );
		if( pozycja1 == POS_NOT_FOUND ) {
			pozycja1 = mapa1.LosujPozycje( POS(1, 1), mapa1.Rozmiar(-2, -2) );
			break;
		}
		if( mapa1.SzukajPrzejscia( pozycja1 ) == -1 ) break;
	}
	for( unsigned j = 0; ; j++ ) {
		pozycja2 = mapa2.SzukajKafelka( kafelek2, j );
		if( pozycja2 == POS_NOT_FOUND ) {
			pozycja2 = mapa2.LosujPozycje( POS(1, 1), mapa2.Rozmiar(-2, -2) );
			break;
		}
		if( mapa2.SzukajPrzejscia( pozycja2 ) == -1 ) break;
	}

	StanGry->PolaczMapy( index_mapy1, index_mapy2, kafelek1, kafelek2, (def_polaczenia.Size() > 4 ? def_polaczenia[3] : 0 ), pozycja1, pozycja2 );

	return true;
}

/****************************************************************************************************/

const BYTE ILOSC_KLOCKOW = 36;
DEF_KLOCKA KLOCKI[] = {
{
"###.###"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#######",
1,0,0,0
},
{
"#.....#"
"#.....#"
"#.....#"
"###+###"
"#.....#"
"#.....#"
"#######",
1,0,0,0
},
{
"#.....#"
"###+###"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#######",
1,0,0,0
},
{
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#######",
1,0,0,0
},
{
"###.###"
"#.....#"
"#.###.#"
"#.###.#"
"#.###.#"
"#.....#"
"#######",
1,0,0,0
},
{
"###.###"
"###+###"
"##...##"
"##...##"
"##...##"
"#######"
"#######",
1,0,0,0
},
{
"###.###"
"#.....#"
"#.###.#"
"#.###.#"
"#.###.#"
"#.....#"
"###.###",
1,0,1,0
},
{
"###.###"
"###.###"
"###.###"
"###.###"
"###.###"
"###.###"
"###.###",
1,0,1,0
},
{
"#.....#"
"#.....#"
"#.....#"
"###+###"
"#.....#"
"#.....#"
"#.....#",
1,0,1,0
},
{
"###.###"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"#.....#"
"###.###",
1,0,1,0
},
{
"###...#"
"###...#"
"###...#"
"###.###"
"#...###"
"#...###"
"#...###",
1,0,1,0
},
{
"#...###"
"#...###"
"#...###"
"#...###"
"#...###"
"#...###"
"#...###",
1,0,1,0
},
{
"###...#"
"###...#"
"###...#"
"###...#"
"###...#"
"###...#"
"###...#",
1,0,1,0
},
{
"###.###"
"#....##"
"#.##.##"
"#.##.##"
"#.##.##"
"#....##"
"###.###",
1,0,1,0
},
{
"###.###"
"##....#"
"##.##.#"
"##.##.#"
"##.##.#"
"##....#"
"###.###",
1,0,1,0
},
{
"###...#"
"#####.#"
"#...#.#"
"#...+.#"
"#...#.#"
"#####.#"
"###...#",
1,0,1,0
},
{
"#...###"
"#.#####"
"#.#...#"
"#.+...#"
"#.#...#"
"#.#####"
"#...###",
1,0,1,0
},
{
"###.###"
"###+###"
"##...##"
".+...##"
"##...##"
"#######"
"#######",
1,0,0,1
},
{
"###.###"
"###.###"
"##...##"
".....##"
"##...##"
"#######"
"#######",
1,0,0,1
},
{
"......#"
"......#"
"......#"
"......#"
"......#"
"......#"
"#######",
1,0,0,1
},
{
"......#"
".##+###"
".#....#"
".+....#"
"##....#"
"##....#"
"#######",
1,0,0,1
},
{
"###.###"
"###.###"
"###.###"
"......."
"#######"
"#######"
"#######",
1,1,0,1
},
{
"......."
"......."
"......."
"......."
"......."
"......."
"#######",
1,1,0,1
},
{
"###.###"
"##...##"
"#.....#"
"......."
"#.....#"
"#.....#"
"#######",
1,1,0,1
},
{
"###.###"
"#.....#"
"#.#.#.#"
"......."
"#######"
"#######"
"#######",
1,1,0,1
},
{
"###.###"
"###.+.#"
"###.#.#"
"....#.."
"....###"
"......."
"#######",
1,1,0,1
},
{
"###.###"
"###+###"
"##...##"
".+...+."
"##...##"
"#######"
"#######",
1,1,0,1
},
{
"###.###"
"###.###"
"###.###"
"......."
"###.###"
"###.###"
"###.###",
1,1,1,1
},
{
"###.###"
"#.....#"
"#.....#"
"......."
"#.....#"
"#.....#"
"###.###",
1,1,1,1
},
{
"......."
"......."
"......."
"......."
"......."
"......."
".......",
1,1,1,1
},
{
"......."
".#####."
".#####."
".#####."
".#####."
".#####."
".......",
1,1,1,1
},
{
"......."
".##.##."
".##.##."
"......."
".##.##."
".##.##."
".......",
1,1,1,1
},
{
"......."
".#####."
".#...#."
".+...+."
".#...#."
".#####."
".......",
1,1,1,1
},
{
"......."
".##+##."
".#...#."
".#...#."
".#...#."
".##+##."
".......",
1,1,1,1
},
{
"###.###"
"#.....#"
"#.###.#"
"..###.."
"#.###.#"
"#.....#"
"###.###",
1,1,1,1
},
{
"###.###"
"###...#"
"###.#.#"
"..#.#.."
"#.#.###"
"#...###"
"###.###",
1,1,1,1
}
};