#include "stdafx.h"

/****************************************************************************************************/

bool PLIK::Otworz( CEL_OTWARCIA CelOtwarcia, SPOSOB_OTWARCIA SposobOtwarcia )
{
	if( m_CelOtwarcia != NIE_OTWARTY ) Zamknij();

	switch( CelOtwarcia )
	{
	case ZAPIS:
		m_Plik = CreateFileA( m_Adres.c_str(),
							  GENERIC_WRITE, 0, NULL, SposobOtwarcia, FILE_ATTRIBUTE_NORMAL, NULL );
		break;

	case ODCZYT:
		m_Plik = CreateFileA( m_Adres.c_str(),
							  GENERIC_READ, 0, NULL, SposobOtwarcia, FILE_ATTRIBUTE_NORMAL, NULL );
		break;

	case ZAPIS_I_ODCZYT:
		m_Plik = CreateFileA( m_Adres.c_str(),
							  GENERIC_ALL, 0, NULL, SposobOtwarcia, FILE_ATTRIBUTE_NORMAL, NULL );
	}
	if( m_Plik == INVALID_HANDLE_VALUE ) return false;

	m_CelOtwarcia = CelOtwarcia;

	return true;
}

/****************************************************************************************************/

bool PLIK::Zamknij()
{
	if( !m_Plik ) return false;

	CloseHandle( m_Plik );

	m_CelOtwarcia = NIE_OTWARTY;
	m_Plik = 0;

	return true;
}

/****************************************************************************************************/

bool PLIK::Zapisz( LPCVOID Zmienna, DWORD Ilosc )
{
	if( m_CelOtwarcia != ZAPIS && m_CelOtwarcia != ZAPIS_I_ODCZYT ) return false;

	DWORD ilosc_zapisanych_bajtow;

	if( WriteFile( m_Plik, Zmienna, Ilosc, &ilosc_zapisanych_bajtow, NULL ) == 0 ||
		ilosc_zapisanych_bajtow != Ilosc ) return false;

	return true;
}

/****************************************************************************************************/

bool PLIK::Wczytaj( LPVOID Zmienna, DWORD Ilosc )
{
	if( m_CelOtwarcia != ODCZYT && m_CelOtwarcia != ZAPIS_I_ODCZYT ) return false;

	DWORD ilosc_wczytanych_bajtow;

	if( ReadFile( m_Plik, Zmienna, Ilosc, &ilosc_wczytanych_bajtow, NULL ) == 0 ||
		ilosc_wczytanych_bajtow != Ilosc ) return false;

	return true;
}

/****************************************************************************************************/

string PLIK::WczytajDoStringa()
{
	string text;
	text.resize( GetFileSize( m_Plik, NULL ) );
	for( unsigned i = 0; i < text.length(); i++ )
		*this >> text[ i ];
	
	return text;
}

/****************************************************************************************************/

unsigned PLIK::Rozmiar()
{
	return GetFileSize( m_Plik, NULL );
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( string Zmienna )
{
	*this << Zmienna.length();
	for( unsigned i = 0; i < Zmienna.length(); i++ )
		*this << Zmienna[ i ];
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( int Zmienna )
{
	Zapisz( &Zmienna, sizeof( int ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( unsigned Zmienna )
{
	Zapisz( &Zmienna, sizeof( unsigned ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( float Zmienna )
{
	Zapisz( &Zmienna, sizeof( float ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( double Zmienna )
{
	Zapisz( &Zmienna, sizeof( double ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( short Zmienna )
{
	Zapisz( &Zmienna, sizeof( short ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( WORD Zmienna )
{
	Zapisz( &Zmienna, sizeof( WORD ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( char Zmienna )
{
	Zapisz( &Zmienna, sizeof( char ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( UCHAR Zmienna )
{
	Zapisz( &Zmienna, sizeof( UCHAR ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator << ( bool Zmienna )
{
	Zapisz( &Zmienna, sizeof( bool ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( string& Zmienna )
{
	unsigned dlugosc;
	*this >> dlugosc;
	Zmienna.resize( dlugosc );
	for( unsigned i = 0; i < dlugosc; i++ )
		*this >> Zmienna[ i ];
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( int& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( int ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( unsigned& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( unsigned ) );
	return *this;
}
/****************************************************************************************************/

PLIK& PLIK::operator >> ( float& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( float ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( double& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( double ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( short& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( short ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( WORD& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( WORD ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( char& Zmienna )
{
	if( !Wczytaj( &Zmienna, sizeof( char ) ) ) Zmienna = -1;
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( UCHAR& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( UCHAR ) );
	return *this;
}

/****************************************************************************************************/

PLIK& PLIK::operator >> ( bool& Zmienna )
{
	Wczytaj( &Zmienna, sizeof( bool ) );
	return *this;
}

/****************************************************************************************************/

bool PLIK::Szyfruj()
{
	if( m_CelOtwarcia != NIE_OTWARTY ) return false; 

	char znak;
	string bufor;
	
	if( !Otworz( ODCZYT, OTWORZ_ISTNIEJACY ) ) return false;

	UCHAR j = 20;
	bool plus = true;

	for( unsigned i = 0; i < Rozmiar(); i++ )
	{
		if( plus ) j++;
		else j--;
		if( j >= 30 ) plus = false;
		if( j <= 6 ) plus = true;
		*this >> znak;
		bufor += znak ^ j;
	}

	Zamknij();

	Otworz( ZAPIS );

	for( int i = 0; i < bufor.length(); i++ )
		*this << bufor[ i ];

	Zamknij();

	return true;
}

/****************************************************************************************************/

bool UsunPlik( string Adres )
{
	if( DeleteFileA( Adres.c_str() ) ) return true;

	return false;
}

/****************************************************************************************************/