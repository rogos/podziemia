#include "stdafx.h"

/****************************************************************************************************/

void AlokacjaCzarow()
{
	_TRY_BEGIN

	const RLGL::ELEMENT& relacja = DaneGry( "SPELLS" );
	CZAR* czar;
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		CZAR _c;
		const RLGL::ELEMENT& c = relacja( i );
		const RLGL::ELEMENT& s = c("SPELL");
		
		_c.ID = c.GetName();
		_c.Nazwa = AS_STR c("NAME").GetValue();
		_c.KosztRzucenia = c("COST").GetValue();
		_c.StopienTrudnosci = c("LEVEL").GetValue();
		_c.Urok.Zaladuj( c("SPELL") );

		string typ_czaru = c("TYPE").GetValue();

		if( typ_czaru == "missile" ) {
			CZAR_POCISK* c2 = new CZAR_POCISK;
			_c.Typ = TC_POCISK;
			c2->Zasieg = c("RANGE").GetValue();
			c2->ZasiegFaliUderzeniowej = ( c.Exist("DAMAGE_RANGE") ? c("DAMAGE_RANGE").GetValue() : 0 );
			c2->PrzechodziPrzezWrogow = c("RAM").GetValue();
			czar = c2;
		}
		else if( typ_czaru == "oneself" ) {
			CZAR* c2 = new CZAR;
			_c.Typ = TC_NA_SIEBIE;
			czar = c2;
		}

		*czar = _c;
		Czary.add( czar );
	}

	_CATCH( const char* komunikat )
		error( komunikat );
	_CATCH_END
}

/****************************************************************************************************/