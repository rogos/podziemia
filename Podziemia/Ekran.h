#pragma once

/****************************************************************************************************/

#include "stdafx.h"

/****************************************************************************************************/

struct BUFOR
{
private:
	CHAR_INFO StareZnaki[ 80 * 40 ];
	POS StaraPozycjaKursora;
public:
	CHAR_INFO Znaki[ 80 * 40 ];
	POS PozycjaKursora;

	bool operator () ();
	BUFOR()
	{
		for( int i = 0; i < 80 * 40; i++ )
		{
			Znaki[ i ].Char.AsciiChar = ' ';
			Znaki[ i ].Attributes = 0;
		}
	}
};

/****************************************************************************************************/

class STD_BUFOR
{
	BUFOR* DomyslnyBufor;
	BUFOR* StdBufor;
	POS PozycjaPisania;
	WORD KolorPisania;
public:
	STD_BUFOR() : DomyslnyBufor( new BUFOR )
	{
		StdBufor = DomyslnyBufor;
		KolorPisania = xSza;
	}
	~STD_BUFOR()
	{
		delete DomyslnyBufor;
	}
	void WstawZnak( char Znak, WORD Kolor, POS Pozycja, bool PoprawNaPolskieLitery = true );
	void PokolorujZnak( WORD Kolor, POS Pozycja );
	void Napisz( string Tekst, unsigned Od = 0, unsigned Do = 0 );
	void NapiszIZawijaj( string Tekst, WORD Szerokosc, POS Pozycja );
	STD_BUFOR& operator << ( string Tekst );
	STD_BUFOR& operator << ( int Tekst );
	STD_BUFOR& operator >> ( WORD Kolor );
	STD_BUFOR& operator >> ( POS Pozycja );
	void Wypelnij( WORD Kolor, char Znak = 0 );
	void Czysc();
	void Czysc( POS Pozycja, WORD Dlugosc );
	void Ustaw( BUFOR& Bufor );
	void UstawDomyslny();
	POS PobierzPozycjePisania() { return PozycjaPisania; }
	WORD PobierzKolorPisania() { return KolorPisania; }
	void UstawPozycjePisania( POS NowaPozycja );
	void UstawKolorPisania( WORD NowyKolor );
	void UstawPozycjeKursora( POS NowaPozycja );
	void SchowajKursor();
	void PokazKursor( DWORD Rozmiar = 1 );
	void Rysuj();
}; extern STD_BUFOR StdBuf;

/****************************************************************************************************/

char PoprawZPolskimiLiterami( char& Znak );

/****************************************************************************************************/