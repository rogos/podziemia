#pragma once

/****************************************************************************************************/

#include "stdafx.h"
struct POSTAC;

/****************************************************************************************************/

enum TYP_OBRAZEN { OBR_BRON, OBR_POCISKI, OBR_MAGIA };

union OCHRONA {

	short Wartosci[3];
	struct { short Bron, Pociski, Magia; };

	OCHRONA() : Bron(0), Pociski(0), Magia(0) {}
	OCHRONA( short bron, short pociski, short magia ) : Bron(bron), Pociski(pociski), Magia(magia) {}
	OCHRONA& operator += ( const OCHRONA& ochrona ) {
		Bron += ochrona.Bron;
		Pociski += ochrona.Pociski;
		Magia += ochrona.Magia;
		return *this;
	}
	OCHRONA& operator -= ( const OCHRONA& ochrona ) {
		Bron -= ochrona.Bron;
		Pociski -= ochrona.Pociski;
		Magia -= ochrona.Magia;
		return *this;
	}
};

struct OBRAZENIA {
	TYP_OBRAZEN Typ;
	WORD Wartosc;
	OBRAZENIA() : Typ(OBR_BRON), Wartosc(0) {}
	OBRAZENIA( TYP_OBRAZEN typ, WORD wartosc ) : Typ(typ), Wartosc(wartosc) {}
};

/****************************************************************************************************/

enum TYP_WLASCIWOSCI {
	WLA_OBRAZENIA_OD_MAGII,
	WLA_OBRAZENIA_OD_POCISKU,
	WLA_OBRAZENIA_OD_BRONI,
	WLA_DO_TURY,
	WLA_DO_ZYCIA,
	WLA_DO_ENERGII,
	WLA_DO_MAX_ZYCIA,
	WLA_DO_MAX_ENERGII,
	WLA_OD_GLODU,
	WLA_OD_PRAGNIENIA
};

struct WLASCIWOSC
{
	TYP_WLASCIWOSCI Typ;
	short Wartosc;
	WORD CzasRozpoczecia;
	WORD CzasTrwania;

	WLASCIWOSC() : Wartosc(0), CzasRozpoczecia(0), CzasTrwania(0), Typ(WLA_OBRAZENIA_OD_MAGII) {}

	WLASCIWOSC( TYP_WLASCIWOSCI typ, short wartosc, WORD czas_rozpoczecia, WORD czas_trwania )
		: Typ(typ), Wartosc(wartosc), CzasRozpoczecia(czas_rozpoczecia), CzasTrwania(czas_trwania) {}

	bool Uzyj( POSTAC& postac );

	bool Zaladuj( const RLGL::ELEMENT& data );

	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/

struct UROK
{
	array<WLASCIWOSC> Wlasciwosci;
	ANIMACJA Animacja;

	bool Uzyj( POSTAC& postac );

	bool Zaladuj( const RLGL::ELEMENT& data );
	
	void Zapisz( PLIK& Plik );
	void Wczytaj( PLIK& Plik );
};

/****************************************************************************************************/