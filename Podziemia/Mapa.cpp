#include "stdafx.h"

/****************************************************************************************************/

array< KAFELEK > Kafelki;
array< POS > Punkty;

/****************************************************************************************************/

short MAPA::przedmioty[SzerokoscMapy][WysokoscMapy];
short MAPA::postacie[SzerokoscMapy][WysokoscMapy];

void MAPA::StworzMapyPomocnicze()
{
	memset(&przedmioty, -1, SzerokoscMapy * WysokoscMapy * 2);
	memset(&postacie, -1, SzerokoscMapy * WysokoscMapy * 2);

	// wstawianie postaci na map� pomocnicz�
	for( int i = 0; i < Postacie.size(); i++ ) {
		POS p = Postacie[i]->Pozycja - StanGry->Postac.Pozycja + SRODEK_RYSOWANIA;
		if( p.X >= 0 && p.X < SzerokoscMapy && p.Y >= 0 && p.Y < WysokoscMapy ) postacie[p.X][p.Y] = i;
	}
	// wstawianie przedmiot�w na map� pomocnicz�
	for( int i = 0; i < Przedmioty.size(); i++ ) {
		POS p = Przedmioty[i].Pozycja - StanGry->Postac.Pozycja + SRODEK_RYSOWANIA;
		if( p.X >= 0 && p.X < SzerokoscMapy && p.Y >= 0 && p.Y < WysokoscMapy ) przedmioty[p.X][p.Y] = i;
	}
}

/****************************************************************************************************/

void MAPA::Rysuj( WORD Kolor, bool TakzeNiewidoczne, BYTE kolor_ciemnosci )
{
	// Rysuje odkryte tereny
	for( int y = 0; y < WysokoscMapy; y++ )
	{
		for( int x = 0; x < SzerokoscMapy; x++ )
		{
			POS p = StanGry->Postac.Pozycja - SRODEK_RYSOWANIA + POS(x, y);
			POS poz = POS(x, y);
			if( p.X >= 0 && p.X < Rozmiar.X && p.Y >= 0 && p.Y < Rozmiar.Y )
			{				
				if( TakzeNiewidoczne ) {
					if( Odkryte[p.X][p.Y] ) {
						StdBuf.WstawZnak( ::Kafelki[ Kafelki[p.X][p.Y] ].Znak, Kolor, poz + POS(1, 2), false );
						// Je�eli znajdzie sta�y przedmiot rysuje go
						if( przedmioty[poz.X][poz.Y] >= 0 ) {
							PRZEDMIOT* przedmiot = ::Przedmioty[Przedmioty[przedmioty[poz.X][poz.Y]].NR];
							if(przedmiot->TypPrzedmiotu == Relacyjny && dynamic_cast<RELACYJNY*>(przedmiot)->Sciana)
								StdBuf.WstawZnak( przedmiot->Znak, Kolor, poz + POS(1, 2), false );
						}
					}
				}
				else if( Odkryte[p.X][p.Y] && ::Kafelki[ Kafelki[p.X][p.Y] ].Typ == TK_SCIANA )
					StdBuf.WstawZnak( '.', xcSza, POS(x + 1, y + 2), false );

				if( StanGry->WidocznePola[p.X][p.Y] ) {
					// Je�eli znajdzie posta� rysuje j�
					if( postacie[poz.X][poz.Y] >= 0 ) {
						POSTAC_NIEZALEZNA* postac = Postacie[postacie[poz.X][poz.Y]];
						StdBuf.WstawZnak( postac->Znak, postac->Kolor, poz + POS(1, 2), false );
					}
					else {
						// Je�eli znajdzie przedmiot rysuje go
						if( przedmioty[poz.X][poz.Y] >= 0 ) {
							PRZEDMIOT* przedmiot = ::Przedmioty[Przedmioty[przedmioty[poz.X][poz.Y]].NR];
							StdBuf.WstawZnak( przedmiot->Znak, przedmiot->Kolor, poz + POS(1, 2), false );
						}
						else {
							// Rysuje widoczny kafelek
							StdBuf.WstawZnak( ::Kafelki[Kafelki[p.X][p.Y] ].Znak, ::Kafelki[ Kafelki[p.X][p.Y] ].Kolor, poz + POS(1, 2), false );

							// Koloruje nie o�wietlony kafelek
							if( !StanGry->Swiatlo[p.X][p.Y] ) StdBuf.PokolorujZnak( kolor_ciemnosci, poz + POS(1, 2) );
						}
					}
					//// Koloruje nie o�wietlony kafelek
					//	if( !StanGry->Swiatlo[p.X][p.Y] ) StdBuf.PokolorujZnak( kolor_ciemnosci, poz + POS(1, 2) );
				}
			}
		}
	}
}

/****************************************************************************************************/

void MAPA::Odkryj( POS Pozycja )
{
	PUNKTY linia;

	StanGry->UstawRozmiar( Rozmiar );
	for( unsigned i = 0; i < Rozmiar.X; i++ ) memset( StanGry->WidocznePola[i], 0, Rozmiar.Y );
	for( unsigned i = 0; i < Rozmiar.X; i++ ) memset( StanGry->Swiatlo[i], 0, Rozmiar.Y );

	for( WORD i = 0; i < Punkty.size(); i++ )
	{
		linia.Stworz( Pozycja, POS( Pozycja.X + Punkty[i].X, Pozycja.Y + Punkty[i].Y ) );

		for( BYTE punkt = 0; punkt < linia.IloscPunktow; punkt++ )
		{
			if( linia.Punkty[punkt].X < 0 || linia.Punkty[punkt].X >= Rozmiar.X ||
				linia.Punkty[punkt].Y < 0 || linia.Punkty[punkt].Y >= Rozmiar.Y ) break;
			
			Odkryte[linia.Punkty[punkt].X][linia.Punkty[punkt].Y] = true;
			StanGry->WidocznePola[linia.Punkty[punkt].X][linia.Punkty[punkt].Y] = true;
			if( ::Kafelki[ Kafelki[ linia.Punkty[punkt].X ][linia.Punkty[punkt].Y ] ].Typ == TK_SCIANA ) break;
		}
	}

	// rysuje �wiat�o
	float promien = 5.3;
	if( SwiatloSloneczne ) {
		BYTE h = StanGry->Godziny();
		if((h >= 22 && h < 24) || (h >= 0 && h < 5)) promien = 5.3;
		else if(h >= 7 && h < 20) promien = 18;
		else if(h >= 20 && h < 22) promien = 18 - ((StanGry->Sekunda + 30000) % 86400 - 72000) / 7200.0 * (18-5.3);
		else if(h >= 5 && h < 7) promien = 5.3 + ((StanGry->Sekunda + 30000) % 86400 - 18000) / 7200.0 * (18-5.3);
	}
	for( WORD kat = 1; kat <= 360; kat += 1 )
	{
		linia.Stworz( Pozycja, Pozycja + POS(cos(kat * PI / 180) * promien * 1.01, sin(kat * PI / 180) * promien * 1.01) );

		for( BYTE punkt = 0; punkt < linia.IloscPunktow; punkt++ )
		{
			if( linia.Punkty[punkt].X < 0 || linia.Punkty[punkt].X >= Rozmiar.X ||
				linia.Punkty[punkt].Y < 0 || linia.Punkty[punkt].Y >= Rozmiar.Y ) break;
			
			StanGry->Swiatlo[linia.Punkty[punkt].X][linia.Punkty[punkt].Y] = true;
			if( ::Kafelki[ Kafelki[ linia.Punkty[punkt].X ][linia.Punkty[punkt].Y ] ].Typ == TK_SCIANA ) break;
		}
	}
}

/****************************************************************************************************/

void MAPA::UstawRozmiar( POS rozmiar )
{
	if( Kafelki ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Kafelki[i];
	if( Odkryte ) for( unsigned i = 0; i < Rozmiar.X; i++ ) delete[] Odkryte[i];

	Rozmiar = rozmiar;

	Kafelki = new BYTE*[Rozmiar.X];
	Odkryte = new BYTE*[Rozmiar.X];
	for(unsigned i = 0; i < Rozmiar.X; i++) {
		Kafelki[i] = new BYTE[Rozmiar.Y];
		Odkryte[i] = new BYTE[Rozmiar.Y];
		memset( Kafelki[i], 0, Rozmiar.Y );
		memset( Odkryte[i], 0, Rozmiar.Y );
	}
}

/****************************************************************************************************/

bool MAPA::WstawPrzejscie( POS pozycja, WORD mapa, POS nowa_pozycja, bool automatyczne )
{
	for( unsigned i = 0; i < Przejscia.size(); i++ )
		if( Przejscia[i].Pozycja == pozycja ) return false;

	PRZEJSCIE p = { pozycja, mapa, nowa_pozycja, automatyczne };
	Przejscia.push_back( p );
	return true;
}

/****************************************************************************************************/

bool MAPA::UsunPrzejscie( POS pozycja )
{
	for( unsigned i = 0; i < Przejscia.size(); i++ )
		if( Przejscia[i].Pozycja == pozycja )
		{
			Przejscia.erase( Przejscia.begin() + i );
			return true;
		}
	return false;
}

/****************************************************************************************************/

bool MAPA::UsunPostac( WORD postac )
{
	if( postac >= Postacie.size() ) return false;

	Postacie.erase( Postacie.begin() + postac );

	return true;
}

/****************************************************************************************************/

bool MAPA::UsunPrzedmiot( WORD przedmiot, WORD ilosc )
{
	if( przedmiot >= Przedmioty.size() ) return false;

	if( ilosc == Przedmioty[przedmiot].Ilosc ) Przedmioty.erase( Przedmioty.begin() + przedmiot );
	else Przedmioty[przedmiot].Ilosc -= ilosc;

	return true;
}

/****************************************************************************************************/

short MAPA::SzukajPrzejscia( POS pozycja )
{
	for( unsigned i = 0; i < Przejscia.size(); i++ )
		if( Przejscia[i].Pozycja == pozycja ) return i;

	return -1;
}

/****************************************************************************************************/

bool MAPA::WstawKafelek( BYTE kafelek, POS pozycja )
{
	if( pozycja.X < 0 || pozycja.X >= Rozmiar.X ||
		pozycja.Y < 0 || pozycja.Y >= Rozmiar.Y ) return false;

	Kafelki[pozycja.X][pozycja.Y] = kafelek;

	return true;
}

/*..................................................................................................*/

bool MAPA::DodajPrzedmiot( WORD przedmiot, WORD ilosc, POS pozycja )
{
	if( pozycja.X < 0 || pozycja.X >= Rozmiar.X ||
		pozycja.Y < 0 || pozycja.Y >= Rozmiar.Y ) return false;

	short przedmiot_na_danej_pozycji = SzukajPrzedmiotuOPodanymNumerze(przedmiot, pozycja);

	if( przedmiot_na_danej_pozycji >= 0 ) Przedmioty[przedmiot_na_danej_pozycji].Ilosc += ilosc;
	else {
		Przedmioty.resize(Przedmioty.size() + 1);
		Przedmioty.back().NR = przedmiot;
		Przedmioty.back().Ilosc = ilosc;
		Przedmioty.back().Pozycja = pozycja;
	}

	return true;
}

/*..................................................................................................*/

bool MAPA::DodajPostac( WORD postac, POS pozycja )
{
	if( pozycja.X < 0 || pozycja.X >= Rozmiar.X ||
		pozycja.Y < 0 || pozycja.Y >= Rozmiar.Y ) return false;

	if( SzukajPostaci(pozycja) >= 0 ) return false;
	
	WROG_NA_MAPIE* p = new WROG_NA_MAPIE;
	p->ID = Wrogowie[postac].ID;
	p->Kolor = Wrogowie[postac].Kolor;
	p->Znak = Wrogowie[postac].Znak;
	p->Nazwa = Wrogowie[postac].Nazwa;
	p->Ochrona = Wrogowie[postac].Ochrona;
	p->Czar = Wrogowie[postac].Czar;
	p->WrogoNastawiony = Wrogowie[postac].WrogoNastawiony;
	p->Dzikosc = Wrogowie[postac].Dzikosc;
	p->NR = postac;
	p->Pozycja = p->StalaPozycja = p->PozycjaAtaku = pozycja;
	p->ZasiegRuchu = Wrogowie[postac].ZasiegRuchu;
	p->ZasiegAtaku = Wrogowie[postac].ZasiegAtaku;
	p->Tura = 0;
	p->Zycie = Wrogowie[postac].MaxZycie;
	Postacie.push_back( p );

	return true;
}

/*..................................................................................................*/

bool MAPA::DodajOsobe( WORD osoba, POS pozycja )
{
	if( pozycja.X < 0 || pozycja.X >= Rozmiar.X ||
		pozycja.Y < 0 || pozycja.Y >= Rozmiar.Y ) return false;

	if( SzukajPostaci(pozycja) >= 0 ) return false;
	
	OSOBA* o = new OSOBA;
	*o = Osoby[osoba];
	o->Pozycja = o->StalaPozycja = o->PozycjaAtaku = pozycja;
	Postacie.push_back( o );

	return true;
}

/*..................................................................................................*/

POS MAPA::LosujPozycje( POS pozycja_obszaru, POS rozmiar_obszaru )
{
	for(;;)
	{
		POS pozycja = POS( Losuj( pozycja_obszaru.X, pozycja_obszaru.X + rozmiar_obszaru.X - 1 ),
		                   Losuj( pozycja_obszaru.Y, pozycja_obszaru.Y + rozmiar_obszaru.Y - 1 ) );
		if( PozycjaWolna( pozycja ) ) return pozycja;
	}
}

/*..................................................................................................*/

POS MAPA::PozycjaObok( POS pozycja, POS pozycja_obszaru, POS rozmiar_obszaru )
{
	if( rozmiar_obszaru == POS(0, 0) ) rozmiar_obszaru = Rozmiar(-1, -1);
	array< POS > punkty;
	punkty.add( pozycja );

	for( unsigned i = 1; i < 100; i++ )
	{
		if( punkty.size() == 0 ) return POS_NOT_FOUND;

		array< POS > _punkty = punkty;
		punkty.clear();
		
		bool end = false;
		
		for( unsigned j = 0; j < _punkty.size(); j++ )
		{	
			for( BYTE k = 0; k < 8; k++ )
			{
				pozycja = _punkty[j] + WEKTORY[k];

				// Je�eli punkt wykracza poza obszar pomija go
				if( pozycja.X < pozycja_obszaru.X || pozycja.X >= pozycja_obszaru.X + rozmiar_obszaru.X ||
					pozycja.Y < pozycja_obszaru.Y || pozycja.Y >= pozycja_obszaru.Y + rozmiar_obszaru.Y ) continue;
				
				if( !PozycjaWolna(pozycja) ) {
					if( ::Kafelki[ Kafelki[pozycja.X][pozycja.Y] ].Typ == TK_GRUNT ) punkty.add(pozycja);
				}
				else return pozycja;
			}
		}
	}
	return POS_NOT_FOUND;
}

/****************************************************************************************************/

bool MAPA::PozycjaWolna( POS pozycja )
{
	if( pozycja.X >= 0 && pozycja.Y >= 0 && pozycja.X < Rozmiar.X && pozycja.Y < Rozmiar.Y &&
		::Kafelki[ Kafelki[pozycja.X][pozycja.Y] ].Typ == TK_GRUNT &&
		SzukajPostaci(pozycja) < 0 && SzukajPrzedmiotu(pozycja) < 0 && SzukajPrzejscia(pozycja) < 0 )
		return true;
	else return false;
}

/*..................................................................................................*/

bool MAPA::PozycjaWolnaDlaPostaci( POS pozycja )
{
	int p = SzukajPrzedmiotu( pozycja );
	if(	pozycja.X >= 0 && pozycja.Y >= 0 && pozycja.X < Rozmiar.X && pozycja.Y < Rozmiar.Y &&
		::Kafelki[ Kafelki[pozycja.X][pozycja.Y] ].Typ == TK_GRUNT && SzukajPostaci( pozycja ) < 0 &&
		( p < 0 || ::Przedmioty[ Przedmioty[p].NR ]->PodajTyp() != Relacyjny ||
		  dynamic_cast<RELACYJNY*>(::Przedmioty[ Przedmioty[p].NR ])->Sciana == false ) ) return true;
	else return false;
}

/****************************************************************************************************/

POS MAPA::PozycjaDalejOd( POS pozycja, POS pozycja_biezaca )
{
	POS p = pozycja_biezaca;
	POS kierunek = pozycja_biezaca - pozycja;

	if( kierunek.X > 0 && kierunek.Y > 0 && PozycjaWolnaDlaPostaci( p(+1, +1) ) ) return p(+1, +1);
	if( kierunek.X < 0 && kierunek.Y > 0 && PozycjaWolnaDlaPostaci( p(-1, +1) ) ) return p(-1, +1);
	if( kierunek.X < 0 && kierunek.Y < 0 && PozycjaWolnaDlaPostaci( p(-1, -1) ) ) return p(-1, -1);
	if( kierunek.X > 0 && kierunek.Y < 0 && PozycjaWolnaDlaPostaci( p(+1, -1) ) ) return p(+1, -1);

	if( kierunek.Y > 0 && PozycjaWolnaDlaPostaci( p(+0, +1) ) ) return p(+0, +1);
	if( kierunek.X < 0 && PozycjaWolnaDlaPostaci( p(-1, +0) ) ) return p(-1, +0);
	if( kierunek.Y < 0 && PozycjaWolnaDlaPostaci( p(+0, -1) ) ) return p(+0, -1);
	if( kierunek.X > 0 && PozycjaWolnaDlaPostaci( p(+1, +0) ) ) return p(+1, +0);

	if( ( kierunek.X > 0 || kierunek.Y > 0 ) && PozycjaWolnaDlaPostaci( p(+1, +1) ) ) return p(+1, +1);
	if( ( kierunek.X < 0 || kierunek.Y > 0 ) && PozycjaWolnaDlaPostaci( p(-1, +1) ) ) return p(-1, +1);
	if( ( kierunek.X < 0 || kierunek.Y < 0 ) && PozycjaWolnaDlaPostaci( p(-1, -1) ) ) return p(-1, -1);
	if( ( kierunek.X > 0 || kierunek.Y < 0 ) && PozycjaWolnaDlaPostaci( p(+1, -1) ) ) return p(+1, -1);

	return POS_NOT_FOUND;
}

/****************************************************************************************************/

short MAPA::SzukajPostaci( POS Pozycja )
{
	for( int i = 0; i < Postacie.size(); i++ )
		if( Postacie[ i ]->Pozycja == Pozycja ) return i; 

	return -1;
}

/****************************************************************************************************/

short MAPA::SzukajPrzedmiotu(POS Pozycja, WORD KtoryZKolei)
{
	for( int i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].Pozycja == Pozycja )
		{
			KtoryZKolei--;
			if( KtoryZKolei == 0 ) return i;
		}
	return -1;
}

/****************************************************************************************************/

short MAPA::SzukajPrzedmiotuOPodanymNumerze( WORD NR, POS Pozycja )
{
	for( int i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].Pozycja == Pozycja && Przedmioty[ i ].NR == NR ) return i;

	return -1;
}

/****************************************************************************************************/

POS MAPA::SzukajKafelka( BYTE index_kafelka, unsigned kolejnosc )
{
	for( int y = 0; y < Rozmiar.Y; y++ )
		for( int x = 0; x < Rozmiar.X; x++ )
			if( Kafelki[ x ][ y ] == index_kafelka )
			{
				if( kolejnosc == 0 ) return POS( x, y );
				else kolejnosc--;
			}

	return POS_NOT_FOUND;
}

/****************************************************************************************************/

POS MAPA::PozycjaPrzedmiotu( WORD index_przedmiotu )
{
	for( int i = 0; i < Przedmioty.size(); i++ )
		if( Przedmioty[ i ].NR == index_przedmiotu ) return Przedmioty[ i ].Pozycja;
	return POS_NOT_FOUND;
}

/****************************************************************************************************/

void MAPA::Zapisz( PLIK& Plik )
{
	Plik << ID
		 << Nazwa
		 << SwiatloSloneczne;
	Plik.Zapisz( &Rozmiar, sizeof(POS) );
	for(unsigned i = 0; i < Rozmiar.X; i++) {
		Plik.Zapisz( Kafelki[i], Rozmiar.Y );
		Plik.Zapisz( Odkryte[i], Rozmiar.Y );
	}

	Plik << (WORD)( Przedmioty.size() );
	for( WORD i = 0; i < Przedmioty.size(); i++ ) Plik.Zapisz( &Przedmioty[i], sizeof(PRZEDMIOT_NA_MAPIE) );

	Plik << (WORD)( Postacie.size() );
	for( WORD i = 0; i < Postacie.size(); i++ ) {
		Plik << (BYTE)(Postacie[ i ]->Typ);
		Postacie[i]->Zapisz( Plik );
	}

	Plik << (WORD)( Przejscia.size() );
	for( WORD i = 0; i < Przejscia.size(); i++ ) Plik.Zapisz( &Przejscia[i], sizeof(PRZEJSCIE) );
}

/****************************************************************************************************/

void MAPA::Wczytaj( PLIK& Plik )
{
	Plik >> ID
		 >> Nazwa
		 >> SwiatloSloneczne;
	Plik.Wczytaj( &Rozmiar, sizeof(POS) );
	UstawRozmiar(Rozmiar);
	for(unsigned i = 0; i < Rozmiar.X; i++) {
		Plik.Wczytaj( Kafelki[i], Rozmiar.Y );
		Plik.Wczytaj( Odkryte[i], Rozmiar.Y );
	}

	WORD ilosc;
	Plik >> ilosc;
	Przedmioty.resize(ilosc);
	for( WORD i = 0; i < ilosc; i++ ) Plik.Wczytaj( &Przedmioty[i], sizeof(PRZEDMIOT_NA_MAPIE) );

	Plik >> ilosc; 
	Postacie.resize(ilosc);
	BYTE typ;
	for( WORD i = 0; i < ilosc; i++ ) {
		Plik >> typ;
		switch( typ ) {
			case TPO_OSOBA: Postacie[i] = new OSOBA; break;
			case TPO_WROG: Postacie[i] = new WROG_NA_MAPIE; break;
		}
		Postacie[ i ]->Wczytaj( Plik );
	}

	Plik >> ilosc;
	Przejscia.resize(ilosc);
	for( WORD i = 0; i < ilosc; i++ ) Plik.Wczytaj( &Przejscia[i], sizeof(PRZEJSCIE) );
}

/****************************************************************************************************/

void RysujListeWidocznychObiektow( POS pozycja, BYTE max_obiektow )
{
	vector< POSTAC_NIEZALEZNA* > postacie;
	vector< PRZEDMIOT_NA_MAPIE* > przedmioty;

	for( int y = 0; y < WysokoscMapy; y++ )
		for( int x = 0; x < SzerokoscMapy; x++ )
		{
			register POS p = StanGry->Postac.Pozycja - SRODEK_RYSOWANIA + POS(x, y);
			if( p.X >= 0 && p.X < AktualnaMapa->Rozmiar.X && p.Y >= 0 && p.Y < AktualnaMapa->Rozmiar.Y )
			{				
				if( StanGry->WidocznePola[p.X][p.Y] )
				{
					// Je�eli znajdzie posta� dodaje j� do tablicy
					if( AktualnaMapa->postacie[x][y] >= 0 )
						postacie.push_back( AktualnaMapa->Postacie[ AktualnaMapa->postacie[x][y] ] );
					else {
						// Je�eli znajdzie przedmiot dodaje go do tablicy
						if( AktualnaMapa->przedmioty[x][y] >= 0 )
							przedmioty.push_back( &AktualnaMapa->Przedmioty[ AktualnaMapa->przedmioty[x][y] ] );
					}
				}
			}
		}

	BYTE obiektow = 0;
	for( BYTE i = 0; i < postacie.size() && obiektow < max_obiektow; i++ ) {
		StdBuf >> pozycja( 0, obiektow ) >> postacie[i]->Kolor << string() + postacie[i]->Znak;
		postacie[i]->RysujPasekZycia( pozycja( 2, obiektow ) );
		StdBuf >> pozycja( 13, obiektow ) >> xBia << postacie[i]->Nazwa;
		obiektow++;
	}
	for( BYTE i = 0; i < przedmioty.size() && obiektow < max_obiektow; i++ ) {
		StdBuf >> pozycja( 0, obiektow ) >> ::Przedmioty[ przedmioty[i]->NR ]->Kolor << string() + ::Przedmioty[ przedmioty[i]->NR ]->Znak
			<< " " >> xBia << ::Przedmioty[ przedmioty[i]->NR ]->Nazwa;
		obiektow++;
	}
}

/****************************************************************************************************/

void DodajKafelek( string ID, string Nazwa, char Znak, WORD Kolor, TYP_KAFELKA Typ, bool Zakrwawialny )
{
	Kafelki.add( KAFELEK( ID, Nazwa, Znak, Kolor, Typ, Zakrwawialny ) );
}

/****************************************************************************************************/

void StworzPunkty( array< POS >& Punkty )
{
	UCHAR promien = 18;
	for( WORD kat = 3; kat <= 360; kat += 3 )
		Punkty.add( POS( cos( kat * PI / 180 ) * promien * 1.01, sin( kat * PI / 180 ) * promien * 1.01 ) );
}

/****************************************************************************************************/

short IndexKafelka( string ID )
{
	for( BYTE i = 0; i < Kafelki.size(); i++ )
		if( Kafelki[i].ID == ID ) return i;
	return -1;
}

/****************************************************************************************************/