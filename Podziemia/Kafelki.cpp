#include "stdafx.h"

void AlokacjaKafelkow()
{
	const RLGL::ELEMENT& relacja = DaneGry( "TILES" );
	for( WORD i = 0; i < relacja.Size(); i++ )
	{
		const RLGL::ELEMENT& kafelek = relacja( i );
		DodajKafelek( kafelek.GetName(), kafelek[ 0 ], kafelek[ 1 ], kafelek[ 2 ], TYP_KAFELKA(AS_INT(kafelek[ 3 ])), kafelek[ 4 ] );
	}
}