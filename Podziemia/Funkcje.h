#pragma once

#include "stdafx.h"

/****************************************************************************************************/

const double PI = 3.1415926535897932384;
const string NIE_WPROWADZONO = "_~FALSE_";

/****************************************************************************************************/

struct KOMUNIKAT
{
	string NowyTekst;
	array< string > Teksty;
	array< WORD > Kolory;
	UCHAR IloscTekstow;
	bool Rysowane;
	KOMUNIKAT& operator >> (WORD Kolor);
	KOMUNIKAT& operator << (string Tekst);
	KOMUNIKAT& operator << (int Tekst);
	bool operator()();
	void CzyscBufor();
	KOMUNIKAT() : IloscTekstow( 0 ), Rysowane( false ) {}
};

/****************************************************************************************************/

struct PUNKTY
{
	POS* Punkty;
	UCHAR IloscPunktow;
	PUNKTY()
	{
		Punkty = 0;
		IloscPunktow = 0;
	}
	~PUNKTY()
	{
		if( Punkty ) delete[] Punkty;
	}
	PUNKTY& Stworz( POS Od, POS Do );
};

/****************************************************************************************************/

class MAPA;

class PATH
{
private:
	POS*     Path;
	unsigned Size;
	int      Map[100][100];
	POS      FindRegionPos;

public:
	PATH( MAPA& map, POS find_region_pos ) : Path( 0 ) { SetMap( map, find_region_pos ); }
	~PATH() { if( Path ) delete[] Path; }

	void     SetMap( MAPA& map, POS find_region_pos );
	bool     Create( POS Begin, POS End, bool Diagonal, unsigned MaxLength = 49 );
	POS      GetStep( unsigned step ) const { return Path[ step ]; }
	unsigned Length() const { return Size; }
};

/****************************************************************************************************/

string Pobierz( string DozwoloneZnaki, WORD MaxDlugosc, POS Pozycja, WORD Kolor );
int PobierzLiczbe( WORD MaxDlugosc, POS Pozycja, WORD Kolor );
wchar_t PobierzKlawisz();
void UstawPozycjeKursora( POS Pozycja );
int Losuj( int Min, int Max );
void Kurtyna();
void CzekajNaSpacje();
unsigned Doswiadczenie( BYTE poziom, bool suma = true );
void DoDoswiadczenia( WORD Dodaj );
UCHAR IloscZabieranychPunktow( UCHAR Wartosc );
bool PrzydzielPunkty( UCHAR& Wartosc );
void RysujOkienko();
void RysujPanelPatrzenia( POS PozycjaCelu ); // np. Widzisz: Zombi
void RysujPunktWyboruPozycji( PUNKTY& Linia, UCHAR Zasieg );
void RysujRamke( POS Pozycja, POS Rozmiar, WORD Kolor, bool Podwojna );
bool PokazEkw();
void PokazStat();
bool PokazMape();
bool PokazPomoc();
string ZWielkiejLitery( string Tekst );
char PoprawZPolskimiLiterami( char& Znak );
string* WczytajPlikTxt( unsigned& IloscLiniiTekstu, wstring AdresPliku );

/****************************************************************************************************/

void _error( string komunikat, string plik, unsigned linia );
void _warning( string komunikat, string plik, unsigned linia );

#define error( komunikat ) _error( komunikat, __FILE__, __LINE__ )
#define warning( komunikat ) _warning( komunikat, __FILE__, __LINE__ )

/****************************************************************************************************/

const POS WEKTORY[ 8 ] = {
	POS( +0, -1 ),
	POS( +1, +0 ),
	POS( +0, +1 ),
	POS( -1, +0 ),
	POS( +1, +1 ),
	POS( -1, -1 ),
	POS( -1, +1 ),
	POS( +1, -1 )
};

const POS PRZECIWNE_WEKTORY[ 8 ] = {
	POS( +0, +1 ),
	POS( -1, +0 ),
	POS( +0, -1 ),
	POS( +1, +0 ),
	POS( -1, -1 ),
	POS( +1, +1 ),
	POS( +1, -1 ),
	POS( -1, +1 )
};

/****************************************************************************************************/

extern KOMUNIKAT Komunikat;

/****************************************************************************************************/