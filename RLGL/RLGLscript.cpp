#include "stdafx.h"
#include "RLGLscript.h"

//****************************************************************************************************//

RLGL_BEGIN

//****************************************************************************************************//

const int NOT_FOUND = string::npos;

//****************************************************************************************************//

void script_error( const char* content )
{
	char con[255] = "ERROR: ";
	strcat( con, content );
	throw( con );
}

//****************************************************************************************************//

DATA::DATA( string data ) : m_Data( data ) {}
DATA::DATA( char* data ) : m_Data( ( string ) data ) {}
DATA::DATA( char data ) { string str; str.push_back( data ); m_Data = str; }
DATA::DATA( int data ) : m_Data( itostr( data ) ) {}

//****************************************************************************************************//

DATA::operator int() {
	char* s;
	return strtol( m_Data.c_str(), &s, 10 );
}

DATA::operator unsigned() {
	char* s;
	return strtoul( m_Data.c_str(), &s, 10 );
}

DATA::operator short() {
	return int( *this );
}

DATA::operator WORD() {
	return unsigned( *this );
}

DATA::operator BYTE() {
	return unsigned( *this );
}

DATA::operator double() {
	char* s;
	return strtod( m_Data.c_str(), &s );
}

DATA::operator float() {
	return double( *this );
}

DATA::operator string() {
	return m_Data;
}

DATA::operator char() {
	return m_Data[ 0 ];
}

DATA::operator bool() {
	return unsigned( *this );
}

//****************************************************************************************************//

bool DATA::operator !()
{
	return !this->operator bool();
}

//****************************************************************************************************//

ELEMENT_TYPE ELEMENT::Type() const
{
	if( dynamic_cast<const VARIABLE* const>(this) ) return EL_VARIABLE;
	if( dynamic_cast<const COLLECTION* const>(this) ) return EL_COLLECTION;
	if( dynamic_cast<const STRUCTURE* const>(this) ) return EL_STRUCTURE;
	return EL_BASE;
}

//****************************************************************************************************//

const ELEMENT& ELEMENT::operator () ( string name, unsigned index ) const
{
	if( Type() != EL_STRUCTURE ) script_error( ( AS_STR( "(ELEMENT::operator()(name, index)) Element '" )
		+ Name + "." + name + "(" + itostr(index) + ")' not exist becouse '" + Name + "' is not structure." ).c_str() );
	const P_ELEMENT p_element( this );
	int id = p_element.structure->GetID( name, index );
	if( id < 0 ) script_error( ( AS_STR( "(ELEMENT::operator()(name, index)) Element '" ) + Name + "." + name + "(" + itostr(index) + ")' not exist." ).c_str() );
	return *p_element.structure->Elements[ id ];
}

const ELEMENT& ELEMENT::operator () ( unsigned index ) const
{
	if( Type() != EL_STRUCTURE ) script_error( ( AS_STR( "(ELEMENT::operator()(index)) Element '" )
		+ Name + "(" + itostr(index) + ")' not exist becouse '" + Name + "' is not structure." ).c_str() );
	const P_ELEMENT p_element( this );
	if( Type() == EL_STRUCTURE && p_element.structure->Elements.size() > index ) return *p_element.structure->Elements[ index ];
	else script_error( ( AS_STR( "(ELEMENT::operator()(index)) Element '" ) + Name + "(" + itostr(index) + ")' not exist." ).c_str() );
}

//****************************************************************************************************//

DATA ELEMENT::GetValue( unsigned index ) const
{
	const P_ELEMENT p_element( this );
	if( Type() == EL_COLLECTION && p_element.collection->Values.size() > index ) return p_element.collection->Values[ index ];
	else if( Type() == EL_VARIABLE && index == 0 ) return p_element.variable->Value;
	else script_error( ( AS_STR( "(ELEMENT::GetValue(index)) Element '" ) + Name + "[" + itostr(index) + "]' not exist." ).c_str() );
}

DATA ELEMENT::operator [] ( unsigned index ) const
{
	return GetValue( index );
}

DATA ELEMENT::operator * () const {
	return GetValue();
}

//****************************************************************************************************//

string ELEMENT::GetName() const
{
	return Name;
}

//****************************************************************************************************//

bool ELEMENT::Exist( string name, unsigned index ) const
{
	const P_ELEMENT p_element( this );
	if( Type() == EL_STRUCTURE ) return ( p_element.structure->GetID( name, index ) != NOT_FOUND );
	else return false;
}

//****************************************************************************************************//

unsigned ELEMENT::Size() const
{
	const P_ELEMENT p_element( this );
	if( Type() == EL_STRUCTURE ) return p_element.structure->Elements.size();
	if( Type() == EL_COLLECTION ) return p_element.collection->Values.size();
	else if( Type() == EL_VARIABLE ) return 1;
	else script_error( ( AS_STR( "(ELEMENT::Size()) Element '" ) + Name + "' not exist." ).c_str() );
}

//****************************************************************************************************//

STRUCTURE::STRUCTURE( const STRUCTURE& structure )
{
	this->operator = ( structure );
}

//****************************************************************************************************//

STRUCTURE& STRUCTURE::operator = ( const STRUCTURE& structure )
{
	Name = structure.Name;
	for( unsigned i = 0; i < Elements.size(); i++ ) delete Elements[i];
	for( unsigned i = 0; i < structure.Elements.size(); i++ )
	{
		P_ELEMENT el;
		el.element = structure.Elements[ i ];
		switch( structure.Elements[ i ]->Type() )
		{
			case EL_VARIABLE: Elements.add( new VARIABLE( *el.variable ) ); break;
			case EL_COLLECTION: Elements.add( new COLLECTION( *el.collection ) ); break;
			case EL_STRUCTURE: Elements.add( new STRUCTURE( *el.structure ) ); break;
		}	
	}
	return *this;
}

//****************************************************************************************************//

int STRUCTURE::GetID( string name, unsigned index ) const
{
	for( unsigned i = 0; i < Elements.size(); i++ )
		if( Elements[i]->Name == name )
		{
			if(index == 0) return i;
			else index--;
		}
	return NOT_FOUND;
}

//****************************************************************************************************//

int STRUCTURE::GetLastID( string name ) const
{
	int index = NOT_FOUND;
	for( unsigned i = 0; i < Elements.size(); i++ )
		if( Elements[i]->Name == name ) index = i;
	return index;
}
	
//****************************************************************************************************//

bool STRUCTURE::CheckSpace( string& data, int& index )
{
	if( data[index] == 9 ) index++;
	else if( data.substr( index, 2 ) == LINE_END ) index += 2;
	else return false;
	return true;
}

//****************************************************************************************************//

bool STRUCTURE::CheckComment( string& data, int& index )
{
	if( data.substr( index, 2 ) == "##" )
	{
		index = data.find( LINE_END, index + 2 );
		if( index == NOT_FOUND ) index = data.size(); 
		else index += 2;
		return true;
	}
	else if( data.substr( index, 2 ) == "#-" )
	{
		index = data.find( "-#", index + 2 );
		if( index == NOT_FOUND ) index = data.size(); 
		else index += 2;
		return true;
	}
	else return false;
}

//****************************************************************************************************//

bool STRUCTURE::CheckOperators( string& data, int& index )
{
	// Komentarz
	if( CheckComment( data, index ) ) return true;

	// Definicja
	else if( data.substr( index, 4 ) == "#DEF" )
	{
		int def_begin = index; // to poprawi� tak �eby 'index' by� pocz�tkiem  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<-------------------------------------

		// Szuka nazwy definicji
			// szuka pocz�tku
		int begin_index = data.find_first_not_of( ' ', index + 4 );
		if( ALLOWED_CHARS.find_first_of( data[begin_index] ) == NOT_FOUND ) script_error( "(STRUCTURE::CheckOperators) Invalid definition name." );
			// szuka ko�ca
		index = data.find_first_not_of( ALLOWED_CHARS, begin_index + 1 );
			// pobiera nazw�
		string old_text = data.substr( begin_index, index - begin_index );

		// Szuka nowego tekstu kt�ry zostanie wstawiony zamiast starego (nazwy definicji)
			// szuka pocz�tku
		begin_index = data.find_first_not_of( ' ', index );
			// szuka ko�ca (ko�cem jest przej�cie do nast�pnej linii)
		index = data.find( LINE_END, begin_index );
			// pobiera tekst
		string new_text = data.substr( begin_index, index - begin_index );

		// Przesuwa index za koniec linii
		index += 2;

		// Zamienia wszystkie stare teksty na nowe
		while( begin_index = data.find( old_text, index ), begin_index != NOT_FOUND ) data.replace( begin_index, old_text.length(), new_text );

		// Usuwa definicj�
		data.erase( def_begin, index - def_begin );
		index = def_begin;
	}

	// Zawieranie innego pliku
	else if( data.substr( index, 8 ) == "#INCLUDE" )
	{
		int index2 = index;

		// Pobiera adres pliku
		VARIABLE var;
		index2 = data.find_first_not_of( ' ', index2 + 8 );
		var.Load( data, index2 );

		// Wczytuje plik i dodaje do skryptu
		FILE file( var.GetValue() );
		if( !file.Open( OD_READ, OC_OPEN_EXISTING ) ) script_error( "(STRUCTURE::CheckOperators) Can't open including file." );
		data.replace( index, index2 - index, file.ReadAllToString() + LINE_END );
	}

	else return false;

	return true;
}

//****************************************************************************************************//

LOAD_MODE STRUCTURE::GetMode( string& data, int& index )
{
	if( data.substr( index, 4 ) == "ADD " ) {
		index = data.find_first_not_of( ' ', index + 4 );
		return LM_ADD;
	}

	if( data.substr( index, 7 ) == "UPDATE " ) {
		index = data.find_first_not_of( ' ', index + 7 );
		return LM_UPDATE;
	}

	if( data.substr( index, 8 ) == "REPLACE " ) {
		index = data.find_first_not_of( ' ', index + 8 );
		return LM_REPLACE;
	}

	if( data.substr( index, 8 ) == "INHERIT " ) {
		index = data.find_first_not_of( ' ', index + 8 );
		return LM_INHERIT;
	}
	
	return LM_ADD;
}

//****************************************************************************************************//

void STRUCTURE::Load( string& data, int& index )
{
	LOAD_MODE load_mode;
	string name, name2;
	ELEMENT* element;

	for(;;)
	{
		if( index >= data.size() ) return;
		
		// Wyszukuje nazw� zmiennej/struktury/zbioru
		index = data.find_first_not_of( ' ', index );

		// Je�eli jest koniec kodu lub struktury ko�czy funkcj�
		if( index == NOT_FOUND || data[index] == '}' ) { index++; return; }

		// Sprawdza czy nie wyst�puje tu operator, tabulator, b�d� przej�cie do nast�pnej linii
		if( data[index] == '#' && CheckOperators( data, index ) || CheckSpace( data, index ) ) continue;

		if( data[index] == '<' ) load_mode = LM_UPDATE;
		else {
			// Pobiera tryb wczytywania danych
			load_mode = GetMode( data, index );

			if( load_mode == LM_INHERIT ) {
				// Je�eli jest nie dozwolona nazwa wyrzuca b��d
				if( ALLOWED_CHARS.find_first_of( data[index] ) == NOT_FOUND ) script_error( "(STRUCTURE::Load) Invalid inheriting variable name." );

				// Pobiera nazw� bazowej zmiennej/struktury
				int begin_index = index;
				index = data.find_first_not_of( ALLOWED_CHARS, index );
				name2 = data.substr( begin_index, index - begin_index );
				index = data.find_first_not_of( ' ', index );
			}

			// Je�eli jest nie dozwolona nazwa wyrzuca b��d
			if( ALLOWED_CHARS.find_first_of( data[index] ) == NOT_FOUND ) script_error( ( AS_STR( "(STRUCTURE::Load) Invalid variable name in '" ) + Name + "' structure." ).c_str() );
			
			// Pobiera nazw� zmiennej/struktury
			int begin_index = index;
			index = data.find_first_not_of( ALLOWED_CHARS, index );
			name = data.substr( begin_index, index - begin_index );
			
			// Je�eli pierwszy znak nazwy zmiennej to cyfra wyrzuca b��d
			if( name.find_first_of( NUMBERS ) == 0 ) script_error( ( AS_STR( "(STRUCTURE::Load) First character of element name can't be a number in (" ) + Name + ") structure." ).c_str() );

			for(;;) {
				index = data.find_first_not_of( ' ', index );

				// Pomija komentarz, tabulator lub koniec linii
				if( CheckComment( data, index ) || CheckSpace( data, index ) ) continue;

				break;
			}
		}
		
		// Post�puje wed�ug danego trybu (id-index miejsca w tablicy element�w, id2-index bazowej zmiennej/struktury)
		int id = Elements.size(), id2;

		switch( load_mode )
		{
			case LM_ADD: {
				id = Elements.size();
				break;
			}
			case LM_UPDATE: {
				id = GetLastID( name );
				if( id == NOT_FOUND ) id = Elements.size();
				break;
			}
			case LM_REPLACE: {
				id = GetLastID( name );
				if( id == NOT_FOUND ) id = Elements.size();
				else Elements.erase( id );
				break;
			}
			case LM_INHERIT: {
				id = Elements.size();
				id2 = GetID( name2 );
				break;
			}
		}
		
		// Je�eli jest nawias klamrowy to dodaje struktur�
		if( data[index] == '{' )
		{
			switch( load_mode ) {
			case LM_INHERIT: if( id2 != NOT_FOUND ) { element = new STRUCTURE( *P_ELEMENT( Elements[ id2 ] ).structure ); break; }
				case LM_ADD: case LM_REPLACE: element = new STRUCTURE; break;
			}
			index++;
		}
		// Je�eli jest nawias okr�g�y to dodaje zbi�r warto�ci
		else if( data[index] == '(' )
		{
			switch( load_mode ) {
				case LM_INHERIT: if( id2 != NOT_FOUND ) { element = new COLLECTION( *P_ELEMENT( Elements[ id2 ] ).collection ); break; }
				case LM_ADD: case LM_REPLACE: element = new COLLECTION; break;
			}
			index++;
		}
		// Dodaje zwyk�� zmienn�
		else if( data[index] == '.' || data[index] == '<' || data[index] == '-' || data.find_first_of( ALLOWED_CHARS, index ) == index )
		{
			switch( load_mode ) {
				case LM_INHERIT: if( id2 != NOT_FOUND ) { element = new VARIABLE( *P_ELEMENT( Elements[ id2 ] ).variable ); break; }
				case LM_ADD: case LM_REPLACE: element = new VARIABLE; break;
			}
		}

		// Je�eli jest nie znany operator wyrzuca b��d
		else script_error( ( AS_STR( "(STRUCTURE::Load) Invalid operator in '" ) + Name + "' structure." ).c_str() );

		if( load_mode != LM_UPDATE ) Elements.insert( id, element );
		Elements[ id ]->Name = name;
		// Je�eli jest kropka (nie ma warto�ci) nie pobiera nic
		if( data[index] == '.' ) index++;
		// Wczytuje element
		else Elements[ id ]->Load( data, index );
	}
}

//****************************************************************************************************//

void COLLECTION::Load( string& data, int& index )
{
	for(;;) {
		index = data.find_first_not_of( ' ', index );

		// Pomija komentarz, tabulator lub koniec linii
		if( STRUCTURE::CheckComment( data, index ) || STRUCTURE::CheckSpace( data, index ) ) continue;

		// Je�eli koniec kolekcji
		if( data[index] == ')' ) break;

		// Pobiera warto�ci
		VARIABLE var;
		var.Load( data, index );
		Values.add( var.Value );
	}
	index++;
}

//****************************************************************************************************//

void VARIABLE::Correct( string& value )
{		
	// Usuwa z warto�ci znak '\' przed znakiem zamykaj�cym
	int index = 0;
	while( index = value.find( "\\>" ), index != NOT_FOUND ) value.replace( index, 2, ">" );
	
	// Zamienia kombinacj� znaku '\' i liczby na znak o ID r�wnym tej liczbie
	index = 0;
	while( index = value.find_first_of( '\\', index ), index != NOT_FOUND )
	{
		if( NUMBERS.find_first_of( value[index + 1] ) != NOT_FOUND )
		{
			WORD number_length = value.find_first_not_of( NUMBERS, index + 2 ) - index;
			if( number_length > 3 ) number_length = 3;
			int char_number = DATA( value.substr( index + 1, number_length ) );
			value.replace( index, number_length + 1, DATA( AS_CHAR( char_number ) ) );
		}
		index++;
	}
		
	// Zamienia podw�jne "\\" na pojedy�cze '\'
	index = 0;
	while( index = value.find( "\\\\" ), index != NOT_FOUND ) value.replace( index, 2, "\\" );
}

//****************************************************************************************************//

void VARIABLE::Load( string& data, int& index )
{
	int start_index = index;
	
	// Je�eli warto�� jest w nawiasach
	if( data[start_index] == '<' )
	{
		// Wyszukuje koniec warto�ci
		for(;;) {
			index = data.find_first_of( '>', index );
			if( index == NOT_FOUND ) script_error( ( AS_STR( "(STRUCTURE::LoadValue) Missing '>' before end of script, after '" ) + Name + "' variable." ).c_str() );
			bool _break = false;
			for( unsigned i = 0; ; i += 2 )
				if( data[index - 1 - i] != '\\' || ( data[index - 2 - i] == '\\' && data[index - 3 - i] != '\\' ) ) {
					_break = true;
					if( i > 0 ) index++;
					break;
				}
			if( _break ) break;
			
			index++;
		}
		// Pobiera warto��
		string value = data.substr( start_index + 1, index - ( start_index + 1 ) );

		// Poprawia warto�� w miejscach gdzie wyst�puj� kombinacje ze znakiem '\'
		Correct( value );

		// Dodaje pobrany tekst do warto�ci zmiennej
		Value = AS_STR( Value ) + value;

		// Zwi�ksza index o 1 aby nie wskazywa� ostatniego znaku warto�ci ale pierwszy znak po
		index++;
	}
	else
	{
		// Pobiera warto�� liczbow�
		if( data[start_index] == '-' || NUMBERS.find_first_of( data[start_index] ) != NOT_FOUND )
		{
			index = data.find_first_not_of( NUMBERS, start_index + 1 );
			Value = data.substr( start_index, index - start_index );
		}
		// Pobiera warto�� kt�ra nie znajduje si� w nawiasach
		else
		{
			index = data.find_first_not_of( ALLOWED_CHARS, start_index + 1 );
			Value = data.substr( start_index, index - start_index );
		}
	}
}

//****************************************************************************************************//

RLGL_END

//****************************************************************************************************//