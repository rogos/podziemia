#pragma once

template<typename TYPE> struct _POS
{
	TYPE X;
	TYPE Y;

	_POS()
	{
		X = 0;
		Y = 0;
	}

	_POS( TYPE _X, TYPE _Y )
	{
		X = _X;
		Y = _Y;
	}

	_POS Set( TYPE NewX, TYPE NewY )
	{
		_POS<TYPE> old( X, Y );
		X = NewX;
		Y = NewY;
		return old;
	}

	_POS operator () ( TYPE AddToX, TYPE AddToY ) const
	{
		return _POS<TYPE>( X + AddToX, Y + AddToY );	
	}

	_POS operator + ( const _POS<TYPE>& Pos ) const
	{
		return _POS<TYPE>( X + Pos.X, Y + Pos.Y );	
	}

	_POS operator - ( const _POS<TYPE>& Pos ) const
	{
		return _POS<TYPE>( X - Pos.X, Y - Pos.Y );	
	}

	void operator += ( const _POS<TYPE>& Pos )
	{
		X += Pos.X;
		Y += Pos.Y;
	}

	void operator -= ( const _POS<TYPE>& Pos )
	{
		X -= Pos.X;
		Y -= Pos.Y;
	}

	bool operator == ( const _POS<TYPE>& Pos ) const
	{
		if( X == Pos.X && Y == Pos.Y ) return true;
		return false;
	}

	bool operator != ( const _POS<TYPE>& Pos ) const
	{
		if( *this == Pos ) return false;
		return true;
	}

	operator COORD() const
	{
		COORD c = { X, Y };
		return c;
	}
};

 typedef _POS<short> POS;