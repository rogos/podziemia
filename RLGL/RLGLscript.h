#pragma once

//****************************************************************************************************//

#include "stdafx.h"
#include "RLGL.h"
#include "RLGLfiles.h"

//****************************************************************************************************//

using namespace std;

//****************************************************************************************************//

RLGL_BEGIN

//****************************************************************************************************//
	
void DLL script_error( const char* content );

//****************************************************************************************************//

const string ALLOWED_CHARS = "abcdefghijklmnoqprstuvwxyzABCDEFGHIJKLMNOQPRSTUVWXYZ1234567890_";
const string NUMBERS = "1234567890";
const string LINE_END = "\r\n";

//****************************************************************************************************//

#pragma warning(disable: 4267)

//****************************************************************************************************//

#define AS_STR (string)
#define AS_INT (int)
#define AS_UNS (bool)
#define AS_DBL (double)
#define AS_CHAR (char)
#define AS_BOOL (bool)

//****************************************************************************************************//

class DLL DATA
{
private:
	string m_Data;
public:
	DATA() : m_Data( "" ) {}

	DATA( string data );
	DATA( char* data );
	DATA( char data );
	DATA( int data );
	operator int();
	operator unsigned();
	operator short();
	operator WORD();
	operator BYTE();
	operator double();
	operator float();
	operator string();
	operator char();
	operator bool();
	bool operator !();
};

//****************************************************************************************************//

enum DLL ELEMENT_TYPE { EL_BASE, EL_VARIABLE, EL_COLLECTION, EL_STRUCTURE };
enum DLL LOAD_MODE { LM_ADD, LM_UPDATE, LM_REPLACE, LM_INHERIT };

//****************************************************************************************************//

struct DLL ELEMENT
{
	ELEMENT_TYPE Type() const;
	
	string Name;

	const ELEMENT& operator () ( string name, unsigned index = 0 ) const;
	const ELEMENT& operator () ( unsigned index ) const;

	DATA GetValue( unsigned index = 0 ) const; // zwraca wybran� warto�� z kolekcji b�d� sam� warto�� ze zmiennej (je�eli index = 0)
	DATA operator [] ( unsigned index ) const;
	DATA operator * () const; // zwraca warto�� podanego elementu, je�eli wykorzystywane w kolekcji zwraca pierwsz� warto��

	string GetName() const;
	
	bool Exist( string name, unsigned index = 0 ) const;

	unsigned Size() const;
	
	virtual void Load( string& data, int& index ) = 0;
};

//****************************************************************************************************//

struct DLL VARIABLE : public ELEMENT
{
	DATA Value;
	
	void Correct( string& value );
	void Load( string& data, int& index );
};

//****************************************************************************************************//

struct DLL COLLECTION : public ELEMENT
{
	array<DATA> Values;
	
	void Load( string& data, int& index );
};

//****************************************************************************************************//

struct DLL STRUCTURE : public ELEMENT
{	
	array<ELEMENT*> Elements;

	STRUCTURE() {};
	STRUCTURE( const STRUCTURE& structure );

	STRUCTURE& operator = ( const STRUCTURE& structure );
	
	static bool CheckSpace( string& data, int& index );
	static bool CheckComment( string& data, int& index );
	static bool CheckOperators( string& data, int& index );

	LOAD_MODE GetMode( string& data, int& index );

	void Load( string& data, int& index );
	
	int GetID( string name, unsigned index = 0 ) const;
	int GetLastID( string name ) const;

	ELEMENT& Get( string address );
};

//****************************************************************************************************//

union DLL P_ELEMENT {
	const ELEMENT* element;
	const STRUCTURE* structure;
	const COLLECTION* collection;
	const VARIABLE* variable;

	P_ELEMENT() : element( 0 ) {}
	P_ELEMENT( const ELEMENT* const elem ) : element( elem ) {}
};

//****************************************************************************************************//

RLGL_END

//****************************************************************************************************//