#include "stdafx.h"
#include "RLGLfiles.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

string MainPath = "";

/*____________________________________________________________________________________________________*/

bool FILE::Open( OPEN_DISPOSAL open_disposal, OPEN_CONDUCT open_conduct )
{
	if( m_OpenDisposal != OD_CLOSED ) Close();

	switch( open_disposal )
	{
		case OD_WRITE:
			m_Handle = CreateFileA( m_Path.c_str(), GENERIC_WRITE, 0, NULL, open_conduct, FILE_ATTRIBUTE_NORMAL, NULL );
			break;

		case OD_READ:
			m_Handle = CreateFileA( m_Path.c_str(), GENERIC_READ, 0, NULL, open_conduct, FILE_ATTRIBUTE_NORMAL, NULL );
			break;

		case OD_WRITE_AND_READ:
			m_Handle = CreateFileA( m_Path.c_str(), GENERIC_ALL, 0, NULL, open_conduct, FILE_ATTRIBUTE_NORMAL, NULL );
			break;
	}

	if( m_Handle == INVALID_HANDLE_VALUE ) return false;

	m_OpenDisposal = open_disposal;

	return true;
}

/*____________________________________________________________________________________________________*/

bool FILE::Close()
{
	if( !m_Handle ) return false;

	CloseHandle( m_Handle );

	m_OpenDisposal = OD_CLOSED;
	m_Handle = 0;

	return true;
}

/*____________________________________________________________________________________________________*/

bool FILE::Write( LPCVOID variable, DWORD size )
{
	if( m_OpenDisposal != OD_WRITE && m_OpenDisposal != OD_WRITE_AND_READ ) return false;

	DWORD wrote_bytes;

	if( WriteFile( m_Handle, variable, size, &wrote_bytes, NULL ) == 0 || wrote_bytes != size )
		return false;

	return true;
}

/*____________________________________________________________________________________________________*/

bool FILE::Read( LPVOID variable, DWORD size )
{
	if( m_OpenDisposal != OD_READ && m_OpenDisposal != OD_WRITE_AND_READ ) return false;

	DWORD readed_bytes;

	if( ReadFile( m_Handle, variable, size, &readed_bytes, NULL ) == 0 || readed_bytes != size )
		return false;

	return true;
}

/*____________________________________________________________________________________________________*/

string FILE::ReadAllToString()
{
	string text;
	text.resize( Size() );
	for( unsigned i = 0; i < text.length(); i++ )
		*this >> text[ i ];
	
	return text;
}

/*____________________________________________________________________________________________________*/

void FILE::WriteStringWithoutSize( const string& variable )
{
	Write( &variable, variable.size() );
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const char* variable )
{
	unsigned length = strlen( variable );
	Write( variable, length );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const string variable )
{
	*this << variable.size();
	if(variable.size()) Write( &variable[0], sizeof( char ) * variable.size() );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const int variable )
{
	Write( &variable, sizeof( int ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const unsigned variable )
{
	Write( &variable, sizeof( unsigned ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const float variable )
{
	Write( &variable, sizeof( float ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const double variable )
{
	Write( &variable, sizeof( double ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const short variable )
{
	Write( &variable, sizeof( short ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const WORD variable )
{
	Write( &variable, sizeof( WORD ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const char variable )
{
	Write( &variable, sizeof( char ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const UCHAR variable )
{
	Write( &variable, sizeof( UCHAR ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator << ( const bool variable )
{
	Write( &variable, sizeof( bool ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( char*& variable )
{
	unsigned length;
	*this >> length;
	variable = new char[ length + 1 ];
	variable[ length ] = 0;
	Read( variable, length );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( string& variable )
{
	unsigned length;
	*this >> length;
	variable.resize( length );
	if(variable.size()) Read( &variable[0], sizeof( char ) * length );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( int& variable )
{
	Read( &variable, sizeof( int ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( unsigned& variable )
{
	Read( &variable, sizeof( unsigned ) );
	return *this;
}
/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( float& variable )
{
	Read( &variable, sizeof( float ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( double& variable )
{
	Read( &variable, sizeof( double ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( short& variable )
{
	Read( &variable, sizeof( short ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( WORD& variable )
{
	Read( &variable, sizeof( WORD ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( char& variable )
{
	if( !Read( &variable, sizeof( char ) ) ) variable = -1;
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( UCHAR& variable )
{
	Read( &variable, sizeof( UCHAR ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

FILE& FILE::operator >> ( bool& variable )
{
	Read( &variable, sizeof( bool ) );
	return *this;
}

/*____________________________________________________________________________________________________*/

bool FILE::Code()
{
	if( m_OpenDisposal != OD_CLOSED ) return false; 

	char ch;
	string bufor;
	
	Open( OD_READ, OC_OPEN_EXISTING );

	BYTE j = 20;
	bool plus = true;

	for( unsigned i = 0; i < Size(); i++ )
	{
		if( plus ) j++;
		else j--;
		if( j >= 30 ) plus = false;
		if( j <= 6 ) plus = true;
		*this >> ch;
		bufor += ch ^ j;
	}

	Close();

	Open( OD_WRITE );

	for( int i = 0; i < bufor.length(); i++ )
		*this << bufor[ i ];

	Close();

	return true;
}

/*____________________________________________________________________________________________________*/

unsigned FILE::Size()
{
	return GetFileSize( m_Handle, NULL );
}

/*____________________________________________________________________________________________________*/

bool DeleteFile( string path )
{
	if( DeleteFileA( path.c_str() ) ) return true;

	return false;
}

/*____________________________________________________________________________________________________*/

RLGL_END