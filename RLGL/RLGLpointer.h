#pragma once

#include "stdafx.h"
#include "RLGL.h"

RLGL_BEGIN

/****************************************************************************************************/

template< typename TYPE > class pointer
{
private:
	TYPE* m_pointer;
public:
	pointer() : m_pointer(0) {}
	pointer( TYPE* _pointer ) : m_pointer(_pointer) {}
	~pointer() { if(m_pointer) delete m_pointer; }

	void set( TYPE* _pointer ) {
		if(m_pointer) delete m_pointer;
		m_pointer = _pointer;
	}
	TYPE& operator = ( TYPE* _pointer ) {
		set(_pointer);
		return *this;
	}
	operator TYPE* () {
		return m_pointer;
	}
	TYPE* operator -> () { return m_pointer; }
	TYPE& operator *  () { return *m_pointer; }
	TYPE* operator &  () { return m_pointer; }
};

/****************************************************************************************************/

RLGL_END