#pragma once

#include "stdafx.h"
#include "RLGL.h"

RLGL_BEGIN

/****************************************************************************************************/

#define LEFT_BUTTON_PRESSED 0x01
#define RIGHT_BUTTON_PRESSED 0x02
#define MIDDLE_BUTTON_PRESSED 0x04

struct DLL EVENT
{
	bool  Exist;
	WORD  KeyCode;
	bool  KeyPressed;
	DWORD ControlKeyState;
	WORD  RepeatCount;
	BYTE  MouseCode;
	POS   MousePos;
	bool  MouseMoved;
	bool  MouseDoubleClick;
};

void DLL WaitForEvent();
EVENT DLL GetEvent( bool if_key_down = true );

/****************************************************************************************************/

enum DLL KEYBOARD
{
	KB_NOT_HIT = 0,

	KB_ESCAPE    = 27,
	KB_ENTER     = 13,
	KB_SPACE     = 32,
	KB_BACKSPACE = 8,
	KB_TAB       = 9,

	KB_INSERT    = 45,
	KB_DELETE    = 46,
	KB_HOME      = 36,
	KB_END       = 35,
	KB_PAGE_UP   = 33,
	KB_PAGE_DOWN = 34,

	KB_ARROW_UP    = 38,
	KB_ARROW_DOWN  = 40,
	KB_ARROW_LEFT  = 37,
	KB_ARROW_RIGHT = 39,

	KB_F1  = 112,
	KB_F2  = 113,
	KB_F3  = 114,
	KB_F4  = 115,
	KB_F5  = 116,
	KB_F6  = 117,
	KB_F7  = 118,
	KB_F8  = 119,
	KB_F9  = 120,
	KB_F10 = 121,
	KB_F11 = 122,
	KB_F12 = 123,

	KB_SCROLL_LOCK = 145,
	KB_CAPS_LOCK   = 20,
	KB_NUM_LOCK    = 144,

	KB_CTRL  = 17,
	KB_SHIFT = 16,
	KB_ALT   = 18,

	KB_0 = 48,
	KB_1 = 49,
	KB_2 = 50,
	KB_3 = 51,
	KB_4 = 52,
	KB_5 = 53,
	KB_6 = 54,
	KB_7 = 55,
	KB_8 = 56,
	KB_9 = 57,

	KB_A = 65,
	KB_B = 66,
	KB_C = 67,
	KB_D = 68,
	KB_E = 69,
	KB_F = 70,
	KB_G = 71,
	KB_H = 72,
	KB_I = 73,
	KB_J = 74,
	KB_K = 75,
	KB_L = 76,
	KB_M = 77,
	KB_N = 78,
	KB_O = 79,
	KB_P = 80,
	KB_Q = 81,
	KB_R = 82,
	KB_S = 83,
	KB_T = 84,
	KB_U = 85,
	KB_V = 86,
	KB_W = 87,
	KB_X = 88,
	KB_Y = 89,
	KB_Z = 90,

	KB_SUBTRACT             = 189, // -
	KB_COMMA                = 188, // ,
	KB_PERIOD               = 190, // .
	KB_SEMICOLON            = 186, // ;
	KB_LEFT_SQUARE_BRACKET  = 219, // [
	KB_RIGHT_SQUARE_BRACKET = 221, // ]
	KB_SLASH                = 191, // /
	KB_BACKSLASH            = 220, // \ 
	KB_CLOSING_APOSTROPHE   = 222, // ' 
	KB_OPENING_APOSTROPHE   = 192, // `

	KB_LEFT_WIN  = 91,
	KB_RIGHT_WIN = 92,
	KB_LIST      = 93,

	KB_NUM_0 = 96,
	KB_NUM_1 = 97,
	KB_NUM_2 = 98,
	KB_NUM_3 = 99,
	KB_NUM_4 = 100,
	KB_NUM_5 = 101,
	KB_NUM_6 = 102,
	KB_NUM_7 = 103,
	KB_NUM_8 = 104,
	KB_NUM_9 = 105,
	KB_NUM_ADD      = 107, // +
	KB_NUM_SUBTRACT = 109, // -
	KB_NUM_MULTIPLY = 106, // *
	KB_NUM_DIVIDE   = 111, // /
	KB_NUM_DECIMAL  = 110, // .

	KB_ALT_PRESSED    = 0x0100,
	KB_CTRL_PRESSED   = 0x0200,
	KB_SHIFT_PRESSED  = 0x0400,
	KB_KEY_FREE       = 0x0800,

	KB_NUM_LOCK_ON    = 0x10000,
	KB_SCROLL_LOCK_ON = 0x20000,
	KB_CAPS_LOCK_ON   = 0x40000
};

/****************************************************************************************************/

union DLL KB_EVENT
{
	struct
	{
		BYTE Code;
		bool Alt          : 1;
		bool Ctrl         : 1;
		bool Shift        : 1;
		bool KeyFree      : 5;
		bool NumLockOn    : 1;
		bool ScrollLockOn : 1;
		bool CapsLockOn   : 1;
	} Event;

	WORD Flags;

	bool operator == ( int flags ) { return Flags == flags; }
	bool operator != ( int flags ) { return Flags != flags; }
	operator WORD() { return Flags; }
	operator bool() { if( Flags ) return true; return false; }
};

/****************************************************************************************************/

KB_EVENT DLL KeyHit( bool if_free = false );
KB_EVENT DLL GetKey( bool if_free = false );

// To przenie�� do jednej klasy razem ze strumieniami wyj�cia
string DLL GetText( WORD max_length, POS pos, WORD color, string allowed_chars = "" );
int    DLL GetNum( WORD max_length, POS pos, WORD color );

/****************************************************************************************************/

enum DLL CONSOLE_COLORS
{
	C_BLACK = 0,
	C_DBLUE = 1,
	C_DGREEN = 2,
	C_DCYAN = 3,
	C_DRED = 4,
	C_DVIOLET = 5,
	C_DYELLOW = 6,
	C_GREY = 7,
	C_DGREY = 8,
	C_BLUE = 9,
	C_GREEN = 10,
	C_CYAN = 11,
	C_RED = 12,
	C_VIOLET = 13,
	C_YELLOW = 14,
	C_WHITE = 15,

	C_BG = 16
};

/****************************************************************************************************/

struct DLL CHARINFO
{
	BYTE Color;
	char Char;

	CHARINFO() : Color( 0 ), Char( 0 ) {}
	CHARINFO( BYTE color, char _char ) : Color( color ), Char( _char ) {}
};

/****************************************************************************************************/

class DLL BUFFER
{
private:
	CHAR_INFO* Chars;         // Tablica informacji o znakach kt�ra przechowuje dane do wy�wietlenia w konsoli
	POS        Size;          // Rozmiar buforu
	POS        WritePosition; // Pozycja pisania
	WORD       WriteColor;    // Kolor pisania

public:
	BUFFER() : Size( POS( 80, 25 ) )
	{
		Chars = new CHAR_INFO[ Size.X * Size.Y ];
		WriteColor = C_GREY + C_BG * C_BLACK;
		Clear();
	}

	BUFFER( POS size ) : Size( size )
	{
		Chars = new CHAR_INFO[ Size.X * Size.Y ];
		WriteColor = C_GREY + C_BG * C_BLACK;
		Clear();
	}

	~BUFFER()
	{
		delete[] Chars;
	}

	POS      GetPosition()          { return WritePosition; }
	WORD     GetColor()             { return WriteColor; }
	CHARINFO GetCharInfo( POS pos ) {
		return CHARINFO( Chars[ pos.Y * Size.X + pos.X ].Attributes, Chars[ pos.Y * Size.X + pos.X ].Char.AsciiChar );
	}

	void     Resize( POS new_size );                         // Zmienia rozmiar buforu
	void     Print();                                        // Rysuje w konsoli zawarto�� buforu
	void     Clear();                                        // Czy�ci zawarto�� buforu
	void     Clear( POS pos, unsigned size );                // Czy�ci zawarto�� buforu w wybranym miejscu
	void     SetPosition( POS position = POS( 0, 0 ) );      // Ustawia pozycj� pisania w buforze
	void     SetColor( WORD color );                         // Ustawia kolor pisania w buforze
	void     ShowCursor( DWORD size = 1 );                   // Pokazuje kursor o wybranym rozmiarze
	void     SetCursorPosition( POS position );              // Pokazuje kursor na podanej pozycji
	void     WriteChar( char ch, WORD color, POS position ); // Rysuje w buforze pojedy�czy znak
	void     ColorChar( WORD color, POS position );          // Zmienia kolor pojedy�czego znaku
	unsigned WriteWrapped( string text, POS pos, POS size ); // Wypisuje do buforu zawijany tekst
	void     Write( string text, POS pos, BYTE color );      // Rysuje w buforze tekst w wybranej pozycji i o wybranym kolorze
	void     Write( string text, POS pos );
	void     Write( string text );
	void     Write( int num, POS pos, BYTE color );          // Rysuje w buforze liczb� ca�kowit� w wybranej pozycji i o wybranym kolorze
	void     Write( int num, POS pos );
	void     Write( int num );
	BUFFER&  operator << ( string text );                    // Rysuje w buforze tekst
	BUFFER&  operator << ( int num );                        // Rysuje w buforze liczb� ca�kowit�
	BUFFER&  operator () ( POS position );                   // Ustawia pozycj� pisania w buforze
	BUFFER&  operator () ( WORD color );                     // Ustawia kolor pisania w buforze
	BUFFER&  operator () ( POS position, WORD color );       // Ustawia pozycj� i kolor pisania w buforze
};

extern BUFFER DLL Buffer;
extern BUFFER DLL * StdBuffer;

/****************************************************************************************************/

void DLL SetStdBuffer( BUFFER* buffer );
void DLL PrintFrame( POS pos, POS size, WORD color, bool _double );
void DLL PrintProgressLine( POS pos, BYTE width, WORD color, unsigned value, unsigned max_value );
char DLL ConvertToLatinII( char ch );
void DLL SetInLatinII( bool set );

/****************************************************************************************************/

RLGL_END