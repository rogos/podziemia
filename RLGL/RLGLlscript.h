#pragma once

#include "stdafx.h"
#include "RLGL.h"
#include "RLGLscript.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

class DLL LSCRIPT
{
private:
	string m_Data;
public:
	void Load( string data );
	DATA Get( WORD group, WORD number = 0 );
};

/*____________________________________________________________________________________________________*/

RLGL_END