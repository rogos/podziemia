// RLGL.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "RLGL.h"

#ifdef _MANAGED
#pragma managed(push, off)
#endif

/****************************************************************************************************/

BOOL APIENTRY DllMain( HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved )
{
	RLGL::stdrand.set_seed(time(NULL));
    return true;
}

/****************************************************************************************************/

#ifdef _MANAGED
#pragma managed(pop)
#endif

/****************************************************************************************************/

RLGL_BEGIN

/****************************************************************************************************/

randomizer stdrand;

/****************************************************************************************************/

string itostr( int num )
{
	char* _text = new char[65];
	_itoa( num, _text, 10 );
	string text = _text;
	delete[] _text;
	return text;
}

/****************************************************************************************************/

int random( int min, int max )
{
	return stdrand.random(min, max);
}

/*..................................................................................................*/

double frandom( double min, double max )
{
	return stdrand.frandom(min, max);
}

/****************************************************************************************************/

double randomizer::next_value()
{
	x = ( a * x + c ) % m;

	return double(x) / 10000; // 10^4 (przesuwa przecinek o 4 miejsca czyli b�d� 4 cyfry po przecinku)
}

/*..................................................................................................*/

int randomizer::random( int min, int max )
{
	if( min == max ) return min;
	return int(next_value()) % ( max - min + 1 ) + min;
}

/*..................................................................................................*/

double randomizer::frandom( double min, double max )
{
	if( min == max ) return min;
	return fmod( double(next_value()), ( max - min ) ) + min;
}

/****************************************************************************************************/

_POS _POS::operator + ( const _POS &Pos ) const {
	return _POS( X + Pos.X, Y + Pos.Y );
}

/****************************************************************************************************/

BYTE ToAroundDir( DIRECTION direction )
{
	for( BYTE i = 0; i < 8; i++ )
		if( AROUND_DIRECTIONS[ i ] == direction ) return i;
	return DI_C;
}

/****************************************************************************************************/

array<POS> CreateStraight( POS begin, POS end )
{
	int xadd1, xadd2, yadd1, yadd2, den, num, numadd, numpixels;
	int dx = abs( end.X - begin.X );
	int dy = abs( end.Y - begin.Y );
	int x = begin.X;
	int y = begin.Y;

	if( end.X >= begin.X ) {
		xadd1 = 1;
		xadd2 = 1;
	} else {
		xadd1 = -1;
		xadd2 = -1;
	}

	if( end.Y >= begin.Y ) {
		yadd1 = 1;
		yadd2 = 1;
	} else {
		yadd1 = -1;
		yadd2 = -1;
	}

	if( dx >= dy ) {
		xadd1 = 0;
		yadd2 = 0;
		den = dx;
		num = dx / 2;
		numadd = dy;
		numpixels = dx;
	} else {
		xadd2 = 0;
		yadd1 = 0;
		den = dy;
		num = dy / 2;
		numadd = dx;
		numpixels = dy;
	}

	array<POS> line(numpixels + 1);

	for( int i = 0; i <= numpixels; i++ )
	{
		line[i] = POS(x, y);
		num += numadd;
		if ( num >= den ) {
			num -= den;
			x += xadd1;
			y += yadd1;
		}
		x += xadd2;
		y += yadd2;
	}

	return line;
}

/****************************************************************************************************/

RLGL_END