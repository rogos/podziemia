#include "stdafx.h"
#include "RLGLio.h"

RLGL_BEGIN

/****************************************************************************************************/

BUFFER Buffer;
BUFFER* StdBuffer = &Buffer;

/****************************************************************************************************/

KB_EVENT KeyHit( bool if_free )
{
	INPUT_RECORD buffer = { 0 };
	DWORD        num_events = 0, length;
	DWORD        state = 0;
	KB_EVENT     kb_event = { 0 };

	GetNumberOfConsoleInputEvents( GetStdHandle( STD_INPUT_HANDLE ), &length );

	for( int i = 0; i < length; i++ )
	{
		ReadConsoleInputW( GetStdHandle( STD_INPUT_HANDLE ), &buffer, 1, &num_events );
		if( num_events != 0 &&
			buffer.EventType & KEY_EVENT && 
			buffer.Event.KeyEvent.bKeyDown != if_free )
		{ 
			kb_event.Flags = buffer.Event.KeyEvent.wVirtualKeyCode;
	
			state = buffer.Event.KeyEvent.dwControlKeyState;
			if( state & ( RIGHT_ALT_PRESSED | LEFT_ALT_PRESSED ) )   kb_event.Event.Alt = true;
			if( state & ( RIGHT_CTRL_PRESSED | LEFT_CTRL_PRESSED ) ) kb_event.Event.Ctrl = true;
			if( state & ( SHIFT_PRESSED ) )                          kb_event.Event.Shift = true;
			if( !buffer.Event.KeyEvent.bKeyDown )                    kb_event.Event.KeyFree = true;
			if( state & ( NUMLOCK_ON ) )                             kb_event.Event.NumLockOn = true;
			if( state & ( SCROLLLOCK_ON ) )                          kb_event.Event.ScrollLockOn = true;
			if( state & ( CAPSLOCK_ON ) )                            kb_event.Event.CapsLockOn = true;

			break;
		}
	}

	return kb_event;
}

/****************************************************************************************************/

void WaitForEvent()
{
	DWORD length;
	for(;;) {
		GetNumberOfConsoleInputEvents( GetStdHandle( STD_INPUT_HANDLE ), &length );
		if(length > 0) return;
		Sleep(1);
	}
}

/****************************************************************************************************/

EVENT GetEvent( bool if_key_pressed )
{
	INPUT_RECORD       buffer = { 0 };
	DWORD              num_events = 0, length;
	EVENT              _event = { 0 };

	GetNumberOfConsoleInputEvents( GetStdHandle( STD_INPUT_HANDLE ), &length );

	for( int i = 0; i < length; i++ )
	{
		ReadConsoleInputW( GetStdHandle( STD_INPUT_HANDLE ), &buffer, 1, &num_events );
		
		if( buffer.EventType == MOUSE_EVENT || buffer.EventType == KEY_EVENT )
		{
			if( buffer.EventType == KEY_EVENT )
			{
				if( if_key_pressed && !buffer.Event.KeyEvent.bKeyDown ) continue;
				_event.KeyPressed = buffer.Event.KeyEvent.bKeyDown;
				_event.KeyCode = buffer.Event.KeyEvent.wVirtualKeyCode;
				_event.ControlKeyState = buffer.Event.KeyEvent.dwControlKeyState;
				_event.RepeatCount = buffer.Event.KeyEvent.wRepeatCount;
			}
			if( buffer.EventType == MOUSE_EVENT )
			{
				_event.MouseCode = buffer.Event.MouseEvent.dwButtonState;
				_event.ControlKeyState = buffer.Event.MouseEvent.dwControlKeyState;
				if( buffer.Event.MouseEvent.dwEventFlags & MOUSE_MOVED ) _event.MouseMoved = true;
				if( buffer.Event.MouseEvent.dwEventFlags & DOUBLE_CLICK ) _event.MouseDoubleClick = true;
			}
			_event.MousePos = POS(buffer.Event.MouseEvent.dwMousePosition.X, buffer.Event.MouseEvent.dwMousePosition.Y);
			_event.Exist = true;
			break;
		}
	}

	return _event;
}

/****************************************************************************************************/

KB_EVENT GetKey( bool if_free )
{
	KB_EVENT kb_event = { 0 };
	while( Sleep(1), kb_event = KeyHit( if_free ), !kb_event );
	return kb_event;
}

/*____________________________________________________________________________________________________*/

bool WriteInLatinII = true;

void SetInLatinII( bool set ) { WriteInLatinII = set; }

/*____________________________________________________________________________________________________*/

string GetText( WORD max_length, POS pos, WORD color, string allowed_chars )
{
	/* Konwertuje dozwolone znaki */
	if( WriteInLatinII )
		for( unsigned i = 0; i < allowed_chars.length(); i++ )
			allowed_chars[ i ] = ConvertToLatinII( allowed_chars[ i ] );

	char ch;
	string text;

	StdBuffer->SetColor( color );

	do
	{
		ch = _getch();

		/* Koniec pisania */
		if( ch == 13 ) break;

		/* Usuwanie znaku */
		if( ch == 8 )
		{
			if( text.length() > 0 )
			{
				text.resize( text.length() - 1 );
				StdBuffer->Clear( pos( text.length(), 0 ), 1 );
				StdBuffer->Print();
			}
			continue;
		}

		/* Dodaje znak je�li jest dozwolony... */
		int index = allowed_chars == "" ? 0 : allowed_chars.find( ch );
		if( index >= 0 )
		{
			/* ...i tekst nie przekracza maksymalnego rozmiaru */
			if( text.length() < max_length )
			{
				text += ch;
				StdBuffer->SetPosition( pos );
				*StdBuffer << text;
				StdBuffer->Print();
			}
			else continue;
		}
		else continue;

	} while( ch != 27 && ch != 13 );

	if( ch == 13 ) return text;
	else return "";
}

/*____________________________________________________________________________________________________*/

int GetNum( WORD max_length, POS pos, WORD color )
{
	char* s;
	return strtol( GetText( max_length, pos, color, "-0123456789" ).c_str(), &s, 10 );
}

/*____________________________________________________________________________________________________*/

void BUFFER::Resize( POS new_size )
{
	Size = new_size;
	delete[] Chars;
	Chars = new CHAR_INFO[ Size.X * Size.Y ];
}

/*____________________________________________________________________________________________________*/

void BUFFER::Print()
{
	COORD pos = { 0, 0 };
	COORD size = { Size.X, Size.Y };
	SMALL_RECT rect = { 0, 0, Size.X - 1, Size.Y - 1 };
	WriteConsoleOutputA( GetStdHandle(STD_OUTPUT_HANDLE), Chars, size, pos, &rect );
}

/*____________________________________________________________________________________________________*/

void BUFFER::Clear()
{
	for( unsigned i = 0; i <  Size.X * Size.Y; i++ )
	{
		Chars[ i ].Attributes = C_BLACK;
		Chars[ i ].Char.AsciiChar = 0;
	}
	WritePosition = POS( 0, 0 );
}

/*____________________________________________________________________________________________________*/

void BUFFER::Clear( POS pos, unsigned size )
{
	for( unsigned i = pos.Y * Size.X + pos.X; i < pos.Y * Size.X + pos.X + size; i++ )
	{
		Chars[ i ].Attributes = C_BLACK;
		Chars[ i ].Char.AsciiChar = 0;
	}
}

/*____________________________________________________________________________________________________*/

void BUFFER::SetPosition( POS position ) { WritePosition = position; }
void BUFFER::SetColor( WORD color ) { WriteColor = color; }

/*____________________________________________________________________________________________________*/

void BUFFER::ShowCursor( DWORD size )
{
	CONSOLE_CURSOR_INFO cursor = { ( size == 0 ? 100 : size ), ( size == 0 ? false : true ) };
	SetConsoleCursorInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &cursor );
}

/*____________________________________________________________________________________________________*/

void BUFFER::SetCursorPosition( POS position )
{
	COORD pos = { position.X, position.Y };
	SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), pos );
}

/*____________________________________________________________________________________________________*/

void BUFFER::WriteChar( char ch, WORD color, POS position )
{
	Chars[ position.Y * Size.X + position.X ].Char.AsciiChar = ch;
	Chars[ position.Y * Size.X + position.X ].Attributes = color;
}

/*____________________________________________________________________________________________________*/

void BUFFER::ColorChar( WORD color, POS position )
{
	Chars[ position.Y * Size.X + position.X ].Attributes = color;
}

/*____________________________________________________________________________________________________*/

unsigned BUFFER::WriteWrapped( string text, POS pos, POS size )
{
	WORD     line_sign = 0;
	WORD     line = 0;
	unsigned writen_words = 0;
	unsigned index = 0, index2;

	for(;;)
	{
		SetPosition( pos + POS( line_sign, line ) );

		index2 = text.find_first_of( ' ', index );
		
		string word = text.substr( index, ( index2 == string::npos ? text.length() : index2 ) - index );

		if( line_sign > 0 && line_sign + 1 + word.length() > size.X )
		{
			line_sign = 0;
			line++;
			if( line >= size.Y ) break;
			SetPosition( pos + POS( 0, line ) );
		}
		if( line_sign > 0 ) {
			*this << " ";
			line_sign++;
		}
		*this << word;
		line_sign += word.length();

		if( index2 == string::npos ) break;

		index = index2 + 1;
	}

	return writen_words;
}

/*____________________________________________________________________________________________________*/

void BUFFER::Write( string text, POS pos, BYTE color )
{
	WritePosition = pos;
	WriteColor = color;

	for( unsigned i = 0; i < text.length(); i++ )
	{
		WriteChar( ( WriteInLatinII ? ConvertToLatinII( text[i] ) : text[i] ), WriteColor, WritePosition );
		if( WritePosition.X >= Size.X )
		{
			WritePosition.Y++;
			WritePosition.X = 0;
		}
		else WritePosition.X++;
	}
}

void BUFFER::Write( string text, POS pos ) { Write( text, pos, WriteColor ); }
void BUFFER::Write( string text ) { Write( text, WritePosition, WriteColor ); }

/*____________________________________________________________________________________________________*/

void BUFFER::Write( int num, POS pos, BYTE color )
{
	Write( itostr( num ), pos, color );
}

void BUFFER::Write( int num, POS pos ) { Write( num, pos, WriteColor ); }
void BUFFER::Write( int num ) { Write( num, WritePosition, WriteColor ); }

/*____________________________________________________________________________________________________*/

BUFFER& BUFFER::operator << ( string text )
{
	Write( text );

	return *this;
}

/*____________________________________________________________________________________________________*/

BUFFER& BUFFER::operator << ( int num )
{
	Write( num );

	return *this;
}

/****************************************************************************************************/

BUFFER& BUFFER::operator () ( POS position )
{
	WritePosition = position;
	return *this;
}

/****************************************************************************************************/

BUFFER& BUFFER::operator () ( WORD color )
{
	WriteColor = color;
	return *this;
}

/****************************************************************************************************/

BUFFER& BUFFER::operator () ( POS position, WORD color )
{
	WritePosition = position;
	WriteColor = color;
	return *this;
}

/****************************************************************************************************/

void DLL SetStdBuffer( BUFFER* buffer )
{
	StdBuffer = buffer;
}

/*____________________________________________________________________________________________________*/

void PrintFrame( POS pos, POS size, WORD color, bool _double )
{
	StdBuffer->WriteChar( _double ? 201 : 218, color, pos );
	StdBuffer->WriteChar( _double ? 187 : 191, color, pos + POS( size.X - 1, 0 ) );
	StdBuffer->WriteChar( _double ? 200 : 192, color, pos + POS( 0, size.Y - 1 ) );
	StdBuffer->WriteChar( _double ? 188 : 217, color, pos + POS( size.X - 1, size.Y - 1 ) );

	for( BYTE i = 1; i < size.X - 1; i++ )
	{
		StdBuffer->WriteChar( _double ? 205 : 196, color, pos + POS( i, 0 ) );
		StdBuffer->WriteChar( _double ? 205 : 196, color, pos + POS( i, size.Y - 1 ) );
	}

	for( BYTE i = 1; i < size.Y - 1; i++ )
	{
		StdBuffer->WriteChar( _double ? 186 : 179, color, pos + POS( 0, i ) );
		StdBuffer->WriteChar( _double ? 186 : 179, color, pos + POS( size.X - 1, i ) );
	}
}

/****************************************************************************************************/

void PrintProgressLine( POS pos, BYTE width, WORD color, unsigned value, unsigned max_value )
{
	WORD num = ceil( (double)value * width / max_value );
	for( BYTE i = 0; i < width; i++ ) {
		if( num <= i ) Buffer.WriteChar( 196, C_WHITE, POS( pos.X + i, pos.Y ) );
		else Buffer.WriteChar( 205, color, POS( pos.X + i, pos.Y ) );
	}
}

/****************************************************************************************************/

char ConvertToLatinII( char ch )
{
	char c = ch;
	switch( ch )
	{
		case '�' : c = 165; break;
		case '�' : c = 134; break;
		case '�' : c = 169; break;
		case '�' : c = 136; break;
		case '�' : c = 228; break;
		case '�' : c = 162; break;
		case '�' : c = 152; break;
		case '�' : c = 190; break;
		case '�' : c = 171; break;
		case '�' : c = 164; break;
		case '�' : c = 143; break;
		case '�' : c = 168; break;
		case '�' : c = 157; break;
		case '�' : c = 227; break;
		case '�' : c = 224; break;
		case '�' : c = 151; break;
		case '�' : c = 189; break;
		case '�' : c = 141; break;
	}
	return c;
}

/*____________________________________________________________________________________________________*/

RLGL_END