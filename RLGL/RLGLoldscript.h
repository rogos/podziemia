#pragma once

#include "stdafx.h"
#include "RLGL.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

const string LINE_END = "\r\n";

#define NOT_FOUND string::npos

/*____________________________________________________________________________________________________*/

class DLL DATA
{
private:
	string m_Data;
public:
	DATA() {}

	DATA( string data );
	DATA( char* data );
	DATA( int data );
	operator int();
	operator unsigned();
	operator short();
	operator WORD();
	operator BYTE();
	operator double();
	operator float();
	operator string();
	operator char();
	operator bool();
	bool operator !();
};

/*____________________________________________________________________________________________________*/

class DLL SCRIPT
{
private:
	string m_Data;
public:
	void Load( string data );
	unsigned ValueBegin( unsigned index );
	unsigned ValueEnd( unsigned index );
	unsigned FindVariable( string name, SECTOR range, unsigned index = 0 );
	SECTOR FindStructure( string name, SECTOR range, unsigned index = 0 );
	SCRIPT ToSingleOut( string name, unsigned index );
	string GetValue( unsigned index );
	DATA Get( DATA data, bool value = true );
};

/*____________________________________________________________________________________________________*/

RLGL_END