#include "stdafx.h"
#include "RLGLsound.h"

RLGL_BEGIN

bool DLL LoadSound( string path, PLAY_LOOP play_loop = PLOOP_NO );

	FMOD::System     *_system;
    FMOD::Sound      *_sound1, *sound2;
    FMOD::Channel    *channel = 0;
    FMOD_RESULT       result;
	
	result = FMOD::System_Create(&system);
	result = system->init(32, FMOD_INIT_NORMAL, 0);

	
	result = sound1->setMode(FMOD_LOOP_BIDI);

	result = system->playSound(FMOD_CHANNEL_FREE, sound1, false, &channel);

RLGL_END