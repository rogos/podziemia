#include "stdafx.h"
#include "RLGLscript.h"
#include "RLGLfiles.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

DATA::DATA( string data ) : m_Data( data ) {}
DATA::DATA( char* data ) : m_Data( ( string ) data ) {}
DATA::DATA( int data ) : m_Data( itostr( data ) ) {}

/*____________________________________________________________________________________________________*/

DATA::operator int() {
	char* s;
	return strtol( m_Data.c_str(), &s, 10 );
}

DATA::operator unsigned() {
	char* s;
	return strtoul( m_Data.c_str(), &s, 10 );
}

DATA::operator short() {
	return int( *this );
}

DATA::operator WORD() {
	return unsigned( *this );
}

DATA::operator BYTE() {
	return unsigned( *this );
}

DATA::operator double() {
	char* s;
	return strtod( m_Data.c_str(), &s );
}

DATA::operator float() {
	return double( *this );
}

DATA::operator string() {
	return m_Data;
}

DATA::operator char() {
	return m_Data[ 0 ];
}

DATA::operator bool() {
	if( m_Data == "" ) return false;
	else return true;
}

/*____________________________________________________________________________________________________*/

bool DATA::operator !()
{
	return !this->operator bool();
}

/*____________________________________________________________________________________________________*/

void SCRIPT::Load( string data )
{
	m_Data = data;

	if( data.length() == 0 ) return;

	unsigned index = 0;

	// wczytuje pliki podane za operatorem :INCLUDE
	while( index = m_Data.find( ":INCLUDE" ), index != NOT_FOUND )
	{
		FILE file( GetValue( index ) );
		if( !file.Open( OD_READ, OC_OPEN_EXISTING ) ) break;

		string content;
		for( unsigned i = 0; i < file.Size(); i++ )
		{
			char ch;
			file >> ch;
			content += ch;
		}
		unsigned index2 = ValueBegin( index + 1 );
		m_Data.replace( index, ValueEnd( index2 ) + 3 - index, content );
	}

	// usuwa tabulatory
	while( index = m_Data.find( static_cast< char >( 9 ) ), index != NOT_FOUND )
		m_Data.erase( index, 1 );

	// usuwa komentarze
	index = -2;
	while( index = m_Data.find( "##", index + 2 ), index != NOT_FOUND )
	{
		int index2 = m_Data.find_first_of( '>', index );
		int index3 = m_Data.find_first_of( '<', index );
		if( index2 < index3 ) continue;
		index2 = m_Data.find_first_of( LINE_END, index );
		if( index2 == NOT_FOUND ) index2 = m_Data.length() - 1;
		m_Data.erase( index, index2 - index );
	}

	// usuwa przej�cia do nast�pnej linijki
	while( index = m_Data.find( LINE_END ), index != NOT_FOUND )
		m_Data.erase( index, 2 );

	// usuwa przerwy w warto�ciach
	index = 0;
	unsigned index2;
	while( index = ValueEnd( index ), index2 = m_Data.find_first_not_of( ' ', index + 2 ),
		   m_Data[ index2 ] == '<' || index + 1 != NOT_FOUND )
	{
		if( m_Data[ index2 ] != '<' ) index += 2;
		else m_Data.erase( index + 1, index2 - index );
	}

	// zamienia tre�ci podane za operatorem :DEF
	while( index = m_Data.find( ":DEF" ), index != NOT_FOUND )
	{
		// szuka starego tekstu
		unsigned index2 = m_Data.find_first_not_of( ' ', index + 4 );
		string old_txt = m_Data.substr( index2, m_Data.find_first_of( ' ', index2 ) - index2 );

		// szuka nowego tekstu kt�ry zostanie wstawiony zamiast starego
		index2 += old_txt.length();
		string new_txt = GetValue( index2 );

		// usuwa dan� definicj�
		m_Data.erase( index, ValueEnd( index2 ) + 2 - index );

		// zamienia wszystkie stare teksty na nowe
		while( index = m_Data.find( old_txt ), index != NOT_FOUND )
		{
			m_Data.replace( index, old_txt.length(), new_txt );
		}
	}
}

/*____________________________________________________________________________________________________*/

unsigned SCRIPT::ValueBegin( unsigned index )
{
	return m_Data.find_first_of( '<', index ) + 1;
}

/*____________________________________________________________________________________________________*/

unsigned SCRIPT::ValueEnd( unsigned index )
{
	return m_Data.find_first_of( '>', index ) - 1;
}

/*____________________________________________________________________________________________________*/

string SCRIPT::GetValue( unsigned index )
{
	// szuka pozycj� warto�ci
	index = ValueBegin( index );

	return m_Data.substr( index, ValueEnd( index ) - index + 1 );
}

/*____________________________________________________________________________________________________*/

unsigned SCRIPT::FindVariable( string name, SECTOR range, unsigned index )
{
	int level = 0;
	for( unsigned i = range.Begin; i <= range.End; i++ )
	{
		// je�eli napotka na warto�� pomija j�
		if( m_Data[ i ] == '<' ) i = ValueEnd( i ) + 2;

		// zmienia poziom je�li napotka na nawias klamrowy
		if( m_Data[ i ] == '{' ) level++;
		if( m_Data[ i ] == '}' ) level--;
		if( level < 0 ) return -1;

		// zwraca index je�li zmienna si� zgadza
		if( name == m_Data.substr( i, name.length() ) && level == 0 &&
			( m_Data[ i + name.length() ] == '{' ||
			  m_Data[ i + name.length() ] == '<' ||
			  m_Data[ i + name.length() ] == ' ' ) )
		{
			if( index == 0 ) return i; 
			else index--;
		}
	}
	return -1;
}

/*____________________________________________________________________________________________________*/

SECTOR SCRIPT::FindStructure( string name, SECTOR range, unsigned index )
{
	int level = 0;

	index = FindVariable( name, range, index );
	if( index == NOT_FOUND ) return SECTOR( 0, 0 );

	// wyszukuje pocz�tek struktury
	unsigned begin = m_Data.find_first_of( '{', index ) + 1;

	// je�li zmienna nie jest struktur� przerywa funkcj�
	if( m_Data[ m_Data.find_first_not_of( ' ', index + name.length() ) ] != '{' ) return SECTOR( 0, 0 );

	for( unsigned i = begin; i <= range.End; i++ )
	{
		// zmienia poziom je�li napotka na nawias klamrowy
		if( m_Data[ i ] == '{' ) level++;
		if( m_Data[ i ] == '}' ) level--;

		// je�li wyjdzie z bloku zwraca go
		if( level < 0 ) return SECTOR( begin, i - 1 );
	}

	return SECTOR( 0, 0 );
}

/*____________________________________________________________________________________________________*/

SCRIPT SCRIPT::ToSingleOut( string name, unsigned index )
{
	SCRIPT script;
	SECTOR structure = FindStructure( name, SECTOR( 0, m_Data.length() ), index );
	if( structure == SECTOR( 0, 0 ) ) return script;
	script.Load( m_Data.substr( structure.Begin, structure.End - structure.Begin + 1 ) );
	return script;
}

/*____________________________________________________________________________________________________*/

DATA SCRIPT::Get( DATA data, bool value )
{
	string list = data;
	SECTOR range( 0, m_Data.length() );
	unsigned before_variable, after_variable = -1;
	string reference;

	// szuka podan� nazw�
	for(;;)
	{
		// wyszukuje nazw� szukanej zmiennej
		before_variable = after_variable + 1;
		if( list[ before_variable ] == '.' ) 
			return Get( reference + list.substr( ++before_variable, list.size() - before_variable ) );
		after_variable = list.find_first_of( '.', before_variable );
		if( after_variable == NOT_FOUND ) after_variable = list.length();
		
		// pobiera nazw� szukanej zmiennej razem z indeksem struktury
		string for_search = list.substr( before_variable, after_variable - before_variable );

		// wyci�ga z nazwy index struktury
		unsigned index = 0, i = for_search.find_first_of( '[' );
		if( i != NOT_FOUND )
		{
			index = static_cast< int >( DATA( for_search.substr( i + 1, for_search.find_first_of( ']' ) - i - 1 ) ) );
			for_search.erase( i, for_search.length() - i );
		}
		else i = 0;

		// je�eli jest koniec listy...
		if( after_variable == list.length() )
		{
			// ...pobiera warto��
			i = FindVariable( for_search, range, index );
			if( i != NOT_FOUND )
			{
				if( value ) return GetValue( i );
				else return "\1";
			}
			else return "";
		}
		// je�li wyst�puje odwo�anie zapisuje jego nazw�
		else if( list.find( "..", before_variable ) == after_variable )
			reference = GetValue( FindVariable( for_search, range, index ) );
		else
		{
			// szuka strukture o podanym indeksie
			range = FindStructure( for_search, range, index );

			if( range == SECTOR( 0, 0 ) ) break;
		}
	}
	return "";
}

/*____________________________________________________________________________________________________*/

RLGL_END