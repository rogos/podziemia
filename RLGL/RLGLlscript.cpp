#include "stdafx.h"
#include "RLGLlscript.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

void LSCRIPT::Load( string data )
{
	m_Data = data;

	if( data.length() == 0 ) return;

	int index = 0;

	// usuwa tabulatory
	while( index = m_Data.find( static_cast< char >( 9 ) ), index != NOT_FOUND )
		m_Data.erase( index, 1 );

	// usuwa przej�cia do nast�pnej linijki
	while( index = m_Data.find( LINE_END ), index != NOT_FOUND )
		m_Data.erase( index, 2 );
}

/*____________________________________________________________________________________________________*/

DATA LSCRIPT::Get( WORD group, WORD number )
{
	WORD current_group = 0,
	     current_number = 0;

	int begin = 0,
	    end = 0;

	for(;;)
	{
		begin = m_Data.find_first_not_of( ' ', begin );

		if( begin == NOT_FOUND ) return "";

		if( m_Data[ begin ] == ';' ) /* Kolejna grupa warto�ci */
		{
			current_group++;
			current_number = 0;
			begin++;
			continue;
		}
		if( m_Data[ begin ] == '\'' ) /* Je�eli warto�� jest w apostrofach */
		{
			begin++;
			end = m_Data.find_first_of( '\'', begin );
			while( m_Data[ end + 1 ] == '\'' && end != NOT_FOUND ) /* Je�eli s� 2 apostrofy */
				end = m_Data.find_first_of( '\'', end + 2 );
		}
		else end = m_Data.find_first_of( ' ', begin ); /* Je�eli warto�� NIE jest w apostrofach */

		if( begin == NOT_FOUND ) return "";
		if( end == NOT_FOUND ) end = m_Data.size();

		if( current_group == group && current_number == number ) /* Je�eli jest odpowiednia warto�� zwraca j� */
		{
			string value = m_Data.substr( begin, end - begin );

			int index = 0;  /* Zamienianie podw�jnych apostrof�w na pojedy�cze */
			while( index < value.length() - 1 )
			{
				index = value.find_first_of( '\'', index );
				if(index == NOT_FOUND ) break;
				if( m_Data[ index + 1 ] == '\'' ) value.erase( index );
				index++;
			}

			return value;
		}
		current_number++;
		begin = end + 1;
	}
}

/*____________________________________________________________________________________________________*/

RLGL_END