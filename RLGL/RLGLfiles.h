#pragma once

#include "stdafx.h"
#include "RLGL.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

enum DLL OPEN_DISPOSAL{ OD_CLOSED, OD_WRITE, OD_READ, OD_WRITE_AND_READ };
enum DLL OPEN_CONDUCT{ OC_CREATE_ALWAYS = 2, OC_CREATE = 1, OC_OPEN_ALWAYS = 4, OC_OPEN_EXISTING = 3 };

/*____________________________________________________________________________________________________*/

extern DLL string MainPath;

/*____________________________________________________________________________________________________*/

class DLL FILE
{
private:
	string m_Path;
	HANDLE m_Handle;
	OPEN_DISPOSAL m_OpenDisposal;
public:
	FILE( string path ) : m_Path( MainPath + path ), m_Handle( 0 ), m_OpenDisposal( OD_CLOSED ) {}
	~FILE()
	{
		if( m_Handle ) CloseHandle( m_Handle );
	}

	bool Open( OPEN_DISPOSAL open_disposal, OPEN_CONDUCT open_conduct = OC_CREATE_ALWAYS );
	bool Close();

	bool Write( LPCVOID variable, DWORD size );
	bool Read( LPVOID variable, DWORD size );
	string ReadAllToString();
	void WriteStringWithoutSize( const string& variable );

	FILE& operator << ( const char* variable );
	FILE& operator << ( const string variable );
	FILE& operator << ( const int variable );
	FILE& operator << ( const unsigned variable );
	FILE& operator << ( const float variable );
	FILE& operator << ( const double variable );
	FILE& operator << ( const short variable );
	FILE& operator << ( const WORD variable );
	FILE& operator << ( const char variable );
	FILE& operator << ( const UCHAR variable );
	FILE& operator << ( const bool variable );

	FILE& operator >> ( char*& variable );
	FILE& operator >> ( string& variable );
	FILE& operator >> ( int& variable );
	FILE& operator >> ( unsigned& variable );
	FILE& operator >> ( float& variable );
	FILE& operator >> ( double& variable );
	FILE& operator >> ( short& variable );
	FILE& operator >> ( WORD& variable );
	FILE& operator >> ( char& variable );
	FILE& operator >> ( UCHAR& variable );
	FILE& operator >> ( bool& variable );

	bool Code();
	unsigned Size();
};

/*____________________________________________________________________________________________________*/

bool DLL DeleteFile( string path );

/*____________________________________________________________________________________________________*/

RLGL_END