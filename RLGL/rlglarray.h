#pragma once

#include <string.h>
#include <stdarg.h>

#pragma warning(disable: 4996)

template< typename TYPE > class array
{
private:
	TYPE*    m_array; // Wska�nik na tablic�
	unsigned m_size;  // Rozmiar tablicy
	unsigned m_index; // Index startowy, np je�li wynosi 3 to aby odwo�a� si� do pierwszego elementu tablicy nale�y poda� my_example_array[3]

	void m_error( const char* content ) const
	{
		char con[255] = "ARRAY ERROR: ";
		strcat( con, content );
		throw( con );
	}

	void m_init( const unsigned index, const unsigned size )
	{
		m_size = size;
		m_array = new TYPE[ m_size ];
		m_index = index;
	}

	void m_clear_all()
	{
		if( m_array ) delete[] m_array;
		m_array = 0;
		m_size = 0;
		m_index = 0;
	}
	
public:
	array() : m_array( 0 ), m_size( 0 ), m_index( 0 )
	{
		m_init( 0, 0 );
	}

	array( const unsigned size )
	{
		m_init( 0, size );
	}

	array( const unsigned index, const unsigned size )
	{
		m_init( index, size );
	}

	array( const unsigned index, const unsigned size, TYPE value, ... )
	{
		m_init( index, size );
		m_array[ 0 ] = value;
		va_list values;
		va_start( values, value );
		for( unsigned i = 1; i < m_size; i++ )
			m_array[ i ] = va_arg( values, TYPE );
		va_end( values );
	}

	~array()
	{
		m_clear_all();
	}

	array( const array<TYPE>& arr )
	{
		m_array = 0;
		*this = arr;
	}

	array& operator = ( const array<TYPE>& arr )
	{
		m_clear_all();
		insert( m_index, arr );
		m_index = arr.begin();
		return *this;
	}

	void set( const unsigned index, const unsigned size )
	{
		m_clear_all();
		m_init( index, size );
	}

	unsigned size() const {
		return m_size;
	}

	unsigned begin() const {
		return m_index;
	}

	unsigned end() const {
		return m_index + m_size - 1;
	}

	TYPE& operator[] ( const unsigned index )
	{
		if( index < m_index || index >= m_size + m_index )
			m_error( "(operator[]) Invalid 'index' parameter! Out of range." );
		return m_array[ index - m_index ];
	}

	TYPE& operator[] ( const unsigned index ) const
	{
		if( index < m_index || index >= m_size + m_index )
			m_error( "(operator[]) Invalid 'index' parameter! Out of range." );
		return m_array[ index - m_index ];
	}

	TYPE& front() {
		return (*this)[ begin() ];
	}

	TYPE& back() {
		return (*this)[ end() ];
	}

	void resize( const unsigned size )
	{
		if( size == m_size ) return;

		TYPE* _m_array = new TYPE[ size ];
		for( unsigned i = 0; i < size && i < m_size; i++ )
			_m_array[ i ] = m_array[ i ];

		m_size = size;
		delete[] m_array;
		m_array = _m_array;
	}

	void set_all( const TYPE value )
	{
		for( unsigned i = 0; i < m_size; i++ )
			m_array[ i + m_index ] = value;
	}

	void add( const TYPE value )
	{
		resize( m_size + 1 );
		back() = value;
	}

	void delete_last()
	{
		resize( m_size - 1 );
	}

	void insert( const unsigned index, const array<TYPE>& arr )
	{
		if( index < m_index || index > m_size + m_index )
			m_error( "(insert) Invalid 'index' parameter! Out of range." );
		
		resize( m_size + arr.size() );
		for( int i = m_size - arr.size() - 1; i >= (signed)( index - m_index ); i-- )
			m_array[ i + arr.size() ] = m_array[ i ];
		for( unsigned i = 0; i < arr.size(); i++ ) 
			m_array[ index - m_index + i ] = arr[ arr.begin() + i ];
	}

	void insert( const unsigned index, const TYPE value )
	{
		insert( index, array( 0, 1, value ) );
	}

	void swap( const array<TYPE>& arr )
	{
		insert( m_size + m_index, arr );
	}

	array<TYPE> copy( const unsigned index, const unsigned size ) const
	{
		if( index < m_index || index > m_size + m_index )
			m_error( "(copy) Invalid 'index' parameter! Out of range." );

		array<TYPE> arr( index + size > m_size + m_index ? m_size + m_index - index : size );
		for( unsigned i = 0; i < arr.size(); i++ ) 
			arr[ i ] = m_array[ index - m_index + i ];

		return arr;
	}

	array<TYPE> cut_out( const unsigned index, const unsigned size = 1 )
	{
		if( index < m_index || index > m_size + m_index )
			m_error( "(cut_out) Invalid 'index' parameter! Out of range." );

		array<TYPE> arr = copy( index, size );
		erase( index, size );

		return arr;
	}

	TYPE* to_const() const
	{
		TYPE* arr = new TYPE[ m_size ];
		memcpy( arr, m_array, m_size * sizeof(TYPE) );
		return arr;
	}

	void erase( const unsigned index, const unsigned size = 1 )
	{
		if( index < m_index || index > m_size + m_index )
			m_error( "(erase) Invalid 'index' parameter! Out of range." );

		unsigned s = index + size > m_size + m_index ? m_size + m_index - index : size;

		for( int i = index - m_index; i < m_size - s; i++ )
			m_array[ i ] = m_array[ i + s ];

		resize( m_size - s );
	}

	void clear()
	{
		if( m_array ) delete[] m_array;
		m_array = 0;
		m_size = 0;
	}
};

template< typename TYPE > class dvar
{
private:
	TYPE* m_poiner;
public:
	dvar() : m_poiner(0) {}
	~dvar() {
		delete m_poiner;
	}

	dvar& operator = ( TYPE* variable_poiner ) {
		if(m_Poiner) delete m_poiner;
		m_poiner = new_poiner;
		return *this;
	}

	TYPE* operator -> () {
		return m_poiner;
	}
};