#include "stdafx.h"
#include "RLGLerror.h"
#include "RLGLio.h"

/****************************************************************************************************/

RLGL_BEGIN

/****************************************************************************************************/

void _error( string content, string file, unsigned line )
{
	PrintFrame( POS( 1, 1 ), POS( 78, 4 ), C_RED, false );
	Buffer.Clear( POS( 2, 2 ), 76 );
	Buffer.Clear( POS( 2, 3 ), 76 );
	Buffer.SetColor( C_WHITE );
	Buffer.Write( " ERROR: ", POS( 3, 1 ) );
	Buffer.Write( (int)(file.size()) - 70 < 0 ? file : file.substr( file.size() - 70, 70 ), POS( 2, 2 ) );
	Buffer.Write( ": " );
	Buffer.Write( line );
	Buffer.Write( content, POS( 2, 3 ) );
	Buffer.Print();
	_getch();
	exit(0);
}

/****************************************************************************************************/

void _warning( string content, string file, unsigned line )
{
	PrintFrame( POS( 1, 1 ), POS( 78, 4 ), C_YELLOW, false );
	Buffer.Clear( POS( 2, 2 ), 76 );
	Buffer.Clear( POS( 2, 3 ), 76 );
	Buffer.SetColor( C_WHITE );
	Buffer.Write( " WARNING: ", POS( 3, 1 ) );
	Buffer.Write( (int)(file.size()) - 70 < 0 ? file : file.substr( file.size() - 70, 70 ), POS( 2, 2 ) );
	Buffer.Write( ": " );
	Buffer.Write( line );
	Buffer.Write( content, POS( 2, 3 ) );
	Buffer.Print();
	_getch();
}

/****************************************************************************************************/

RLGL_END

/****************************************************************************************************/