#pragma once

#include "stdafx.h"
#include "RLGLarray.h"

/****************************************************************************************************/

#ifdef RLGL_LIB_EXPORTS
 #define DLL __declspec(dllexport)
#else
 #define DLL __declspec(dllimport)
#endif

/****************************************************************************************************/

#define RLGL_BEGIN namespace RLGL {
#define RLGL_END }

/****************************************************************************************************/

RLGL_BEGIN

/****************************************************************************************************/

int    DLL random( int min, int max );
double DLL frandom( double min, double max );
string DLL itostr( int num );

/****************************************************************************************************/

typedef unsigned char    byte;
typedef unsigned short   ushort;
typedef unsigned int     uint;
typedef unsigned __int64 ulong;
typedef unsigned __int64 unsigned64;

/*..................................................................................................*/

/* Liniowy generator liczb pseudolosowych
   x_i = ( a * x_(i-1) + c ) mod m */

class DLL randomizer
{
private:
	unsigned64 x;

	static const unsigned64 m = 34359738368; // 2^35 ( max = m - 1 )
	static const unsigned a = 3141592653;
	static const unsigned c = 2718281829;

public:
	randomizer( unsigned64 seed = 1 ) : x( seed ) {}

	void set_seed( unsigned64 seed ) { x = seed; }
	double next_value();                      // zwraca liczbę pseudolosową z zakresu od 0.0 do 3435973.8367
	int random( int min, int max );           // zwraca całkowitą liczbę pseudolosową z podanego zakresu
	double frandom( double min, double max ); // zwraca rzeczywistą liczbę pseudolosową z podanego zakresu
};

/*..................................................................................................*/

extern randomizer stdrand;

/****************************************************************************************************/

typedef struct DLL _POS
{
	short X;
	short Y;

	_POS()
	{
		X = 0;
		Y = 0;
	}

	_POS( WORD _X, WORD _Y )
	{
		X = _X;
		Y = _Y;
	}

	_POS Set( short NewX, short NewY )
	{
		_POS old( X, Y );
		X = NewX;
		Y = NewY;
		return old;
	}

	_POS operator () ( short AddToX, short AddToY ) const
	{
		return _POS( X + AddToX, Y + AddToY );	
	}

	_POS operator + ( const _POS& Pos ) const;

	_POS operator - ( const _POS& Pos ) const
	{
		return _POS( X - Pos.X, Y - Pos.Y );	
	}

	void operator += ( const _POS& Pos )
	{
		X += Pos.X;
		Y += Pos.Y;
	}

	void operator -= ( const _POS& Pos )
	{
		X -= Pos.X;
		Y -= Pos.Y;
	}

	bool operator == ( const _POS& Pos ) const
	{
		if( X == Pos.X && Y == Pos.Y ) return true;
		return false;
	}

	bool operator != ( const _POS& Pos ) const
	{
		if( *this == Pos ) return false;
		return true;
	}

	operator COORD() const
	{
		COORD c = { X, Y };
		return c;
	}
} DLL POS,*PPOS;

/****************************************************************************************************/

enum DLL DIRECTION
{
	DI_NW = 7,  DI_N  = 8,  DI_NE = 9,
	DI_W  = 4,  DI_C  = 5,  DI_E  = 6,
	DI_SW = 1,  DI_S  = 2,  DI_SE = 3
};

const BYTE AROUND_DIRECTIONS[ 8 ] = { DI_N, DI_NE, DI_E, DI_SE, DI_S, DI_SW, DI_W, DI_NW };

const POS DIRECT_VECTOR[ 9 ] =
{
	POS( -1, +1 ),
	POS( +0, +1 ),
	POS( +1, +1 ),
	POS( -1, +0 ),
	POS( +0, +0 ),
	POS( +1, +0 ),
	POS( -1, -1 ),
	POS( +0, -1 ),
	POS( +1, -1 )
};

BYTE DLL ToAroundDir( DIRECTION direction );

const double PI = 3.1415923565;

/****************************************************************************************************/

typedef struct DLL _SECTOR
{
	double Begin;
	double End;

	_SECTOR()
	{
		Begin = 0;
		End = 0;
	}

	_SECTOR( double _Begin, double _End )
	{
		Begin = _Begin;
		End = _End;
	}

	_SECTOR Set( double NewBegin, double NewEnd )
	{
		_SECTOR Old( Begin, End );
		Begin = NewBegin;
		End = NewEnd;
		return Old;
	}

	bool operator == ( const _SECTOR& Sector )
	{
		if( Begin == Sector.Begin && End == Sector.End ) return true;
		return false;
	}

	bool operator != ( const _SECTOR& Sector )
	{
		if( *this == Sector ) return false;
		return true;
	}

	_SECTOR operator() ( double AddToBegin, double AddToEnd )
	{
		_SECTOR Returning( Begin + AddToBegin, End + AddToEnd );
		return Returning;	
	}

	double Rand()
	{
		return frandom( Begin, End );
	}
} DLL SECTOR;

/****************************************************************************************************/

array<POS> DLL CreateStraight( POS begin, POS end );

/****************************************************************************************************/

RLGL_END