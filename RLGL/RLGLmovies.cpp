#include "stdafx.h"
#include "RLGLmovies.h"
#include "RLGLio.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

void IMAGE::Print( POS pos )
{
	for( WORD i = 0; i < Points.size(); i++ )
	{
		StdBuffer->WriteChar( Points[i].Char, Points[i].Color,
		                  pos + POS( Points[i].Pos % Size.X, Points[i].Pos / Size.X ) );
	}
}

/*____________________________________________________________________________________________________*/

bool IMAGE::Clear( POS pos )
{
	if( pos.X >= Size.X || pos.Y >= Size.Y ) return false;

	WORD _pos = pos.Y * Size.X + pos.X;

	for( WORD i = 0; i < Points.size(); i++ )
		if( Points[i].Pos == _pos ) Points.erase(i);

	return true;
}

/*____________________________________________________________________________________________________*/

bool IMAGE::Draw( POS pos, BYTE color, char _char )
{
	if( !Clear( pos ) ) return false;

	Points.add( POINT( pos.Y * Size.X + pos.X, color, _char ) );

	return true;
}

/*____________________________________________________________________________________________________*/

bool IMAGE::Save( FILE& file )
{
	file.Write( &Size, sizeof( POS ) );

	file << WORD( Points.size() );

	if( Points.size() > 0 )
		file.Write( &Points.front(), sizeof( POINT ) * Points.size() );

	return true;
}

/*____________________________________________________________________________________________________*/

bool IMAGE::Load( FILE& file )
{
	file.Read( &Size, sizeof( POS ) );

	WORD size;
	file >> size;
	Points.resize( size );
	
	if( Points.size() > 0 )
		file.Read( &Points.front(), sizeof( POINT ) * Points.size() );

	return true;
}

/*____________________________________________________________________________________________________*/

void FRAME::Print( POS pos, WORD width )
{
	for( WORD i = 0; i < Points.size(); i++ )
	{
		StdBuffer->WriteChar( Points[i].Char, Points[i].Color,
		                  pos + POS( Points[i].Pos % width, Points[i].Pos / width ) );
	}
}

/*____________________________________________________________________________________________________*/

POS      MOVIE::GetSize() { return Size; }
unsigned MOVIE::GetNumFrames() { return Frames.size(); }
WORD     MOVIE::GetFrameTime( unsigned frame ) { return Frames[ frame ].Time; }

/*____________________________________________________________________________________________________*/

void MOVIE::SetFrameTime( unsigned frame, WORD time ) { Frames[ frame ].Time = time; }

/*____________________________________________________________________________________________________*/

void MOVIE::AddFrame( const FRAME& frame, unsigned index ) { Frames.insert( index, frame ); }
void MOVIE::DeleteFrame( unsigned index ) { Frames.erase( index ); }

/*____________________________________________________________________________________________________*/

bool MOVIE::Clear( unsigned frame, POS pos )
{
	if( pos.X >= Size.X || pos.Y >= Size.Y || frame >= Frames.size() ) return false;

	WORD _pos = pos.Y * Size.X + pos.X;

	for( WORD i = 0; i < Frames[ frame ].Points.size(); i++ )
		if( Frames[ frame ].Points[i].Pos == _pos ) Frames[ frame ].Points.erase(i);

	return true;
}

/*____________________________________________________________________________________________________*/

bool MOVIE::Draw( unsigned frame, POS pos, BYTE color, char _char )
{
	if( !Clear( frame, pos ) ) return false;

	Frames[ frame ].Points.add( POINT( pos.Y * Size.X + pos.X, color, _char ) );

	return true;
}

/*____________________________________________________________________________________________________*/

bool MOVIE::Show( POS pos )
{
	for( unsigned i = 0; i < Frames.size(); i++ )
	{
		Frames[i].Print( pos, Size.X );
		StdBuffer->Print();
		for( WORD j = 0; j <= Frames[i].Time; j += 10 )
		{
			Sleep( j + 10 > Frames[i].Time ? Frames[i].Time - j : 10 );
			if( kbhit() )
			{
				_getch();
				return false;
			}
		}
	}

	return true;
}

/*____________________________________________________________________________________________________*/

bool MOVIE::PrintFrame( POS pos, unsigned frame_id )
{
	if( frame_id >= Frames.size() ) return false;

	for( unsigned i = 0; i < frame_id + 1; i++ )
		Frames[i].Print( pos, Size.X );

	StdBuffer->Print();

	return true;
}

/*____________________________________________________________________________________________________*/

bool MOVIE::Save( FILE& file )
{
	file.Write( &Size, sizeof( POS ) );

	file << Frames.size();
	for( unsigned i = 0; i < Frames.size(); i++ )
	{
		file << Frames[i].Time;
		file << WORD( Frames[i].Points.size() );
		if( Frames[i].Points.size() > 0 )
			file.Write( &Frames[i].Points.front(), sizeof( POINT ) * Frames[i].Points.size() );
	}

	return true;
}

/*____________________________________________________________________________________________________*/

bool MOVIE::Load( FILE& file )
{
	file.Read( &Size, sizeof( POS ) );
	
	unsigned size;
	file >> size;
	Frames.resize( size );
	for( unsigned i = 0; i < Frames.size(); i++ )
	{
		WORD size2;
		file >> Frames[i].Time;
		file >> size2;
		Frames[i].Points.resize( size2 );
		if( Frames[i].Points.size() > 0 )
			file.Read( &Frames[i].Points.front(), sizeof( POINT ) * Frames[i].Points.size() );
	}

	return true;
}

/*____________________________________________________________________________________________________*/

RLGL_END