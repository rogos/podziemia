#pragma once

#include "stdafx.h"
#include "RLGLarray.h"
#include "RLGLfiles.h"
#include "RLGL.h"

RLGL_BEGIN

/*____________________________________________________________________________________________________*/

struct DLL POINT
{
	WORD Pos;
	BYTE Color;
	char Char;

	POINT() : Pos( 0 ), Color( 0 ), Char( 0 ) {}
	POINT( WORD pos, BYTE color, char _char ) : Pos( pos ), Color( color ), Char( _char ) {}
};

/*____________________________________________________________________________________________________*/

class DLL IMAGE
{
private:
	array<POINT> Points;
	POS Size;

public:
	IMAGE() {}
	IMAGE( POS size ) : Size( size ) {}
	
	POS GetSize() { return Size; }

	void Print( POS pos );

	bool Draw( POS pos, BYTE color, char _char );
	bool Clear( POS pos );

	bool Save( FILE& file );
	bool Load( FILE& file );
};

/*____________________________________________________________________________________________________*/

struct DLL FRAME
{
	WORD Time;
	array<POINT> Points;
	void Print( POS pos, WORD width );

	FRAME() : Time( 0 ) {}
	FRAME( WORD time, const array<POINT>& points ) : Time( time ), Points( points ) {}
};

/*____________________________________________________________________________________________________*/

class DLL MOVIE
{
private:
	POS Size;
	array<FRAME> Frames;

public:
	MOVIE() : Frames( 1 ) {}
	MOVIE( POS size ) : Size( size ), Frames( 1 ) {}

	POS      GetSize();
	unsigned GetNumFrames();
	WORD     GetFrameTime( unsigned frame );
	//POINT    GetFramePoint( unsigned frame, POS pos ); ///////////

	void SetFrameTime( unsigned frame, WORD time );

	void AddFrame( const FRAME& frame, unsigned index );
	void DeleteFrame( unsigned index );
	bool Draw( unsigned frame, POS pos, BYTE color, char _char );
	bool Clear( unsigned frame, POS pos );

	bool Show( POS pos );
	bool PrintFrame( POS pos, unsigned frame_id );

	bool Save( FILE& file );
	bool Load( FILE& file );
};

/*____________________________________________________________________________________________________*/

RLGL_END